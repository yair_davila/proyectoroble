from fpdf import FPDF, HTMLMixin
from datetime import date,datetime
#import pdfkit
#from weasyprint import HTML

def obtener_ano_actual():
    return int(date.today().strftime("%Y"))


def tiene_permiso(user, permiso):
    if user.is_staff:
        return True

    if user.user_permissions.filter(name=permiso):
        return True

    grupos = user.groups.all()
    if len(grupos) > 0:
        if(grupos[0].permissions.filter(name=permiso)):
            return True

    return False

class HTML2PDF(FPDF, HTMLMixin):
    pass
def separar_cadena(cadena,aumento):
    resultado=[]
    inicio=0
    maximo=0
    vueltas = (len(cadena)%aumento)
    vueltas = (len(cadena)-vueltas)
    variante=1
    
    while inicio<vueltas:
        maximo=cadena[inicio:inicio+aumento].strip().rfind(' ')
        if(maximo == -1):
            maximo=aumento
            variante = 0
        else:
            variante=1
        ingresar = cadena[inicio:inicio+maximo].strip()
        if(ingresar != ''):
            resultado.append(ingresar)
        inicio=inicio+maximo+variante

    ingresar=cadena[inicio:inicio+aumento].strip()
    if(ingresar != ''):
            resultado.append(ingresar)
    return resultado

def generar_row(ejercicio,ingreso,egreso,descripcion,monto,aumento):
    lista=[]
    lista1=separar_cadena(ingreso,aumento)
    lista2=separar_cadena(egreso,aumento)
    lista3=separar_cadena(descripcion,aumento)
    lista.append(len(lista1))
    lista.append(len(lista2))
    lista.append(len(lista3))
    c=0
    op = "<tr border='1'>"
    while c<max(lista):
        try:
            var1 = '.'  
            if (c < len(lista1)):
                var1 = lista1[c]
            var2 = '.'  
            if (c < len(lista2)):

                var2 = (lista2[c])                
            var3 = '.'  
            if (c < len(lista3)):
                var3 = (lista3[c])                
        
            
            if(c == 0):
                
                op = op + '<td>'+(ejercicio)+'</td>\n'
                op = op + '<td>'+(var1)+'</td>\n'
                op = op + '<td>'+(var2)+'</td>\n'
                op = op + '<td>'+(var3)+'</td>\n'
                op = op + '<td>'+(monto)+'</td>\n'
                op = op + '</tr><tr>' + '\n'
                
                
            else:
                
                op = op + '<td>.'+'</td>\n'
                op = op + '<td>'+(var1)+'</td>\n'
                op = op + '<td>'+(var2)+'</td>\n'
                op = op + '<td>'+(var3)+'</td>\n'
                op = op + '<td>.'+'</td>\n'
                op = op + '</tr><tr>' + '\n'
                
                
        except:
            op = op + '</tr><tr>' + '\n'
        c=c+1
    return op[:len(op)]

def escribir_pdf(valores):
    c = ""
    total = 0
    contador=0
    cadena=''
    for valor in valores:
        try:
            
            variable =  generar_row(
                str(valor.ejercicio),
                str(valor.tipo_ingreso).capitalize(),
                str(valor.tipo_egreso).capitalize(),
                str(valor.descripcion).capitalize(),
                str(valor.monto),25
                )
            
            c = c + variable+"</tr>"
            
            
            total=total+valor.monto
        except:
            cadena = cadena
    c = c+"<tr>" +\
            "<th colspan='4'>Total</th>" +\
            "<th>"+str(total)+"</th>" +\
            "</tr>"
            

    cadena = "<title>Financiero</title>" + \
         "<table width='100%' >" + \
         "<thead>" + \
         "<tr>" + \
         "<th width='10%' align='left'>Ejercicio</th>" + \
         "<th width='27%' align='left'>Tipo_ingreso</th>" + \
         "<th width='27%' align='left'>Tipo_egreso</th>" + \
         "<th width='26%' align='left'>Descripcion</th>" + \
         "<th width='10%' align='left'>Monto</th>" + \
         "</tr>" + \
         "</thead>" + \
         "<tbody>" + \
         c + \
         "</tbody>" + \
         "</table>"

    pdf = HTML2PDF()
    pdf.add_page()
    pdf.write_html(cadena)
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
    pdf.output('media/ImagenEgresos/financiero-'+dt_string+'.pdf')
    #pdfkit.from_string(cadena, 'media/financiero-'+dt_string+'.pdf')
    
    #HTML(string=cadena).write_pdf('media/financiero-'+dt_string+'.pdf')
    return "financiero-"+dt_string+".pdf"
