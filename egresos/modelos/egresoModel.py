from egresos.modelos.tipoEgresoModel import TipoEgreso
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from django.db import models
from django.contrib.auth.models import User
class Prueba(models.Model):
    fecha = models.DateField('Fecha')  # Field name made lowercase.
    nombre = models.CharField('Nombre',max_length=10)


class Egreso(models.Model):
    tipo_ingreso = models.ForeignKey(
        TipoIngreso, models.DO_NOTHING, 'ID_TIPO_INGRESO_FK')
    tipo_egreso = models.ForeignKey(
        TipoEgreso, models.DO_NOTHING, 'ID_TIPO_EGRESO_FK')
    monto = models.DecimalField(
        'Monto', max_digits=8, decimal_places=2, blank=True, null=True)
    fecha = models.DateField('Fecha')  # Field name made lowercase.
    referencia = models.ImageField(upload_to='egresos')
    descripcion = models.CharField(
        'Descipción', max_length=100, blank=True, null=True)
    ejercicio = models.SmallIntegerField('Año_Aplica', blank=True, null=True)
    folio_recibo = models.IntegerField(
        'FolioRecibo', blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Egreso')
    fecha_edicion = models.DateTimeField('Edicion_Egreso', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Egreso')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Egreso', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def imprimir(self):
        return str(self.id)+','+str(self.tipo_ingreso.id)+','+\
            str(self.tipo_egreso.id)+','+str(self.monto)+','+\
            str(self.fecha)+','+str(self.referencia)+','+\
            str(self.descripcion)+','+str(self.ejercicio)+','+\
            str(self.folio_recibo)

    def __str__(self):
        return self.descripcion