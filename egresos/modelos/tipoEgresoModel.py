from django.db import models
from django.contrib.auth.models import User
class TipoEgreso(models.Model):
    concepto = models.CharField(
        'Concepto', max_length=35, blank=True, null=True)
    estatus = models.SmallIntegerField('Estatus', blank=True, null=True)
    monto = models.DecimalField(
        'Monto', max_digits=8, decimal_places=2, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_TipoEgreso')
    fecha_edicion = models.DateTimeField('Edicion_TipoEgreso', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_TipoEgreso')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_TipoEgreso', null=True)
    borrado = models.SmallIntegerField('borrado', default=0)

    def __str__(self):
        return self.concepto

    def imprimir(self):
        return str(self.id)+','+str(self.concepto)+','+\
            str(self.estatus)+','+str(self.monto)

    class Meta:
        app_label = "egresos"