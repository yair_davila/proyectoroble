MES_CHOICES = (
    ("1", "Enero"),
    ("2", "Febrero"),
    ("3", "Marzo"),
    ("4", "Abril"),
    ("5", "Mayo"),
    ("6", "Junio"),
    ("7", "Julio"),
    ("8", "Agosto"),
    ("9", "Septiembre"),
    ("10", "Octubre"),
    ("11", "Noviembre"),
    ("12", "Diciembre"),

)

ESTATUS_CHOICES = (
    ('1', 'Activo'),
    ('0', "Inactivo"),
)

TITULAR_CHOICES = (
    ('1', 'SI'),
    ('0', "NO"),
)