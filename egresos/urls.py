from egresos.modelos.egresoModel import Egreso
from egresos.vistas.egresoView import EgresoView
from egresos.modelos.tipoEgresoModel import TipoEgreso
from egresos.vistas.tipoEgresoView import TipoEgresoView
from django.urls import path

urlpatterns = [
	path('tipoegreso_nuevo', TipoEgresoView.nuevo_tipoegreso,
         name='nuevo_tipoegreso'),
    path('tipoegreso_registrar', TipoEgresoView.registrar_js_tipoegreso,
         name='registrar_tipoegreso'),
    path('tipoegreso_actualizar', TipoEgresoView.actualizar_js_tipoegreso,
         name='actualizar_tipoegreso'),
    path('tipoegreso_obteneruno', TipoEgresoView.obteneruno_js_tipoegreso,
         name='obteneruno_tipoegreso'),
    path('tipoegreso_borrar/',
         TipoEgresoView.borrar_js_tipoegreso, name='borrar_tipoegreso'),

    path('nuevo_egreso', EgresoView.nuevo_egreso,
         name='nuevo_egreso'),
    path('egreso_borrar/',
         EgresoView.borrar_js_egreso, name='borrar_egreso'),
    path('egreso_editar/<int:id>', EgresoView.editar_egreso, name='editar_egreso'),

    path('pdf_egresos_crear', EgresoView.crear_pdf,
         name='crear_pdf_egresos'),
    path('egresos_10', EgresoView.obtener_diez,
         name='10egresos'),
    # path('egreso_lista', EgresoView.obtener_egresos, name='lista_egresos'),
    
]