from egresos.modelos.reporteModel import Reporte
from egresos.validador import *
from django import forms

class FormReporte (forms.Form):
    year = forms.IntegerField(initial=obtener_ano_actual)
