from egresos.modelos.egresoModel import Egreso,Prueba
from egresos.validador import *
from egresos.utils import obtener_ano_actual
from django import forms
from egresos.modelos.tipoEgresoModel import TipoEgreso
from ingresos.modelos.tipoIngresoModel import TipoIngreso


class FormEgreso (forms.ModelForm):
    descripcion = forms.CharField(widget=forms.Textarea(attrs={'cols': 30, 'rows': 2}),validators=[validador_cadenas])
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    #fecha = forms.DateTimeField(input_formats=lista,required=False)
    fecha = forms.DateTimeField()
    ejercicio = forms.IntegerField(initial=obtener_ano_actual)
    #colono = forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    tipo_egreso = forms.ModelChoiceField(queryset=TipoEgreso.objects.filter(estatus=1))
    tipo_ingreso = forms.ModelChoiceField(queryset=TipoIngreso.objects.filter(estatus=1))

    class Meta:
        model = Egreso
        fields = ('tipo_egreso', 'tipo_ingreso', 'monto',
                  'referencia', 'descripcion', 'ejercicio', 'folio_recibo','fecha')