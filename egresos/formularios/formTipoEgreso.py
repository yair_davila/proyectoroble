from egresos.modelos.tipoEgresoModel import TipoEgreso
from egresos.validador import *
from django import forms
from egresos.choices import ESTATUS_CHOICES

class FormTipoEgreso (forms.ModelForm):
    concepto = forms.CharField(validators=[validador_cadenas])
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    estatus = forms.ChoiceField(choices=ESTATUS_CHOICES)

    class Meta:
        model = TipoEgreso
        fields = ('concepto', 'estatus', 'monto')


