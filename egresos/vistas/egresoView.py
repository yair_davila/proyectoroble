from django.db import connection
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from selenium.webdriver.support.expected_conditions import element_selection_state_to_be
from datetime import date
from egresos.utils import escribir_pdf
from egresos.modelos.egresoModel import Egreso
from egresos.formularios.formEgreso import FormEgreso
from django.db.models import Max
from anuncios.modelos.imagenEgresosModel import ImagenEgresos
import os
from django.conf import settings
import datetime

class EgresoView():

    @login_required()
    @permission_required('egresos.delete_egreso', login_url='egreso')
    def borrar_js_egreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            egreso = Egreso.objects.get(pk=o)
            egreso.estatus = 0
            egreso.save()

        data = EgresoView.obtener_json_egresos(Egreso.objects.filter(estatus=1).order_by('folio_recibo')[0:10])
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.view_egreso', login_url='egreso')
    def obtener_egresos(context):
        egresos = Egreso.objects.filter(borrado=0).order_by('folio_recibo')
        return render(context, 'lista_egreso.html', {'accion': 'Lista', 'egresos': ingresos})

    def obtener_json_egresos(egresos):

        egresosJson = []
        for egreso in egresos:
            print(egreso.id)
            egresosJson.append(
                {
                    'id' : egreso.id,
                    'tipo_ingreso': str(egreso.tipo_ingreso),
                    'tipo_egreso': str(egreso.tipo_egreso),
                    'monto': str(egreso.monto),
                    'fecha': str(egreso.fecha),
                    'referencia': str(egreso.referencia),
                    'descripcion': str(egreso.descripcion),
                    'ejercicio': str(egreso.ejercicio),
                    'folio_recibo': str(egreso.folio_recibo),
                        }
                    )
        data = simplejson.dumps(egresosJson)
        return data

    

    @login_required()
    @permission_required('egresos.add_egreso', login_url='egreso')
    def nuevo_egreso(context):
        
        if (context.method == 'POST'):
            form = FormEgreso(context.POST, context.FILES)
            if form.is_valid():
                egreso = form.save(commit=False)
                egreso.usuario_creacion = context.user
                egreso.fecha_creacion = date.today()
                egreso.save()
                print("Guardar")
                egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')[0:10]
                totales = Egreso.objects.filter(estatus=1).count()
                totales = range(0, totales, 10)
                form = FormEgreso()
                return render(context, 'nuevo_egreso.html',
                              {'formulario': form,
                               'accion': 'Nuevo',
                                'egresos': egresos,
                                 'exito': True,
                                 'totales': totales})
            else:
                egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')[0:10]
                totales = Egreso.objects.filter(estatus=1).count()
                totales = range(0, totales, 10)
                errores = str(form.errors)
                return render(context, 'nuevo_egreso.html',
                              {'formulario': form,
                               'accion': 'Nuevo',
                                'egresos': egresos,
                                 'error': errores,
                                 'totales': totales})
        else:
            egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')[0:10]
            totales = Egreso.objects.filter(estatus=1).count()
            totales = range(0, totales, 10)
            form = FormEgreso()
        return render(context, 'nuevo_egreso.html',
                      {'formulario': form,
                       'accion': 'Nuevo',
                       'egresos': egresos,
                       'totales': totales})

    def obtener_diez(context):
        id = int(context.GET.get('id', None))
        id = (id*10)
        print(str(id-10)+" "+str(id))
        inicio = int(id-10)
        fin = int(id)
        print(str(inicio)+" "+str(fin))
        egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')[inicio:fin]
        data = EgresoView.obtener_json_egresos(egresos)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.view_egreso')
    def egresos_foto(context, id):
        ref = Egreso.objects.get(id=id).referencia
        direccion = "static/media/" + str(ref)
        image_data = open(direccion, "rb").read()
        return HttpResponse(image_data, content_type="image/png")

    @login_required()
    @permission_required('egresos.view_egreso')
    def lista_egresos(context):
        egresos = Egreso.objects.all()
        return render(context, 'egresos.html', {'egresos': egresos})

    @login_required()
    @permission_required('egresos.change_egreso',login_url='egreso')
    def editar_egreso(request, id):
        egreso = Egreso.objects.get(pk=id)
        if request.method == 'POST':
            form = FormEgreso(request.POST, request.FILES, instance=egreso)
            print(form.is_valid())
            print(form.errors)
            if form.is_valid():
                egreso = form.save()
                egreso.save()
                return redirect('nuevo_egreso')
            else:
                egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')[0:10]
                totales = Egreso.objects.filter(estatus=1).count()
                totales = range(0, totales, 10)
                return render(request, 'nuevo_egreso.html',
                          {'formulario': form
                          , 'accion': 'Editar'
                          , 'egresos': egresos
                          , 'error': form.errors,
                            'totales': totales})
        else:
            form = FormEgreso(instance=egreso)
            egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')[0:10]
            totales = Egreso.objects.filter(estatus=1).count()
            totales = range(0, totales, 10)
        return render(request, 'nuevo_egreso.html', 
                {'formulario': form,
                 'accion': 'Editar',
                 'egresos':egresos,
                 'totales': totales
                 })

    def crear_pdf(context):
        exito=False
        uno = ""
        dos = ""
        max_fecha_creacion =Egreso.objects.filter(estatus=1).aggregate(Max('fecha_creacion'))
        max_creacion = max_fecha_creacion['fecha_creacion__max']
        max_creacion = max_creacion if max_creacion != None else datetime.datetime(2000,1,1)
        max_creacion = max_creacion.strftime("%Y%m%d")
        max_creacion = int(max_creacion)
        print(max_creacion)

        max_fecha_edicion =Egreso.objects.filter(estatus=1).aggregate(Max('fecha_edicion'))
        max_edicion = max_fecha_edicion['fecha_edicion__max']
        max_edicion = max_edicion if max_edicion != None else datetime.datetime(2000,1,1)
        max_edicion = max_edicion.strftime("%Y%m%d")
        max_edicion = int(max_edicion)
        print(max_edicion)
        
        if(max_creacion > max_edicion):
            print("creacion")
            uno=max_creacion
        else:
            uno=max_edicion
            print("edicion")
        
       

        max_fecha_creacion =ImagenEgresos.objects.filter(estatus=1).aggregate(Max('fecha_creacion'))
        max_creacion = max_fecha_creacion['fecha_creacion__max']
        max_creacion = max_creacion if max_creacion != None else datetime.datetime(2000,1,1)
        max_creacion = max_creacion.strftime("%Y%m%d")
        max_creacion = int(max_creacion)
        print(max_creacion)

        max_fecha_edicion =ImagenEgresos.objects.filter(estatus=1).aggregate(Max('fecha_edicion'))
        max_edicion = max_fecha_edicion['fecha_edicion__max']
        max_edicion = max_edicion if max_edicion != None else datetime.datetime(2000,1,1)
        max_edicion = max_edicion.strftime("%Y%m%d")
        max_edicion = int(max_edicion)
        print(max_edicion)

        if(max_creacion > max_edicion):
            print("creacion")
            dos=max_creacion
        else:
            dos=max_edicion
            print("edicion")
        
        if(uno > dos):
            print("crear")
            dos=uno
            exito=True
        else:
            dos=dos
            print("ya nada")

        ruta=""
        
        """
        initial_path = car.photo.path
        car.photo.name = 'cars/chevy_ii.jpg'
        new_path = settings.MEDIA_ROOT + car.photo.name
        os.rename(initial_path, new_path)
        car.save()
        car.photo.path
        """

        if(exito):
            egresos = Egreso.objects.filter(estatus=1).order_by('folio_recibo')
            ruta = escribir_pdf(egresos)
            print("\n"*10)
            print(ruta)
            print("\n"*10)
            reporte = ImagenEgresos()
            arreglo = ruta.split('/')
            reporte.foto.name = "ImagenEgresos/"+arreglo[len(arreglo)-1]
            
            """
            initial_path = reporte.foto.path
            reporte.foto.name = ruta
            new_path = settings.MEDIA_ROOT + reporte.foto.name
            os.rename(initial_path, new_path)
            """


            reporte.titulo = "Reporte de general de egresos"
            
            reporte.fecha_creacion = date.today()
            reporte.usuario_creacion = context.user
            reporte.save()
        else:            
            ruta = ImagenEgresos.objects.latest('foto').foto
        return render(context, 'reporte.html',{'ruta': ruta});
