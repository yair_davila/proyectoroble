from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from egresos.modelos.tipoEgresoModel import TipoEgreso
from egresos.formularios.formTipoEgreso import FormTipoEgreso

class TipoEgresoView():

    @login_required()
    @permission_required('egresos.add_tipoegreso', login_url='tipoegreso')
    def nuevo_tipoegreso(context):
        form = FormTipoEgreso()
        tipoegresos = TipoEgreso.objects.filter(borrado=0)
        return render(context, 'nuevo_tipoegreso.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tipoegresos': tipoegresos})

    @login_required()
    @permission_required('egresos.add_tipoegreso', login_url='tipoegreso')
    def registrar_js_tipoegreso(context):
        if (context.method == 'POST'):
            form = FormTipoEgreso(context.POST)
            if form.is_valid():
                tipoegreso = form.save(commit=False)
                tipoegreso.usuario_creacion = context.user
                tipoegreso.fecha_creacion = date.today()
                tipoegreso.save()
                data = TipoEgresoView.obtener_json_tipoegresos(TipoEgreso.objects.filter(borrado=0))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.add_tipoegreso', login_url='tipoegreso')
    def actualizar_js_tipoegreso(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tipoegreso = TipoEgreso.objects.get(pk=id)

            form = FormTipoEgreso(context.POST, instance=tipoegreso)
            if form.is_valid():
                tipoegreso = form.save()
                tipoegreso.save()
                data = TipoEgresoView.obtener_json_tipoegresos(TipoEgreso.objects.filter(borrado=0))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.change_tipoegreso', login_url='tipoegreso')
    def obteneruno_js_tipoegreso(context):
        id = context.POST.get('id',None)
        data = TipoEgresoView.obtener_json_tipoegresos(TipoEgreso.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.delete_tipoegreso', login_url='tipoegreso')
    def borrar_js_tipoegreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tipoegreso = TipoEgreso.objects.get(pk=o)
            tipoegreso.borrado = 1
            tipoegreso.save()

        data = TipoEgresoView.obtener_json_tipoegresos(TipoEgreso.objects.filter(borrado=0))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_tipoegresos(tipoegresos):
        
        tipoegresosJson = []
        for tipoegreso in tipoegresos:
            tipoegresosJson.append(
                {
            'id' : tipoegreso.id,
        	'concepto': tipoegreso.concepto,
        	'monto': tipoegreso.monto,
            'estatus':tipoegreso.estatus

                    
                }
            )
        data = simplejson.dumps(tipoegresosJson)
        return data
