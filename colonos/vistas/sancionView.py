from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from sancion.modelos.sancionModel import Sancion
from sancion.formularios.formSancion import FormSancion
from usuarios.modelos.colonoModel import Colono

class SancionView():

    def get_colono_logged(context):
        return Colono.objects.get(pk=context.user.id)

    @login_required()
    def nuevo_sancion(context):
        sancions = Sancion.objects.filter(estatus=1,colono=SancionView.get_colono_logged(context))
        return render(context, 'nuevo_sancion_colono.html',
                      { 'accion': 'Ver', 'sancions': sancions})

    @login_required()
    @permission_required('sancion.change_sancion', login_url='sancion')
    def obteneruno_js_sancion(context):
        id = context.POST.get('id',None)
        data = SancionView.obtener_json_sancions_carga(Sancion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('sancion.delete_sancion', login_url='sancion')
    def borrar_js_sancion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            sancion = Sancion.objects.get(pk=o)
            sancion.estatus = 0
            sancion.save()

        data = SancionView.obtener_json_sancions(Sancion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_sancions(sancions):
        
        sancionsJson = []
        for sancion in sancions:
            sancionsJson.append(
                {
            'id' : sancion.id,
        	'colono': str(sancion.colono),
        	'tipo_sancion': str(sancion.tipo_sancion),
            'fecha': str(sancion.fecha.strftime('%d de %B de %Y')),
        	'descripcion': str(sancion.descripcion),
        	'monto': str(sancion.monto),
                }
            )
        data = simplejson.dumps(sancionsJson)
        return data


    def obtener_json_sancions_carga(sancions):
        
        sancionsJson = []
        for sancion in sancions:
            sancionsJson.append(
                {
            'id' : sancion.id,
          'colono': sancion.colono.id,
          'tipo_sancion': sancion.tipo_sancion.id,
          'fecha': str(sancion.fecha.strftime('%Y-%m-%d')),
          'descripcion': sancion.descripcion,
          'monto': sancion.monto,
                }
            )
        data = simplejson.dumps(sancionsJson)
        return data


    

    @login_required()
    @permission_required('sancion.add_sancion', login_url='sancion')
    def registrar_js_sancion(context):
        if (context.method == 'POST'):
            form = FormSancion(context.POST)
            if form.is_valid():
                sancion = form.save(commit=False)
                sancion.usuario_creacion = context.user
                sancion.fecha_creacion = date.today()
                sancion.save()
                data = SancionView.obtener_json_sancions(Sancion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('sancion.add_sancion', login_url='sancion')
    def actualizar_js_sancion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            sancion = Sancion.objects.get(pk=id)

            form = FormSancion(context.POST, instance=sancion)
            if form.is_valid():
                sancion = form.save()
                sancion.save()
                data = SancionView.obtener_json_sancions(Sancion.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
