from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from mascotas.modelos.mascotaModel import Mascota
from mascotas.modelos.colonoMascotaModel import ColonoMascota
from usuarios.modelos.colonoModel import Colono
from colonos.formularios.formMascotaColono import FormMascotaColono
from mascotas.modelos.razaModel import Raza
from datetime import date

class MascotaView():

    @login_required()
    @permission_required('mascotas.add_mascota', login_url='login')
    def nuevo_mascota(context):
        if (context.method == 'POST'):
            form = FormMascotaColono(context.POST, context.FILES)
            if form.is_valid():
                mascota = form.save(commit=False)
                mascota.usuario_creacion = context.user
                mascota.fecha_creacion = date.today()
                mascota.save()
                print(mascota)
                colonoMascota = ColonoMascota();
                colonoEnCuestion = Colono.objects.get(id=context.user.id)
                colonoMascota.colono = colonoEnCuestion
                colonoMascota.mascota = mascota
                colonoMascota.usuario_creacion = context.user
                colonoMascota.fecha_creacion = date.today()
                colonoMascota.save()
                relaciones = ColonoMascota.objects.filter(colono = context.user.id).only("mascota")
                mascotas = Mascota.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
                return render(context, 'mascota_colono.html',
                              {'formulario': form, 'accion': 'Nuevo', 'mascotas': mascotas,'exito':True})
            else:
                relaciones = ColonoMascota.objects.filter(colono=context.user.id).only("mascota")
                print(relaciones)
                mascotas = Mascota.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
                print(mascotas)
                return render(context, 'mascota_colono.html',
                              {'formulario': form, 'accion': 'Nuevo', 'mascotas': mascotas, 'error': form.errors})
        else:
            form = FormMascotaColono()
        relaciones = ColonoMascota.objects.filter(colono=context.user.id).only("mascota")
        print(relaciones)
        mascotas = Mascota.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
        print(mascotas)
        return render(context, 'mascota_colono.html', {'formulario': form, 'accion': 'Nuevo', 'mascotas': mascotas})

    @login_required()
    @permission_required('mascotas.change_mascota', login_url='login')
    def editar_mascota(context,id):
        mascota = Mascota.objects.get(pk=id)
        if context.method == 'POST':
            form = FormMascotaColono(context.POST, context.FILES, instance=mascota)
            if form.is_valid():
                mascota = form.save()
                mascota.save()
                return redirect('nuevo_mascota_colono')
            else:
                relaciones = ColonoMascota.objects.filter(colono=context.user.id).only("mascota")
                mascotas = Mascota.objects.filter(id__in=relaciones,estatus=1).order_by('-id')
                return render(context, 'mascota_colono.html',
                          {'formulario': form, 'accion': 'Editar', 'mascotas': mascotas, 'error': form.errors})
        else:
            form = FormMascotaColono(instance=mascota)
        relaciones = ColonoMascota.objects.filter(colono=context.user.id).only("mascota")
        mascotas = Mascota.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
        return render(context, 'mascota_colono.html', {'formulario': form, 'accion': 'Editar','mascotas':mascotas})


    @login_required()
    @permission_required('mascotas.delete_mascota', login_url='login')
    def borrar_js_mascota(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            mascota = Mascota.objects.get(pk=o)
            mascota.estatus = 0
            mascota.save()
        data = MascotaView.obtener_json_mascotas(Mascota.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_mascotas(mascotas):
        mascotasJson = []
        for mascota in mascotas:
            mascotasJson.append(
                {
            'id' : mascota.id,
        	'animal': mascota.animal.id,
        	'raza': mascota.raza.id,
        	'tamano': mascota.tamano,
        	'color': mascota.color,
        	'genero': mascota.genero,
        	'nombre': mascota.nombre,
        	'foto': mascota.foto,
                }
            )
        data = simplejson.dumps(mascotasJson)
        return data
