from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from google.protobuf import descriptor
from django.http import JsonResponse
from usuarios.modelos.colonoModel import Colono
from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from colonos.formularios.formVisitaColono import FormVisitaColono
from colonos.formularios.fromVisitaVehiculoColono import FormVisitaVehiculoColono
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.personaAjenaModel import PersonaAjena
import datetime

class VisitasView():

    @login_required()
    @permission_required('visitantes.add_visitaautorizada', login_url='login')
    def nuevo_visita(context):
        form = FormVisitaColono()
        formVehiculo = FormVisitaVehiculoColono()
        visitas = VisitaAutorizada.objects.filter(estatus=1, colono=context.user.id).exclude(fecha_estimada__lte = (datetime.date.today() - datetime.timedelta(days=1)))
        return render(context, 'visita_colono.html',
                      {'formulario': form, 'accion': 'Nuevo', 'visitas': visitas,'formVehiculo':formVehiculo})

    @login_required()
    @permission_required('visitantes.change_visitaautorizada', login_url='login')
    def obteneruno_js_visita(context):
        id = context.POST.get('id',None)
        data = VisitasView.obtener_json_visitas(VisitaAutorizada.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.delete_visitaautorizada', login_url='login')
    def borrar_js_visita(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            visita = VisitaAutorizada.objects.get(pk=o)
            visita.estatus = 0
            visita.save()

        data = VisitasView.obtener_json_visitas(VisitaAutorizada.objects.filter(estatus=1)[0:10])
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_visitas(visitas):
        
        visitasJson = []
        for visita in visitas:
            visitasJson.append(
                {
            'id' : visita.id,
            'colono': str(visita.colono),
            'persona_ajena': str(visita.persona_ajena),
            'fecha': str(visita.fecha),
            'hora_entrada': str(visita.hora_entrada),
            'hora_salida': str(visita.hora_salida),
            'identificacion': str(visita.identificacion),
            'numero_personas': str(visita.numero_personas),
            'foto': str(visita.foto),
            'tipo_acceso': str(visita.tipo_acceso),
            'fecha_estimada': str(visita.fecha_estimada),
            'hora_estimada_min': str(visita.hora_estimada_min),
            'hora_estimada_max': str(visita.hora_estimada_max),

                    
                }
            )
        data = simplejson.dumps(visitasJson)
        return data

    def validar_vehiculo(context):
        if (context.method == 'POST'):
            form = FormVisitaVehiculoColono(context.POST)
            if form.is_valid():
                data = simplejson.dumps([{'exito': 'exito'}])
                return HttpResponse(data, content_type="application/json")
            data = simplejson.dumps([{'error': str(form.errors)}])
            return HttpResponse(data, content_type="application/json")


    @login_required()
    @permission_required('visitantes.add_visitaautorizada', login_url='login')
    def registrar_js_visita(context):
        if (context.method == 'POST'):
            form = FormVisitaColono(context.POST)
            persona = PersonaAjena()
            if form.is_valid():
                hora_estimada_min = context.POST.get('hora_estimada_min', None).strip()
                hora_estimada_max = context.POST.get('hora_estimada_max', None).strip()
                fecha_estimada = context.POST.get('fecha_estimada', None).strip()
                datetime_object = datetime.datetime.strptime(fecha_estimada, '%Y-%m-%d')
                uno = int(hora_estimada_min[:2] + hora_estimada_min[3:])
                dos = int(hora_estimada_max[:2] + hora_estimada_max[3:])

                datetime_object = datetime_object.replace(hour=int(hora_estimada_min[:2]),minute=int(hora_estimada_min[3:]))


                if (uno < dos and datetime_object >= datetime.datetime.today() ):
                    colono = Colono.objects.get(pk=context.user.id)
                    persona.nombre = context.POST.get('nombre', None).strip()
                    persona.apaterno = context.POST.get('apaterno', None).strip()
                    persona.amaterno = context.POST.get('amaterno', None).strip()
                    persona.telefono = context.POST.get('telefono', None).strip()
                    persona.email = context.POST.get('email', None).strip()

                    filtro = PersonaAjena.objects.filter(
                        nombre = persona.nombre,
                        apaterno = persona.apaterno,
                        amaterno = persona.amaterno,
                        usuario_creacion = context.user
                    );

                    if (len(filtro) == 0):
                        persona.usuario_creacion = context.user
                        persona.usuario_creacion = colono
                        persona.fecha_creacion = date.today()
                        tipoPersona = TipoPersonaAjena.objects.get(descripcion='VISITANTE')
                        persona.tipo = tipoPersona
                        persona.save()
                    else:
                        persona = filtro[0]
                    visita = form.save(commit=False)
                    visita.tipo_acceso = 1
                    visita.persona_ajena = persona
                    visita.colono = colono
                    visita.usuario_creacion = context.user
                    visita.fecha_creacion = date.today()
                    visita.save()
                    visitas = VisitaAutorizada.objects.filter(estatus=1, colono= colono.id).order_by('-id')[0:10]
                    vehiculo = context.POST.get('vehiculo', None).strip()

                    if(vehiculo == '1'):
                        new_vehiculo = VehiculoVisitante()
                        placas = str(context.POST.get('placas', None)).strip()

                        new_vehiculo.placas = placas;
                        new_vehiculo.color = str(context.POST.get('color', None)).strip()
                        new_vehiculo.marca = str(context.POST.get('marca', None)).strip()
                        new_vehiculo.linea = str(context.POST.get('linea', None)).strip()
                        new_vehiculo.tipo = str(context.POST.get('tipo', None)).strip()
                        new_vehiculo.fecha_creacion = date.today()
                        new_vehiculo.usuario_creacion = colono
                        new_vehiculo.visita_autorizada = visita
                        new_vehiculo.save()

                    data = simplejson.dumps([{'exito': 'Visita registrada con exito'}])
                    return HttpResponse(data, content_type="application/json")
                else:
                    cadena = ''
                    cadena = cadena + 'La hora minima debe ser menor a la hora maxima <br>' if (uno > dos) else cadena
                    cadena = cadena + 'La fecha y hora minima deben ser mayores a la fecha y hora actual' if (datetime_object < datetime.datetime.today()) else cadena
                    data = simplejson.dumps([{'error': cadena}])
                    """
                    except:
                        print('fallo')
                        data = simplejson.dumps([{'error': 'Error de conexión'}])
                        return HttpResponse(data, content_type="application/json")
                    """
                    return HttpResponse(data, content_type="application/json")
            else:
                print(str(form.errors))
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            print('fallo{{{{{{{{{{{')
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.change_visitaautorizada', login_url='login')
    def actualizar_js_visita(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            visita = VisitaAutorizada.objects.get(pk=id)

            form = FormVisitaColono(context.POST, instance=visita)
            if form.is_valid():
                visita = form.save()
                visita.save()
                data = VisitasView.obtener_json_visitas(VisitaAutorizada.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    def obtener_visitas(context):
        visitas =  VisitaAutorizada.objects.filter(estatus=1, colono=context.user.id).exclude(fecha_estimada__lte = (datetime.date.today() - datetime.timedelta(days=1)))
        visitasJson = []
        for visita in visitas:
            visitasJson.append(
                {
                    'id':str(visita.id),
                    'title': str(visita.persona_ajena)+" con "+str(visita.numero_personas)+" personas",
                    'start': str(visita.fecha_estimada.strftime('%Y-%m-%d')) + " "+str(visita.hora_estimada_min),
                    'end': str(visita.fecha_estimada.strftime('%Y-%m-%d')) +" "+ str(visita.hora_estimada_max)

                }
            )
        data = simplejson.dumps(visitasJson)
        return JsonResponse(data, safe=False);
