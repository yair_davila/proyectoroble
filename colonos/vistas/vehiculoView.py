from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from vehiculos.modelos.vehiculoModel import Vehiculo
from vehiculos.modelos.colonoVehiculoModel import ColonoVehiculo
from colonos.formularios.formVehiculo import FormVehiculo
from usuarios.modelos.colonoModel import Colono


class VehiculoView():

    def get_colono_logged(context):
        return Colono.objects.get(pk=context.user.id)

    @login_required()
    @permission_required('vehiculos.add_vehiculo')
    def nuevo_vehiculo(context):
        form = FormVehiculo()
        vehiculos = Vehiculo.objects.filter(estatus=1)
        relaciones = ColonoVehiculo.objects.filter(colono = VehiculoView.get_colono_logged(context)).only("vehiculo")
        
        rel=[]
        for r in relaciones:
            rel.append(r.vehiculo.id)

        
        vehiculos = Vehiculo.objects.filter(id__in=rel, estatus=1)
        
        return render(context, 'nuevo_vehiculo_colono.html',
                      {'formulario': form, 'accion': 'Nuevo', 'vehiculos': vehiculos})

    @login_required()
    @permission_required('vehiculos.add_vehiculo')
    def registrar_js_vehiculo(context):
        if (context.method == 'POST'):
            form = FormVehiculo(context.POST)
            if form.is_valid():
                vehiculo = form.save(commit=False)

                vehiculo.usuario_creacion = context.user
                vehiculo.fecha_creacion = date.today()
                vehiculo.save()
                
                colonoVehiculo = ColonoVehiculo()
                colonoVehiculo.colono = VehiculoView.get_colono_logged(context)
                colonoVehiculo.vehiculo = vehiculo
                colonoVehiculo.usuario_creacion = context.user
                colonoVehiculo.fecha_creacion = date.today()
                colonoVehiculo.save()


                relaciones = ColonoVehiculo.objects.filter(colono = context.user.id).only("vehiculo")
                vehiculos = Vehiculo.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
                data = VehiculoView.obtener_json_vehiculos(vehiculos)
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.add_vehiculo')
    def actualizar_js_vehiculo(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            vehiculo = Vehiculo.objects.get(pk=id)

            form = FormVehiculo(context.POST, instance=vehiculo)
            if form.is_valid():
                vehiculo = form.save()
                vehiculo.save()
                relaciones = ColonoVehiculo.objects.filter(colono = context.user.id).only("vehiculo")
                vehiculos = Vehiculo.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
                data = VehiculoView.obtener_json_vehiculos(vehiculos)

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.change_vehiculo')
    def obteneruno_js_vehiculo(context):
        id = context.POST.get('id',None)
        print(id)
        data = VehiculoView.obtener_json_vehiculos(Vehiculo.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.change_vehiculo')
    def borrar_js_vehiculo(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            print(o)
            vehiculo = Vehiculo.objects.get(pk=o)
            vehiculo.estatus = 0
            vehiculo.save()
            print(vehiculo)
            colonoVehiculo = ColonoVehiculo.objects.filter(vehiculo=vehiculo)[0]
            print(colonoVehiculo)
            colonoVehiculo.estatus=0
            colonoVehiculo.fecha_edicion = date.today()
            colonoVehiculo.save()

        relaciones = ColonoVehiculo.objects.filter(colono = context.user.id).only("vehiculo")
        vehiculos = Vehiculo.objects.filter(id__in=relaciones, estatus=1).order_by('-id')
        data = VehiculoView.obtener_json_vehiculos(vehiculos)
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_vehiculos(vehiculos):
        
        vehiculosJson = []
        for vehiculo in vehiculos:
            vehiculosJson.append(
                {
            'id' : vehiculo.id,
        	'numero_placas': vehiculo.numero_placas,
        	'marca': vehiculo.marca,
        	'modelo': vehiculo.modelo,
        	'color': vehiculo.color,
        	'tipo': vehiculo.tipo,
        	'linea': vehiculo.linea,

                    
                }
            )
        data = simplejson.dumps(vehiculosJson)
        return data
