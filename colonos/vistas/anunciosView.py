from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.imagenAnunciosModel import ImagenAnuncios
from anuncios.modelos.imagenEgresosModel import ImagenEgresos
from anuncios.modelos.imagenIngresosModel import ImagenIngresos
from anuncios.modelos.imagenFinancieroModel import ImagenFinanciero
from datetime import date

class   AnunciosView():

    @login_required()
    def obtener_imagenAnuncios(context):
        imagenAnuncios = ImagenAnuncios.objects.all()
        return render(context, 'anuncios_colono.html', {'datos': imagenAnuncios})

    @login_required()
    def obtener_imagenEgresos(context):
        datos = ImagenEgresos.objects.all()
        return render(context,'financiero_colono.html',{'modulo':'Egresos','datos':datos})

    @login_required()
    def obtener_imagenIngresos(context):
        datos = ImagenIngresos.objects.all()
        return render(context, 'financiero_colono.html', {'modulo': 'Ingresos', 'datos': datos})

    @login_required()
    def obtener_imagenFinanciero(context):
        datos = ImagenFinanciero.objects.all()
        return render(context, 'financiero_colono.html', {'modulo': 'Finanicero', 'datos': datos})