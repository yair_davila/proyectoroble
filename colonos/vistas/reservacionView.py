from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from areasComunes.modelos.reservacionModel import Reservacion
from usuarios.modelos.colonoModel import Colono
from colonos.formularios.formReservacion import FormReservacion
from datetime import date

class ReservacionColonosView():



    def get_colono_logged(context):
        return Colono.objects.get(pk=context.user.id)

    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='login')
    def nuevo_reservacion(context):
        form = FormReservacion()
        reservacions = Reservacion.objects.filter(estatus=1,colono=ReservacionColonosView.get_colono_logged(context))
        return render(context, 'reservacion_colono.html',
                      {'formulario': form, 'accion': 'Nuevo', 'reservacions': reservacions})

    """
    @login_required()
    @permission_required('areasComunes.change_reservacion', login_url='reservacion')
    def obteneruno_js_reservacion(context):
        id = context.POST.get('id',None)
        data = ReservacionColonosView.obtener_json_reservacions_carga(Reservacion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('areasComunes.delete_reservacion', login_url='reservacion')
    def borrar_js_reservacion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            reservacion = Reservacion.objects.get(pk=o)
            reservacion.estatus = 0
            reservacion.save()

        data = ReservacionColonosView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    """
    def obtener_json_reservacions(reservacions):
        
        reservacionsJson = []
        for reservacion in reservacions:
            reservacionsJson.append(
                {
                    'area_comun': str(reservacion.area_comun),
                    'tipo_evento': str(reservacion.tipo_evento),
                    'fecha': str(reservacion.fecha.strftime('%d de %B de %Y')),
                    'hora_inicial': str(reservacion.hora_inicial),
                    'hora_final': str(reservacion.hora_final),
                    'observaciones': str(reservacion.observaciones)
                }
            )
        data = simplejson.dumps(reservacionsJson)
        return data

    """
    def obtener_json_reservacions_carga(reservacions):
        
        reservacionsJson = []
        for reservacion in reservacions:
            reservacionsJson.append(
                {
                  'area_comun': str(reservacion.area_comun.id),
                  'tipo_evento': str(reservacion.tipo_evento.id),
                  'fecha': str(reservacion.fecha.strftime('%Y-%m-%d')),
                  'hora_inicial': str(reservacion.hora_inicial),
                  'hora_final': str(reservacion.hora_final),
                  'observaciones': str(reservacion.observaciones),
                }
            )
        data = simplejson.dumps(reservacionsJson)
        return data
    """



    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='login')
    def registrar_js_reservacion(context):
        if (context.method == 'POST'):
            form = FormReservacion(context.POST)
            if (form.is_valid()):
                print("------------------------------")
                print("valido")
                print("------------------------------")
                reservacion = form.save(commit=False)
                reservacion.colono = Colono.objects.get(id=context.user.id)
                reservacion.usuario_creacion = context.user
                reservacion.fecha_creacion = date.today()
                reservacion.save()
                data = ReservacionColonosView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    """
    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='reservacion')
    def actualizar_js_reservacion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            reservacion = Reservacion.objects.get(pk=id)
            form = FormReservacion(context.POST, instance=reservacion)
            try:

                colono  = context.user.id
                area_comun  = context.POST.get('area_comun',None)
                tipo_evento  = context.POST.get('tipo_evento',None)
                fecha  = context.POST.get('fecha',None)
                hora_inicial  = context.POST.get('hora_inicial',None)
                hora_final  = context.POST.get('hora_final',None)
                observaciones  = context.POST.get('observaciones',None)

                id = context.POST.get('id', None)
                query = "UPDATE areasComunes_reservacion SET " + \
                   "colono_id = "+ colono + ","+\
                   "area_comun_id = "+ area_comun + ","+\
                   "tipo_evento_id = "+ tipo_evento + ","+\
                   "fecha = '"+ fecha + "',"+\
                   "hora_inicial = '"+ hora_inicial + "',"+\
                   "hora_final = '"+ hora_final + "',"+\
                   "observaciones = '"+ observaciones + "'"+\
                        " WHERE id = "+id+";"
                print(query)
                cursor = connection.cursor()
                cursor.execute(query)
                row = cursor.fetchone()
                data = ReservacionColonosView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            except Exception as e:
                print(e)
                data = simplejson.dumps([{'error': 'alerta'},{'alerta','Los datos ingresados son incorrectos'}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'error'}, {'descripcion': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """