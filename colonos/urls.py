from django.urls import path
from colonos.vistas.reservacionView import ReservacionColonosView
from colonos.vistas.anunciosView import AnunciosView
from colonos.vistas.mascotasView import MascotaView
from colonos.vistas.visitasView import VisitasView
from colonos.vistas.reporteVecinoView import ReporteVecinoView
from colonos.vistas.sancionView import SancionView
from colonos.vistas.ingresosView import IngresoView
from colonos.vistas.vehiculoView import VehiculoView

urlpatterns = [

    path('colono_reservacion_nuevo', ReservacionColonosView.nuevo_reservacion ,
         name='nuevo_reservacion_colono'),
    path('reservacion_registrar', ReservacionColonosView.registrar_js_reservacion ,
         name='registrar_reservacion '),
    path('obtener_imagenAnuncios', AnunciosView.obtener_imagenAnuncios ,
         name='imagenAnuncios_obtener'),
    path('obtener_imagenIngresos', AnunciosView.obtener_imagenIngresos ,
         name='imagenIngresos_obtener'),
    path('obtener_imagenEgresos', AnunciosView.obtener_imagenEgresos ,
         name='imagenEgresos_obtener'),
    path('obtener_imagenFinanciero', AnunciosView.obtener_imagenFinanciero,
         name='imagenFinanciero_obtener'),
    path('mascota_nuevo_colono', MascotaView.nuevo_mascota,
         name='nuevo_mascota_colono'),
    path('mascota_editar_colono/<int:id>', MascotaView.editar_mascota,
         name='editar_mascota_colono'),

    path('visita_nuevo', VisitasView.nuevo_visita,
         name='nuevo_visita'),
    path('visita_registrar', VisitasView.registrar_js_visita,
         name='registrar_visita'),
    path('visita_actualizar', VisitasView.actualizar_js_visita,
         name='actualizar_visita'),
    path('visita_obteneruno', VisitasView.obteneruno_js_visita,
         name='obteneruno_visita'),
    path('visita_borrar/',
         VisitasView.borrar_js_visita, name='borrar_visita'),
    path('visitas_obtener/',
         VisitasView.obtener_visitas,name='visitas_obtener'),
    path('visita_vehiculo/',
         VisitasView.validar_vehiculo,name='visita_vehiculo'),

    path('reportevecino_nuevo_colono', ReporteVecinoView.nuevo_reportevecino,
         name='nuevo_reportevecino_colono'),
     path('sancion_nuevo_colono', SancionView.nuevo_sancion,
         name='nuevo_sancion_colono'),
         
     path('ingreso_nuevo/',
         IngresoView.nuevo_ingreso,name='nuevo_ingreso_colono'),
     path('ingreso_registrar',
          IngresoView.registrar_js_ingreso,name='registrar_ingreso_colono'),
     
     path('vehiculo_nuevo', VehiculoView.nuevo_vehiculo,
         name='nuevo_vehiculo_colono'),
    path('vehiculo_registrar', VehiculoView.registrar_js_vehiculo,
         name='registrar_vehiculo_colono'),
    path('vehiculo_actualizar', VehiculoView.actualizar_js_vehiculo,
         name='actualizar_vehiculo_colono'),
    path('vehiculo_obteneruno', VehiculoView.obteneruno_js_vehiculo,
         name='obteneruno_vehiculo_colono'),
    path('vehiculo_borrar/',
         VehiculoView.borrar_js_vehiculo, name='borrar_vehiculo_colono'),

]
