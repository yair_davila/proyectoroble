from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from vehiculos.choices import COLOR_CHOICES, MARCA_CHOICES , TIPO_CHOICES,LINEA_CHOICES
from django import forms
from colonos.validador import validador_cadenas

class FormVisitaVehiculoColono(forms.ModelForm):
    color = forms.ChoiceField(choices=COLOR_CHOICES)
    marca = forms.ChoiceField(choices=MARCA_CHOICES)
    tipo = forms.ChoiceField(choices=TIPO_CHOICES, required=False)
    linea = forms.ChoiceField(choices=LINEA_CHOICES, required=False)
    placas = forms.CharField(validators=[validador_cadenas], required=False)

    class Meta:
        model = VehiculoVisitante
        fields = ('placas','color', 'marca', 'linea', 'tipo')