from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from colonos.validador import validador_horas
from visitantes.validador import validador_numero_max,validador_numero_min
from visitantes.choices import HOUR_CHOICES, IDENTIFICACION_CHOICES, ACCESO_CHOICES
from django import forms
from usuarios.validador import validador_telefono
VEHICULO_CHOICES = [
    ('1', 'Vehicular'),
    ('0', 'Peatonal')
]

class FormVisitaColono (forms.ModelForm):
    
    fecha_estimada = forms.DateField(#widget=forms.SelectDateWidget(),
        label = 'Fecha',
        required=True)
    #hora_estimada_min = forms.ChoiceField(choices=HOUR_CHOICES, required=True)
    #hora_estimada_max = forms.ChoiceField(choices=HOUR_CHOICES, required=True)
    hora_estimada_min = forms.CharField(widget=forms.TimeInput, required=True,
                                        validators=[validador_horas])
    hora_estimada_max = forms.CharField(widget=forms.TimeInput, required=True,
                                        validators=[validador_horas])
    numero_personas = forms.IntegerField(
        validators=[validador_numero_min, validador_numero_max], required=False)
    nombre = forms.CharField(label='Nombre Visitante',max_length=50, help_text='Maximo 50 caracteres', required=True)
    apaterno = forms.CharField(label='Apellido Paterno Visitante',max_length=50, help_text='Maximo 50 caracteres', required=True)
    amaterno = forms.CharField(label='Apellido Materno Visitante',max_length=50, help_text='Maximo 50 caracteres', required=False)
    email = forms.EmailField(help_text='Escribe un email valido', required=False)
    telefono = forms.CharField(max_length=10, validators=[
                                    validador_telefono], required=False)
    """vehiculo = forms.MultipleChoiceField(
        label = 'Acceso de la visita',

        required=True,
        #widget=forms.CheckboxSelectMultiple,
        widget=forms.RadioSelect
        ,choices=VEHICULO_CHOICES,
    )"""


    class Meta:
        model = VisitaAutorizada
        fields = ('fecha_estimada', 'hora_estimada_min', 'hora_estimada_max',
                  'numero_personas', 'nombre', 'apaterno', 'amaterno', 'email', 'telefono')

