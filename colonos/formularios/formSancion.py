from sancion.modelos.sancionModel import Sancion
from sancion.validador import *
from django import forms
from usuarios.modelos.colonoModel import Colono
from sancion.modelos.tipoSancionModel import TipoSancion

class FormSancion (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    colono = forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    fecha = forms.DateTimeField(input_formats=['%Y-%m-%d'], widget=forms.DateTimeInput(attrs={
        'class': 'datetimepicker-input',
    }))
    tipo_sancion = forms.ModelChoiceField(queryset= TipoSancion.objects.filter(estatus=1))

    class Meta:
        model = Sancion
        fields = ('colono', 'tipo_sancion', 'fecha', 'descripcion', 'monto')


