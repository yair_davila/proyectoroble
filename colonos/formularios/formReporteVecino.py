from sancion.modelos.reporteVecinoModel import ReporteVecino
from sancion.validador import *
from django import forms

class FormReporteVecino (forms.ModelForm):
    observaciones = forms.CharField(validators=[validador_cadenas])
    

    class Meta:
        model = ReporteVecino
        fields = ('observaciones', 'foto')
