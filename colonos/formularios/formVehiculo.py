from vehiculos.modelos.vehiculoModel import Vehiculo
from vehiculos.choices import COLOR_CHOICES,MARCA_CHOICES,TIPO_CHOICES,LINEA_CHOICES
from django import forms

class FormVehiculo (forms.ModelForm):
    color = forms.ChoiceField(choices=COLOR_CHOICES)
    marca = forms.ChoiceField(choices=MARCA_CHOICES)
    tipo = forms.ChoiceField(choices=TIPO_CHOICES)
    linea = forms.ChoiceField(choices=LINEA_CHOICES)

    class Meta:
        model = Vehiculo
        fields = ('numero_placas', 'marca', 'modelo', 'color', 'tipo', 'linea')


