MARCA_CHOICES = (
    ("NISSAN", "NISSAN"),
    ("FORD", "FORD"),
    ("HONDA", "NISSAN"),
    ("KIA", "KIA"),
    ("CHEVROLET", "CHEVROLET"),
    ("PONTIAC", "PONTIAC"),
    ("SEAT", "SEAT"),
    ("OTRA", "Otra"),
)


COLOR_CHOICES = (
    ("VERDE", "Verde"),
    ("AZUL", "Azul"),
    ("ROJO", "Rojo"),
    ("BLANCO", "Blanco"),
    ("NEGRO", "Negro"),
    ("CAFE", "Cafe"),
    ("AMARILLO", "Amarillo"),
    ("GRIS", "Gris"),
    ("ROSA", "Rosa"),
)


TIPO_CHOICES = (
    ("SEDAN", "Sedan"),
    ("COMPACTO", "Compacto"),
    ("TRABAJO", "Trabajo"),
    ("MINIVAN", "MiniVan"),
)

LINEA_CHOICES = (
    ("LUJO", "Lujo"),
    ("ESTANDAR", "Estandar"),
)
