from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from vehiculos.modelos.colonoVehiculoModel import ColonoVehiculo
from vehiculos.formularios.formColonoVehiculo import FormColonoVehiculo

class ColonoVehiculoView():

    @login_required()
    @permission_required('vehiculos.add_colonovehiculo', login_url='colonovehiculo')
    def nuevo_colonovehiculo(context):
        form = FormColonoVehiculo()
        colonovehiculos = ColonoVehiculo.objects.filter(estatus=1)
        return render(context, 'nuevo_colonovehiculo.html',
                      {'formulario': form, 'accion': 'Nuevo', 'colonovehiculos': colonovehiculos})

    @login_required()
    @permission_required('vehiculos.change_colonovehiculo', login_url='colonovehiculo')
    def obteneruno_js_colonovehiculo(context):
        id = context.POST.get('id',None)
        data = ColonoVehiculoView.obtener_json_colonovehiculos_carga(ColonoVehiculo.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.delete_colonovehiculo', login_url='colonovehiculo')
    def borrar_js_colonovehiculo(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            colonovehiculo = ColonoVehiculo.objects.get(pk=o)
            colonovehiculo.estatus = 0
            colonovehiculo.save()

        data = ColonoVehiculoView.obtener_json_colonovehiculos(ColonoVehiculo.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_colonovehiculos(colonovehiculos):
        
        colonovehiculosJson = []
        for colonovehiculo in colonovehiculos:
            colonovehiculosJson.append(
                {
            'id' : colonovehiculo.id,
        	'colono': str(colonovehiculo.colono),
        	'vehiculo': str(colonovehiculo.vehiculo),

                    
                }
            )
        data = simplejson.dumps(colonovehiculosJson)
        return data


    def obtener_json_colonovehiculos_carga(colonovehiculos):
        
        colonovehiculosJson = []
        for colonovehiculo in colonovehiculos:
            colonovehiculosJson.append(
                {
            'id' : colonovehiculo.id,
          'colono': colonovehiculo.colono.id,
          'vehiculo': colonovehiculo.vehiculo.id,

                    
                }
            )
        data = simplejson.dumps(colonovehiculosJson)
        return data


    

    @login_required()
    @permission_required('vehiculos.add_colonovehiculo', login_url='colonovehiculo')
    def registrar_js_colonovehiculo(context):
        if (context.method == 'POST'):
            form = FormColonoVehiculo(context.POST)
            if form.is_valid():
                
                colonovehiculo = form.save(commit=False)
                colonovehiculo.usuario_creacion = context.user
                colonovehiculo.fecha_creacion = date.today()
                colonovehiculo.save()
                
                data = ColonoVehiculoView.obtener_json_colonovehiculos(ColonoVehiculo.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.add_colonovehiculo', login_url='colonovehiculo')
    def actualizar_js_colonovehiculo(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            colonovehiculo = ColonoVehiculo.objects.get(pk=id)

            form = FormColonoVehiculo(context.POST, instance=colonovehiculo)
            if form.is_valid():
                colonovehiculo = form.save()
                colonovehiculo.save()
                data = ColonoVehiculoView.obtener_json_colonovehiculos(ColonoVehiculo.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
