from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from vehiculos.modelos.vehiculoModel import Vehiculo
from vehiculos.formularios.formVehiculo import FormVehiculo

class VehiculoView():

    @login_required()
    @permission_required('vehiculos.add_vehiculo', login_url='vehiculo')
    def nuevo_vehiculo(context):
        form = FormVehiculo()
        vehiculos = Vehiculo.objects.filter(estatus=1)
        return render(context, 'nuevo_vehiculo.html',
                      {'formulario': form, 'accion': 'Nuevo', 'vehiculos': vehiculos})

    @login_required()
    @permission_required('vehiculos.add_vehiculo', login_url='vehiculo')
    def registrar_js_vehiculo(context):
        if (context.method == 'POST'):
            form = FormVehiculo(context.POST)
            if form.is_valid():
                vehiculo = form.save(commit=False)
                vehiculo.usuario_creacion = context.user
                vehiculo.fecha_creacion = date.today()
                vehiculo.save()
                data = VehiculoView.obtener_json_vehiculos(Vehiculo.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.add_vehiculo', login_url='vehiculo')
    def actualizar_js_vehiculo(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            vehiculo = Vehiculo.objects.get(pk=id)

            form = FormVehiculo(context.POST, instance=vehiculo)
            if form.is_valid():
                vehiculo = form.save()
                vehiculo.save()
                data = VehiculoView.obtener_json_vehiculos(Vehiculo.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.change_vehiculo', login_url='vehiculo')
    def obteneruno_js_vehiculo(context):
        id = context.POST.get('id',None)
        data = VehiculoView.obtener_json_vehiculos(Vehiculo.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vehiculos.delete_vehiculo', login_url='vehiculo')
    def borrar_js_vehiculo(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            vehiculo = Vehiculo.objects.get(pk=o)
            vehiculo.estatus = 0
            vehiculo.save()

        data = VehiculoView.obtener_json_vehiculos(Vehiculo.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_vehiculos(vehiculos):
        
        vehiculosJson = []
        for vehiculo in vehiculos:
            vehiculosJson.append(
                {
            'id' : vehiculo.id,
        	'numero_placas': vehiculo.numero_placas,
        	'marca': vehiculo.marca,
        	'modelo': vehiculo.modelo,
        	'color': vehiculo.color,
        	'tipo': vehiculo.tipo,
        	'linea': vehiculo.linea,

                    
                }
            )
        data = simplejson.dumps(vehiculosJson)
        return data
