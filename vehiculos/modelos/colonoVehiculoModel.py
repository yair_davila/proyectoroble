from vehiculos.modelos.vehiculoModel import Vehiculo
from usuarios.modelos.colonoModel import Colono
from django.contrib.auth.models import User
from django.db import models

class ColonoVehiculo(models.Model):
    colono = models.ForeignKey(Colono, models.DO_NOTHING, 'ID_COLONO_VEHICULO')
    vehiculo = models.ForeignKey(Vehiculo, models.DO_NOTHING, 'ID_VEHICULO')
    fecha_creacion = models.DateTimeField('Creacion_ColonoVehiculo')
    fecha_edicion = models.DateTimeField('Edicion_ColonoVehiculo', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_ColonoVehiculo')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_ColonoVehiculo', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.colono.__str__()+" "+self.vehiculo.__str__()

    def imprimir(self):
        return str(self.id)+','+str(self.colono.id)+','+\
            str(self.vehiculo.id)

    class Meta:
        app_label = "vehiculos"