from django.db import models
from django.contrib.auth.models import User

class Vehiculo(models.Model):
    numero_placas = models.CharField(
        'Numero_Placas', max_length=10, blank=True, null=True)
    marca = models.CharField('Marca', max_length=35, blank=True, null=True)
    modelo = models.SmallIntegerField('Modelo', blank=True, null=True)
    color = models.CharField('Color', max_length=35, blank=True, null=True)
    tipo = models.CharField('Tipo', max_length=35, blank=True, null=True)
    linea = models.CharField('Linea', max_length=35, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Vehiculo')
    fecha_edicion = models.DateTimeField('Edicion_Vehiculo', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Vehiculo')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Vehiculo', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.numero_placas+" "+self.color

    def imprimir(self):
        return str(self.id)+','+str(self.numero_placas)+','+\
            str(self.marca)+','+str(self.modelo)+','+\
            str(self.color)+','+str(self.tipo)+','+\
            str(self.linea)

    class Meta:
        app_label = "vehiculos"