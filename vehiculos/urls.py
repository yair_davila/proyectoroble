from vehiculos.modelos.colonoVehiculoModel import ColonoVehiculo
from vehiculos.vistas.colonoVehiculoView import ColonoVehiculoView
from vehiculos.modelos.vehiculoModel import Vehiculo
from vehiculos.vistas.vehiculoView import VehiculoView
from django.urls import path

urlpatterns = [
	path('vehiculo_nuevo', VehiculoView.nuevo_vehiculo,
         name='nuevo_vehiculo'),
    path('vehiculo_registrar', VehiculoView.registrar_js_vehiculo,
         name='registrar_vehiculo'),
    path('vehiculo_actualizar', VehiculoView.actualizar_js_vehiculo,
         name='actualizar_vehiculo'),
    path('vehiculo_obteneruno', VehiculoView.obteneruno_js_vehiculo,
         name='obteneruno_vehiculo'),
    path('vehiculo_borrar/',
         VehiculoView.borrar_js_vehiculo, name='borrar_vehiculo'),

	path('colonovehiculo_nuevo', ColonoVehiculoView.nuevo_colonovehiculo,
         name='nuevo_colonovehiculo'),
    path('colonovehiculo_registrar', ColonoVehiculoView.registrar_js_colonovehiculo,
         name='registrar_colonovehiculo'),
    path('colonovehiculo_actualizar', ColonoVehiculoView.actualizar_js_colonovehiculo,
         name='actualizar_colonovehiculo'),
    path('colonovehiculo_obteneruno', ColonoVehiculoView.obteneruno_js_colonovehiculo,
         name='obteneruno_colonovehiculo'),
    path('colonovehiculo_borrar/',
         ColonoVehiculoView.borrar_js_colonovehiculo, name='borrar_colonovehiculo'),


]