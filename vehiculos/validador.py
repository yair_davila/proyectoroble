from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _


def validador_cadenas(value):
    no_es_int = True
    try:
        a = int(value)
        no_es_int = False
    except Exception as e:
        no_es_int = True
    print(no_es_int)
    if no_es_int:
        raise ValidationError(
            _('Error {0} debe ser un numero sin letras'.format(value))
        )


def validador_telefono(value):
    excepcion = False
    try:
        valor = int(value)
        if int(value) > 9999999999:
            excepcion = True
        elif valor < 1000000000:
            excepcion = True

    except Exception as e:
        excepcion = True

    if excepcion:
        raise ValidationError(
            _('Error {0} el formato es incorrecto'.format(value))
        )
