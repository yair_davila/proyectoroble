from vehiculos.modelos.colonoVehiculoModel import ColonoVehiculo
from django import forms
from usuarios.modelos.colonoModel import Colono
from vehiculos.modelos.vehiculoModel import Vehiculo

class FormColonoVehiculo (forms.ModelForm):
    colono = forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    vehiculo = forms.ModelChoiceField(queryset=Vehiculo.objects.filter(estatus=1))

    class Meta:
        model = ColonoVehiculo
        fields = ('colono', 'vehiculo')
