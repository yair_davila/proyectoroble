$('#id_animal').change(function(){
    $.ajax({
        data : {'id':$('#id_animal').val()},
		url : '/mascotas/carga_razas',
		type : 'GET',
		success: function(data){
            $('#id_raza').children().remove();
			for (i=0;i<data.length;i++)
            {
                $("#id_raza").append('<option value='+data[i]['id']+'>'+data[i]['raza']+'</option>');

            }
		},
		error : function(xhr,errmsg,err) {
	        console.log(xhr.responseText);
	    }
    });
});
