function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function cancelar() {
    document.getElementById("id_area_comun").value = ""
    document.getElementById("id_tipo_evento").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_hora_inicial").value = ""
    document.getElementById("id_hora_final").value = ""
    document.getElementById("id_observaciones").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove()
        $("#btn_guardar").append("<p>Guardar</p>")
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        registrarReservacion();
        crearCalendario()
    }
}

function registrarReservacion() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/colonos/reservacion_registrar",
            type: "POST",
            data: {
                area_comun: $('#id_area_comun').val().toUpperCase(),
                tipo_evento: $('#id_tipo_evento').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                hora_inicial: $('#id_hora_inicial').val().toUpperCase(),
                hora_final: $('#id_hora_final').val().toUpperCase(),
                observaciones: $('#id_observaciones').val().toUpperCase(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                crearCalendario();
                //actualizarTablaReservacion(data,0)
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function actualizarReservacion() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/areasComunes/reservacion_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                area_comun: $('#id_area_comun').val().toUpperCase(),
                tipo_evento: $('#id_tipo_evento').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                hora_inicial: $('#id_hora_inicial').val().toUpperCase(),
                hora_final: $('#id_hora_final').val().toUpperCase(),
                observaciones: $('#id_observaciones').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;
                actualizarTablaReservacion(data, $("#id_provicional").val());

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function abrirModal(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/areasComunes/reservacion_obtenerdescripcion',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            x = document.getElementById("label_areaComun");
            x.innerHTML = data[0]['area_comun'];
            x = document.getElementById("label_tipoEvento");
            x.innerHTML = data[0]['tipo_evento'];
            x = document.getElementById("label_fecha");
            x.innerHTML = data[0]['fecha'];
            x = document.getElementById("label_horaInicio");
            x.innerHTML = data[0]['hora_inicial'];
            x = document.getElementById("label_horaFin");
            x.innerHTML = data[0]['hora_final'];
            x = document.getElementById("label_observaciones");
            x.innerHTML = data[0]['observaciones'];


            $("#boton_modal").click();

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}


function crearCalendario(jquery) {
    $.ajax({
        url: "../areasComunes/obtener_eventos",
        type: "GET",

        success: function(data) {
            var event_arr = $.parseJSON(data);
            var calendarEl = document.getElementById('calendarNuevo');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                eventClick: function(info) {

                    // alert('Event: ' + info.event.title);
                    // alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
                    // alert('View: ' + info.view.type);
                    abrirModal(info.event.id)
                    // window.open(info.event.url)
                },
                plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },
                defaultDate: new Date(),
                defaultView: 'dayGridMonth' //'dayGridWeek'//'timeGridDay'//'listWeek',//'dayGridMonth'
                    ,
                navLinks: true, // can click day/week names to navigate views
                businessHours: true, // display business hours
                editable: false,
                events: event_arr,
                locale: "es",
                selectable: true,
                selectHelper: true,
                /*select: function(arg) {
                    var title = prompt('Event Title:');
                    if (title) {
                      calendar.addEvent({
                        title: title,
                        start: arg.start,
                        end: arg.end,
                        allDay: arg.allDay
                      })
                    }
                    calendar.unselect()
                },*/
            });
            calendar.render();
        }
    });
};

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

function limpiar() {
    //document.getElementsByName("id_provicional")[0].value =""
    $("#id_provicional").val("")
    document.getElementById("id_area_comun").value = ""
    document.getElementById("id_tipo_evento").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_hora_inicial").value = ""
    document.getElementById("id_hora_final").value = ""
    document.getElementById("id_observaciones").value = ""


}