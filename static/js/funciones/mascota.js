//al presionar nuevo
window.onload = function() {
    document.getElementById("btn_nuevo").click();
}

function nuevo() {
    var si = $("#btn_nuevo").val();
    if (si == 1) {
        $("#btn_nuevo").val(0);
    } else {
        $("#btn_nuevo").val(1);
    }
}

function cancelar() {

    document.getElementById("id_animal").value = "";
    document.getElementById("id_raza").value = "";
    document.getElementById("id_tamano").value = "";
    document.getElementById("id_color").value = "";
    document.getElementById("id_genero").value = "";
    document.getElementById("id_nombre").value = "";
    document.getElementById("id_foto").value = "";



    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0);
}

function actualizarTablaMascota(data, focus) {
    $("#alerta").children().remove()
    if (data.length > 0) {
        if (data[0]['error'] != undefined) {
            document.getElementById("alerta").className = "alert alert-block alert-danger"
            $("#alerta").append(data[0]['error'])


        } else {
            document.getElementById("alerta").className = "alert alert-info"
            document.getElementById("alerta_exito").hidden = false;
            setTimeout(function() {
                document.getElementById("alerta_exito").hidden = true;
            }, 3000);
            document.getElementById("id_animal").value = "";
            document.getElementById("id_raza").value = "";
            document.getElementById("id_tamano").value = "";
            document.getElementById("id_color").value = "";
            document.getElementById("id_genero").value = "";
            document.getElementById("id_nombre").value = "";
            document.getElementById("id_foto").value = "";


            $("#alerta").append("<p> Completa el formulario </p>")
            $("#body-tabla").children().remove();

            if (data.length > 0) {

                for (i = data.length - 1; i >= 0; i--) {
                    $("#body-tabla").append(
                        "<tr id='info_" + data[i]['id'] + "'>\n" +
                        "<td>" + data[i]['nombre'] + "</td>" +
                        "<td>" + data[i]['animal'] +
                        "<td>" + data[i]['raza'] +
                        "<td>" + data[i]['tamano'] +
                        "<td>" + data[i]['color'] +
                        "<td>" + data[i]['genero'] + "</td>" +
                        "<td>" + "<a href='/media/" + data[i]['foto'] + "'>Imagen</a>" +
                        "<td>\n" +
                        "<a id='change' href='#' onclick='cargarEdicionMascota(" + data[i]['id'] + ")'>\n" +
                        "Editar\n" +
                        "<br>\n" +
                        "</a>\n" +
                        "<a id='delete' class='a_eliminar' data-toggle='modal' onclick='cargarEliminacionMascota(" + data[i]['id'] + ")' data-target='#myModal2'>\n" +
                        "\nEliminar\n" +
                        "</a>\n" +
                        "</td>\n" +
                        "</tr>\n"
                    )
                }
            }
        }
        document.formulario_temp.id_provicional.value = ''
    } else {
        $("#body-tabla").children().remove();
    }
}

function cargarEliminacionMascota(id) {

    $("#id_provicional").val(id);
}

function eliminar() {

    $.ajax({
        url: '/mascotas/mascota_borrar/',
        type: 'POST',
        data: {
            id: document.formulario_temp.id_provicional.value,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaMascota(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
}

function limpiar() {
    var a = document.getElementById("id_titulo").title

    if (a == 'Editar') {
        document.getElementById("btn_nuevo").click()
    } else {
        document.getElementById("id_animal").value = "";
        document.getElementById("id_raza").value = "";
        document.getElementById("id_tamano").value = "";
        document.getElementById("id_color").value = "";
        document.getElementById("id_genero").value = "";
        document.getElementById("id_nombre").value = "";
        document.getElementById("id_foto").value = "";

    }
}

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}
document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}
$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}