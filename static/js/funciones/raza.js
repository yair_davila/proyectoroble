//al presionar nuevo
$(document).on("click", "#btn_nuevo", function(evento) {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
});

$(document).on("click", "#btn_cancelar", function(evento) {
    evento.preventDefault();
    document.getElementById("id_animal").value = ""
    document.getElementById("id_nombre").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove()
        $("#btn_guardar").append("<p>Guardar</p>")
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
});

//al enviar formulario Registro o actualizacion
$("#form").on("submit", function(event) {
    event.preventDefault();

    if ($("#id_provicional").val() == '') {

        registrarRaza();
    } else {

        actualizarRaza();
    }
});

function actualizarTablaRaza(data, focus) {
    $("#alerta").children().remove()

    if (data[0]['error'] == "error") {
        document.getElementById("alerta").className = "alert alert-block alert-danger"
        $("#alerta").append("<p>Animal:" + data[1]['animal'] + "</p>")
        $("#alerta").append("<p>Nombre:" + data[1]['nombre'] + "</p>")



    } else {
        document.getElementById("alerta").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);
        document.getElementById("id_animal").value = ""
        document.getElementById("id_nombre").value = ""


        $("#alerta").append("<p> Completa el formulario </p>")
        $(".class_tabla").children().remove();
        $(".class_tabla").append(
            "<tr>" +
            "<th onclick='sortTable2(0)' style='cursor:pointer'>Animal</th>" +
            "<th onclick='sortTable2(1)' style='cursor:pointer'>Nombre</th>" +

            "</tr>"
        )

        for (i = data.length - 1; i >= 0; i--) {
            $(".class_tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['animal'] + "</td>" +
                "<td>" + data[i]['nombre'] + "</td>" +

                "<td>\n" +
                "<a id='change' href='javascript: cargarEdicionRaza(" + data[i]['id'] + ")'>\n" +
                "Editar\n" +
                "<br>\n" +
                "</a>\n" +
                "<a id='delete' class='a_eliminar' data-toggle='modal' onclick='cargarEliminacionRaza(" + data[i]['id'] + ")' href='#myModal2'>\n" +
                "\nEliminar\n" +
                "</a>\n" +
                "</td>\n" +
                "</tr>\n"
            )
        }

        $("#id_provicional").val('');
    }
}

function registrarRaza() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/mascotas/raza_registrar",
            type: "POST",
            data: {
                id: id,
                animal: $('#id_animal').val().toUpperCase(),
                nombre: $('#id_nombre').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaRaza(data, 0)
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function actualizarRaza() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/mascotas/raza_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                animal: $('#id_animal').val().toUpperCase(),
                nombre: $('#id_nombre').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove()
                $("#btn_guardar").append("<p> Guardar </p>")

                actualizarTablaRaza(data, $("#id_provicional").val())

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function cargarEliminacionRaza(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionRaza(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/mascotas/raza_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_animal").value = data[0]['animal']
            document.getElementById("id_nombre").value = data[0]['nombre']

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $("#btn_guardar").children().remove()
            $("#btn_guardar").append("<p> Guardar Actualizacion </p>")

            console.log($("#id_provicional").val())

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/mascotas/raza_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaRaza(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}