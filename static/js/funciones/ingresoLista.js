$(document).ready(
    function() {
        $("#btn_nuevo").click();
        document.getElementById("id_fecha").type = "date";
    }
);

function limpiado() {
    document.getElementById('id_colono').value = '';
    document.getElementById('id_tipo_ingreso').value = '';
    document.getElementById('id_fecha').value = '';
    document.getElementById('id_monto').value = '';
    document.getElementById('id_referencia').value = '';
    document.getElementById('id_estatus').value = 1;
    document.getElementById('id_abono').value = '';
    document.getElementById('id_descripcion').value = '';
    document.getElementById('id_ejercicio').value = '';
    document.getElementById('id_folio_recibo').value = '';

}



function cargarEliminacionIngreso(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionIngreso(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/ingresos/ingreso_obteneruno',
        type: "POST",
        data: {
            id: id,
            descripcion: 'consulta',
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            alert(data[0]['colono']);
            document.getElementById("id_colono").childNodes[0].nodeValue = data[0]['colono'];
            document.getElementById("id_tipo_ingreso").childNodes[0].nodeValue = data[0]['tipo_ingreso'];
            document.getElementById("id_fecha").childNodes[0].nodeValue = data[0]['fecha'];
            document.getElementById("id_monto").childNodes[0].nodeValue = data[0]['monto'];
            document.getElementById("id_referencia").childNodes[0].nodeValue = data[0]['referencia'];
            document.getElementById("id_estatus").childNodes[0].nodeValue = data[0]['estatus'];
            document.getElementById("id_abono").childNodes[0].nodeValue = data[0]['abono'];
            document.getElementById("id_descripcion").childNodes[0].nodeValue = data[0]['descripcion'];
            document.getElementById("id_ejercicio").childNodes[0].nodeValue = data[0]['ejercicio'];
            document.getElementById("id_folio_recibo").childNodes[0].nodeValue = data[0]['folio_recibo'];

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar_ingreso", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/ingresos/ingreso_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaIngreso(data);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}