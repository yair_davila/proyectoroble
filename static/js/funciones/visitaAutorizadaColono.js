//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo_visita").val()
    if (si == 1) {
        $("#btn_nuevo_visita").val(0)
    } else {
        $("#btn_nuevo_visita").val(1)
    }
}

function cancelar() {

    $("#id_fecha_estimada").val("");
    $("#id_hora_estimada_min").val("");
    $("#id_hora_estimada_max").val("");
    $("#id_numero_personas").val("");
    $("#id_nombre").val("");
    $("#id_apaterno").val("");
    $("#id_amaterno").val("");
    $("#id_email").val("");
    $("#id_telefono").val("");




    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo_visita").val(0)
}

//al enviar formulario Registro o actualizacion
function guardar() {


    if ($("#id_provicional").val() == '') {

        registrarVisita();
    } else {

        actualizarVisita();
    }
}

function actualizarTablaVisita(data, focus) {
    $("#alerta_visita").children().remove()
    if (data[0]['error'] != undefined) {
        document.getElementById("alerta_visita").className = "alert alert-block alert-danger"
        $("#alerta_visita").append(data[0]['error'])
    } else {
        document.getElementById("alerta_visita").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);
        document.getElementById("id_colono").value = ""
        document.getElementById("id_persona_ajena").value = ""
        document.getElementById("id_fecha").value = ""
        document.getElementById("id_hora_entrada").value = ""
        document.getElementById("id_hora_salida").value = ""
        document.getElementById("id_identificacion").value = ""
        document.getElementById("id_numero_personas").value = ""
        document.getElementById("id_foto").value = ""
        document.getElementById("id_tipo_acceso").value = ""
        document.getElementById("id_fecha_estimada").value = ""
        document.getElementById("id_hora_estimada_min").value = ""
        document.getElementById("id_hora_estimada_max").value = ""


        $("#alerta_visita").append("<p> Completa el formulario </p>")
        $(".class_tabla_visitaautorizada").children().remove();
        $(".class_tabla_visitaautorizada").append(
            "<tr>" +
            "<th onclick='sortTable2(0)' style='cursor:pointer'>Colono</th>" +
            "<th onclick='sortTable2(1)' style='cursor:pointer'>Persona_ajena</th>" +
            "<th onclick='sortTable2(2)' style='cursor:pointer'>Fecha</th>" +
            "<th onclick='sortTable2(3)' style='cursor:pointer'>Hora_entrada</th>" +
            "<th onclick='sortTable2(4)' style='cursor:pointer'>Hora_salida</th>" +
            "<th onclick='sortTable2(5)' style='cursor:pointer'>Identificacion</th>" +
            "<th onclick='sortTable2(6)' style='cursor:pointer'>Numero_personas</th>" +
            "<th onclick='sortTable2(7)' style='cursor:pointer'>Foto</th>" +
            "<th onclick='sortTable2(8)' style='cursor:pointer'>Tipo_acceso</th>" +
            "<th onclick='sortTable2(9)' style='cursor:pointer'>Fecha_estimada</th>" +
            "<th onclick='sortTable2(10)' style='cursor:pointer'>Hora_estimada_min</th>" +
            "<th onclick='sortTable2(11)' style='cursor:pointer'>Hora_estimada_max</th>" +
            "<th onclick='sortTable2(12)' style='cursor:pointer'>User_register</th>" +

            "</tr>"
        )

        for (i = data.length - 1; i >= 0; i--) {
            $(".class_tabla_visitaautorizada").append(
                "<tr id='info_visitaautorizada_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['colono'] + "</td>" +
                "<td>" + data[i]['persona_ajena'] + "</td>" +
                "<td>" + data[i]['fecha'] + "</td>" +
                "<td>" + data[i]['hora_entrada'] + "</td>" +
                "<td>" + data[i]['hora_salida'] + "</td>" +
                "<td>" + data[i]['identificacion'] + "</td>" +
                "<td>" + data[i]['numero_personas'] + "</td>" +
                "<td>" + data[i]['foto'] + "</td>" +
                "<td>" + data[i]['tipo_acceso'] + "</td>" +
                "<td>" + data[i]['fecha_estimada'] + "</td>" +
                "<td>" + data[i]['hora_estimada_min'] + "</td>" +
                "<td>" + data[i]['hora_estimada_max'] + "</td>" +

                "<td>\n" +
                "<a id='change' href='javascript: cargarEdicionVisita(" + data[i]['id'] + ")'>\n" +
                "Editar\n" +
                "<br>\n" +
                "</a>\n" +
                "<a id='delete' class='a_eliminar_visita' data-toggle='modal' onclick='cargarEliminacionVisita(" + data[i]['id'] + ")' href='#myModal2'>\n" +
                "\nEliminar\n" +
                "</a>\n" +
                "</td>\n" +
                "</tr>\n"
            )
        }
    }

    $("#id_provicional").val('');

}

function registrarVisita() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/visitantes/visitaautorizada_registrar",
            type: "POST",
            data: {
                id: id,
                colono: $('#id_colono').val().toUpperCase(),
                persona_ajena: $('#id_persona_ajena').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                hora_entrada: $('#id_hora_entrada').val().toUpperCase(),
                hora_salida: $('#id_hora_salida').val().toUpperCase(),
                identificacion: $('#id_identificacion').val().toUpperCase(),
                numero_personas: $('#id_numero_personas').val().toUpperCase(),
                foto: $('#id_foto').val().toUpperCase(),
                tipo_acceso: $('#id_tipo_acceso').val().toUpperCase(),
                fecha_estimada: $('#id_fecha_estimada').val().toUpperCase(),
                hora_estimada_min: $('#id_hora_estimada_min').val().toUpperCase(),
                hora_estimada_max: $('#id_hora_estimada_max').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaVisita(data, 0)
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function actualizarVisita() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/visitantes/visitaautorizada_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                colono: $('#id_colono').val().toUpperCase(),
                persona_ajena: $('#id_persona_ajena').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                hora_entrada: $('#id_hora_entrada').val().toUpperCase(),
                hora_salida: $('#id_hora_salida').val().toUpperCase(),
                identificacion: $('#id_identificacion').val().toUpperCase(),
                numero_personas: $('#id_numero_personas').val().toUpperCase(),
                foto: $('#id_foto').val().toUpperCase(),
                tipo_acceso: $('#id_tipo_acceso').val().toUpperCase(),
                fecha_estimada: $('#id_fecha_estimada').val().toUpperCase(),
                hora_estimada_min: $('#id_hora_estimada_min').val().toUpperCase(),
                hora_estimada_max: $('#id_hora_estimada_max').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;

                actualizarTablaVisita(data, $("#id_provicional").val());

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function cargarEliminacionVisita(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionVisita(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/visitantes/visitaautorizada_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_colono").value = data[0]['colono']
            document.getElementById("id_persona_ajena").value = data[0]['persona_ajena']
            document.getElementById("id_fecha").value = data[0]['fecha']
            document.getElementById("id_hora_entrada").value = data[0]['hora_entrada']
            document.getElementById("id_hora_salida").value = data[0]['hora_salida']
            document.getElementById("id_identificacion").value = data[0]['identificacion']
            document.getElementById("id_numero_personas").value = data[0]['numero_personas']
            document.getElementById("id_foto").value = data[0]['foto']
            document.getElementById("id_tipo_acceso").value = data[0]['tipo_acceso']
            document.getElementById("id_fecha_estimada").value = data[0]['fecha_estimada']
            document.getElementById("id_hora_estimada_min").value = data[0]['hora_estimada_min']
            document.getElementById("id_hora_estimada_max").value = data[0]['hora_estimada_max']

            if ($("#btn_nuevo_visita").val() == 0) {
                $("#btn_nuevo_visita").click()
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;



        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar_visita", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/visitantes/visitaautorizada_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaVisita(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla_visitaautorizada");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla_visitaautorizada", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

function limpiar() {
    document.getElementsByName("id_provicional")[0].value = ""
    document.getElementById("id_colono").value = ""
    document.getElementById("id_persona_ajena").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_hora_entrada").value = ""
    document.getElementById("id_hora_salida").value = ""
    document.getElementById("id_identificacion").value = ""
    document.getElementById("id_numero_personas").value = ""
    document.getElementById("id_foto").value = ""
    document.getElementById("id_tipo_acceso").value = ""
    document.getElementById("id_fecha_estimada").value = ""
    document.getElementById("id_hora_estimada_min").value = ""
    document.getElementById("id_hora_estimada_max").value = ""


}