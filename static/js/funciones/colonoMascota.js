//al presionar nuevo
$(document).on("click", "#btn_nuevo", function(evento) {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
});

$(document).on("click", "#btn_cancelar", function(evento) {
    evento.preventDefault();
    document.getElementById("id_colono").value = ""
    document.getElementById("id_mascota").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
});

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarColonoMascota();
    } else {
        actualizarColonoMascota();
    }
};

function actualizarTablaColonoMascota(data, focus) {
    $("#alerta").children().remove()
    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger"
        $("#alerta").append(data[0]['error'])
    } else {
        document.getElementById("alerta").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);
        document.getElementById("id_colono").value = ""
        document.getElementById("id_mascota").value = ""


        $("#alerta").append("<p> Completa el formulario </p>")
        $("#body-tabla").children().remove();


        for (i = data.length - 1; i >= 0; i--) {
            $("#body-tabla").append(
                "<tr id='info_colonomascota_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['colono'] + "</td>" +
                "<td>" + data[i]['mascota'] + "</td>" +

                "<td>\n" +
                "<a class='btn btn-sm btn-info' id='change' onclick='cargarEdicionColonoMascota(" + data[i]['id'] + ")'>\n" +
                "<i class='fa fa-pencil'></i>" +
                "</a>\n" +
                "<a class='btn btn-sm btn-danger' id='delete' data-toggle='modal' onclick='cargarEliminacionColonoMascota(" + data[i]['id'] + ")' data-target='#myModal2'>\n>" +
                "<i class='fa fa-trash'></i>" +
                "</a>\n" +
                "</td>\n" +
                "</tr>\n"
            )
        }


        $("#id_provicional").val('');
    }
}

function registrarColonoMascota() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/mascotas/colonomascota_registrar",
            type: "POST",
            data: {
                id: id,
                colono: $('#id_colono').val().toUpperCase(),
                mascota: $('#id_mascota').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaColonoMascota(data, 0)
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function actualizarColonoMascota() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/mascotas/colonomascota_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                colono: $('#id_colono').val().toUpperCase(),
                mascota: $('#id_mascota').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;

                actualizarTablaColonoMascota(data, $("#id_provicional").val())

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function cargarEliminacionColonoMascota(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionColonoMascota(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/mascotas/colonomascota_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_colono").value = data[0]['colono']
            document.getElementById("id_mascota").value = data[0]['mascota']

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;

            console.log($("#id_provicional").val())

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/mascotas/colonomascota_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaColonoMascota(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla_colonomascota");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla_colonomascota", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

function limpiar() {
    document.getElementsByName("id_provicional")[0].value = ""
    document.getElementById("id_colono").value = ""
    document.getElementById("id_mascota").value = ""


}