//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo_vehiculo").val();
    if (si == 1) {
        $("#btn_nuevo_vehiculo").val(0);
    } else {
        $("#btn_nuevo_vehiculo").val(1);
    }
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        registrarVehiculo();
    } else {
        actualizarVehiculo();
    }
}

function cancelar() {

    limpiado();
    document.getElementById("alerta_vehiculo").className = "alert alert-info";
    $("#alerta_vehiculo").children().remove();
    $("#alerta_vehiculo").append("<p> Completa el formulario </p>");

    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo_vehiculo").val(0);
}

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarVehiculo();
    } else {
        actualizarVehiculo();
    }
}

function mostrarAlertas() {
    document.getElementById("alerta_vehiculo").className = "alert alert-info";
    document.getElementById("alerta_exito").hidden = false;
    document.getElementById("alerta_exito2").hidden = false;
    setTimeout(function() {
        document.getElementById("alerta_exito").hidden = true;
        document.getElementById("alerta_exito2").hidden = true;
    }, 5000);
    $("#alerta_vehiculo").children().remove();
    $("#alerta_vehiculo").append("<p> Completa el formulario </p>");
}

function limpiado() {
    document.getElementById('id_numero_placas').value = '';
    document.getElementById('id_marca').value = '';
    document.getElementById('id_modelo').value = '';
    document.getElementById('id_color').value = '';
    document.getElementById('id_tipo').value = '';
    document.getElementById('id_linea').value = '';

}

function actualizarTablaVehiculo(data) {

    if (data[0] == undefined) {
        $("#body_tabla").children().remove();
        $("#id_provicional").val('');
        mostrarAlertas();
        return;
    }

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta_vehiculo").className = "alert alert-block alert-danger";
        $("#alerta_vehiculo").children().remove();
        $("#alerta_vehiculo").append(data[0]['error']);
    } else {
        mostrarAlertas();
        limpiado();
        $("#body_tabla").children().remove();

        for (i = 0; i < data.length; i++) {
            $("#body_tabla").append(
                "<tr id='info_vehiculo_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['numero_placas'] + "</td>" +
                "<td>" + data[i]['marca'] + "</td>" +
                "<td>" + data[i]['modelo'] + "</td>" +
                "<td>" + data[i]['color'] + "</td>" +
                "<td>" + data[i]['tipo'] + "</td>" +
                "<td>" + data[i]['linea'] + "</td>" +

                "<td>" +
                "<a id='change-vehiculo' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionVehiculo(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                /*"<a id='delete-vehiculo' href='#myModalDeleteVehiculo' onclick='cargarEliminacionVehiculo("+data[i]['id']+")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>"+
                            "<i class='fa fa-trash'></i>"+
                        "</a>"+*/
                "</td>" +
                "</tr>"
            );
        }
        $("#id_provicional").val('');
    }
}

function registrarVehiculo() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/colonos/vehiculo_registrar",
            type: "POST",
            data: {
                id: id,
                numero_placas: $('#id_numero_placas').val().toUpperCase(),
                marca: $('#id_marca').val().toUpperCase(),
                modelo: $('#id_modelo').val().toUpperCase(),
                color: $('#id_color').val().toUpperCase(),
                tipo: $('#id_tipo').val().toUpperCase(),
                linea: $('#id_linea').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaVehiculo(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function actualizarVehiculo() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/colonos/vehiculo_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                numero_placas: $('#id_numero_placas').val().toUpperCase(),
                marca: $('#id_marca').val().toUpperCase(),
                modelo: $('#id_modelo').val().toUpperCase(),
                color: $('#id_color').val().toUpperCase(),
                tipo: $('#id_tipo').val().toUpperCase(),
                linea: $('#id_linea').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;

                actualizarTablaVehiculo(data);

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function cargarEliminacionVehiculo(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionVehiculo(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/colonos/vehiculo_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            document.getElementById("id_numero_placas").value = data[0]['numero_placas'];
            document.getElementById("id_marca").value = data[0]['marca'];
            document.getElementById("id_modelo").value = data[0]['modelo'];
            document.getElementById("id_color").value = data[0]['color'];
            document.getElementById("id_tipo").value = data[0]['tipo'];
            document.getElementById("id_linea").value = data[0]['linea'];

            if ($("#btn_nuevo_vehiculo").val() == 0) {
                $("#btn_nuevo_vehiculo").click();
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar_vehiculo", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/colonos/vehiculo_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaVehiculo(data);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}