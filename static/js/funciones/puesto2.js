//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val();
    if (si == 1) {
        $("#btn_nuevo").val(0);
    } else {
        $("#btn_nuevo").val(1);
    }
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        alert('crearpuesto');
        registrarPuesto();
    } else {
        alert('actualizar');
        actualizarPuesto();
    }
}

function cancelar() {

    document.getElementById('id_descripcion').value = '';


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<p>Guardar</p>");
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0);
}

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarPuesto();
    } else {
        actualizarPuesto();
    }
}

function actualizarTablaPuesto(data, focus) {
    $("#alerta").children().remove();

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger";
        $("#alerta").append(data[0]['error']);
    } else {
        document.getElementById("alerta").className = "alert alert-info";
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 5000);
        document.getElementById('id_descripcion').value = '';

        $("#alerta").append("<p> Completa el formulario </p>");
        $("#tabla").children().remove();
        $("#tabla").append("<tbody>");
        $("#tabla").append(
            "<tr>" +
            "<th onclick='sortTable2(0)'>Descripcion</th>" +

            "</tr>"
        );

        for (i = data.length - 1; i >= 0; i--) {
            $("#tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['descripcion'] + "</td>" +

                "<td>" +
                "<a id='change-puesto' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionPuesto(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-puesto' href='#myModal2' onclick='cargarEliminacionPuesto(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            );
        }
        $("#tabla").append("</tbody>");
        $("#id_provicional").val('');
    }
}

function registrarPuesto() {
    var id = 0
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/asociaciones/puesto_registrar",
            type: "POST",
            data: {
                id: id,
                descripcion: $('#id_descripcion').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaPuesto(data, 0);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function actualizarPuesto() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/asociaciones/puesto_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                descripcion: $('#id_descripcion').val().toUpperCase(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                alert('suces actualizar')
                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<p> Guardar </p>");
                actualizarTablaPuesto(data, $("#id_provicional").val());

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function cargarEliminacionPuesto(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionPuesto(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/asociaciones/puesto_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_descripcion").value = data[0]['descripcion']

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click();
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<p> Guardar Actualizacion </p>");
            alert('termine');
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/asociaciones/puesto_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaPuesto(data, 0);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}