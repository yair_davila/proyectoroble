//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function cancelar() {

    document.getElementById("id_visita_autorizada").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_placas").value = ""
    document.getElementById("id_color").value = ""
    document.getElementById("id_marca").value = ""
    document.getElementById("id_linea").value = ""
    document.getElementById("id_tipo").value = ""
    document.getElementById("id_foto").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardarVisitante").append("<label>Guardar<label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

//al enviar formulario Registro o actualizacion
function guardar(event) {


    if ($("#id_provicional").val() == '') {

        registrarVehiculoVisitante();
    } else {

        actualizarVehiculoVisitante();
    }
}

function actualizarTablaVehiculoVisitante(data, focus) {
    $("#alerta").children().remove()
    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger"
        $("#alerta").append(data[0]['error'])
    } else {
        document.getElementById("alerta").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);
        document.getElementById("id_visita_autorizada").value = ""
        document.getElementById("id_fecha").value = ""
        document.getElementById("id_placas").value = ""
        document.getElementById("id_color").value = ""
        document.getElementById("id_marca").value = ""
        document.getElementById("id_linea").value = ""
        document.getElementById("id_tipo").value = ""
        document.getElementById("id_foto").value = ""


        $("#alerta").append("<p> Completa el formulario </p>")
        $(".class_tabla").children().remove();
        $(".class_tabla").append(
            "<tr>" +
            "<th onclick='sortTable2(0)' style='cursor:pointer'>Visita_autorizada</th>" +
            "<th onclick='sortTable2(1)' style='cursor:pointer'>Fecha</th>" +
            "<th onclick='sortTable2(2)' style='cursor:pointer'>Placas</th>" +
            "<th onclick='sortTable2(3)' style='cursor:pointer'>Color</th>" +
            "<th onclick='sortTable2(4)' style='cursor:pointer'>Marca</th>" +
            "<th onclick='sortTable2(5)' style='cursor:pointer'>Linea</th>" +
            "<th onclick='sortTable2(6)' style='cursor:pointer'>Tipo</th>" +
            "<th onclick='sortTable2(7)' style='cursor:pointer'>Foto</th>" +
            "<th onclick='sortTable2(8)' style='cursor:pointer'>User_register</th>" +

            "</tr>"
        )

        for (i = data.length - 1; i >= 0; i--) {
            $(".class_tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['visita_autorizada'] + "</td>" +
                "<td>" + data[i]['fecha'] + "</td>" +
                "<td>" + data[i]['placas'] + "</td>" +
                "<td>" + data[i]['color'] + "</td>" +
                "<td>" + data[i]['marca'] + "</td>" +
                "<td>" + data[i]['linea'] + "</td>" +
                "<td>" + data[i]['tipo'] + "</td>" +
                "<td>" + data[i]['foto'] + "</td>" +

                "<td>\n" +
                "<a id='change' href='javascript: cargarEdicionVehiculoVisitante(" + data[i]['id'] + ")'>\n" +
                "Editar\n" +
                "<br>\n" +
                "</a>\n" +
                "<a id='delete' class='a_eliminar' data-toggle='modal' onclick='cargarEliminacionVehiculoVisitante(" + data[i]['id'] + ")' href='#myModal2'>\n" +
                "\nEliminar\n" +
                "</a>\n" +
                "</td>\n" +
                "</tr>\n"
            )
        }
    }

    $("#id_provicional").val('');

}

function registrarVehiculoVisitante() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/visitantes/vehiculovisitante_registrar",
            type: "POST",
            data: {
                id: id,
                visita_autorizada: $('#id_visita_autorizada').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                placas: $('#id_placas').val().toUpperCase(),
                color: $('#id_color').val().toUpperCase(),
                marca: $('#id_marca').val().toUpperCase(),
                linea: $('#id_linea').val().toUpperCase(),
                tipo: $('#id_tipo').val().toUpperCase(),
                foto: $('#id_foto').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaVehiculoVisitante(data, 0)
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function actualizarVehiculoVisitante() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/visitantes/vehiculovisitante_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                visita_autorizada: $('#id_visita_autorizada').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                placas: $('#id_placas').val().toUpperCase(),
                color: $('#id_color').val().toUpperCase(),
                marca: $('#id_marca').val().toUpperCase(),
                linea: $('#id_linea').val().toUpperCase(),
                tipo: $('#id_tipo').val().toUpperCase(),
                foto: $('#id_foto').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardarVisitante").append("<label>Guardar<label>");;

                actualizarTablaVehiculoVisitante(data, $("#id_provicional").val());

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function cargarEliminacionVehiculoVisitante(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionVehiculoVisitante(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/visitantes/vehiculovisitante_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_visita_autorizada").value = data[0]['visita_autorizada']
            document.getElementById("id_fecha").value = data[0]['fecha']
            document.getElementById("id_placas").value = data[0]['placas']
            document.getElementById("id_color").value = data[0]['color']
            document.getElementById("id_marca").value = data[0]['marca']
            document.getElementById("id_linea").value = data[0]['linea']
            document.getElementById("id_tipo").value = data[0]['tipo']
            document.getElementById("id_foto").value = data[0]['foto']

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $("#btn_guardarVisitante").append("<label>Guardar Actualizacion<label>");;



        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/visitantes/vehiculovisitante_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaVehiculoVisitante(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

function limpiar() {
    document.getElementsByName("id_provicional")[0].value = ""
    document.getElementById("id_visita_autorizada").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_placas").value = ""
    document.getElementById("id_color").value = ""
    document.getElementById("id_marca").value = ""
    document.getElementById("id_linea").value = ""
    document.getElementById("id_tipo").value = ""
    document.getElementById("id_foto").value = ""


}