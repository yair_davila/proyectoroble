$(document).ready(
    function() {
        $("#pagina1").addClass('active');
    }
);
//al presionar nuevo

function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function activarColono(parametro) {
    $.ajax({
        url: "/colono/enviar",
        type: "GET",
        data: {
            id: parametro
        },
        success: function() {
            console.log("exito");
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};

function cancelar() {

    document.getElementById("id_amaterno").value = ""
    document.getElementById("id_genero").value = "-"
    document.getElementById("id_telefono_celular").value = ""
    document.getElementById("id_telefono_fijo").value = ""
    document.getElementById("id_acepta_visitas").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").html('Guardar');
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        registrarColono();
    } else {
        actualizarColono();
    }
}

function limpiarCampos() {
    $("#alerta").append("<p> Completa el formulario </p>")
    document.getElementById("id_first_name").value = "";
    document.getElementById("id_last_name").value = "";
    document.getElementById("id_email").value = "";
    document.getElementById("id_domicilio").value = null;
    document.getElementById("id_amaterno").value = "";
    document.getElementById("id_genero").value = "0";
    document.getElementById("id_telefono_celular").value = "";
    document.getElementById("id_telefono_fijo").value = "";
    document.getElementById("id_acepta_visitas").value = "0";
    document.getElementById("id_titular").value = "0";
}

function actualizarTablaColono2(data, focus) {
    $("#alerta").children().remove()

    if (data[0]['error'] != undefined) {
        alert('error al actualizar tabla');
        $("#alerta").append(data[0]['error'])
    } else {
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 5000);
        document.getElementById("alerta").className = "alert alert-info"
        limpiarCampos();
        $("#body_tabla").children().remove();

        for (i = 0; i < data.length; i++) {
            $("#body_tabla").append(
                "<tr id='info'>" +
                "<td>" + data[i]['first_name'] +
                " " + data[i]['last_name'] + "</td>" +
                "<td>" + (data[i]['telefono_celular'] == null ? '' : data[i]['telefono_celular']) + "</td>" +
                "<td>" + data[i]['email'] + "</td>" +
                "<td>" + (data[i]['username'] == null ? '' : data[i]['username']) + "</td>" +
                "<td>" +
                "<a id='change-colono' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionColono(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-colono' data-target='#myModal2' onclick='cargarEliminacionColono(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            )
        }

        $("#id_provicional").val('');
    }
}

function actualizarTablaColono(data, focus) {
    $("#alerta").children().remove()

    if (data[0]['error'] != undefined) {
        alert('error al actualizar tabla');
        $("#alerta").append(data[0]['error'])
    } else {
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 5000);
        limpiarCampos();
        document.getElementById("alerta").className = "alert alert-info"
        limpiarCampos();
        $("#body_tabla").children().remove();


        for (i = 0; i < data.length; i++) {
            $("#body_tabla").append(
                "<tr id='info'>" +
                "<td>" + data[i]['first_name'] + "</td>" +
                "<td>" + data[i]['last_name'] + "</td>" +
                "<td>" + data[i]['telefono_celular'] + "</td>" +
                "<td>" + data[i]['telefono_fijo'] + "</td>" +
                "<td>" + data[i]['acepta_visitas'] + "</td>" +
                "<td>" + data[i]['email'] + "</td>" +
                "<td>" + (data[i]['username'] == null ? '' : data[i]['username']) + "</td>" +
                "<td>" +
                "<td>" +
                "<a id='change-colono' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionColono(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-colono' href='#myModal2' onclick='cargarEliminacionColono(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            )
        }

        $("#id_provicional").val('');
    }
}

function todosTablaColono() {
    $.ajax({
        url: '/colono/colonos_todos',
        type: "POST",
        data: {
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {


            $("#form")[0].reset();
            $("#id_provicional").val('');



            actualizarTablaColono2(data, '')

        },
        error: function(xhr, errmsg, err) {

            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

function registrarColono() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        var id = 0
        $.ajax({
            url: "/colono/colono_registrar",
            type: "POST",
            data: {
                first_name: $('#id_first_name').val().toUpperCase(),
                last_name: $('#id_last_name').val().toUpperCase(),
                amaterno: $('#id_amaterno').val().toUpperCase(),
                genero: $('#id_genero').val().toUpperCase(),
                telefono_celular: $('#id_telefono_celular').val().toUpperCase(),
                telefono_fijo: $('#id_telefono_fijo').val().toUpperCase(),
                acepta_visitas: $('#id_acepta_visitas').val().toUpperCase(),
                email: $('#id_email').val(),
                titular: $('#id_titular').val().toUpperCase(),
                domicilio: $('#id_domicilio').val().toUpperCase(),
                calle: $('#id_calle').val().toUpperCase(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                alert("exito")
                $("#id_provicional").val('');
                $("#form")[0].reset();
                actualizarTablaColono2(data, 0)
            },
            error: function(data, xhr, errmsg, err) {

                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
                actualizarTablaColono2(data, 0)
            }
        });
    }
};

function actualizarColono() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/colono/colono_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                first_name: $('#id_first_name').val().toUpperCase(),
                last_name: $('#id_last_name').val().toUpperCase(),
                amaterno: $('#id_amaterno').val().toUpperCase(),
                genero: $('#id_genero').val().toUpperCase(),
                telefono_celular: $('#id_telefono_celular').val().toUpperCase(),
                telefono_fijo: $('#id_telefono_fijo').val().toUpperCase(),
                acepta_visitas: $('#id_acepta_visitas').val().toUpperCase(),
                email: $('#id_email').val(),
                titular: $('#id_titular').val().toUpperCase(),
                domicilio: $('#id_domicilio').val().toUpperCase(),
                calle: $('#id_calle').val().toUpperCase(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                $("#form")[0].reset();
                $("#id_provicional").val('');
                alert("exito");
                $("#btn_guardar").html('Guardar');

                actualizarTablaColono2(data, $("#id_provicional").val())

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

var pagina = 1;

function obtener10(parametro) {
    $.ajax({
        url: "/colono/colonos_10",
        type: "GET",
        data: {
            id: parametro
        },
        success: function(data) {
            $("#pagina" + pagina).removeClass('active');
            pagina = parametro;
            $("#pagina" + pagina).addClass('active');


            actualizarTablaColono2(data, 0);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};

function cargarEliminacionColono(id) {

    $("#id_provicional").val(id);
}

function cambiaCalleCarga(calle, domicilio) {
    $.ajax({
        data: { 'id': calle },
        url: '/domicilios/domicilios_por_calle',
        type: 'GET',
        success: function(data) {
            $('#id_domicilio').children().remove();
            for (i = 0; i < data.length; i++) {
                $("#id_domicilio").append('<option value=' + data[i]['id'] + '>' + data[i]['num_domicilio'] + ' ' + data[i]['calle'] + '</option>');
                $("#id_domicilio").val(domicilio);
            }
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
}

function cargarEdicionColono(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/colono/colono_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_first_name").value = data[0]['first_name']
            document.getElementById("id_last_name").value = data[0]['last_name']
            document.getElementById("id_titular").hidden = true;
            document.getElementById("id_domicilio").value = data[0]['domicilio']
            document.getElementById("id_email").value = data[0]['email']

            document.getElementById("id_amaterno").value = data[0]['amaterno']
            document.getElementById("id_genero").value = data[0]['genero']
            document.getElementById("id_telefono_celular").value = data[0]['telefono_celular']
            document.getElementById("id_telefono_fijo").value = data[0]['telefono_fijo']
            document.getElementById("id_acepta_visitas").value = data[0]['acepta_visitas']
            if (data[0]['calle'] > 0) {

                document.getElementById("id_calle").value = data[0]['calle'];
                cambiaCalleCarga(data[0]['calle'], data[0]['domicilio'])
            }

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $('html, body').animate({ scrollTop: 0 }, 1250);
            $("#btn_guardar").text('Guardar Actualización');
            $("#form").focus();

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/colono/colono_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaColono(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function limpiar() {}

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla ", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

$(document).on("change", "#id_domicilio", function(evento) {
    evento.preventDefault();
    var domicilio = ""
    try {
        domicilio = $('#id_domicilio').val();
    } catch (error) {
        domicilio = "0"
    }
    $.ajax({
        type: 'POST',
        url: '/colono/consulta_titular/',
        data: {
            domicilio: domicilio,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            if (data[0]['titular'] == '1') {
                document.getElementById('id_titular').hidden = true;


            }
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});