$(document).ready(function() {
    cargarDomicilios();
    crearCalendario();
})

function abrirModal(calle, numero) {
    form = document.getElementById("id_formulario_temp");
    form.calle_consultar.value = calle;
    form.num_consultar.value = numero;

    $.ajax({

        url: '/domicilios/domicilio_consultaruno',
        type: "POST",
        data: {
            calle: calle,
            numero: numero,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            document.getElementById("calle_modal").textContent = data[0]['calle'];
            document.getElementById("numero_modal").textContent = data[0]['num_domicilio'];
            document.getElementById("titular_modal").textContent = data[0]['titular'];


        },
        error: function(xhr, errmsg, err) {}
    });
}


function cargarDomicilios() {
    $.ajax({
        url: "/domicilios/domicilio_casas",
        type: "GET",

        success: function(data) {
            for (i = data.length - 1; i >= 0; i--) {
                let idelement = data[i]['id']
                try {
                    document.getElementById(data[i]['id'] + "").className = "btn btn-primary btn-sm"
                } catch (error) {}
            }
        },
        error: function(xhr, errmsg, err) {}
    });
};