$(document).ready(
    function() {
        $("#btn_nuevo").click();
        limpiado();
        //x.innerHTML = monto;
        //$("#id_input_Fecha").val(currentYear);


        $("#btn_nuevo_ingreso").click();
        $("#id_tipo_ingreso").change(function() {


            var currentYear = (new Date).getFullYear();
            var selectedText = $("#id_tipo_ingreso option:selected").html();
            var monto = selectedText.split('~')[1];
            var x = document.getElementById("id_monto");
            x.innerHTML = monto;
            $("#id_monto").val(monto);
            $("#id_abono").val(monto);
            x = document.getElementById("id_ejercicio");
            x.innerHTML = currentYear;
            $("#id_ejercicio").val(currentYear);

            selectedText = selectedText.replace('~', ' ')
            $("#id_descripcion").val(currentYear + " " + selectedText);
        });
    }
);

$('#id_colono').change(function() {
    $.ajax({
        data: { 'id_colono': $('#id_colono').val() },
        url: '/domicilios/colonodomicilio_filtro_colono',
        type: 'GET',
        success: function(data) {
            $('#id_domicilio').children().remove();
            for (i = 0; i < data.length; i++) {
                $("#id_domicilio").append('<option value=' + data[i]['domicilio_id'] + '>' + data[i]['domicilio'] + '</option>');

            }
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});


//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val();
    if (si == 1) {
        $("#btn_nuevo").val(0);
    } else {
        $("#btn_nuevo").val(1);
    }
}

function guardar() {
    registrarIngreso();

}



//al enviar formulario Registro o actualizacion
var pagina = 1;

function obtener10(parametro) {
    if (parametro != pagina) {


        $('#btn_abrir_modal_espera').click();
        $.ajax({
            url: "/ingresos/ingresos_10",
            type: "GET",
            data: {
                id: parametro
            },
            success: function(data) {
                $("#pagina" + pagina).children().remove();
                $("#pagina" + pagina).append("<a onclick='obtener10(" + pagina + ")'>" + pagina + "</a>");
                pagina = parametro;
                $("#pagina" + parametro).children().remove();
                $("#pagina" + parametro).append("<a onclick='obtener10(" + parametro + ")'><h5>" + parametro + "</h5></a>");
                actualizarTablaIngreso(data, 0);
                $('#btn_cerrar_modal_espera').click();
                pagina = parametro;
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function enviar() {

    registrarIngreso();
}

function mostrarAlertas() {
    document.getElementById("alerta").className = "alert alert-info";
    document.getElementById("alerta_exito").hidden = false;
    document.getElementById("alerta_exito2").hidden = false;
    setTimeout(function() {
        document.getElementById("alerta_exito").hidden = true;
        document.getElementById("alerta_exito2").hidden = true;
    }, 5000);
    $("#alerta").children().remove();
    $("#alerta").append("<p> Completa el formulario </p>");
}

function limpiado() {
    var x = document.getElementById("id_monto");
    x.innerHTML = '';
    $("#id_monto").val('');
    $("#id_abono").val('');
    x = document.getElementById("id_ejercicio");
    x.innerHTML = '';
    $("#id_ejercicio").val('');
    $("#id_descripcion").val('');
    document.getElementById('id_domicilio').value = '';
    document.getElementById('id_colono').value = '';
    document.getElementById('id_tipo_ingreso').value = '';
    //document.getElementById('id_fecha').value = '';
    document.getElementById('id_monto').value = '';
    //document.getElementById('id_referencia').value = '';
    //document.getElementById('id_estatus').value = 1;
    document.getElementById('id_abono').value = '';
    document.getElementById('id_descripcion').value = '';
    //document.getElementById('id_ejercicio').value = '';
    //document.getElementById('id_folio_recibo').value = '';

}

function actualizarTablaIngreso(data) {

    if (data[0] == undefined) {
        $("#tabla").children().remove();
        $("#tabla").append("<thead>" +
            "<th onclick='sortTable2(0)'>Colono</th>" +
            "<th onclick='sortTable2(1)'>Tipo_ingreso</th>" +
            "<th onclick='sortTable2(8)'>Ejercicio</th>" +
            // "<th onclick='sortTable2(2)'>Fecha</th>"+
            "<th onclick='sortTable2(3)'>Monto</th>" +
            // "<th onclick='sortTable2(4)'>Referencia</th>"+
            // "<th onclick='sortTable2(5)'>Estatus</th>"+
            "<th onclick='sortTable2(6)'>Abono</th>" +
            // "<th onclick='sortTable2(7)'>Descripcion</th>"+

            // "<th onclick='sortTable2(9)'>Folio_recibo</th>"+
            "<th>Opciones</th>"
        );
        $("#tabla").append("</thead>");
        $("#id_provicional").val('');
        mostrarAlertas();
        return;
    }

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger";
        $("#alerta").children().remove();
        $("#alerta").append(data[0]['error']);
    } else {
        mostrarAlertas();
        limpiado();
        $("#tabla").children().remove();
        $("#tabla").append("<thead>" +
            "<th onclick='sortTable2(0)'>Colono</th>" +
            "<th onclick='sortTable2(1)'>Tipo_ingreso</th>" +
            "<th onclick='sortTable2(8)'>Ejercicio</th>" +
            //"<th onclick='sortTable2(2)'>Fecha</th>"+
            "<th onclick='sortTable2(3)'>Monto</th>" +
            //"<th onclick='sortTable2(4)'>Referencia</th>"+
            //"<th onclick='sortTable2(5)'>Estatus</th>"+
            "<th onclick='sortTable2(6)'>Abono</th>" +
            "<th onclick='sortTable2(4)'>Referencia</th>"
            //+
            //"<th onclick='sortTable2(7)'>Descripcion</th>"+

            //"<th onclick='sortTable2(9)'>Folio_recibo</th>"+
            //"<th>Opciones</th>"		
        );
        $("#tabla").append("</thead><tbody>");

        var estatus = '';
        for (i = 0; i < 10; i++) {


            if (data[i]['estatus'] === 1) {
                estatus = 'Activo';
            } else {
                estatus = 'Inactivo'
            }
            $("#tabla").append(
                "<tr id='info" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['colono'] + "</td>" +
                "<td>" + data[i]['tipo_ingreso'] + "</td>" +
                "<td>" + data[i]['ejercicio'] + "</td>" +
                //"<td>" + data[i]['fecha'] + "</td>"+
                "<td>" + data[i]['monto'] + "</td>" +
                //"<td>" + data[i]['referencia'] + "</td>"+
                //"<td>" + estatus + "</td>"+
                "<td>" + data[i]['abono'] + "</td>" +
                "<td><a href='/" + data[i]['referencia'] + "'>Referencia</a></td>" +
                //"<td>" + data[i]['descripcion'] + "</td>"+
                //"<td>" + data[i]['folio_recibo'] + "</td>"+

                /*"<td>" +
						"<a id='change-ingreso' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionIngreso("+data[i]['id']+")'>"+
							"<i class='fa fa-pencil'></i>"+
	                	"</a>"+
	                	"<a id='delete-ingreso' href='#myModalDeleteIngreso' onclick='cargarEliminacionIngreso("+data[i]['id']+")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>"+
	                        "<i class='fa fa-trash'></i>"+
						"</a>"+
					"</td>" +*/
                "</tr>"
            );
        }
        $("#tabla").append("</tbody>");
        $("#id_provicional").val('');
    }
}

function registrarIngreso() {
    var id = 0
    //alert($('#id_fecha').val());
    var exito = false;
    try {
        var domicilio = $('#id_domicilio').val();
        exito = true;
    } catch (error) {
        exito = false;
    }
    if (exito) {
        $.ajax({
            url: "/ingresos/ingreso_pagar_js/",
            type: "POST",
            data: {
                id: id,
                domicilio: $('#id_domicilio').val(),
                colono: $('#id_colono').val().toUpperCase(),
                tipo_ingreso: $('#id_tipo_ingreso').val().toUpperCase(),
                //fecha:$('#id_fecha').val(),
                monto: $('#id_monto').val().toUpperCase(),
                //referencia:$('#id_referencia').val().toUpperCase(),
                //estatus:$('#id_estatus').val().toUpperCase(),
                abono: $('#id_abono').val().toUpperCase(),
                descripcion: $('#id_descripcion').val().toUpperCase(),
                ejercicio: $('#id_ejercicio').val().toUpperCase(),
                //folio_recibo:$('#id_folio_recibo').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                actualizarTablaIngreso(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};


function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}