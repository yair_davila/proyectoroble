$(document).ready(function() {
    limpiado();
    $("#btn_nuevo").click();
});

function nuevo() {
    $("#div_formulario").show()
}

function guardar() {
    if ($('#id_provicional').val() == '') {
        registrar();
    } else {
        actualizar();
    }
}

function limpiado() {
    $('#id_area_comun').val('');
    $('#id_ubicacion').val('');
    $('#id_hora_inicio_servicio').val('');
    $('#id_hora_fin_servicio').val('');
    $('#id_dias_servicios').val('');

    if ($('#id_provicional').val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $('#id_provicional').val('');
}

function cancelar() {
    limpiado();
}

function enviar() {
    if ($('#id_provicional').val() == '') {
        registrar();
    } else {
        actualizar();
    }
}

function actualizarTabla(data) {
    $("#alerta").children().remove();

    if (data[0]['error'] != undefined) {
        $("#alerta").addClass("alert alert-block alert-danger");
        $("#alerta").append(data[0]['error']);
    } else {
        $("#alerta").addClass("alert alert-info");
        document.getElementById("alerta_exito").hidden = false;
        document.getElementById("alerta_exito2").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
            document.getElementById("alerta_exito2").hidden = true;
        }, 5000);
        limpiado()

        $("#alerta").append("<p> Completa el formulario </p>");
        /*$("#tabla").children().remove();
        $("#tabla").append("<tbody>");
        $("#tabla").append(
            "<tr>" +
                "<th onclick='sortTable2(0)'>Area_comun</th>"+
                "<th onclick='sortTable2(1)'>Ubicacion</th>"+
                "<th onclick='sortTable2(2)'>Hora_inicio_servicio</th>"+
                "<th onclick='sortTable2(3)'>Hora_fin_servicio</th>"+
                "<th onclick='sortTable2(4)'>Dias_servicios</th>"+

            "</tr>"
        );*/
        $("#body_tabla").children().remove();
        for (i = data.length - 1; i >= 0; i--) {
            $("#body_tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['area_comun'] + "</td>" +
                "<td>" + data[i]['ubicacion'] + "</td>" +
                "<td>" + data[i]['hora_inicio_servicio'] + "</td>" +
                "<td>" + data[i]['hora_fin_servicio'] + "</td>" +
                "<td>" + data[i]['dias_servicios'] + "</td>" +

                "<td>" +
                "<a id='change-areaComun' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicion(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-areaComun' href='#myModalDelete' onclick='cargarEliminacion(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            );
        }
        //$("#tabla").append("</tbody>");
        $('#id_provicional').val('');
    }
}

function registrar() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/areasComunes/areacomun_registrar",
            type: "POST",
            data: {
                id: id,
                area_comun: $('#id_area_comun').val().toUpperCase(),
                ubicacion: $('#id_ubicacion').val().toUpperCase(),
                hora_inicio_servicio: $('#id_hora_inicio_servicio').val().toUpperCase(),
                hora_fin_servicio: $('#id_hora_fin_servicio').val().toUpperCase(),
                dias_servicios: $('#id_dias_servicios').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTabla(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function actualizar() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/areasComunes/areacomun_actualizar',
            type: "POST",
            data: {
                id: $('#id_provicional').val(),
                area_comun: $('#id_area_comun').val().toUpperCase(),
                ubicacion: $('#id_ubicacion').val().toUpperCase(),
                hora_inicio_servicio: $('#id_hora_inicio_servicio').val().toUpperCase(),
                hora_fin_servicio: $('#id_hora_fin_servicio').val().toUpperCase(),
                dias_servicios: $('#id_dias_servicios').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;
                actualizarTabla(data);

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function cargarEliminacion(id) {
    $("#id_provicional").val(id)
}

function cargarEdicion(id) {
    $("#id_provicional").val(id)
    $.ajax({
        url: '/areasComunes/areacomun_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            $('#id_area_comun').val(data[0]['area_comun']);
            $('#id_ubicacion').val(data[0]['ubicacion']);
            $('#id_hora_inicio_servicio').val(data[0]['hora_inicio_servicio']);
            $('#id_hora_fin_servicio').val(data[0]['hora_fin_servicio']);
            $('#id_dias_servicios').val(data[0]['dias_servicios']);

            $("#div_formulario").show();
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

function eliminar() {
    $.ajax({
        url: '/areasComunes/areacomun_borrar/',
        type: 'POST',
        data: {
            id: $('#id_provicional').val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTabla(data);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
}

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}