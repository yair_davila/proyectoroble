$(document).ready(
    function() {
        document.getElementById("id_fecha").type = "date";
        $("#pagina1").addClass('active');
    }
);
//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function limpiar() {
    if ($("#id_provicional").val() != '') {
        if (document.getElementById("id_tipo_ingreso").value !== "") {
            document.getElementById("id_tipo_ingreso").value = ""
        }
        if (document.getElementById("id_tipo_egreso").value !== "") {
            document.getElementById("id_tipo_egreso").value = "";
        }
        if (document.getElementById("id_monto").value !== "") {
            document.getElementById("id_monto").value = "";
        }
        if (document.getElementById("id_fecha").value !== "") {
            document.getElementById("id_fecha").value = "";
        }
        if (document.getElementById("id_referencia").value !== "") {
            document.getElementById("id_referencia").value = "";
        }
        if (document.getElementById("id_descripcion").value !== "") {
            document.getElementById("id_descripcion").value = "";
        }

        if (document.getElementById("id_folio_recibo").value !== "") {
            document.getElementById("id_folio_recibo").value = "";
        }
        if (document.getElementById("id_tipo_egresos").value !== "") {
            document.getElementById("id_tipo_egresos").value = "";
        }
    }


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>")
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

function cancelar() {
    document.getElementById("id_tipo_ingreso").value = ""
    document.getElementById("id_tipo_egreso").value = ""
    document.getElementById("id_monto").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_referencia").value = ""
    document.getElementById("id_descripcion").value = ""
    document.getElementById("id_ejercicio").value = "2020"
    document.getElementById("id_folio_recibo").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>")
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}
var pagina = 1;

function obtener10(parametro) {
    $('#btn_abrir_modal_espera').click();
    $.ajax({
        url: "/egresos/egresos_10",
        type: "GET",
        data: {
            id: parametro
        },
        success: function(recepcion) {
            $("#pagina" + pagina).removeClass('active');
            pagina = parametro;
            $("#pagina" + pagina).addClass('active');

            console.log(recepcion)
            actualizarTablaEgreso(recepcion, 0);
            $('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};

function actualizarTablaEgreso(data, focus) {
    $("#alerta").children().remove()

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger"
        $("#alerta").append(data[0]['error'])



    } else {
        document.getElementById("alerta").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);

        document.getElementById("id_tipo_ingreso").value = ""
        document.getElementById("id_tipo_egreso").value = ""
        document.getElementById("id_monto").value = ""
        document.getElementById("id_fecha").value = ""
        document.getElementById("id_referencia").value = ""
        document.getElementById("id_descripcion").value = ""
        document.getElementById("id_ejercicio").value = "2020"
        document.getElementById("id_folio_recibo").value = ""


        $("#alerta").append("<p> Completa el formulario </p>")
        $("#body_tabla").children().remove();
        let inicio = "</br><a href='/media/";
        let fin = "'>Imagen</a>"

        if (data.length > 0) {
            for (i = 0; i < data.length; i++) {


                $("#body_tabla").append(
                    "<tr id='info_" + data[i]['id'] + "'>\n" +
                    "<td>" + data[i]['tipo_ingreso'] + "</td>" +
                    "<td>" + data[i]['ejercicio'] + "</td>" +
                    "<td>" + data[i]['tipo'] + "</td>" +
                    "<td>" + data[i]['monto'] + "</td>" +
                    "<td>" + data[i]['descripcion'] +
                    inicio + data[i]['referencia'] + fin + "</td>" +
                    "<td>" + data[i]['fecha'] + "</td>" +
                    "<td>" + data[i]['folio_recibo'] + "</td>" +

                    "<td>\n" +
                    "<a id='change' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' href='../../egresos/egreso_editar/" + data[i]['id'] + "'>\n" +
                    "<i class='fa fa-pencil'></i>\n" +
                    "</a></br>\n" +

                    "<a id='delete-egreso' data-target='#myModal2' onclick='cargarEliminacionEgreso(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                    "<i class='fa fa-trash'>" +
                    "</i>" +
                    "</a>" +
                    "</td>\n" +
                    "</tr>\n"
                );
            }
        }

        $("#id_provicional").val('');
    }
}

function cargarEliminacionEgreso(id) {

    $("#id_provicional").val(id);
}

function eliminar() {

    //$('#btn_abrir_modal_espera').click();

    $.ajax({
        url: '/egresos/egreso_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            actualizarTablaEgreso(data, 0);
            //$('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {

        }
    })
}

function cargarEdicionEgreso(id) {

    $("#id_provicional").val(id);
    $('#btn_abrir_modal_espera').click();
    $.ajax({
        url: '/egresos/egreso_editar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            $('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {


        }
    })
}

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}