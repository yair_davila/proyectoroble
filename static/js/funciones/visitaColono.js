$(document).ready(
    function() {
        $("#btn_nuevo").click();
        document.getElementById("id_fecha_estimada").type = "date";
        document.getElementById("id_hora_estimada_min").type = "time";
        document.getElementById("id_hora_estimada_max").type = "time";
        crearCalendario();
    }
);
//al presionar nuevo
function vehiculoClick() {
    $("#boton_vehiculo").click();
}

function validarVehiculo() {
    /*alert('validar');
    alert($('#id_color').val());
    alert($('#id_color').val() === '');
    alert($('#id_color').val() == '');
    alert($('#id_color').val() == null);
    alert($('#id_color').val() === null);
    alert($('#id_color').val() == undefined);
    alert($('#id_color').val() === undefined);*/
    if (

        $('#id_color').val() == '' ||
        $('#id_color').val() == null ||
        $('#id_color').val() == undefined ||
        $('#id_marca').val() == '' ||
        $('#id_marca').val() == null ||
        $('#id_marca').val() == undefined

    ) {
        //$('#id_placas').val() =='' ||
        //|| $('#id_linea').val() ==''||
        //$('#id_tipo').val() ==''
        document.getElementById("alerta_vehiculo").className = "alert alert-block alert-danger";
        $("#alerta_vehiculo").children().remove();
        $("#alerta_vehiculo").append('<p>El color y la marca son requeridos</p>');
        $("#boton_vehiculo").click();
        $("#id_valido").val('0');
    } else {
        document.getElementById("alerta_vehiculo").className = "alert alert-success";
        $("#alerta_vehiculo").children().remove();
        $("#alerta_vehiculo").append('<p>Vehiculo valido</p>');
        $("#id_valido").val('1');
        $("#btn_cerrar_vehiculo").click();

    }
}

function validar(data) {
    if (data[0]['error'] != undefined) {
        document.getElementById("alerta_vehiculo").className = "alert alert-block alert-danger";
        $("#alerta_vehiculo").children().remove();
        $("#alerta_vehiculo").append(data[0]['error']);
        $("#boton_vehiculo").click();
    } else {
        if (data[0]['exito'] != undefined) {
            $("#btn_cerrar_vehiculo").click();
        }
    }
}

function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function cancelar() {
    limpiar();
    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }
    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

//al enviar formulario Registro o actualizacion
function guardar() {


    if ($("#id_provicional").val() == '') {

        registrarVisita();
    } else {

        actualizarVisita();
    }
}

function mostrarAlertas(data) {
    $("#alertas").children().remove();
    if (data[0]['error'] != undefined) {
        $("#alertas").children().remove();
        document.getElementById("alertas").className = "alert alert-block alert-danger";
        $("#alertas").append('<p>' + data[0]['error'] + '</p>');
    } else {
        if (data[0]['exito'] != undefined) {
            $("#alertas").children().remove();
            document.getElementById("alerta").className = "alert alert-info"
            $("#alertas").append("<p>Completa el formulario</p>");
            document.getElementById("alerta_exito").hidden = false;
            setTimeout(function() {
                document.getElementById("alerta_exito").hidden = true;
            }, 3000);
            limpiar();
        }
    }
}

function actualizarTablaVisita(data, focus) {
    $("#alertas").children().remove();
    if (data[0]['error'] != undefined) {
        document.getElementById("alertas").className = "alert alert-block alert-danger";
        //document.getElementById("alertas").childNodes[0].nodeValue=data[0]['error'];
        $("#alertas").append(data[0]['error']);
    } else {
        mostrarAlertas(data);

        limpiar();


        $("#alerta").append("<p> Completa el formulario </p>")
        $("#tabla").children().remove();
        $("#tabla").append(
            "<tr>" +
            "<th onclick='sortTable2(0)' style='cursor:pointer' >Colono</th>" +
            "<th onclick='sortTable2(1)' style='cursor:pointer' >Visitante</th>" +
            "<th onclick='sortTable2(2)' style='cursor:pointer' >Numero personas</th>" +
            "<th onclick='sortTable2(3)' style='cursor:pointer' >Fecha estimada</th>" +
            "<th onclick='sortTable2(4)' style='cursor:pointer' >Hora entrada</th>" +
            "<th onclick='sortTable2(5)' style='cursor:pointer' >Hora salida</th>" +
            "<th onclick='sortTable2(6)' style='cursor:pointer' >User register</th>" +
            "</tr>"
        )

        for (i = data.length - 1; i >= 0; i--) {
            $("#tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['colono'] + "</td>" +
                "<td>" + data[i]['persona_ajena'] + "</td>" +
                "<td>" + data[i]['numero_personas'] + "</td>" +
                "<td>" + data[i]['fecha_estimada'] + "</td>" +
                "<td>" + data[i]['hora_estimada_min'] + "</td>" +
                "<td>" + data[i]['hora_estimada_max'] + "</td>" +
                "<td>" + data[i]['user_register'] + "</td>" +

                "<td>\n" +
                "<a id='change' href='javascript: cargarEdicionVisita(" + data[i]['id'] + ")'>\n" +
                "Editar\n" +
                "<br>\n" +
                "</a>\n" +
                "<a id='delete' class='a_eliminar' data-toggle='modal' onclick='cargarEliminacionVisita(" + data[i]['id'] + ")' href='#myModal2'>\n" +
                "\nEliminar\n" +
                "</a>\n" +
                "</td>\n" +
                "</tr>\n"
            )
        }
    }

    $("#id_provicional").val('');

}

function registrarVisita() {
    var id = 0;
    var vehiculoValor = '0';
    if ($('#id_vehiculo_0').val() == '1') {
        vehiculoValor = '1';
    }

    if ($('#id_valido').val() == '1') {
        $.ajax({
            url: "/colonos/visita_registrar",
            type: "POST",
            data: {
                id: id,
                vehiculo: vehiculoValor,
                numero_personas: $('#id_numero_personas').val().toUpperCase(),
                fecha_estimada: $('#id_fecha_estimada').val().toUpperCase(),
                hora_estimada_min: $('#id_hora_estimada_min').val().toUpperCase(),
                hora_estimada_max: $('#id_hora_estimada_max').val().toUpperCase(),
                nombre: $('#id_nombre').val().toUpperCase(),
                apaterno: $('#id_apaterno').val().toUpperCase(),
                amaterno: $('#id_amaterno').val().toUpperCase(),
                email: $('#id_email').val().toUpperCase(),
                telefono: $('#id_telefono').val().toUpperCase(),
                placas: $('#id_placas').val().toUpperCase(),
                color: ($('#id_color').val() != null) ? $('#id_color').val().toUpperCase() : '',
                marca: ($('#id_marca').val() != null) ? $('#id_marca').val().toUpperCase() : '',
                linea: ($('#id_linea').val() != null) ? $('#id_linea').val().toUpperCase() : '',
                tipo: ($('#id_tipo').val() != null) ? $('#id_tipo').val().toUpperCase() : '',
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                crearCalendario();
                //limpiar();
                mostrarAlertas(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });

    } else {
        alert("invalido");
        document.getElementById("alertas").className = "alert alert-block alert-danger";
        $("#alertas").children().remove();
        $("#alertas").append('<p>El vehiculo no se registro con exito</p>');
    }
};

function actualizarVisita() {

    $.ajax({
        url: '/colonos/visita_actualizar',
        type: "POST",
        data: {
            id: $("#id_provicional").val(),
            numero_personas: $('#id_numero_personas').val().toUpperCase(),
            fecha_estimada: $('#id_fecha_estimada').val().toUpperCase(),
            hora_estimada_min: $('#id_hora_estimada_min').val().toUpperCase(),
            hora_estimada_max: $('#id_hora_estimada_max').val().toUpperCase(),
            hora_estimada_max: $('#id_nombre').val().toUpperCase(),
            apaterno: $('#id_apaterno').val().toUpperCase(),
            amaterno: $('#id_amaterno').val().toUpperCase(),
            email: $('#id_email').val().toUpperCase(),
            telefono: $('#id_telefono').val().toUpperCase(),

            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar</label>");;
            crearCalendario();
            limpiar();
            mostrarAlertas(data);

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};

function cargarEliminacionVisita(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionVisita(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/colonos/visita_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_colono").value = data[0]['colono']
            document.getElementById("id_persona_ajena").value = data[0]['persona_ajena']
            document.getElementById("id_fecha").value = data[0]['fecha']
            document.getElementById("id_hora_entrada").value = data[0]['hora_entrada']
            document.getElementById("id_hora_salida").value = data[0]['hora_salida']
            document.getElementById("id_identificacion").value = data[0]['identificacion']
            document.getElementById("id_numero_personas").value = data[0]['numero_personas']
            document.getElementById("id_foto").value = data[0]['foto']
            document.getElementById("id_tipo_acceso").value = data[0]['tipo_acceso']
            document.getElementById("id_fecha_estimada").value = data[0]['fecha_estimada']
            document.getElementById("id_hora_estimada_min").value = data[0]['hora_estimada_min']
            document.getElementById("id_hora_estimada_max").value = data[0]['hora_estimada_max']

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;



        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/colonos/visita_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            crearCalendario();
            //actualizarTablaVisita(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tablaautorizada");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

/*document.querySelector("#buscar").onkeyup = function(){
        $TableFilter("#tablaautorizada", this.value);
}¨*/

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

function limpiar() {
    document.getElementById("id_fecha_estimada").value = "";
    document.getElementById("id_hora_estimada_min").value = "";
    document.getElementById("id_hora_estimada_max").value = "";
    document.getElementById("id_numero_personas").value = "";
    document.getElementById("id_nombre").value = "";
    document.getElementById("id_apaterno").value = "";
    document.getElementById("id_amaterno").value = "";
    document.getElementById("id_email").value = "";
    document.getElementById("id_telefono").value = "";
    $('#id_vehiculo_1').click();
    $('#id_placas').val('');
    $('#id_color').val('');
    $('#id_marca').val('');
    $('#id_linea').val('');
    $('#id_tipo').val('');


}

function crearCalendario(jquery) {
    $.ajax({
        url: "/colonos/visitas_obtener",
        type: "GET",

        success: function(data) {
            var event_arr = $.parseJSON(data);
            $("#calendarNuevo").children().remove();
            var calendarEl = document.getElementById('calendarNuevo');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },
                defaultDate: new Date(),
                defaultView: 'listDay' //'dayGridWeek'//'timeGridDay'//'listWeek',//'dayGridMonth'
                    ,
                navLinks: true, // can click day/week names to navigate views
                businessHours: true, // display business hours
                editable: false,
                events: event_arr,
                locale: "es",
                selectable: true,
                selectHelper: true,
                /*select: function(arg) {
                    var title = prompt('Event Title:');
                    if (title) {
                      calendar.addEvent({
                        title: title,
                        start: arg.start,
                        end: arg.end,
                        allDay: arg.allDay
                      })
                    }
                    calendar.unselect()
                },*/
            });
            calendar.render();
        }
    });
};