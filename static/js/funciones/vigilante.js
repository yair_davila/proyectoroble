$(document).ready(
    function() {
        document.getElementById("id_fecha_inicio_servicio").type = "date";

    }
);
//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        registrarVigilante();
    } else {
        actualizarVigilante();
    }
}

function cancelar() {
    $("#id_first_name").val('');
    $("#id_email").val('');
    $("#id_last_name").val('');
    $("#id_amaterno").val('');
    $("#id_genero").val('0');
    $("#id_telefono").val('');
    $("#id_fecha_inicio_servicio").val('');


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarVigilante();
    } else {
        actualizarVigilante();
    }
}

function actualizarTablaVigilante(data, focus) {
    $("#alerta").children().remove()

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger"
        $("#alerta").append(data[0]['error'])
    } else {
        document.getElementById("alerta").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 5000);
        document.getElementById("alerta").className = "alert alert-info";
        console.log("cambie alerta");
        $("#id_first_name").val('');
        $("#id_email").val('');
        $("#id_last_name").val('');
        $("#id_amaterno").val('');
        $("#id_genero").val('0');
        $("#id_telefono").val('');
        $("#id_fecha_inicio_servicio").val('');


        $("#alerta").append("<p> Completa el formulario </p>")
        $("#body_tabla").children().remove();


        for (i = data.length - 1; i >= 0; i--) {
            $("#body_tabla").append(
                "<tr id='info'>" +
                "<td>" + data[i]['first_name'] + "" +
                "" + data[i]['last_name'] + "" +
                "" + data[i]['amaterno'] + "</td>" +
                "<td>" + data[i]['telefono'] + "</td>" +
                "<td>" + data[i]['fecha_inicio_servicio'].toString().substring(0, 10) + "</td>" +
                "<td>" + data[i]['fecha_fin_servicio'].toString().substring(0, 10) + "</td>" +
                "<td>" + data[i]['email'] + "</td>" +
                "<td>" + data[i]['username'] + "</td>" +
                "<td>" +
                "<a id='change' onclick='cargarEdicionVigilante(" + data[i]["id"] + ")'>" +
                "Editar" +
                "</a>" +
                "<br>" +
                "<a id='delete' href='#myModal2' onclick='cargarEliminacionVigilante(" + data[i]['id'] + ")' class='a_eliminar' data-toggle='modal'>" +
                "Eliminar" +
                "</a>" +
                "</td>" +
                "</tr>"
            )
        }

        $("#id_provicional").val('');
    }
}

function registrarVigilante() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/vigilante/vigilante_registrar",
            type: "POST",
            data: {
                first_name: $('#id_first_name').val().toUpperCase(),
                last_name: $('#id_last_name').val().toUpperCase(),
                amaterno: $('#id_amaterno').val().toUpperCase(),
                genero: $('#id_genero').val().toUpperCase(),
                telefono: $('#id_telefono').val().toUpperCase(),
                fecha_inicio_servicio: $('#id_fecha_inicio_servicio').val(),
                email: $('#id_email').val(),
                turno_id: $("#id_turno").val(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaVigilante(data, 0)
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function actualizarVigilante() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/vigilante/vigilante_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                first_name: $('#id_first_name').val().toUpperCase(),
                last_name: $('#id_last_name').val().toUpperCase(),
                amaterno: $('#id_amaterno').val().toUpperCase(),
                genero: $('#id_genero').val().toUpperCase(),
                telefono: $('#id_telefono').val().toUpperCase(),
                fecha_inicio_servicio: $('#id_fecha_inicio_servicio').val(),
                email: $('#id_email').val(),
                turno: $('#id_turno').val().toUpperCase(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;

                actualizarTablaVigilante(data, $("#id_provicional").val());

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function cargarEliminacionVigilante(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionVigilante(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/vigilante/vigilante_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            console.log(data)
            $("#id_first_name").val(data[0]['first_name']);
            $("#id_last_name").val(data[0]['last_name']);
            $("#id_amaterno").val(data[0]['amaterno']);
            $("#id_genero").val(data[0]['genero']);
            $("#id_telefono").val(data[0]['telefono']);
            $("#id_fecha_inicio_servicio").val(data[0]['fecha_inicio_servicio'].toString().substring(0, 10));

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");;



        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/vigilante/vigilante_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaVigilante(data, 0)
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}