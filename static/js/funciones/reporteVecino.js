$(document).ready(
    function() {
        document.getElementById("id_fecha").type = "date";
        $("#btn_nuevo").click();
    }
);
//al presionar nuevo
function reporte_new() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function cancelar() {
    document.getElementById("id_colono").value = ""
    document.getElementById("id_fecha").value = ""
    document.getElementById("id_observaciones").value = ""
    document.getElementById("id_foto").value = ""


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove()
        $("#btn_guardar").append("<p>Guardar</p>")
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}


function actualizarTablaReporteVecino(data, focus) {
    $("#body-tabla").children().remove();
    if (data.length > 0) {

        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);
        document.getElementById("alerta").className = "alert alert-info"
        $("#alerta").append("<p> Completa el formulario </p>")


        for (i = data.length - 1; i >= 0; i--) {
            $("#body-tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['colono'] + "</td>" +
                "<td>" + data[i]['fecha'] + "</td>" +
                "<td>" + data[i]['observaciones'] + "</td>" +
                "<td><a href='/media/'" + data[i]['foto'] + ">Imagen</a></td>" +
                "<td>\n" +
                "<a class='btn btn-info btn-sm' id='change' href='/sancion/reportevecino_editar/" + data[i]['id'] + "'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete' class='btn btn-danger btn-sm' data-target='#myModal2' onclick='cargarEliminacionReporteVecino(" + data[i]['id'] + ")' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>\n" +
                "</tr>\n"
            )
        }
        $("#id_provicional").val('');
    }
}


function cargarEliminacionReporteVecino(id) {

    $("#id_provicional").val(id);
}

function eliminar() {

    $.ajax({
        url: '/sancion/reportevecino_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            $(location).attr('href', '/sancion/reportevecino_nuevo');
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
}

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //	shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}

function limpiar() {
    var a = document.getElementById("id_titulo").title

    if (a == 'Editar') {
        document.getElementById("btn_nuevo").click()
    } else {
        document.getElementsByName("id_provicional")[0].value = ""
        document.getElementById("id_colono").value = ""
        document.getElementById("id_fecha").value = ""
        document.getElementById("id_observaciones").value = ""
        document.getElementById("id_foto").value = ""
    }
}