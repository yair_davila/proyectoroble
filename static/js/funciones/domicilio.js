var pagina = 1;
$(document).ready(function() {
    $("#pagina1").addClass('active');
    pagina = 1;
    $("#btn_nuevo").click();
});

//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val()
    if (si == 1) {
        $("#btn_nuevo").val(0)
    } else {
        $("#btn_nuevo").val(1)
    }
}

function cancelar() {

    document.getElementById("id_calle").value = "";
    document.getElementById("id_num_domicilio").value = "";
    document.getElementById("id_tipo_domicilio").value = "";
    document.getElementById("id_letra").value = "";
    document.getElementById("id_titular").value = "";
    document.getElementById("alerta").className = "alert alert-info"
    $("#alerta").children().remove()
    $("#alerta").append("<p>Completa el formulario</p>")


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0)
}

//al enviar formulario Registro o actualizacion
function guardar() {

    if ($("#id_provicional").val() == '') {

        registrarDomicilio();
    } else {

        actualizarDomicilio();
    }
}

function actualizarTablaDomicilio(data, focus) {
    $("#alerta").children().remove()
    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger"
        $("#alerta").append(data[0]['error'])
    } else {
        document.getElementById("alerta").className = "alert alert-info"
        document.getElementById("alerta_exito").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
        }, 3000);
        document.getElementById("id_calle").value = ""
        document.getElementById("id_num_domicilio").value = ""
        document.getElementById("id_tipo_domicilio").value = ""
        document.getElementById("id_letra").value = ""

        $("#alerta").append("<p> Completa el formulario </p>")
        $("#body_tabla").children().remove();


        for (i = 0; i < data.length; i++) {
            $("#body_tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['calle'] + "</td>" +
                "<td>" + data[i]['num_domicilio'] + "</td>" +
                "<td>" + data[i]['tipo_domicilio'] + "</td>" +
                "<td>" + data[i]['titular'] + "</td>" +
                "<td>\n" +
                "<a id='change' class='btn btn-info btn-sm' href='javascript: cargarEdicionDomicilio(" + data[i]['id'] + ")'>\n" +
                "<i class='fa fa-pencil'></i>\n" +
                "<br>\n" +
                "</a>\n" +
                "<a id='delete' class='btn btn-danger btn-sm' class='a_eliminar' data-toggle='modal' onclick='cargarEliminacionDomicilio(" + data[i]['id'] + ")' data-target='#myModal2'>\n" +
                "\n<i class='fa fa-trash'></i>\n" +
                "</a>\n" +
                "</td>\n" +
                "</tr>\n"
            )
        }
    }

    $("#id_provicional").val('');

}

function filtrar() {
    var id = 0
    $('#btn_abrir_modal_espera').click();
    $.ajax({
        url: "/domicilios/domicilio_filtrar",
        type: "GET",
        data: {
            calle: $('#filter_calle').val().toUpperCase(),
            tipo: $('#filter_tipo').val().toUpperCase()
        },
        success: function(data) {
            actualizarTablaDomicilio(data, 0)
            $('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};

function registrarDomicilio() {
    var id = 0
    $('#btn_abrir_modal_espera').click();
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/domicilios/domicilio_registrar",
            type: "POST",
            data: {
                id: id,
                calle: $('#id_calle').val().toUpperCase(),
                num: $('#id_num_domicilio').val(),
                letra: $('#id_letra').val() == null ? $('#id_letra').val() : $('#id_letra').val().toUpperCase(),
                tipo: $('#id_tipo_domicilio').val().toUpperCase(),
                titular: $('#id_titular').val(),


                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaDomicilio(data, 0)
                $('#btn_cerrar_modal_espera').click();
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function obtener10(parametro) {
    $('#btn_abrir_modal_espera').click();

    $.ajax({
        url: "/domicilios/domicilios_10",
        type: "GET",
        data: {
            id: parametro
        },
        success: function(data) {
            $("#pagina" + pagina).removeClass('active');
            pagina = parametro;
            $("#pagina" + pagina).addClass('active');
            actualizarTablaDomicilio(data, 0);
            $('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};


function obtenerTodos() {

    $('#btn_abrir_modal_espera').click();

    $.ajax({
        url: "/domicilios/domicilios_todos",
        type: "GET",
        data: {

        },
        success: function(data) {

            actualizarTablaDomicilio(data, 0);
            $('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
};

function clickNuevo() {
    if ($("#btn_nuevo").val() == 0) {
        $("#btn_nuevo").val(1);
    } else {
        $("#btn_nuevo").val(0);
    }
}

function actualizarDomicilio() {
    $('#btn_abrir_modal_espera').click();
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/domicilios/domicilio_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                calle: $('#id_calle').val().toUpperCase(),
                num: Number($('#id_num_domicilio').val()),
                tipo: $('#id_tipo_domicilio').val().toUpperCase(),
                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;

                actualizarTablaDomicilio(data, $("#id_provicional").val())
                $('#btn_cerrar_modal_espera').click();
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg)
                console.log(err)
            }
        });
    }
};

function cargarEliminacionDomicilio(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionDomicilio(id) {
    $("#id_provicional").val(id);
    $('#btn_abrir_modal_espera').click();

    $.ajax({
        url: '/domicilios/domicilio_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_calle").value = data[0]['calle']
            document.getElementById("id_num_domicilio").value = data[0]['num']
            document.getElementById("id_tipo_domicilio").value = data[0]['tipo']
            document.getElementById("id_letra").value = data[0]['letra']
            document.getElementById("id_titular").value = data[0]['titular']


            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click()
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");
            $('#btn_cerrar_modal_espera').click();


        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg)
            console.log(err)
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $('#btn_abrir_modal_espera').click();

    $.ajax({
        url: '/domicilios/domicilio_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaDomicilio(data, 0);
            $('#btn_cerrar_modal_espera').click();
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    })
});

function sortTable2(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}