$(document).ready(
    function() {
        document.getElementById("id_fecha_inicio").type = "date";
        document.getElementById("id_fecha_fin").type = "date";
        $("#btn_nuevo").click();
    }
);
//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val();
    if (si == 1) {
        $("#btn_nuevo").val(0);
    } else {
        $("#btn_nuevo").val(1);
    }
}

function guardar() {

    if ($("#id_provicional").val() == '') {
        registrarAdministracion();
    } else {
        actualizarAdministracion();
    }
}

function cancelar() {

    limpiado();
    document.getElementById("alerta").className = "alert alert-info";
    $("#alerta").children().remove();
    $("#alerta").append("<p> Completa el formulario </p>");

    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");;
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0);
}

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarAdministracion();
    } else {
        actualizarAdministracion();
    }
}

function mostrarAlertas() {
    document.getElementById("alerta").className = "alert alert-info";
    document.getElementById("alerta_exito").hidden = false;
    document.getElementById("alerta_exito2").hidden = false;
    setTimeout(function() {
        document.getElementById("alerta_exito").hidden = true;
        document.getElementById("alerta_exito2").hidden = true;
    }, 5000);
    $("#alerta").children().remove();
    $("#alerta").append("<p> Completa el formulario </p>");
}

function limpiado() {

    document.getElementById('id_nombre').value = '';
    document.getElementById('id_fecha_inicio').value = '';
    document.getElementById('id_fecha_fin').value = '';

}

function actualizarTablaAdministracion(data) {

    if (data[0] == undefined) {
        $("#body_tabla").children().remove();
        $("#id_provicional").val('');
        mostrarAlertas();
        return;
    }

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger";
        $("#alerta").children().remove();
        $("#alerta").append(data[0]['error']);
    } else {
        mostrarAlertas();
        limpiado();

        $("#body_tabla").children().remove();

        for (i = 0; i < data.length; i++) {
            $("#body_tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +

                "<td>" + data[i]['nombre'] + "</td>" +
                "<td>" + data[i]['fecha_inicio'] + "</td>" +
                "<td>" + data[i]['fecha_fin'] + "</td>" +

                "<td>" +
                "<a id='change-administracion' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionAdministracion(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-administracion' data-target='#myModalDeleteAdministracion' onclick='cargarEliminacionAdministracion(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            );
        }
        $("#id_provicional").val('');
    }
}

function registrarAdministracion() {
    var id = 0
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/asociaciones/administracion_registrar",
            type: "POST",
            data: {
                id: id,

                nombre: $('#id_nombre').val().toUpperCase(),
                fecha_inicio: $('#id_fecha_inicio').val().toUpperCase(),
                fecha_fin: $('#id_fecha_fin').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaAdministracion(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function actualizarAdministracion() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/asociaciones/administracion_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),

                nombre: $('#id_nombre').val().toUpperCase(),
                fecha_inicio: $('#id_fecha_inicio').val().toUpperCase(),
                fecha_fin: $('#id_fecha_fin').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                //$("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label>Guardar</label>");;
                var x = $("#btn_guardar");
                x.innerHTML = "Guardar";

                actualizarTablaAdministracion(data);

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function cargarEliminacionAdministracion(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionAdministracion(id) {

    $("#id_provicional").val(id);
    $.ajax({
        url: '/asociaciones/administracion_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {

            document.getElementById("id_nombre").value = data[0]['nombre'];
            document.getElementById("id_fecha_inicio").value = data[0]['fecha_inicio'];
            document.getElementById("id_fecha_fin").value = data[0]['fecha_fin'];

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click();
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label>Guardar Actualizacion</label>");

        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/asociaciones/administracion_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaAdministracion(data);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;

    //table1 = document.getElementById("tabla_nombre");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}