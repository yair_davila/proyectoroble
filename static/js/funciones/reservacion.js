$(document).ready(
    function() {
        document.getElementById("id_fecha").type = "date";
        $("#btn_nuevo").click();
    }
);
//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val();
    if (si == 1) {
        $("#btn_nuevo").val(0);
    } else {
        $("#btn_nuevo").val(1);
    }
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        registrarReservacion();
    } else {
        actualizarReservacion();
    }
}

function cancelar() {

    document.getElementById('id_colono').value = '';
    document.getElementById('id_area_comun').value = '';
    document.getElementById('id_tipo_evento').value = '';
    document.getElementById('id_fecha').value = '';
    document.getElementById('id_hora_inicial').value = '';
    document.getElementById('id_hora_final').value = '';
    document.getElementById('id_observaciones').value = '';


    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0);
}

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarReservacion();
    } else {
        actualizarReservacion();
    }
}

function actualizarTablaReservacion(data) {
    $("#alerta").children().remove();

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger";
        $("#alerta").append(data[0]['error']);
    } else {
        document.getElementById("alerta").className = "alert alert-info";
        document.getElementById("alerta_exito").hidden = false;
        document.getElementById("alerta_exito2").hidden = false;
        setTimeout(function() {
            document.getElementById("alerta_exito").hidden = true;
            document.getElementById("alerta_exito2").hidden = true;
        }, 5000);
        document.getElementById('id_colono').value = '';
        document.getElementById('id_area_comun').value = '';
        document.getElementById('id_tipo_evento').value = '';
        document.getElementById('id_fecha').value = '';
        document.getElementById('id_hora_inicial').value = '';
        document.getElementById('id_hora_final').value = '';
        document.getElementById('id_observaciones').value = '';

        $("#alerta").append("<p> Completa el formulario </p>");
        $("#table_body_id").children().remove();


        for (i = data.length - 1; i >= 0; i--) {
            $("#table_body_id").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['colono'] + "</td>" +
                "<td>" + data[i]['area_comun'] + "</td>" +
                "<td>" + data[i]['tipo_evento'] + "</td>" +
                "<td>" + data[i]['fecha'] + "</td>" +
                "<td>" + data[i]['hora_inicial'] + "</td>" +
                "<td>" + data[i]['hora_final'] + "</td>" +
                "<td>" + data[i]['observaciones'] + "</td>" +

                "<td>" +
                "<a id='change-reservacion' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionReservacion(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-reservacion' data-target='#myModalDeleteReservacion' onclick='cargarEliminacionReservacion(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            );
        }
        $("#id_provicional").val('');
    }
}

function registrarReservacion() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/areasComunes/reservacion_registrar",
            type: "POST",
            data: {
                id: id,
                colono: $('#id_colono').val().toUpperCase(),
                area_comun: $('#id_area_comun').val().toUpperCase(),
                tipo_evento: $('#id_tipo_evento').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                hora_inicial: $('#id_hora_inicial').val().toUpperCase(),
                hora_final: $('#id_hora_final').val().toUpperCase(),
                observaciones: $('#id_observaciones').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaReservacion(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function actualizarReservacion() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/areasComunes/reservacion_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                colono: $('#id_colono').val().toUpperCase(),
                area_comun: $('#id_area_comun').val().toUpperCase(),
                tipo_evento: $('#id_tipo_evento').val().toUpperCase(),
                fecha: $('#id_fecha').val().toUpperCase(),
                hora_inicial: $('#id_hora_inicial').val().toUpperCase(),
                hora_final: $('#id_hora_final').val().toUpperCase(),
                observaciones: $('#id_observaciones').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label> Guardar </label>");

                actualizarTablaReservacion(data);

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function cargarEliminacionReservacion(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionReservacion(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/areasComunes/reservacion_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            document.getElementById("id_colono").value = data[0]['colono'];
            document.getElementById("id_area_comun").value = data[0]['area_comun'];
            document.getElementById("id_tipo_evento").value = data[0]['tipo_evento'];
            document.getElementById("id_fecha").value = data[0]['fecha'];
            document.getElementById("id_hora_inicial").value = data[0]['hora_inicial'];
            document.getElementById("id_hora_final").value = data[0]['hora_final'];
            document.getElementById("id_observaciones").value = data[0]['observaciones'];

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click();
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label> Guardar Actualizacion </label>");
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/areasComunes/reservacion_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaReservacion(data);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    //table1 = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");
        //rows1 = table1.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];
            //x1 = rows1[i].getElementsByTagName("TD")[n];
            //y1 = rows1[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() > y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                //if (x1.innerHTML.toLowerCase() < y1.innerHTML.toLowerCase()) {
                //  shouldSwitch= true;
                //}
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            //rows1[i].parentNode.insertBefore(rows1[i + 1], rows1[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}