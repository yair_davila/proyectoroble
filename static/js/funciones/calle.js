$(document).ready(
    function() {
        $("#btn_nuevo").click();
    }
);
//al presionar nuevo
function nuevo() {
    var si = $("#btn_nuevo").val();
    if (si == 1) {
        $("#btn_nuevo").val(0);
    } else {
        $("#btn_nuevo").val(1);
    }
}

function guardar() {
    if ($("#id_provicional").val() == '') {
        registrarCalle();
    } else {
        actualizarCalle();
    }
}

function cancelar() {

    limpiado();
    document.getElementById("alerta").className = "alert alert-info";
    $("#alerta").children().remove();
    $("#alerta").append("<p> Completa el formulario </p>");

    if ($("#id_provicional").val() != '') {
        $("#btn_guardar").children().remove();
        $("#btn_guardar").append("<label>Guardar</label>");
    }

    $("#id_provicional").val('');
    $("#btn_nuevo").val(0);
}

//al enviar formulario Registro o actualizacion
function enviar() {
    if ($("#id_provicional").val() == '') {
        registrarCalle();
    } else {
        actualizarCalle();
    }
}

function mostrarAlertas() {

    $("#alerta_exito").show();
    $("#alerta_exito2").show();
    setTimeout(function() {
        $("#alerta_exito").hide();
        $("#alerta_exito2").hide();
    }, 5000);
}

function limpiado() {
    document.getElementById('id_nombre').value = '';

}

function actualizarTablaCalle(data) {
    if (data[0] == undefined) {
        $("#tabla").children().remove();
        $("#tabla").append("<thead>" +
            "<th onclick='sortTable2(0)'>Nombre</th>" +
            "<th>Opciones</th>"
        );
        $("#tabla").append("</thead>");
        $("#id_provicional").val('');
        mostrarAlertas();
        return;
    }

    if (data[0]['error'] != undefined) {
        document.getElementById("alerta").className = "alert alert-block alert-danger";
        $("#alerta").children().remove();
        $("#alerta").append(data[0]['error']);
    } else {
        mostrarAlertas();
        limpiado();
        $("#tabla").children().remove();
        $("#tabla").append("<thead>" +
            "<th onclick='sortTable2(0)'>Nombre</th>" +
            "<th>Opciones</th>"
        );
        $("#tabla").append("</thead><tbody>");

        for (i = 0; i < data.length; i++) {
            $("#tabla").append(
                "<tr id='info_" + data[i]['id'] + "'>\n" +
                "<td>" + data[i]['nombre'] + "</td>" +

                "<td>" +
                "<a id='change-calle' class='btn btn-info btn-sm' data-toggle='tooltip' title='Editar' onclick='cargarEdicionCalle(" + data[i]['id'] + ")'>" +
                "<i class='fa fa-pencil'></i>" +
                "</a>" +
                "<a id='delete-calle' href='#myModalDeleteCalle' onclick='cargarEliminacionCalle(" + data[i]['id'] + ")' class='btn btn-danger btn-sm' title='Eliminar' data-toggle='modal'>" +
                "<i class='fa fa-trash'></i>" +
                "</a>" +
                "</td>" +
                "</tr>"
            );
        }
        $("#tabla").append("</tbody>");
        $("#id_provicional").val('');
    }
}

function registrarCalle() {
    var id = 0;
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: "/domicilios/calle_registrar",
            type: "POST",
            data: {
                id: id,
                nombre: $('#id_nombre').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {
                actualizarTablaCalle(data);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function actualizarCalle() {
    var validator = $("#form").validate({});
    validator.form();
    if (validator.form() == true) {
        $.ajax({
            url: '/domicilios/calle_actualizar',
            type: "POST",
            data: {
                id: $("#id_provicional").val(),
                nombre: $('#id_nombre').val().toUpperCase(),

                csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
            },
            success: function(data) {

                $("#btn_guardar").children().remove();
                $("#btn_guardar").append("<label> Guardar </label>");

                actualizarTablaCalle(data);

            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
                console.log(errmsg);
                console.log(err);
            }
        });
    }
};

function cargarEliminacionCalle(id) {

    $("#id_provicional").val(id);
}

function cargarEdicionCalle(id) {
    $("#id_provicional").val(id);
    $.ajax({
        url: '/domicilios/calle_obteneruno',
        type: "POST",
        data: {
            id: id,
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            document.getElementById("id_nombre").value = data[0]['nombre'];

            if ($("#btn_nuevo").val() == 0) {
                $("#btn_nuevo").click();
            }
            $("#btn_guardar").children().remove();
            $("#btn_guardar").append("<label> Guardar Actualizacion </label>");
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.status + ": " + xhr.responseText);
            console.log(errmsg);
            console.log(err);
        }
    });
}

//al confirmar que si se va a elimnar
//ajax
$(document).on("click", ".btn_si_eliminar", function(evento) {
    evento.preventDefault();
    $.ajax({
        url: '/domicilios/calle_borrar/',
        type: 'POST',
        data: {
            id: $("#id_provicional").val(),
            csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val()
        },
        success: function(data) {
            actualizarTablaCalle(data);
        },
        error: function(xhr, errmsg, err) {
            console.log(xhr.responseText);
        }
    });
});

function sortTable2(n) {
    var table, table1, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("tabla");
    switching = true;

    dir = "asc";
    while (switching) {

        switching = false;
        rows = table.getElementsByTagName("TR");

        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                if (shouldSwitch == true) {
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                }
                if (shouldSwitch == true) {
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}

document.querySelector("#buscar").onkeyup = function() {
    $TableFilter("#tabla", this.value);
}

$TableFilter = function(id, value) {
    var rows = document.querySelectorAll(id + ' tbody tr');

    for (var i = 0; i < rows.length; i++) {
        var showRow = false;

        var row = rows[i];
        row.style.display = 'none';

        for (var x = 0; x < row.childElementCount; x++) {
            if (row.children[x].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1) {
                showRow = true;
                break;
            }
        }

        if (showRow) {
            row.style.display = null;
        }
    }
}