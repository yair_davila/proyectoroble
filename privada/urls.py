
from django.contrib import admin
from django.urls import path, include
from login.views import login_inicio

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('login.urls')),
    path('admin/', admin.site.urls),
    path('colono/', include('usuarios.urls')),
    path('areasComunes/', include('areasComunes.urls')),
    path('asociaciones/', include('asociaciones.urls')),
    path('domicilios/', include('domicilios.urls')),
    path('egresos/',include('egresos.urls')),
    path('ingresos/', include('ingresos.urls')),
    path('mascotas/', include('mascotas.urls')),
    path('sancion/', include('sancion.urls')),
    path('vehiculos/', include('vehiculos.urls')),
    path('visitantes/', include('visitantes.urls')),
    path('accounts/login/', login_inicio, name='sesion'),
    path('vigilante/', include('vigilante.urls')),
    path('respaldo/', include('respaldo.urls')),
    path('colonos/', include('colonos.urls')),
    path('anuncios/', include('anuncios.urls')),
] + static(settings.STATIC_URL, document_root = settings.STATIC_ROOT) + static(settings.STATIC_MEDIA, document_root = settings.STATIC_ROOT_MEDIA)
