from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.reunionModel import Reunion
from anuncios.formularios.fromReunion import FormReunion
from datetime import date

class ReunionView():
    @login_required()
    def nuevo(request):

        if (request.method == 'POST'):
            formI = FormReunion(request.POST, request.FILES)

            if formI.is_valid():
                infoUser = formI.save(commit=False)
                infoUser.usuario_creacion = request.user
                infoUser.fecha_creacion = date.today()
                infoUser.save()
                return redirect('nuevo_reunion')
        else:
            formI = FormReunion()
        imagenanuncios = Reunion.objects.all()
        return render(request, 'nuevo_reunion.html',
                      {'formulario': formI, 'accion': 'Nuevo', 'datos': imagenanuncios})

    @login_required()
    def list_reunion(context):
        reunion = Reunion.objects.all()
        return render(context, 'nuevo_reunion.html', {'reunion': reunion})

    @login_required()
    def eliminar_reunion(context, id):
        try:
            reunion = Reunion.objects.get(pk=id)
            reunion.delete()
            return redirect('nuevo_reunion')
        except Reunion.DoesNotExist:
            reunion = Reunion.objects.all()
            return render(context, 'nuevo_reunion.html',
                          {'reunion': reunion, 'error': 'No se encuentra el reunion'})

    @login_required()
    def editar_reunion(request, id):
        reunion = Reunion.objects.get(pk=id)
        if request.method == "POST":
            form = FormReunion(request.POST, instance=reunion)
            if form.is_valid():
                reunion = form.save()
                reunion.save()
                return redirect('nuevo_reunion')
        else:
            form = FormReunion(instance=reunion)
        return render(
            request,
            'nuevo_reunion.html', {'formReunion': form, 'accion': 'Editar'})

    """
    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def nuevo_imagenanuncios(context):
        form = FormReunion()

        return render(context, 'nuevo_imagenanuncios.html',
                      {'formulario': form, 'accion': 'Nuevo'})



    @login_required()
    @permission_required('anuncios.delete_imagenanuncios', login_url='imagenanuncios')
    def borrar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            imagenanuncios = Reunion.objects.get(pk=o)
            imagenanuncios.estatus = 0
            imagenanuncios.save()
        data = ReunionView.obtener_json_imagenanuncioss(Reunion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")



    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def registrar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            form = FormReunion(context.POST)

            if(form.is_valid()):
                imagenanuncios=form.save()
                imagenanuncios.save()
                data = ReunionView.obtener_json_imagenanuncioss(Reunion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def actualizar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            imagenanuncios = Reunion.objects.get(pk=id)
            form = FormReunion(context.POST, instance=imagenanuncios)
            if(form.is_valid()):
                imagenanuncios=form.save()
                imagenanuncios.save()
                data = ReunionView.obtener_json_imagenanuncioss(Reunion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """




