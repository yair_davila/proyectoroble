from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.imagenAnunciosModel import ImagenAnuncios
from anuncios.formularios.fromImagenAnuncios import FormImagenAnuncios
from datetime import date
from django.http import HttpResponse
import simplejson

class ImagenAnunciosView():
    @login_required()
    def nuevo(request):
        if (request.method == 'POST'):
            formI = FormImagenAnuncios(request.POST, request.FILES)

            if formI.is_valid():

                infoUser = formI.save(commit=False)
                infoUser.usuario_creacion = request.user
                infoUser.fecha_creacion = date.today()
                infoUser.save()

                return redirect('nuevo_imagenAnuncios')
        else:
            formI = FormImagenAnuncios()
        imagenanuncios = ImagenAnuncios.objects.all()
        return render(request, 'nuevo_imagenAnuncio.html', {'formulario': formI, 'accion': 'Nuevo','datos':imagenanuncios})


    @login_required()
    def list_imagenAnuncios(context):
        imagenAnuncios = ImagenAnuncios.objects.all()
        return render(context, 'nuevo_imagenAnuncio.html', {'imagenAnuncios': imagenAnuncios})

    def obtener_anuncios(context):
        imagenAnuncios = ImagenAnuncios.objects.all()
        imagenAnunciosJson = []
        for imagenAnuncio in imagenAnuncios:
            imagenAnunciosJson.append(
                {
                    'titulo': imagenAnuncio.titulo,
                    'foto': str(imagenAnuncio.foto)
                }
            )
        data = simplejson.dumps(imagenAnunciosJson)
        return HttpResponse(data, content_type="application/json")


    @login_required()
    def eliminar_imagenAnuncios(context, id):
        try:
            imagenAnuncios = ImagenAnuncios.objects.get(pk=id)
            imagenAnuncios.delete()
            return redirect('nuevo_imagenAnuncios')
        except ImagenAnuncios.DoesNotExist:
            imagenAnuncios = ImagenAnuncios.objects.all()
            return render(context, 'nuevo_imagenAnuncio.html', {'imagenAnuncios': imagenAnuncios, 'error': 'No se encuentra el imagenAnuncios'})


    @login_required()
    def editar_imagenAnuncios(request, id):
        imagenAnuncios = ImagenAnuncios.objects.get(pk=id)
        if request.method == "POST":
            form = FormImagenAnuncios(request.POST, instance=imagenAnuncios)
            if form.is_valid():
                imagenAnuncios = form.save()
                imagenAnuncios.save()
                return redirect('nuevo_imagenAnuncios')
        else:
            form = FormImagenAnuncios(instance=imagenAnuncios)
        return render(
            request, 
            'nuevo_imagenAnuncio.html', {'formImagenAnuncios': form, 'accion': 'Editar'})
    """
    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def nuevo_imagenanuncios(context):
        form = FormImagenAnuncios()
        
        return render(context, 'nuevo_imagenanuncios.html',
                      {'formulario': form, 'accion': 'Nuevo'})

    

    @login_required()
    @permission_required('anuncios.delete_imagenanuncios', login_url='imagenanuncios')
    def borrar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            imagenanuncios = ImagenAnuncios.objects.get(pk=o)
            imagenanuncios.estatus = 0
            imagenanuncios.save()
        data = ImagenAnunciosView.obtener_json_imagenanuncioss(ImagenAnuncios.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    

    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def registrar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            form = FormImagenAnuncios(context.POST)

            if(form.is_valid()):
                imagenanuncios=form.save()
                imagenanuncios.save()
                data = ImagenAnunciosView.obtener_json_imagenanuncioss(ImagenAnuncios.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def actualizar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            imagenanuncios = ImagenAnuncios.objects.get(pk=id)
            form = FormImagenAnuncios(context.POST, instance=imagenanuncios)
            if(form.is_valid()):
                imagenanuncios=form.save()
                imagenanuncios.save()
                data = ImagenAnunciosView.obtener_json_imagenanuncioss(ImagenAnuncios.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """
    
        

    
