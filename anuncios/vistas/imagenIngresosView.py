from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.imagenIngresosModel import ImagenIngresos
from anuncios.formularios.fromImgenIngresos import FormImagenIngresos
from datetime import date

class ImagenIngresosView():
    @login_required()
    def nuevo(request):

        if (request.method == 'POST'):
            formI = FormImagenIngresos(request.POST, request.FILES)

            if formI.is_valid():
                infoUser = formI.save(commit=False)
                infoUser.usuario_creacion = request.user
                infoUser.fecha_creacion = date.today()
                infoUser.save()
                return redirect('nuevo_imagenIngresos')
        else:
            formI = FormImagenIngresos()
        imageningresos = ImagenIngresos.objects.all()
        return render(request, 'nuevo_imagenIngreso.html', {'formulario': formI, 'accion': 'Nuevo','datos':imageningresos})


    @login_required()
    def list_imagenIngresos(context):
        imagenIngresos = ImagenIngresos.objects.all()
        return render(context, 'nuevo_imagenIngreso.html', {'imagenIngresos': imagenIngresos})


    @login_required()
    def eliminar_imagenIngresos(context, id):
        try:
            imagenIngresos = ImagenIngresos.objects.get(pk=id)
            imagenIngresos.delete()
            return redirect('nuevo_imagenIngresos')
        except ImagenIngresos.DoesNotExist:
            imagenIngresos = ImagenIngresos.objects.all()
            return render(context, 'nuevo_imagenIngreso.html', {'imagenIngresos': imagenIngresos, 'error': 'No se encuentra el imagenIngresos'})


    @login_required()
    def editar_imagenIngresos(request, id):
        imagenIngresos = ImagenIngresos.objects.get(pk=id)
        if request.method == "POST":
            form = FormImagenIngresos(request.POST, instance=imagenIngresos)
            if form.is_valid():
                imagenIngresos = form.save()
                imagenIngresos.save()
                return redirect('nuevo_imagenIngresos')
        else:
            form = FormImagenIngresos(instance=imagenIngresos)
        return render(
            request, 
            'nuevo_imagenIngreso.html', {'formImagenIngresos': form, 'accion': 'Editar'})
    """
    @login_required()
    @permission_required('ingresos.add_imageningresos', login_url='imageningresos')
    def nuevo_imageningresos(context):
        form = FormImagenIngresos()
        
        return render(context, 'nuevo_imageningresos.html',
                      {'formulario': form, 'accion': 'Nuevo'})

    

    @login_required()
    @permission_required('ingresos.delete_imageningresos', login_url='imageningresos')
    def borrar_js_imageningresos(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            imageningresos = ImagenIngresos.objects.get(pk=o)
            imageningresos.estatus = 0
            imageningresos.save()
        data = ImagenIngresosView.obtener_json_imageningresoss(ImagenIngresos.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    

    @login_required()
    @permission_required('ingresos.add_imageningresos', login_url='imageningresos')
    def registrar_js_imageningresos(context):
        if (context.method == 'POST'):
            form = FormImagenIngresos(context.POST)

            if(form.is_valid()):
                imageningresos=form.save()
                imageningresos.save()
                data = ImagenIngresosView.obtener_json_imageningresoss(ImagenIngresos.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_imageningresos', login_url='imageningresos')
    def actualizar_js_imageningresos(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            imageningresos = ImagenIngresos.objects.get(pk=id)
            form = FormImagenIngresos(context.POST, instance=imageningresos)
            if(form.is_valid()):
                imageningresos=form.save()
                imageningresos.save()
                data = ImagenIngresosView.obtener_json_imageningresoss(ImagenIngresos.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """
    
        

    
