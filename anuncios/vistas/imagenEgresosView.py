from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.imagenEgresosModel import ImagenEgresos
from anuncios.formularios.fromImagenEgresos import FormImagenEgresos
from datetime import date

class ImagenEgresosView():
    @login_required()
    def nuevo(request):

        if (request.method == 'POST'):
            formI = FormImagenEgresos(request.POST, request.FILES)

            if formI.is_valid():
                infoUser = formI.save(commit=False)
                infoUser.usuario_creacion = request.user
                infoUser.fecha_creacion = date.today()
                infoUser.save()
                return redirect('nuevo_imagenEgresos')
        else:
            formI = FormImagenEgresos()
        imagenegresos = ImagenEgresos.objects.all()
        return render(request, 'nuevo_imagenEgreso.html', {'formulario': formI, 'accion': 'Nuevo','datos':imagenegresos})


    @login_required()
    def list_imagenEgresos(context):
        imagenEgresos = ImagenEgresos.objects.all()
        return render(context, 'nuevo_imagenEgreso.html', {'imagenEgresos': imagenEgresos})


    @login_required()
    def eliminar_imagenEgresos(context, id):
        try:
            imagenEgresos = ImagenEgresos.objects.get(pk=id)
            imagenEgresos.delete()
            return redirect('nuevo_imagenEgresos')
        except ImagenEgresos.DoesNotExist:
            imagenEgresos = ImagenEgresos.objects.all()
            return render(context, 'nuevo_imagenEgreso.html', {'imagenEgresos': imagenEgresos, 'error': 'No se encuentra el imagenEgresos'})


    @login_required()
    def editar_imagenEgresos(request, id):
        imagenEgresos = ImagenEgresos.objects.get(pk=id)
        if request.method == "POST":
            form = FormImagenEgresos(request.POST, instance=imagenEgresos)
            if form.is_valid():
                imagenEgresos = form.save()
                imagenEgresos.save()
                return redirect('nuevo_imagenEgresos')
        else:
            form = FormImagenEgresos(instance=imagenEgresos)
        return render(
            request, 
            'nuevo_imagenEgreso.html', {'formImagenEgresos': form, 'accion': 'Editar'})
    """
    @login_required()
    @permission_required('egresos.add_imagenegresos', login_url='imagenegresos')
    def nuevo_imagenegresos(context):
        form = FormImagenEgresos()
        
        return render(context, 'nuevo_imagenegresos.html',
                      {'formulario': form, 'accion': 'Nuevo'})

    

    @login_required()
    @permission_required('egresos.delete_imagenegresos', login_url='imagenegresos')
    def borrar_js_imagenegresos(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            imagenegresos = ImagenEgresos.objects.get(pk=o)
            imagenegresos.estatus = 0
            imagenegresos.save()
        data = ImagenEgresosView.obtener_json_imagenegresoss(ImagenEgresos.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    

    @login_required()
    @permission_required('egresos.add_imagenegresos', login_url='imagenegresos')
    def registrar_js_imagenegresos(context):
        if (context.method == 'POST'):
            form = FormImagenEgresos(context.POST)

            if(form.is_valid()):
                imagenegresos=form.save()
                imagenegresos.save()
                data = ImagenEgresosView.obtener_json_imagenegresoss(ImagenEgresos.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.add_imagenegresos', login_url='imagenegresos')
    def actualizar_js_imagenegresos(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            imagenegresos = ImagenEgresos.objects.get(pk=id)
            form = FormImagenEgresos(context.POST, instance=imagenegresos)
            if(form.is_valid()):
                imagenegresos=form.save()
                imagenegresos.save()
                data = ImagenEgresosView.obtener_json_imagenegresoss(ImagenEgresos.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """
    
        

    
