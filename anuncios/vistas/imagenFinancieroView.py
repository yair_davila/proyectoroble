from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.imagenFinancieroModel import ImagenFinanciero
from anuncios.formularios.fromImagenFinanciero import FormImagenFinanciero
from datetime import date

class ImagenFinancieroView():
    @login_required()
    def nuevo(request):

        if (request.method == 'POST'):
            formI = FormImagenFinanciero(request.POST, request.FILES)

            if formI.is_valid():
                infoUser = formI.save(commit=False)
                infoUser.usuario_creacion = request.user
                infoUser.fecha_creacion = date.today()
                infoUser.save()
                return redirect('nuevo_imagenFinanciero')
        else:
            formI = FormImagenFinanciero()
        imagenegresos = ImagenFinanciero.objects.all()
        return render(request, 'nuevo_imagenFinanciero.html', {'formulario': formI, 'accion': 'Nuevo','datos':imagenegresos})


    @login_required()
    def list_imagenFinanciero(context):
        imagenFinanciero = ImagenFinanciero.objects.all()
        return render(context, 'nuevo_imagenFinanciero.html', {'imagenFinanciero': imagenFinanciero})


    @login_required()
    def eliminar_imagenFinanciero(context, id):
        try:
            imagenFinanciero = ImagenFinanciero.objects.get(pk=id)
            imagenFinanciero.delete()
            return redirect('nuevo_imagenFinanciero')
        except ImagenFinanciero.DoesNotExist:
            imagenFinanciero = ImagenFinanciero.objects.all()
            return render(context, 'nuevo_imagenFinanciero.html', {'imagenFinanciero': imagenFinanciero, 'error': 'No se encuentra el imagenFinanciero'})


    @login_required()
    def editar_imagenFinanciero(request, id):
        imagenFinanciero = ImagenFinanciero.objects.get(pk=id)
        if request.method == "POST":
            form = FormImagenFinanciero(request.POST, instance=imagenFinanciero)
            if form.is_valid():
                imagenFinanciero = form.save()
                imagenFinanciero.save()
                return redirect('nuevo_imagenFinanciero')
        else:
            form = FormImagenFinanciero(instance=imagenFinanciero)
        return render(
            request, 
            'nuevo_imagenFinanciero.html', {'formImagenFinanciero': form, 'accion': 'Editar'})
    """
    @login_required()
    @permission_required('egresos.add_imagenegresos', login_url='imagenegresos')
    def nuevo_imagenegresos(context):
        form = FormImagenFinanciero()
        
        return render(context, 'nuevo_imagenegresos.html',
                      {'formulario': form, 'accion': 'Nuevo'})

    

    @login_required()
    @permission_required('egresos.delete_imagenegresos', login_url='imagenegresos')
    def borrar_js_imagenegresos(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            imagenegresos = ImagenFinanciero.objects.get(pk=o)
            imagenegresos.estatus = 0
            imagenegresos.save()
        data = ImagenFinancieroView.obtener_json_imagenegresoss(ImagenFinanciero.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    

    @login_required()
    @permission_required('egresos.add_imagenegresos', login_url='imagenegresos')
    def registrar_js_imagenegresos(context):
        if (context.method == 'POST'):
            form = FormImagenFinanciero(context.POST)

            if(form.is_valid()):
                imagenegresos=form.save()
                imagenegresos.save()
                data = ImagenFinancieroView.obtener_json_imagenegresoss(ImagenFinanciero.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('egresos.add_imagenegresos', login_url='imagenegresos')
    def actualizar_js_imagenegresos(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            imagenegresos = ImagenFinanciero.objects.get(pk=id)
            form = FormImagenFinanciero(context.POST, instance=imagenegresos)
            if(form.is_valid()):
                imagenegresos=form.save()
                imagenegresos.save()
                data = ImagenFinancieroView.obtener_json_imagenegresoss(ImagenFinanciero.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """
    
        

    
