from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from anuncios.modelos.reglamentoModel import Reglamento
from anuncios.formularios.fromReglamento import FormReglamento
from datetime import date


class ReglamentoView():
    @login_required()
    def nuevo(request):

        if (request.method == 'POST'):
            formI = FormReglamento(request.POST, request.FILES)

            if formI.is_valid():
                infoUser = formI.save(commit=False)
                infoUser.usuario_creacion = request.user
                infoUser.fecha_creacion = date.today()
                infoUser.save()
                return redirect('nuevo_reglamento')
        else:
            formI = FormReglamento()
        imagenanuncios = Reglamento.objects.all()
        return render(request, 'nuevo_reglamento.html',
                      {'formulario': formI, 'accion': 'Nuevo', 'datos': imagenanuncios})

    @login_required()
    def list_reglamento(context):
        reglamento = Reglamento.objects.all()
        return render(context, 'nuevo_reglamento.html', {'reglamento': reglamento})

    @login_required()
    def eliminar_reglamento(context, id):
        try:
            reglamento = Reglamento.objects.get(pk=id)
            reglamento.delete()
            return redirect('nuevo_reglamento')
        except Reglamento.DoesNotExist:
            reglamento = Reglamento.objects.all()
            return render(context, 'nuevo_reglamento.html',
                          {'reglamento': reglamento, 'error': 'No se encuentra el reglamento'})

    @login_required()
    def editar_reglamento(request, id):
        reglamento = Reglamento.objects.get(pk=id)
        if request.method == "POST":
            form = FormReglamento(request.POST, instance=reglamento)
            if form.is_valid():
                reglamento = form.save()
                reglamento.save()
                return redirect('nuevo_reglamento')
        else:
            form = FormReglamento(instance=reglamento)
        return render(
            request,
            'nuevo_reglamento.html', {'formReglamento': form, 'accion': 'Editar'})

    """
    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def nuevo_imagenanuncios(context):
        form = FormReglamento()

        return render(context, 'nuevo_imagenanuncios.html',
                      {'formulario': form, 'accion': 'Nuevo'})



    @login_required()
    @permission_required('anuncios.delete_imagenanuncios', login_url='imagenanuncios')
    def borrar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            imagenanuncios = Reglamento.objects.get(pk=o)
            imagenanuncios.estatus = 0
            imagenanuncios.save()
        data = ReglamentoView.obtener_json_imagenanuncioss(Reglamento.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")



    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def registrar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            form = FormReglamento(context.POST)

            if(form.is_valid()):
                imagenanuncios=form.save()
                imagenanuncios.save()
                data = ReglamentoView.obtener_json_imagenanuncioss(Reglamento.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('anuncios.add_imagenanuncios', login_url='imagenanuncios')
    def actualizar_js_imagenanuncios(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            imagenanuncios = Reglamento.objects.get(pk=id)
            form = FormReglamento(context.POST, instance=imagenanuncios)
            if(form.is_valid()):
                imagenanuncios=form.save()
                imagenanuncios.save()
                data = ReglamentoView.obtener_json_imagenanuncioss(Reglamento.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    """




