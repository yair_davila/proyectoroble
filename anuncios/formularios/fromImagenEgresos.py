from anuncios.modelos.imagenEgresosModel import ImagenEgresos
from django import forms

class FormImagenEgresos (forms.ModelForm):
    class Meta:
        model = ImagenEgresos
        fields = ('titulo','foto')