from anuncios.modelos.imagenAnunciosModel import ImagenAnuncios
from django import forms
import datetime

class FormImagenAnuncios (forms.ModelForm):
    #fecha=forms.DateField();

    class Meta:
        model = ImagenAnuncios
        fields = ('titulo','foto')


