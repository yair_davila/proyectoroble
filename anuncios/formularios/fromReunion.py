from anuncios.modelos.reunionModel import Reunion
from django import forms

class FormReunion (forms.ModelForm):
    class Meta:
        model = Reunion
        fields = ('titulo','documento')