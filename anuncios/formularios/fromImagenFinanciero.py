from anuncios.modelos.imagenFinancieroModel import ImagenFinanciero
from django import forms

class FormImagenFinanciero (forms.ModelForm):
    class Meta:
        model = ImagenFinanciero
        fields = ('titulo','documento')