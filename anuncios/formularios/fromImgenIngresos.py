from anuncios.modelos.imagenIngresosModel import ImagenIngresos
from django import forms

class FormImagenIngresos (forms.ModelForm):
    class Meta:
        model = ImagenIngresos
        fields = ('titulo','foto')