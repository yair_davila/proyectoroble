from anuncios.modelos.reglamentoModel import Reglamento
from django import forms

class FormReglamento (forms.ModelForm):
    class Meta:
        model = Reglamento
        fields = ('titulo','documento')