from django.urls import path
from anuncios.vistas.imagenAnunciosView import ImagenAnunciosView
from anuncios.vistas.reglamentoView import ReglamentoView
from anuncios.vistas.reunionView import ReunionView
from anuncios.vistas.imagenEgresosView import ImagenEgresosView
from anuncios.vistas.imagenIngresosView import ImagenIngresosView
from anuncios.vistas.imagenFinancieroView import ImagenFinancieroView

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    #path('', ImagenAnunciosView.list_imagenAnuncios, name='list_imagenAnuncios'),
    path('', ImagenAnunciosView.nuevo, name='nuevo_imagenAnuncios'),
    path('eliminar/<int:id>', ImagenAnunciosView.eliminar_imagenAnuncios, name='eliminar_imagenAnuncios'),
    path('editar/<int:id>', ImagenAnunciosView.editar_imagenAnuncios, name='editar_imagenAnuncios'),
    path('reglamento', ReglamentoView.nuevo, name='nuevo_reglamento'),
    path('reunion', ReunionView.nuevo, name='nuevo_reunion'),
    path('imagenEgresos', ImagenEgresosView.nuevo, name='nuevo_imagenEgresos'),
    path('imagenIngreso', ImagenIngresosView.nuevo, name='nuevo_imagenIngresos'),
    path('imagenFinanciero', ImagenFinancieroView.nuevo, name='nuevo_imagenFinanciero'),
    path('anuncios_obtener', ImagenAnunciosView.obtener_anuncios,name='anuncios_obtener'),
]+ static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
