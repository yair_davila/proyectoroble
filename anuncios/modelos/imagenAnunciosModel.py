from django.db import models
from django.contrib.auth.models import User

class ImagenAnuncios (models.Model):
    titulo = models.CharField('Título', max_length=100, null=False)
    foto = models.ImageField(upload_to='ImagenAnuncios')
    fecha_creacion = models.DateTimeField('Creacion_Anuncio')
    fecha_edicion = models.DateTimeField('Edicion_Anuncio', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Anuncio')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Anuncio', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    class Meta:
        app_label = "anuncios"