from django.db import models
from django.contrib.auth.models import User

class ImagenEgresos (models.Model):
    titulo = models.CharField('Título', max_length=100, null=False)
    foto = models.FileField(upload_to='ImagenEgresos')
    fecha_creacion = models.DateTimeField('Creacion_ImagenEgresos')
    fecha_edicion = models.DateTimeField('Edicion_ImagenEgresos', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_ImagenEgresos')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_ImagenEgresos', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    class Meta:
        app_label = "anuncios"