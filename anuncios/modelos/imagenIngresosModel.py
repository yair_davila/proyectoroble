from django.db import models
from django.contrib.auth.models import User

class ImagenIngresos (models.Model):
    titulo = models.CharField('Título', max_length=100, null=False)
    foto = models.FileField(upload_to='ImagenIngresos')
    fecha_creacion = models.DateTimeField('Creacion_ImagenIngresos')
    fecha_edicion = models.DateTimeField('Edicion_ImagenIngresos', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_ImagenIngresos')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_ImagenIngresos', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    class Meta:
        app_label = "anuncios"