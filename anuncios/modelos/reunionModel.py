from django.db import models
from django.contrib.auth.models import User

class Reunion (models.Model):
    titulo = models.CharField('Título', max_length=100, null=False)
    documento = models.FileField(upload_to='reglamento')
    fecha_creacion = models.DateTimeField('Creacion_Reunion')
    fecha_edicion = models.DateTimeField('Edicion_Reunion', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Reunion')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Reunion', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    class Meta:
        app_label = "anuncios"