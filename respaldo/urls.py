from django.urls import path
from .views import *

urlpatterns = [

    path('', RespaldoView.subir_archivo, name='subir_archivo'),
    path('cargar_respaldo/<int:id>',
         RespaldoView.cargar_respaldo, name='cargar_respaldo'),
    path('imprimir',
         RespaldoView.imprimir_respaldo, name='imprimir'),

]
