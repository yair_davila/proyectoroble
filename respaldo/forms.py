from django import forms
from .models import *
from respaldo.choices import MODELO_CHOICES

class FormRespaldo(forms.ModelForm):
    modelo = forms.ChoiceField(choices=MODELO_CHOICES)

    class Meta:
        model = Respaldo
        fields = ('archivo','modelo')