from django.shortcuts import render, redirect
from respaldo.forms import *
from django.contrib.auth.decorators import login_required
import codecs
from usuarios.modelos.colonoModel import Colono
from usuarios.utils import  make_user, generador_password_random
from django.contrib.auth.models import Group
from django.db import connection

from areasComunes.modelos.tipoEventoModel import TipoEvento
from areasComunes.modelos.areaComunModel import AreaComun
from areasComunes.modelos.reservacionModel import Reservacion
from areasComunes.modelos.diasServicioModel import DiasServicio
from asociaciones.modelos.puestoModel import Puesto
from asociaciones.modelos.asociacionModel import Asociacion
from domicilios.modelos.calleModel import Calle
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from domicilios.modelos.domicilioModel import Domicilio
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio
from anuncios.modelos.imagenAnunciosModel import ImagenAnuncios
from anuncios.modelos.imagenEgresosModel import ImagenEgresos
from anuncios.modelos.imagenIngresosModel import ImagenIngresos
from anuncios.modelos.reglamentoModel import Reglamento
from anuncios.modelos.reunionModel import Reunion
from egresos.modelos.egresoModel import Egreso
from egresos.modelos.tipoEgresoModel import TipoEgreso
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from ingresos.modelos.ingresoModel import Ingreso
from mascotas.modelos.mascotaModel import Mascota
from mascotas.modelos.colonoMascotaModel import ColonoMascota
from mascotas.modelos.razaModel import Raza
from mascotas.modelos.animalModel import Animal
from sancion.modelos.tipoSancionModel import TipoSancion
from sancion.modelos.sancionModel import Sancion
from sancion.modelos.reporteVecinoModel import ReporteVecino
from vehiculos.modelos.colonoVehiculoModel import ColonoVehiculo
from vehiculos.modelos.vehiculoModel import Vehiculo
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion
from usuarios.modelos.colonoModel import Colono

from datetime import date,datetime
from usuarios.utils import generador_user,generador_password_random



class RespaldoView():

    @login_required()
    def subir_archivo(context):
        if (context.method == 'POST'):
            form = FormRespaldo(context.POST, context.FILES)

            if form.is_valid():
                respaldo = form.save(commit=False)
                respaldo.fecha = date.today()
                respaldo.save()
                return redirect('subir_archivo')
        else:
            form = FormRespaldo()
        respaldos = Respaldo.objects.all()
        return render(context, 'respaldo_subir.html', {'formulario': form, 'accion': 'Nuevo', 'respaldos':respaldos})

    def prueba(context):

        new_colono=Colono();
        new_colono.last_name=""
        new_colono.amaterno=""
        new_colono.first_name=""
        new_colono.telefono_celular=""
        new_colono.email=""
        new_colono.password=""
        new_colono.username=""

    def imprimir_respaldo(context):
        listaFinal = []
        lista = TipoEvento.objects.filter(estatus=1)
        listaFinal.append('TipoEvento')
        listaFinal = []
        lista = TipoEvento.objects.filter(estatus=1)
        listaFinal.append('TipoEvento')
        listaFinal.append('_' * 100)

        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = AreaComun.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('AreaComun')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Reservacion.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Reservacion')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = DiasServicio.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('DiasServicio')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Puesto.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Puesto')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Asociacion.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Asociacion')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        listaFinal.append('_' * 100)
        lista = Calle.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('_' * 100)
        listaFinal.append('Calle')
        listaFinal.append('_' * 100)

        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = TipoDomicilio.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('TipoDomicilio')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Domicilio.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Domicilio')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = ColonoDomicilio.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('ColonoDomicilio')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        """
        lista = ImagenAnuncios.objects.filter(estatus=1)
        listaFinal.append('ImagenAnuncios')
        listaFinal.append('_'*100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = ImagenEgresos.objects.filter(estatus=1)
        listaFinal.append('ImagenEgresos')
        listaFinal.append('_'*100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = ImagenIngresos.objects.filter(estatus=1)
        listaFinal.append('ImagenIngresos')
        listaFinal.append('_'*100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')


        lista = Reglamento.objects.filter(estatus=1)
        listaFinal.append('Reglamento')
        listaFinal.append('_'*100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Reunion.objects.filter(estatus=1)
        listaFinal.append(eunion' +)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = TipoEgreso.objects.filter(estatus=1)
        listaFinal.append('TipoEgreso')
        listaFinal.append('_'*100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')
        """
        lista = Egreso.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Egreso')
        listaFinal.append('_' * 100)

        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = TipoIngreso.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('TipoIngreso')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Ingreso.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Ingreso')
        listaFinal.append('_' * 100)

        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')
        """
        lista = Mascota.objects.filter(estatus=1)
        listaFinal.append(ascota' +)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = ColonoMascota.objects.filter(estatus=1)
        listaFinal.append('ColonoMascota')
        listaFinal.append('_'*100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Raza.objects.filter(estatus=1)
        cadena = cad 'Raza'ena +
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Animal.objects.filter(estatus=1)
        cadena = caden 'Animal'a +
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')
        """

        lista = TipoSancion.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('TipoSancion')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Sancion.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Sancion')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = ReporteVecino.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('ReporteVecino')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Vehiculo.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Vehiculo')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = ColonoVehiculo.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('ColonoVehiculo')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = TipoPersonaAjena.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('TipoPersonaAjena')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = PersonaAjena.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('PersonaAjena')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = VehiculoVisitante.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('VehiculoVisitante')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = VisitaAutorizada.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('VisitaAutorizada')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = MotivosRestriccion.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('MotivosRestriccion')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')

        lista = Colono.objects.filter(estatus=1)
        listaFinal.append('_' * 100)
        listaFinal.append('Colono')
        listaFinal.append('_' * 100)
        for elemento in lista:
            listaFinal.append(elemento.imprimir() + ' \n ')
        return render(context, 'imprimir.html',
                      {'cadena': listaFinal})

    def crear_calle (context, cadena):
        arr = cadena.split(",")
        new_objeto = Calle()
        new_objeto.nombre = str(arr[0]).upper() if (arr[0] != '') else None
        new_objeto.usuario_creacion = context.user
        new_objeto.fecha_creacion = date.today()
        new_objeto.save()

    def cargar_respaldo(context,id):
        objetoRespaldo = Respaldo.objects.get(pk=id)
        ref = objetoRespaldo.archivo
        print(ref)
        #direccion = "static/media/"+str(ref)
        direccion = "/media/"+str(ref)
        print(objetoRespaldo.modelo)
        if (objetoRespaldo.modelo == 'AreaComun'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = AreaComun()
                    new_objeto.area_comun = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.ubicacion = str(arr[1]).upper() if (arr[1] != '') else None
                    new_objeto.hora_inicio_servicio = str(arr[2]).upper() if (arr[2] != '') else None
                    new_objeto.hora_fin_servicio = str(arr[3]).upper() if (arr[3] != '') else None
                    try:
                        dia = DiasServicio.objects.get(descripcion=str(arr[4]))
                    except:
                        new_dia = DiasServicio()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.descripcion = str(arr[4])
                        new_dia.save()
                        dia = DiasServicio.objects.get(descripcion=str(arr[4]))
                    finally:
                        new_objeto.dias_servicios = dia
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Colono'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Colono()
                    new_objeto.first_name = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.last_name = str(arr[1]).upper() if (arr[1] != '') else None
                    new_objeto.amaterno = str(arr[2]).upper() if (arr[2] != '') else None
                    new_objeto.genero = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.telefono_celular = arr[4] if (arr[4] != '') else None
                    new_objeto.telefono_fijo = arr[5] if (arr[5] != '') else None
                    new_objeto.acepta_visitas = arr[6] if (arr[6] != '') else None
                    new_objeto.email = arr[7] if (arr[7] != '') else 'noaplica@noaplica.com'
                    password = generador_password_random()
                    new_objeto.password = password[0]
                    nameuser = generador_user(new_objeto)
                    new_objeto.username = nameuser
                    new_objeto.confirmado = 0
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.usuario_creacion = context.user
                    new_objeto.save()
                    colono = Colono.objects.get(username = nameuser )
                    calle = Calle.objects.filter(nombre = str(arr[8]).upper())
                    if(len(calle) == 0):
                        RespaldoView.crear_calle(context, str(arr[8]))
                    calle = Calle.objects.get(nombre=str(arr[8]).upper())
                    list_tipo_domicilio = TipoDomicilio.objects.filter(descripcion=str(arr[10]).upper())
                    if(len(list_tipo_domicilio) == 0):
                        new_dia = TipoDomicilio()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.descripcion = str(arr[10]).upper()
                        new_dia.habitable = 1
                        new_dia.save()
                    new_tipo_domicilio = TipoDomicilio.objects.get(descripcion=arr[10])
                    domicilios = Domicilio.objects.filter(calle= calle.id,
                                             num_domicilio= arr[9])

                    if(len(domicilios)==0):
                        new_domicilio = Domicilio()
                        new_domicilio.tipo_domicilio = new_tipo_domicilio
                        new_domicilio.calle = calle
                        new_domicilio.num_domicilio = str(arr[9]).upper()


                        new_domicilio.fecha_creacion = date.today()
                        new_domicilio.usuario_creacion = context.user
                        new_domicilio.save()
                    domicilios = Domicilio.objects.filter(
                        calle = calle.id,
                        num_domicilio = str(arr[9]).upper()
                    )
                    primer_domicilio = domicilios[0]
                    if (int(arr[11]) == 1):
                        primer_domicilio.titular = colono
                        primer_domicilio.save()

                    primer_domicilio.tipo_domicilio = new_tipo_domicilio
                    primer_domicilio.save()
                    new_colono_domicilio = ColonoDomicilio()
                    new_colono_domicilio.colono = colono
                    new_colono_domicilio.domicilio = domicilios[0]
                    new_colono_domicilio.fecha_creacion = date.today()
                    new_colono_domicilio.usuario_creacion = context.user
                    new_colono_domicilio.save()


        if (objetoRespaldo.modelo == 'DiasServicio'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = DiasServicio()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Reservacion'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Reservacion()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    new_objeto.area_comun = AreaComun.objects.get(id=arr[1])
                    try:
                        dia = TipoEvento.objects.get(descripcion=str(arr[2]).upper())
                    except:
                        new_dia = TipoEvento()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.descripcion = str(arr[2])
                        new_dia.save()
                        dia = TipoEvento.objects.get(descripcion=str(arr[2]).upper())
                    finally:
                        new_objeto.tipo_evento = dia
                    fecha = datetime.strptime(str(arr[3]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[3] != '') else None
                    new_objeto.hora_inicial = arr[4] if (arr[4] != '') else None
                    new_objeto.hora_final = arr[5] if (arr[5] != '') else None
                    new_objeto.observaciones = str(arr[6]).upper() if (arr[6] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'TipoEvento'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = TipoEvento()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Asociacion'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Asociacion()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    try:
                        dia = Puesto.objects.get(descripcion=str(arr[1]).upper())
                    except:
                        new_dia = Puesto()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.descripcion = str(arr[1]).upper()
                        new_dia.save()
                        dia = Puesto.objects.get(descripcion=str(arr[1]).upper())
                    finally:
                        new_objeto.puesto = dia
                    fecha = datetime.strptime(str(arr[2]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha_inicio = fecha if (arr[2] != '') else None
                    fecha = datetime.strptime(str(arr[3]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha_fin = fecha if (arr[3] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Puesto'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Puesto()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Calle'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    RespaldoView.crear_calle(context, line.upper())
        if (objetoRespaldo.modelo == 'ColonoDomicilio'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = ColonoDomicilio()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    new_objeto.domicilio = Domicilio.objects.get(id=arr[1])
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Domicilio'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Domicilio()

                    try:
                        dia = Calle.objects.get(nombre=str(arr[0]).upper())
                    except:
                        new_dia = Calle()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.nombre = str(arr[0])
                        new_dia.save()
                        dia = Calle.objects.get(nombre=str(arr[0]).upper())
                    finally:
                        new_objeto.calle = dia
                    new_objeto.num_domicilio = str(arr[1]).upper() if (arr[1] != '') else None
                    tipoDom = TipoDomicilio.objects.get(descripcion=str(arr[2]).strip().upper())
                    new_objeto.tipo_domicilio = tipoDom
                    # new_objeto.titular = Colono.objects.get(id=arr[3])
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'TipoDomicilio'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = TipoDomicilio()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.habitable = str(arr[1]).upper() if (arr[1] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Egreso'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Egreso()
                    new_objeto.tipo_ingreso = TipoIngreso.objects.get(id=arr[0])
                    new_objeto.tipo_egreso = TipoEgreso.objects.get(id=arr[1])
                    new_objeto.monto = arr[2] if (arr[2] != '') else None
                    fecha = datetime.strptime(str(arr[3]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[3] != '') else None
                    new_objeto.referencia = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.descripcion = str(arr[5]).upper() if (arr[5] != '') else None
                    new_objeto.ejercicio = str(arr[6]).upper() if (arr[6] != '') else None
                    new_objeto.folio_recibo = str(arr[7]).upper() if (arr[7] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'TipoEgreso'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = TipoEgreso()
                    new_objeto.concepto = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.estatus = arr[1] if (arr[1] != '') else None
                    new_objeto.monto = arr[2] if (arr[2] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Ingreso'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Ingreso()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    new_objeto.tipo_ingreso = TipoIngreso.objects.get(id=arr[1])
                    fecha = datetime.strptime(str(arr[2]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[2] != '') else None
                    new_objeto.monto = arr[3] if (arr[3] != '') else None
                    new_objeto.referencia = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.estatus = arr[5] if (arr[5] != '') else None
                    new_objeto.abono = arr[6] if (arr[6] != '') else None
                    new_objeto.descripcion = str(arr[7]).upper() if (arr[7] != '') else None
                    new_objeto.ejercicio = str(arr[8]).upper() if (arr[8] != '') else None
                    new_objeto.folio_recibo = str(arr[9]).upper() if (arr[9] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'TipoIngreso'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = TipoIngreso()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.estatus = arr[1] if (arr[1] != '') else None
                    new_objeto.monto = arr[2] if (arr[2] != '') else None
                    new_objeto.tipo = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.fecha_creacion = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.fecha_edicion = str(arr[5]).upper() if (arr[5] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Sancion'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Sancion()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    new_objeto.tipo_sancion = TipoSancion.objects.get(id=arr[1])
                    fecha = datetime.strptime(str(arr[2]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[2] != '') else None
                    new_objeto.descripcion = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.monto = arr[4] if (arr[4] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'TipoSancion'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = TipoSancion()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.monto = arr[1] if (arr[1] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'Vehiculo'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = Vehiculo()
                    new_objeto.numero_placas = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.marca = str(arr[1]).upper() if (arr[1] != '') else None
                    new_objeto.modelo = str(arr[2]).upper() if (arr[2] != '') else None
                    new_objeto.color = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.tipo = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.linea = str(arr[5]).upper() if (arr[5] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'ColonoVehiculo'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = ColonoVehiculo()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    new_objeto.vehiculo = Vehiculo.objects.get(id=arr[1])
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'MotivosRestriccion'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = MotivosRestriccion()
                    new_objeto.motivo = str(arr[0]).upper() if (arr[0] != '') else None
                    fecha = datetime.strptime(str(arr[1]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[1] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'PersonaAjena'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = PersonaAjena()
                    try:
                        dia = TipoPersonaAjena.objects.get(descripcion=str(arr[0]))
                    except:
                        new_dia = TipoPersonaAjena()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.descripcion = str(arr[0]).upper()
                        new_dia.save()
                        dia = TipoPersonaAjena.objects.get(descripcion=str(arr[0]).upper())
                    finally:
                        new_objeto.tipo = dia
                    new_objeto.apaterno = str(arr[1]).upper() if (arr[1] != '') else None
                    new_objeto.amaterno = str(arr[2]).upper() if (arr[2] != '') else None
                    new_objeto.nombre = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.telefono = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.empresa = str(arr[5]).upper() if (arr[5] != '') else None
                    try:
                        dia = MotivosRestriccion.objects.get(motivo=str(arr[6]).upper())
                    except:
                        new_dia = MotivosRestriccion()
                        new_dia.usuario_creacion = context.user
                        new_dia.fecha_creacion = date.today()
                        new_dia.motivo = str(arr[6]).upper()
                        new_dia.save()
                        dia = MotivosRestriccion.objects.get(motivo=str(arr[6]).upper())
                    finally:
                        new_objeto.restriccion_acceso = dia
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'TipoPersonaAjena'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = TipoPersonaAjena()
                    new_objeto.descripcion = str(arr[0]).upper() if (arr[0] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'VehiculoVisitante'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = VehiculoVisitante()
                    new_objeto.visita_autorizada = VisitaAutorizada.objects.get(id=arr[0])
                    fecha = datetime.strptime(str(arr[1]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[1] != '') else None
                    new_objeto.placas = str(arr[2]).upper() if (arr[2] != '') else None
                    new_objeto.color = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.marca = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.linea = str(arr[5]).upper() if (arr[5] != '') else None
                    new_objeto.tipo = str(arr[6]).upper() if (arr[6] != '') else None
                    new_objeto.foto = str(arr[7]).upper() if (arr[7] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()
        if (objetoRespaldo.modelo == 'VisitaAutorizada'):
            with open(direccion) as f:
                next(f)
                next(f)
                for line in f:
                    arr = line.split(",")
                    new_objeto = VisitaAutorizada()
                    new_objeto.colono = Colono.objects.get(id=arr[0])
                    new_objeto.persona_ajena = PersonaAjena.objects.get(id=arr[1])
                    fecha = datetime.strptime(str(arr[2]), '%m/%d/%y %H:%M:%S')
                    new_objeto.fecha = fecha if (arr[2] != '') else None
                    new_objeto.hora_entrada = str(arr[3]).upper() if (arr[3] != '') else None
                    new_objeto.hora_salida = str(arr[4]).upper() if (arr[4] != '') else None
                    new_objeto.identificacion = str(arr[5]).upper() if (arr[5] != '') else None
                    new_objeto.numero_personas = str(arr[6]).upper() if (arr[6] != '') else None
                    new_objeto.foto = str(arr[7]).upper() if (arr[7] != '') else None
                    new_objeto.tipo_acceso = str(arr[8]).upper() if (arr[8] != '') else None
                    new_objeto.fecha_estimada = str(arr[9]).upper() if (arr[9] != '') else None
                    new_objeto.hora_estimada_min = arr[10] if (arr[10] != '') else None
                    new_objeto.hora_estimada_max = arr[11] if (arr[11] != '') else None
                    new_objeto.usuario_creacion = context.user
                    new_objeto.fecha_creacion = date.today()
                    new_objeto.save()

        """
                # do stuff
            for linea in archivo.readlines():
                linea = archivo.readLine()
                linea = archivo.readLine()
        
        for linea in archivo.readlines():
            if(bandera):
                arr = linea.split(",")
                if(n=='colono'):
                    new_colono=Colono()
                    new_colono.last_name = str(arr[1]).upper() if (arr[1]!='') else None
                    new_colono.amaterno = str(arr[2]).upper() if (arr[2]!='') else None
                    new_colono.first_name = str(arr[3]).upper() if (arr[3]!='') else None
                    new_colono.telefono_celular = str(arr[4]).upper() if (arr[4]!='') else None
                    new_colono.email = str(arr[5]).upper() if (arr[5]!='') else None
                    new_colono.genero='-'
                    passs=generador_password_random()
                    new_colono.set_password(generador_password_random()[1])
                    new_colono.username = make_user(arr[3],arr[1],arr[2])
                    new_colono.acepta_visitas = 1
                    new_colono.save()
                    print(
                        str(new_colono.id)+" "+\
                        new_colono.last_name + " "+\

                        new_colono.first_name + " "+\


                        new_colono.password + " " + \
                        new_colono.username + " "
                    )

                    new_relacion=ColonoDomicilio()
                    new_relacion.colono=new_colono

                    new_relacion.domicilio=Domicilio.objects.get(id=arr[0])
                    print(
                        str(new_relacion.domicilio)+" "+\
                        str(new_relacion.colono)
                    )
                    new_relacion.save()
                    if(new_relacion.domicilio.titular==None):
                        new_relacion.domicilio.titular=new_colono
                        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
                    Group.objects.get(name="Colonos").user_set.add(new_colono)

                elif(n=='calle'):
                    nombre=str(arr[1]).upper() if (arr[1]=='') else None
                    new_calle = Calle()
                    new_calle.nombre = nombre
                    new_calle.save()
                    print(new_calle.nombre)

                else:
                    new_domicilio=Domicilio()

                    new_domicilio.calle=Calle.objects.get(id=arr[1])
                    new_domicilio.num_domicilio=str(arr[2]).upper() if (arr[2]=='') else None
                    new_domicilio.tipo_domicilio=TipoDomicilio.objects.get(id=arr[3])
                    print(
                        str(new_domicilio.calle)+" "+
                        str(new_domicilio.num_domicilio)+" "+
                        str(new_domicilio.tipo_domicilio)
                    )
                    new_domicilio.save()
            else:
                bandera=True
                if(linea.find('colono')>-1):
                    n="colono"
                elif(linea.find('domicilio')>-1):
                    n = "domicilio"
                else:
                    n = "calle"
        """




        #with connection.cursor() as cursor:
        #    for sentencia in lista:
        #        cursor.execute(sentencia)
        #    row = cursor.fetchone()
        return redirect('subir_archivo')


"""
	@login_required()
    @permission_required('areasComunes.view_tipoevento')
    def lista_tipoeventos(context):
        tipoeventos = TipoEvento.objects.all()
        return render(context, 'tipoeventos.html', {'tipoeventos': tipoeventos})

    @login_required()
    @permission_required('areasComunes.add_tipoevento', login_url='tipoevento')
    def nuevo_tipoevento(context):
        if (context.method == 'POST'):
            form = FormTipoEvento(context.POST)
            if form.is_valid():
                tipoevento = form.save()
                tipoevento.save()
                return redirect('nuevo_tipoevento')
        else:
            form = FormTipoEvento()
        tipoeventos = TipoEvento.objects.all()
        return render(context, 'nuevo_tipoevento.html', {'formulario': form, 'accion': 'Nuevo', 'tipoeventos': tipoeventos})
"""
