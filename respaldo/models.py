from django.db import models

# Create your models here.
class Respaldo(models.Model):
	fecha = models.DateTimeField('Fecha', blank=True, null=True) 
	archivo = models.FileField(upload_to='respaldos')
	modelo = models.CharField('Modelo', max_length=150)


	class Meta:
		app_label = "respaldo"