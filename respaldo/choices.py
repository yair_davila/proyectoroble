MODELO_CHOICES = (
	("TipoEvento","TipoEvento"),
	("AreaComun","AreaComun"),
	("Reservacion","Reservacion"),
	("DiasServicio","DiasServicio"),
	("Puesto","Puesto"),
	("Asociacion","Asociacion"),
	("Calle","Calle"),
	("TipoDomicilio","TipoDomicilio"),
	("Domicilio","Domicilio"),
	("ColonoDomicilio","ColonoDomicilio"),
	("ImagenAnuncios","ImagenAnuncios"),
	("ImagenEgresos","ImagenEgresos"),
	("ImagenIngresos","ImagenIngresos"),
	("Reglamento","Reglamento"),
	("Reunion","Reunion"),
	("TipoEgreso","TipoEgreso"),
	("Egreso","Egreso"),
	("TipoIngreso","TipoIngreso"),
	("Ingreso","Ingreso"),
	("Mascota","Mascota"),
	("ColonoMascota","ColonoMascota"),
	("Raza","Raza"),
	("Animal","Animal"),
	("TipoSancion","TipoSancion"),
	("Sancion","Sancion"),
	("ReporteVecino","ReporteVecino"),
	("Vehiculo","Vehiculo"),
	("ColonoVehiculo","ColonoVehiculo"),
	("TipoPersonaAjena","TipoPersonaAjena"),
	("PersonaAjena","PersonaAjena"),
	("VehiculoVisitante","VehiculoVisitante"),
	("VisitaAutorizada","VisitaAutorizada"),
	("MotivosRestriccion","MotivosRestriccion"),
	("Colono","Colono")
)