from django.urls import path
from rest_framework import routers
from rest.viewsets import CalleViewSet
from domicilios.models import Calle

router = routers.SimpleRouter()
router.register('calle',CalleViewSet)

urlpatterns = router.urls