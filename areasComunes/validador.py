from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _

def validador_string(value):

    es_int = True
    try:
        a = int(value)
        es_int = True
    except Exception as e:
        es_int = False
    if len(value)<2:
        raise ValidationError(
            _('Error {0} debe ser mayor a 3 caracteres'.format(value))
        )


    if es_int:
        raise ValidationError(
            _('Error {0} debe ser una cadena de texto'.format(value))
        )

def validador_cadenas(value):
    es_int = False
    try:
        a = int(value)
        es_int = True
    except Exception as e:
        es_int = False

    if es_int:
        raise ValidationError(
            _('Error {0} debe ser un numero sin letras'.format(value))
        )


def validador_telefono(value):
    excepcion = False
    try:
        valor = int(value)
        if int(value) > 9999999999:
            excepcion = True
        elif valor < 1000000000:
            excepcion = True

    except Exception as e:
        excepcion = True

    if excepcion:
        raise ValidationError(
            _('Error {0} el formato es incorrecto'.format(value))
        )
