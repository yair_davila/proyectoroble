from django.contrib import admin

from areasComunes.modelos.tipoEventoModel import TipoEvento
from areasComunes.modelos.areaComunModel import AreaComun
from areasComunes.modelos.reservacionModel import Reservacion
from areasComunes.modelos.diasServicioModel import DiasServicio

admin.site.register(TipoEvento)
admin.site.register(AreaComun)
admin.site.register(Reservacion)
admin.site.register(DiasServicio)

from asociaciones.modelos.puestoModel import Puesto
from asociaciones.modelos.asociacionModel import Asociacion

admin.site.register(Puesto)
admin.site.register(Asociacion)


from domicilios.modelos.calleModel import Calle
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from domicilios.modelos.domicilioModel import Domicilio
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio

admin.site.register(Calle)
admin.site.register(TipoDomicilio)
admin.site.register(Domicilio)
admin.site.register(ColonoDomicilio)

from anuncios.modelos.imagenAnunciosModel import ImagenAnuncios
from anuncios.modelos.imagenEgresosModel import ImagenEgresos
from anuncios.modelos.imagenIngresosModel import ImagenIngresos
from anuncios.modelos.reglamentoModel import Reglamento
from anuncios.modelos.reunionModel import Reunion

admin.site.register(ImagenAnuncios)
admin.site.register(ImagenEgresos)
admin.site.register(ImagenIngresos)
admin.site.register(Reglamento)
admin.site.register(Reunion)



from egresos.modelos.egresoModel import Egreso
from egresos.modelos.tipoEgresoModel import TipoEgreso

admin.site.register(TipoEgreso)
admin.site.register(Egreso)


from ingresos.modelos.tipoIngresoModel import TipoIngreso
from ingresos.modelos.ingresoModel import Ingreso

admin.site.register(TipoIngreso)
admin.site.register(Ingreso)

from mascotas.modelos.mascotaModel import Mascota
from mascotas.modelos.colonoMascotaModel import ColonoMascota
from mascotas.modelos.razaModel import Raza
from mascotas.modelos.animalModel import Animal

admin.site.register(Mascota)
admin.site.register(ColonoMascota)
admin.site.register(Raza)
admin.site.register(Animal)

from sancion.modelos.tipoSancionModel import TipoSancion
from sancion.modelos.sancionModel import Sancion
from sancion.modelos.reporteVecinoModel import ReporteVecino

admin.site.register(TipoSancion)
admin.site.register(Sancion)
admin.site.register(ReporteVecino)

from vehiculos.modelos.colonoVehiculoModel import ColonoVehiculo
from vehiculos.modelos.vehiculoModel import Vehiculo

admin.site.register(Vehiculo)
admin.site.register(ColonoVehiculo)

from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion

admin.site.register(TipoPersonaAjena)
admin.site.register(PersonaAjena)
admin.site.register(VehiculoVisitante)
admin.site.register(VisitaAutorizada)
admin.site.register(MotivosRestriccion)

from usuarios.modelos.colonoModel import Colono

admin.site.register(Colono)


from vigilante.modelos.vigilanteModel import Vigilante
from vigilante.modelos.turnoModel import Turno

admin.site.register(Turno)
admin.site.register(Vigilante)
