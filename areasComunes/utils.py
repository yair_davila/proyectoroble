from areasComunes.modelos.reservacionModels import Reservacion
from usuarios.models import Colono

def get_eliminar():
    a = "<div class='modal fade' id='myModal2' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"
    a = a + "<div class='modal-dialog'>"
    a = a + "<div class='modal-content'>"
    a = a + "<div class='modal-header'>"
    a = a + "<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>"
    a = a + "<h4 class='modal-title'> Confirmaci&oacute;n </h4>"
    a = a + "</div>"
    a = a + "<div class='modal-body'>"
    a = a + "Esta seguro de eliminar el elemento"
    a = a + "</div>"
    a = a + "<div class='modal-footer'>"
    a = a + "<button"
    a = a + "data-dismiss='modal'"
    a = a + "class='btn btn-secondary'"
    a = a + "type='button'>"
    a = a + "No"
    a = a + "</button>"
    a = a + "<!--<a href='{% url 'eliminar_tipoevento' tipoevento.id %}'>-->"
    a = a + "<button"
    a = a + "id='btn_eliminar_tipoevento'"
    a = a + "value='{{tipoevento.id}}'"
    a = a + "data-dismiss='modal'"
    a = a + "class='btn btn-warning'"
    a = a + "type='button'"
    a = a + ">"
    a = a + "Si"
    a = a + "</button>"
    a = a + "<!--</a>-->"
    a = a + "</div>"
    a = a + "</div>"
    a = a + "</div>"
    a = a + "</div>"
    return a;


def tiene_permiso(user, permiso):
    if user.is_staff:
        return True

    if user.user_permissions.filter(name=permiso):
        return True

    grupos = user.groups.all()
    if len(grupos) > 0:
        if(grupos[0].permissions.filter(name=permiso)):
            return True

    return False


def es_colono(user):
    if user.groups.filter(name='Colonos'):
        return True
    return False


def get_reservaciones_colono(user):
    ids = []
    colono = Colono.objects.filter(usuario=user)[0]
    return Reservacion.objects.filter(colono=colono)
