from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from areasComunes.modelos.areaComunModel import AreaComun
from areasComunes.formularios.formAreaComun import FormAreaComun

class AreaComunView():

    @login_required()
    @permission_required('areasComunes.add_areacomun', login_url='areacomun')
    def nuevo_areacomun(context):
        form = FormAreaComun()
        areacomuns = AreaComun.objects.filter(estatus=1)
        return render(context, 'nuevo_areacomun.html',
                      {'formulario': form, 'accion': 'Nuevo', 'areacomuns': areacomuns})

    @login_required()
    @permission_required('areasComunes.change_areacomun', login_url='areacomun')
    def obteneruno_js_areacomun(context):
        id = context.POST.get('id',None)
        data = AreaComunView.obtener_json_areacomuns_carga(AreaComun.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('areasComunes.delete_areacomun', login_url='areacomun')
    def borrar_js_areacomun(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            areacomun = AreaComun.objects.get(pk=o)
            areacomun.estatus = 0
            areacomun.save()
        data = AreaComunView.obtener_json_areacomuns(AreaComun.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_areacomuns(areacomuns):
        
        areacomunsJson = []
        for areacomun in areacomuns:
            areacomunsJson.append(
                {
            'id' : areacomun.id,
        	'area_comun': str(areacomun.area_comun),
        	'ubicacion': str(areacomun.ubicacion),
        	'hora_inicio_servicio': str(areacomun.hora_inicio_servicio),
        	'hora_fin_servicio': str(areacomun.hora_fin_servicio),
        	'dias_servicios': str(areacomun.dias_servicios),

                    
                }
            )
        data = simplejson.dumps(areacomunsJson)
        return data


    def obtener_json_areacomuns_carga(areacomuns):
        
        areacomunsJson = []
        for areacomun in areacomuns:
            areacomunsJson.append(
                {
            'id' : str(areacomun.id),
          'area_comun': str(areacomun.area_comun),
          'ubicacion': str(areacomun.ubicacion),
          'hora_inicio_servicio': str(areacomun.hora_inicio_servicio),
          'hora_fin_servicio': str(areacomun.hora_fin_servicio),
          'dias_servicios': str(areacomun.dias_servicios.id),

                    
                }
            )
        data = simplejson.dumps(areacomunsJson)
        return data

    @login_required()
    @permission_required('areasComunes.add_areacomun', login_url='areacomun')
    def registrar_js_areacomun(context):
        if (context.method == 'POST'):
            form = FormAreaComun(context.POST)

            if(form.is_valid()):
                areacomun = form.save(commit=False)
                areacomun.usuario_creacion = context.user
                areacomun.fecha_creacion = date.today()
                areacomun.save()
                data = AreaComunView.obtener_json_areacomuns(AreaComun.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('areasComunes.add_areacomun', login_url='areacomun')
    def actualizar_js_areacomun(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            areacomun = AreaComun.objects.get(pk=id)
            form = FormAreaComun(context.POST, instance=areacomun)
            if(form.is_valid()):
                areacomun=form.save()
                areacomun.save()
                data = AreaComunView.obtener_json_areacomuns(AreaComun.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
    
    def obtener_areacomun_activas(context):
        data = AreaComunView.obtener_json_areacomuns(AreaComun.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
        

    
