from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from areasComunes.modelos.tipoEventoModel import TipoEvento
from areasComunes.formularios.formTipoEvento import FormTipoEvento

class TipoEventoView():

    @login_required()
    @permission_required('areasComunes.add_tipoevento', login_url='tipoevento')
    def nuevo_tipoevento(context):
        form = FormTipoEvento()
        tipoeventos = TipoEvento.objects.filter(estatus=1)
        return render(context, 'nuevo_tipoevento.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tipoeventos': tipoeventos})

    @login_required()
    @permission_required('areasComunes.change_tipoevento', login_url='tipoevento')
    def obteneruno_js_tipoevento(context):
        id = context.POST.get('id',None)
        data = TipoEventoView.obtener_json_tipoeventos_carga(TipoEvento.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('areasComunes.delete_tipoevento', login_url='tipoevento')
    def borrar_js_tipoevento(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tipoevento = TipoEvento.objects.get(pk=o)
            tipoevento.estatus = 0
            tipoevento.save()

        data = TipoEventoView.obtener_json_tipoeventos(TipoEvento.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_tipoeventos(tipoeventos):
        
        tipoeventosJson = []
        for tipoevento in tipoeventos:
            tipoeventosJson.append(
                {
            'id' : tipoevento.id,
        	'descripcion': str(tipoevento.descripcion),

                    
                }
            )
        data = simplejson.dumps(tipoeventosJson)
        return data


    def obtener_json_tipoeventos_carga(tipoeventos):
        
        tipoeventosJson = []
        for tipoevento in tipoeventos:
            tipoeventosJson.append(
                {
            'id' : tipoevento.id,
          'descripcion': tipoevento.descripcion,

                    
                }
            )
        data = simplejson.dumps(tipoeventosJson)
        return data

    @login_required()
    @permission_required('areasComunes.add_tipoevento', login_url='tipoevento')
    def registrar_js_tipoevento(context):
        if (context.method == 'POST'):
            form = FormTipoEvento(context.POST)
            if form.is_valid():
                tipoevento = form.save(commit=False)
                tipoevento.usuario_creacion = context.user
                tipoevento.fecha_creacion = date.today()
                tipoevento.save()
                data = TipoEventoView.obtener_json_tipoeventos(TipoEvento.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('areasComunes.add_tipoevento', login_url='tipoevento')
    def actualizar_js_tipoevento(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tipoevento = TipoEvento.objects.get(pk=id)

            form = FormTipoEvento(context.POST, instance=tipoevento)
            if form.is_valid():
                tipoevento = form.save()
                tipoevento.save()
                data = TipoEventoView.obtener_json_tipoeventos(TipoEvento.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
