from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from django.http import JsonResponse
from datetime import date
from areasComunes.modelos.reservacionModel import Reservacion
from areasComunes.formularios.formReservacion import FormReservacion

class ReservacionView():

    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='reservacion')
    def nuevo_reservacion(context):
        form = FormReservacion()
        reservacions = Reservacion.objects.filter(estatus=1)
        return render(context, 'nuevo_reservacion.html',
                      {'formulario': form, 'accion': 'Nuevo', 'reservacions': reservacions})

    #@permission_required('areasComunes.view_reservacion', login_url='reservacion')
    @login_required()
    def obteneruno_js_reservacion(context):
        id = context.POST.get('id',None)
        data = ReservacionView.obtener_json_reservacions_carga(Reservacion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    def obtenerdescripcion_js_reservacion(context):
        id = context.POST.get('id',None)
        data = ReservacionView.obtener_json_reservacions_descripcion(Reservacion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    def obtener_eventos(context):
        datos = Reservacion.objects.filter(estatus=1)
        reservacionsJson = []
        for reservacion in datos:
            reservacionsJson.append(
                {
                    'id': str(reservacion.id),
                    'title': str(reservacion.tipo_evento)+" en "+str(reservacion.area_comun),
                    'start': str(reservacion.fecha.strftime('%Y-%m-%d')) + " "+str(reservacion.hora_inicial),
                    'end': str(reservacion.fecha.strftime('%Y-%m-%d')) +" "+ str(reservacion.hora_final)

                }
            )
        data = simplejson.dumps(reservacionsJson)
        return JsonResponse(data, safe=False);



    @login_required()
    @permission_required('areasComunes.delete_reservacion', login_url='reservacion')
    def borrar_js_reservacion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            reservacion = Reservacion.objects.get(pk=o)
            reservacion.estatus = 0
            reservacion.save()
        data = ReservacionView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_reservacions(reservacions):
        reservacionsJson = []
        for reservacion in reservacions:
            reservacionsJson.append(
                {
            'id' : reservacion.id,
        	'colono': str(reservacion.colono),
        	'area_comun': str(reservacion.area_comun),
        	'tipo_evento': str(reservacion.tipo_evento),
        	'fecha': str(reservacion.fecha.strftime('%d de %B de %Y')),
        	'hora_inicial': str(reservacion.hora_inicial),
        	'hora_final': str(reservacion.hora_final),
        	'observaciones': str(reservacion.observaciones) if (str(reservacion.observaciones) != '') else '',
                }
            )
        data = simplejson.dumps(reservacionsJson)
        return data
    
    def obtener_json_reservacions_descripcion(reservacions):
        reservacionsJson = []
        for reservacion in reservacions:
            reservacionsJson.append(
                {
            'id' : reservacion.id,
        	'colono': str(reservacion.colono),
        	'area_comun': str(reservacion.area_comun.area_comun),
        	'tipo_evento': str(reservacion.tipo_evento.descripcion),
        	'fecha': str(reservacion.fecha.strftime('%d de %B de %Y')),
        	'hora_inicial': str(reservacion.hora_inicial),
        	'hora_final': str(reservacion.hora_final),
        	'observaciones': str(reservacion.observaciones) if (str(reservacion.observaciones) != '') else '',
                }
            )
        data = simplejson.dumps(reservacionsJson)
        return data



    def obtener_json_reservacions_carga(reservacions):
        reservacionsJson = []
        
        for reservacion in reservacions:
            reservacionsJson.append(
                {
            'id' : reservacion.id,
          'colono': str(reservacion.colono.id),
          'area_comun': str(reservacion.area_comun.id),
          'tipo_evento': str(reservacion.tipo_evento.id),
          'fecha': str(reservacion.fecha.strftime('%Y-%m-%d')),
          'hora_inicial': str(reservacion.hora_inicial),
          'hora_final': str(reservacion.hora_final),
          'observaciones': str(reservacion.observaciones) if (str(reservacion.observaciones) != '') else '',
                }
            )
        data = simplejson.dumps(reservacionsJson)
        return data

    

    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='reservacion')
    def registrar_js_reservacion(context):
        if (context.method == 'POST'):
            form = FormReservacion(context.POST)
            if(form.is_valid()):
                reservacion = form.save(commit=False)
                reservacion.usuario_creacion = context.user
                reservacion.fecha_creacion = date.today()
                reservacion.observaciones = '' if (reservacion.observaciones is None) else str(reservacion.observaciones)
                reservacion.save()
                data = ReservacionView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='reservacion')
    def actualizar_js_reservacion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            reservacion = Reservacion.objects.get(pk=id)
            form = FormReservacion(context.POST, instance=reservacion)
            if(form.is_valid()):
                reservacion=form.save()
                reservacion.observaciones = '' if (reservacion.observaciones is None) else str(reservacion.observaciones)

                reservacion.save()
                data = ReservacionView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")