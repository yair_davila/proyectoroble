from areasComunes.modelos.tipoEventoModel import TipoEvento
from areasComunes.validador import *
from django import forms
LONGITUD_MAXIMA = 'Ha excedido la longitud para este campo'

class FormTipoEvento (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas,validador_string])

    class Meta:
        model = TipoEvento
        fields = (
            'id'
            , 'descripcion'
        )

        error_messages = {
            'descripcion':
                {'max_length': LONGITUD_MAXIMA,
                 'required': 'Se requiere'}
        }

