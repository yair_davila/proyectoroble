from areasComunes.modelos.reservacionModel import Reservacion,Colono,TipoEvento,AreaComun
from areasComunes.validador import *
from areasComunes.choices import HOUR_CHOICES
from django import forms

class FormReservacion (forms.ModelForm):
    fecha = forms.DateTimeField(input_formats=['%Y-%m-%d'], widget=forms.DateTimeInput(attrs={
        'class': 'datetimepicker-input',
    }))
    hora_inicial= forms.ChoiceField(choices=HOUR_CHOICES)
    hora_final = forms.ChoiceField(choices=HOUR_CHOICES)
    colono= forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    area_comun = forms.ModelChoiceField(queryset=AreaComun.objects.filter(estatus=1))
    tipo_evento = forms.ModelChoiceField(queryset=TipoEvento.objects.filter(estatus=1))

    class Meta:
        model = Reservacion
        fields = ('colono', 'area_comun', 'tipo_evento',
                  'fecha', 'hora_inicial', 'hora_final','observaciones')

