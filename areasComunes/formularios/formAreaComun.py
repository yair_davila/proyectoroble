from areasComunes.modelos.areaComunModel import AreaComun
from areasComunes.validador import *
from django import forms
from areasComunes.choices import HOUR_CHOICES
from areasComunes.modelos.diasServicioModel import DiasServicio

class FormAreaComun (forms.ModelForm):
    area_comun = forms.CharField(validators=[validador_cadenas])
    ubicacion = forms.CharField(validators=[validador_cadenas])
    hora_inicio_servicio = forms.ChoiceField(choices=HOUR_CHOICES)
    hora_fin_servicio = forms.ChoiceField(choices=HOUR_CHOICES)
    dias_servicios = forms.ModelChoiceField(queryset=DiasServicio.objects.all().order_by('id'))


    class Meta:
        model = AreaComun
        fields = ('area_comun', 'ubicacion','hora_inicio_servicio','hora_fin_servicio','dias_servicios')

