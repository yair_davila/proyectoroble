from areasComunes.modelos.tipoEventoModel import TipoEvento
from areasComunes.modelos.areaComunModel import AreaComun
from usuarios.modelos.colonoModel import Colono
from django.db import models
from django.contrib.auth.models import User

class Reservacion(models.Model):
    colono = models.ForeignKey(Colono, models.DO_NOTHING, 'ID_COLONO_RESERVACION')
    area_comun = models.ForeignKey(AreaComun, models.DO_NOTHING, 'ID_AREA_COMUN')
    tipo_evento = models.ForeignKey(TipoEvento, models.DO_NOTHING, 'ID_TIPO_EVENTO')
    fecha = models.DateTimeField('Fecha')  # Field name made lowercase.
    hora_inicial = models.TimeField('Hora_Inicial', blank=True, null=True)
    hora_final = models.TimeField('Hora_Final', blank=True, null=True)
    observaciones = models.CharField(
        'Observaciones', max_length=500, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Reservacion')
    fecha_edicion = models.DateTimeField('Edicion_Reservacion', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Reservacion')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Reservacion', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.area_comun.__str__()+" "+self.hora_inicial.__str__()

    def imprimir(self):
        return str(self.colono.id)+','+\
            str(self.area_comun.id)+','+str(self.tipo_evento)+','+\
            str(self.fecha)+','+str(self.hora_inicial)+','+\
            str(self.hora_final)+','+str(self.observaciones)


    class Meta:
    	app_label = "areasComunes"