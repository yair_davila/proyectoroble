from django.db import models
from areasComunes.modelos.diasServicioModel import DiasServicio
from django.contrib.auth.models import User

class AreaComun(models.Model):
    area_comun = models.CharField('Area_Común', max_length=100, blank=True, null=True)
    ubicacion = models.CharField('Ubicación', max_length=100, blank=True, null=True)
    hora_inicio_servicio = models.TimeField('Hora_Inicio_Servicio', blank=True, null=True)
    hora_fin_servicio = models.TimeField('Hora_Fin_Servicio', blank=True, null=True)
    dias_servicios = models.ForeignKey(DiasServicio, models.DO_NOTHING, 'ID_DIAS_SER')
    fecha_creacion = models.DateTimeField('Creacion_AreaComun')
    fecha_edicion = models.DateTimeField('Edicion_AreaComun', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_AreaComun')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_AreaComun', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.area_comun


    def imprimir(self):
        return str(self.area_comun)+','+\
            str(self.ubicacion)+','+str(self.hora_inicio_servicio)+','+\
            str(self.hora_fin_servicio)+','+str(self.dias_servicios)


    class Meta:
    	app_label = "areasComunes"