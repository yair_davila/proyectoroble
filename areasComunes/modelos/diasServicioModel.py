from django.db import models
from django.contrib.auth.models import User

class DiasServicio(models.Model):
    descripcion = models.CharField('Descripción', max_length=150, unique=True)
    fecha_creacion = models.DateTimeField('Creacion_DiasServicio')
    fecha_edicion = models.DateTimeField('Edicion_DiasServicio', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_DiasServicio')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_DiasServicio', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)


    def __str__(self):
        return self.descripcion


    def imprimir(self):
        return str(self.descripcion)

    class Meta:
        app_label = "areasComunes"