from areasComunes.modelos.areaComunModel import AreaComun
from django.db import models
from django.contrib.auth.models import User

class TipoEvento(models.Model):
    descripcion = models.CharField('Descripción', max_length=35, unique=True)
    fecha_creacion = models.DateTimeField('Creacion_TipoEvento')
    fecha_edicion = models.DateTimeField('Edicion_TipoEvento', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_TipoEvento')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_TipoEvento', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.descripcion


    def imprimir(self):
        return str(self.descripcion)

    class Meta:
        app_label = "areasComunes"