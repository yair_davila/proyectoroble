from django.urls import path
from areasComunes.vistas.areaComunView import AreaComunView
from areasComunes.vistas.reservacionView import ReservacionView

from areasComunes.vistas.tipoEventoView import TipoEventoView


urlpatterns = [

    path('areacomun_nuevo', AreaComunView.nuevo_areacomun ,
         name='nuevo_areacomun'),
    path('areacomun_registrar', AreaComunView.registrar_js_areacomun ,
         name='registrar_areacomun'),
    path('areacomun_actualizar', AreaComunView.actualizar_js_areacomun ,
         name='actualizar_areacomun'),
    path('areacomun_obteneruno', AreaComunView.obteneruno_js_areacomun ,
         name='obteneruno_areacomun'),
    path('areacomun_borrar/',
         AreaComunView.borrar_js_areacomun , name='borrar_areacomun'),
    path('areacomun_obteneractivas',AreaComunView.obtener_areacomun_activas,
        name='obteneractivas_areacomun'),


    path('reservacion_nuevo', ReservacionView.nuevo_reservacion ,
         name='nuevo_reservacion'),
    path('reservacion_registrar', ReservacionView.registrar_js_reservacion ,
         name='registrar_reservacion'),
    path('reservacion_actualizar', ReservacionView.actualizar_js_reservacion ,
         name='actualizar_reservacion'),
    path('reservacion_obteneruno', ReservacionView.obteneruno_js_reservacion ,
         name='obteneruno_reservacion'),
     path('reservacion_obtenerdescripcion', ReservacionView.obtenerdescripcion_js_reservacion ,
         name='obtenerdescripcion_reservacion'),
    path('reservacion_borrar/',
         ReservacionView.borrar_js_reservacion , name='borrar_reservacion '),
    path('obtener_eventos/',
         ReservacionView.obtener_eventos , name='obtener_eventos'),





    path('tipoevento_nuevo', TipoEventoView.nuevo_tipoevento ,
         name='nuevo_tipoevento'),
    path('tipoevento_registrar', TipoEventoView.registrar_js_tipoevento ,
         name='registrar_tipoevento'),
    path('tipoevento_actualizar', TipoEventoView.actualizar_js_tipoevento ,
         name='actualizar_tipoevento'),
    path('tipoevento_obteneruno', TipoEventoView.obteneruno_js_tipoevento ,
         name='obteneruno_tipoevento'),
    path('tipoevento_borrar/',
         TipoEventoView.borrar_js_tipoevento , name='borrar_tipoevento '),

]