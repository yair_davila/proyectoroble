from django.db import models
from usuarios.validador import validador_telefono
from django.contrib.auth.models import User
from vigilante.modelos.turnoModel import Turno

class Vigilante (User):
    amaterno = models.CharField(
        'Amaterno', max_length=35, blank=True, null=True)
    genero = models.CharField('Genero', max_length=1, blank=True, null=True)
    telefono = models.CharField('Telefono', max_length=10, blank=True, null=True, validators=[
                                        validador_telefono])  # Field name made lowercase.
    confirmado = models.CharField(
        'confirmado', max_length=1, blank=True, null=True)
    turno = models.ForeignKey(Turno, models.DO_NOTHING, 'ID_TURNO_VIGILANTE')
    fecha_inicio_servicio = models.DateTimeField('Inicio_Servicio')
    fecha_fin_servicio = models.DateTimeField('Fin_Servicio', null=True)
    fecha_creacion = models.DateTimeField('Creacion_Vigilante')
    fecha_edicion = models.DateTimeField('Edicion_Vigilante', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Vigilante')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Vigilante', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.first_name+" "+self.last_name

    class Meta:
    	app_label = "vigilante"