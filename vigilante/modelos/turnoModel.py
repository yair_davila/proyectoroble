from django.db import models
from django.contrib.auth.models import User

class Turno (models.Model):
    horaInicio = models.TimeField('Hora_Inicio')
    horaFin = models.TimeField('Hora_Fin')
    tipoJornada = models.CharField('Tipo_Jornada', max_length=2)
    fecha_creacion = models.DateTimeField('Creacion_Turno')
    fecha_edicion = models.DateTimeField('Edicion_Turno', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Turno')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Turno', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return str(self.horaInicio)+" "+str(self.tipoJornada)+" hrs"

    class Meta:
    	app_label = "vigilante"
