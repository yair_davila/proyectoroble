from fpdf import FPDF, HTMLMixin
from datetime import date,datetime
from vigilante.numeroLetra import numero_a_letras        
    
    
class HTML2PDF(FPDF, HTMLMixin):
    pass
def escribir_pdf(valor):
    c = ""
    total = 0
    contador=0
    cadena=''
    descripcion = ""
    fecha = ""
    colono = ""
    monto = ""
    letras = ""
    domicilio = ""
    folio=""
    try:
        descripcion = valor.descripcion
        fecha = str(valor.fecha)
        colono = str(valor.colono)
        monto = str(valor.monto)
        letras = (numero_a_letras(valor.monto))
        domicilio = str(valor.domicilio)
        folio = str(valor.folio_recibo)
    except:
        cadena = cadena
            

    cadena = "<title>Financiero</title>" + \
        "<table width='100%' border='1'>" + \
        "<thead>"+\
        "<tr>" + \
         "<th width='33%'>Recibo</th>" + \
         "<th width='33%'>Fecha</th>" + \
         "<th width='33%'>Folio</th>" + \
         "</tr>" + \
        "</thead>"+\
        "<tbody>"+\
        "<tr><td>-</td><td>"+fecha+"</td><td>"+folio+"</td></tr>" + \
        "<tr><td>_____________________________</td><td>_____________________________</td>"+\
        "<td>_____________________________</td></tr>" +\
        "<tr><td>colono</td><td colspan='2'>"+colono+"</td></tr>" + \
        "<tr><td>cantidad</td><td colspan='2'>$ "+ monto +" pesos</td></tr>" + \
        "<tr><td>-</td><td colspan='2'>"+letras+"</td></tr>" + \
        "<tr><td>concepto</td><td colspan='2'>"+descripcion+"</td></tr>" + \
        "<tr><td>domicilio</td><td colspan='2'>"+domicilio+"</td></tr>" + \
        "<tbody>"+\
        "</table>"

    
    pdf = HTML2PDF()
    pdf.add_page()
    pdf.write_html(cadena)
    now = datetime.now()
    dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
    pdf.output('media/Ingresos/'+folio+'.pdf')
    #pdfkit.from_string(cadena, 'media/financiero-'+dt_string+'.pdf')
    
    #HTML(string=cadena).write_pdf('media/financiero-'+dt_string+'.pdf')
    return '/media/Ingresos/'+folio+'.pdf'
