from vigilante.modelos.vigilanteModel import Vigilante
from vigilante.modelos.turnoModel import Turno
from usuarios.choices import GENDER_CHOICES,VISITAS_CHOICES
from usuarios.validador import validador_cadenas_2,validador_telefono
from django import forms


class FormVigilante(forms.ModelForm):
    first_name = forms.CharField(
        label='Nombre', max_length=50, validators=[validador_cadenas_2])
    last_name = forms.CharField(
        label='A Paterno', max_length=35, validators=[validador_cadenas_2])
    amaterno = forms.CharField(label='A Materno', max_length=35, validators=[
                               validador_cadenas_2], required=False)
    genero = forms.ChoiceField(choices=GENDER_CHOICES, required=False)
    email = forms.EmailField(label='E-mail', required=False)
    telefono = forms.CharField(max_length=10, validators=[
                                       validador_telefono], required=False)
    turno = forms.ModelChoiceField(queryset=Turno.objects.filter(estatus=1),required=False)
    fecha_inicio_servicio = forms.DateField(required=True)



    class Meta:
        model = Vigilante
        fields = (
            'first_name', 'last_name', 'amaterno', 'genero',
            'telefono', 'email' , 'fecha_inicio_servicio',
            'turno'
        )