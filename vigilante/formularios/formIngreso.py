from ingresos.modelos.ingresoModel import Ingreso
from ingresos.validador import *
from django import forms
from ingresos.choices import MES_CHOICES,ESTATUS_CHOICES
from usuarios.modelos.colonoModel import Colono
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from egresos.utils import obtener_ano_actual
from ingresos.choices import PAGADO_CHOICES
from vigilante.formularios.choiceModel import MyModelChoiceField

class FormIngreso (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])
    monto = forms.DecimalField(validators=[validador_numero_min, validador_numero_max])
    abono = forms.DecimalField(label='Cantidad recibida',
        validators=[validador_numero_min, validador_numero_max])
    colono = forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    tipo_ingreso = MyModelChoiceField(queryset=TipoIngreso.objects.filter(estatus=1))
    ejercicio = forms.IntegerField(initial=obtener_ano_actual)

    #fecha = forms.DateField(widget=forms.SelectDateWidget())
    #fecha = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
    #        'class': 'datetimepicker-input',
    #    }))
    def label_from_instance(self, obj):
        return obj.descripcion + "-" + str(obj.monto)

    class Meta:
        model = Ingreso
        fields = ('colono', 'tipo_ingreso', 'abono',
                    'descripcion','monto','ejercicio')
