from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from vigilante.validador import validador_horas
from visitantes.validador import validador_numero_max,validador_numero_min
from visitantes.choices import HOUR_CHOICES, IDENTIFICACION_CHOICES, ACCESO_CHOICES
from django import forms
from usuarios.validador import validador_telefono
"""
class FormVisitaFoto (forms.Form):

    id = forms.IntegerField()
    archivo = forms.CharField(label='archivo', max_length=50, help_text='Maximo 50 caracteres', required=True)
    identificacion = forms.ChoiceField(choices=IDENTIFICACION_CHOICES)
    foto = forms.CharField(label='foto', required=True)
"""

class FormVisitaFoto (forms.ModelForm):

    id = forms.IntegerField()
    archivo = forms.CharField(label='archivo', max_length=50, help_text='Maximo 50 caracteres', required=True)
    identificacion = forms.ChoiceField(choices=IDENTIFICACION_CHOICES)

    class Meta:
        model = VisitaAutorizada
        fields = ('id','archivo', 'identificacion', 'foto')
    
