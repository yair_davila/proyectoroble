from vigilante.vistas.vigilanteView import VigilanteView
#from vigilante.vistas.turnoView import TurnoView
from django.urls import path
from vigilante.vistas.visitasView import VisitasView
from vigilante.vistas.ingresosView2 import IngresoView
from vigilante.vistas.reservacionView import ReservacionVigilanteView

urlpatterns = [
	path('vigilante_nuevo', VigilanteView.nuevo_vigilante,
         name='nuevo_vigilante'),
    path('vigilante_registrar', VigilanteView.registrar_js_vigilante,
         name='registrar_vigilante'),
    path('vigilante_actualizar', VigilanteView.actualizar_js_vigilante,
         name='actualizar_vigilante'),
    path('vigilante_obteneruno', VigilanteView.obteneruno_js_vigilante,
         name='obteneruno_vigilante'),
    path('vigilante_borrar',
         VigilanteView.borrar_js_vigilante, name='borrar_vigilante'),
     
     path('vigilante_reservacion_nuevo', ReservacionVigilanteView.nuevo_reservacion ,
         name='nuevo_reservacion_vigilante'),
    path('vigilante_reservacion_registrar', ReservacionVigilanteView.registrar_js_reservacion ,
         name='registrar_reservacion_vigilante'),

    path('visita_nuevo', VisitasView.nuevo_visita,
         name='nuevo_visita_vigilante'),
    path('visita_registrar', VisitasView.registrar_js_visita,
         name='registrar_visita_vigilante'),
    path('visita_actualizar', VisitasView.actualizar_js_visita,
         name='actualizar_visita_vigilante'),
    path('aceptar_visita', VisitasView.aceptar_visita,
         name='aceptar_visita'),
    path('visita_obteneruno', VisitasView.obteneruno_js_visita,
         name='obteneruno_visita_vigilante'),
    path('visita_borrar',
         VisitasView.borrar_js_visita, name='borrar_visita_vigilante'),
    path('visitas_obtener',
         VisitasView.obtener_visitas,name='visitas_obtener_vigilante'),
    path('visita_vehiculo',
         VisitasView.validar_vehiculo,name='visita_vehiculo_vigilante'),

     path('ingreso_nuevo',
         IngresoView.nuevo_ingreso,name='nuevo_ingreso_vigilante'),

     path('ingreso_registrar',
          IngresoView.registrar_js_ingreso,name='registrar_ingreso_vigilante')
]