from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson

from vigilante.modelos.vigilanteModel import Vigilante
from vigilante.formularios.fromVigilante import FormVigilante
from vigilante.modelos.turnoModel import Turno
from usuarios.utils import generador_user,generador_password_random,enviar_portercero
from datetime import date
from django.contrib.auth.models import Group



class VigilanteView():

    @login_required()
    @permission_required('vigilante.add_vigilante', login_url='login')
    def nuevo_vigilante(context):
        form = FormVigilante()
        vigilantes = Vigilante.objects.filter(estatus=1)
        return render(context, 'nuevo_vigilante.html',
                      {'formulario': form, 'accion': 'Nuevo', 'vigilantes': vigilantes})

    @login_required()
    def todas_vigilante(context):
        data = VigilanteView.obtener_json_vigilantes(Vigilante.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vigilante.add_vigilante', login_url='login')
    def registrar_js_vigilante(context):
        if (context.method == 'POST'):
            form = FormVigilante(context.POST)
            if form.is_valid():
                vigilante = form.save(commit=False)
                vigilante.usuario_creacion = context.user
                vigilante.fecha_creacion = date.today()
                password = generador_password_random()
                vigilante.password = password[0]
                vigilante.username = generador_user(vigilante)
                print(vigilante.username)
                print(vigilante.password[1])
                vigilante.confirmado = 0
                vigilante.turno = Turno.objects.get(pk=1)
                vigilante.save()
                my_group = Group.objects.get(name='Vigilantes') 
                my_group.user_set.add(vigilante)
                data = VigilanteView.obtener_json_vigilantes(Vigilante.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vigilante.add_vigilante', login_url='login')
    def actualizar_js_vigilante(context):
        if (context.method == 'POST'):
            id = context.POST.get('id', None)
            vigilante = Vigilante.objects.get(pk=id)

            form = FormVigilante(context.POST, instance=vigilante)
            if form.is_valid():
                vigilante = form.save()
                vigilante.save()
                data = VigilanteView.obtener_json_vigilantes(Vigilante.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vigilante.change_vigilante', login_url='login')
    def obteneruno_js_vigilante(context):
        id = context.POST.get('id', None)
        vigilante = Vigilante.objects.filter(pk=id)
        data = VigilanteView.obtener_json_vigilantes(vigilante)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('vigilante.delete_vigilante', login_url='login')
    def borrar_js_vigilante(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            vigilante = Vigilante.objects.get(pk=o)
            vigilante.estatus = 0
            vigilante.save()

        data = VigilanteView.obtener_json_vigilantes(Vigilante.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")

    def obtener_json_vigilantes(vigilantes):

        vigilantesJson = []
        for vigilante in vigilantes:
            vigilantesJson.append(
                {
                    'id': str(vigilante.id),
                    'first_name':str(vigilante.first_name),
                    'last_name': str(vigilante.last_name),
                    'amaterno': str(vigilante.amaterno),
                    'genero': str(vigilante.genero),
                    'telefono': str(vigilante.telefono),
                    'username': str(vigilante.username),
                    'email': str(vigilante.email),
                    'fecha_inicio_servicio': str(vigilante.fecha_inicio_servicio),
                    'fecha_fin_servicio': ''
                }
            )
            #str(vigilante.fecha_inicio_servicio)
        data = simplejson.dumps(vigilantesJson)
        return data
