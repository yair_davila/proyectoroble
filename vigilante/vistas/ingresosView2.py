from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse,JsonResponse
import simplejson
from ingresos.modelos.ingresoModel import Ingreso
#from ingresos.formularios.formIngreso import FormIngreso
from datetime import date
import json
from vigilante.formularios.formIngreso import FormIngreso
from domicilios.modelos.domicilioModel import Domicilio
from egresos.utils import obtener_ano_actual
import datetime
from django.db.models import Max
from vigilante.utils import escribir_pdf


class IngresoView():

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='login')
    def nuevo_ingreso(context):
        lolo = Ingreso.objects.all().aggregate(Max('folio_recibo'))
        print(lolo['folio_recibo__max'])
        form = FormIngreso()
        fecha = datetime.datetime.now()
        ingresos = Ingreso.objects.filter(usuario_creacion=context.user, fecha_creacion=date.today(),borrado=0)
        fecha = datetime.datetime.now().strftime("%Y-%m-%d")
        return render(context, 'nuevo_ingreso_vigilante.html',
                      {'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos,'fecha':fecha})

    @login_required()
    def todas_ingreso(context):
        data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(usuario_creacion=context.user, fecha_creacion=date.today(),borrado=0))
        return HttpResponse(data, content_type="application/json")


    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='login')
    def registrar_js_ingreso(context):
        if (context.method == 'POST'):
            id_domicilio = context.POST.get('domicilio', None)
            
            if(id_domicilio != None):
                
                form = FormIngreso(context.POST)
                if form.is_valid():
                
                    ingreso = form.save(commit=False)
                    ingreso.domicilio = Domicilio.objects.get(id=id_domicilio)
                    ingreso.usuario_creacion = context.user
                    ingreso.fecha = date.today()
                    ingreso.fecha_creacion = date.today()
                    ingreso.referencia = 'static/media/b.txt'
                    ingreso.estatus=0
                    lolo = Ingreso.objects.all().aggregate(Max('folio'))
                    numero = str(int(lolo['folio__max'])+1) if lolo['folio__max']!=None else '0'
                    print(numero)
                    while(len(numero)<6):
                        numero="0"+numero
                    ingreso.folio = numero
                    ingreso.folio_recibo = str(ingreso.ejercicio)+str(numero)
                    if ingreso.abono == ingreso.monto:
                        ingreso.estatus = 1
                    ingreso.borrado=0
                    #ingreso.referencia=escribir_pdf(ingreso)
                    print("antes de guardar")
                    ingreso.save()
                    ingreso.referencia=escribir_pdf(ingreso)
                    print(ingreso.referencia)
                    ingreso.save()
                    print("despues de guardar")
                
                    datos = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(usuario_creacion=context.user, fecha_creacion=date.today(),borrado=0))
                    return HttpResponse(datos, content_type="application/json")
                else:
                    datos = simplejson.dumps([{'error': str(form.errors)} ])
                    return HttpResponse(datos, content_type="application/json")
            else:
                
                datos = simplejson.dumps([{'error': "Ingrese domicilio"} ])
                return HttpResponse(datos, content_type="application/json")
        else:
            datos = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(datos, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='login')
    def actualizar_js_ingreso(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            ingreso = Ingreso.objects.get(pk=id)

            form = FormIngreso(context.POST, instance=ingreso)
            if form.is_valid():
                ingreso = form.save()
                ingreso.save()
                data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(usuario_creacion=context.user, fecha_creacion=date.today(),borrado=0))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.change_ingreso', login_url='login')
    def obteneruno_js_ingreso(context):
        id = context.POST.get('id',None)
        data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.delete_ingreso', login_url='login')
    def borrar_js_ingreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            ingreso = Ingreso.objects.get(pk=o)
            ingreso.estatus = 0
            ingreso.save()

        data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(usuario_creacion=context.user, fecha_creacion=date.today(),borrado=0))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_ingresos(ingresos):
        
        ingresosJson = []
        for ingreso in ingresos:
            ingresosJson.append(
                {
            'id' : ingreso.id,
        	'colono': str(ingreso.colono),
        	'tipo_ingreso': str(ingreso.tipo_ingreso),
        	'fecha': str(ingreso.fecha.strftime('%d de %B de %Y')),
        	'monto': str(ingreso.monto),
        	'referencia': str(ingreso.referencia),
        	'abono': str(ingreso.abono),
        	'descripcion': str(ingreso.descripcion),
        	'ejercicio': str(ingreso.ejercicio),
        	'folio_recibo': str(ingreso.folio_recibo),
            'estatus': str(ingreso.estatus)

                    
                }
            )
        data = simplejson.dumps(ingresosJson)
        return data
