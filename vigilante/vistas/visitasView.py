from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from google.protobuf import descriptor
from django.http import JsonResponse
from usuarios.modelos.colonoModel import Colono
from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from vigilante.formularios.formVisitaVigilante import FormVisitaVigilante
from vigilante.formularios.fromVisitaVehiculoVigilante import FormVisitaVehiculoVigilante
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.personaAjenaModel import PersonaAjena
from datetime import datetime, timedelta
from vigilante.formularios.formVisitaFoto import FormVisitaFoto

import base64

class VisitasView():

    
    #permission_required('visitantes.add_visitaautorizada', login_url='login')
    @login_required()
    def nuevo_visita(context):
        if (context.method == 'POST'):
            print("______________________________________")
            id=context.POST.get('id',None)
            visita = VisitaAutorizada.objects.get(pk=id)
            if visita != None:
                print("2______________________________________")
                form = FormVisitaFoto (context.POST, context.FILES, instance=visita)

                if form.is_valid():
                    print("3______________________________________")
                    visita = form.save(commit=False)
                    print(visita.hora_salida == None)
                    print(visita.hora_entrada != None)
                    now = datetime.now()
                    #print(str(visita.hora_salida) == "00:00:00")
                    if( visita.hora_salida ==  None and
                        #str(visita.hora_salida) == "00:00:00" and
                        visita.hora_entrada != None):
                        visita.hora_salida = str(now.hour) + ':' + str(now.minute)    
                        visita.save()
                        
                        data = simplejson.dumps([{'exito': "exito"}])
                        return HttpResponse(data, content_type="application/json")



                    print("voy a ser exitoso")
                    identificacion = context.POST.get('identificacion',None)
                    archivo = context.POST.get('archivo',None)
                    foto = context.POST.get('foto',None)

                    visita.fecha = datetime.now() #date.today()
                    visita.hora_entrada = str(now.hour) + ':' + str(now.minute)
                    visita.identificacion = identificacion
                    #visita.hora_salida = "00:00"
                    print("antes de guardar")
                    visita.save()
                    print("despues de guardar")
                else:
                    print(str(form.errors))
        form = FormVisitaVigilante()
        formVehiculo = FormVisitaVehiculoVigilante()
        formFoto = FormVisitaFoto()
        visitas = VisitaAutorizada.objects.filter(estatus=1, colono=context.user.id, fecha__gt = (datetime.today() - timedelta(days=1))).order_by('-id')[0:10]
        print("-------------------------:______________________________________")
        print(visitas)
        print("-------------------------_____________________________________")
        return render(context, 'visita_vigilante.html',
                      {'formulario': form, 
                      'accion': 'Nuevo', 
                      'visitas': visitas,
                      'formVehiculo':formVehiculo,
                      'formFoto': formFoto})

    
    #permission_required('visitantes.change_visitaautorizada', login_url='login')
    @login_required()
    def obteneruno_js_visita(context):
        id = context.POST.get('id',None)
        data = VisitasView.obtener_json_visitas2(VisitaAutorizada.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    
    #permission_required('visitantes.delete_visitaautorizada', login_url='login')
    @login_required()
    def borrar_js_visita(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            visita = VisitaAutorizada.objects.get(pk=o)
            visita.estatus = 0
            visita.save()

        data = VisitasView.obtener_json_visitas(VisitaAutorizada.objects.filter(estatus=1)[0:10])
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_visitas2(visitas):
        
        visitasJson = []

        for visita in visitas:
            f=""
            if (visita.foto!=None and visita.foto!=""):
                if(f.find("visitaautorizada/")):
                    f = str(visita.foto).split("visitaautorizada/")[1]
                    print(f)
                
                
            visitasJson.append(
                {
            'id' : visita.id,
            'colono': str(visita.colono) if(visita.colono!=None) else "",
            'persona_ajena': str(visita.persona_ajena) if(visita.persona_ajena!=None) else "",
            'fecha': str(visita.fecha) if(visita.fecha!=None) else "",
            'hora_entrada': str(visita.hora_entrada) if(visita.hora_entrada!=None) else "",
            'hora_salida': str(visita.hora_salida) if(visita.hora_salida!=None) else "",
            'identificacion': str(visita.identificacion) if(visita.identificacion!=None) else "",
            'numero_personas': str(visita.numero_personas) if(visita.numero_personas!=None) else "",
            'foto': f,
            'tipo_acceso': str(visita.persona_ajena.tipo.descripcion) if(visita.tipo_acceso!=None) else "",
            'fecha_estimada': str(visita.fecha_estimada) if(visita.fecha_estimada!=None) else "",
            'hora_estimada_min': str(visita.hora_estimada_min) if(visita.hora_estimada_min!=None) else "",
            'hora_estimada_max': str(visita.hora_estimada_max) if(visita.hora_estimada_max!=None) else "",

                    
                }
            )
        data = simplejson.dumps(visitasJson)
        return data

    def obtener_json_visitas(visitas):
        
        visitasJson = []
        for visita in visitas:
            visitasJson.append(
                {
            'id' : visita.id,
            'colono': str(visita.colono) if(visita.colono!=None) else "",
            'persona_ajena': str(visita.persona_ajena) if(visita.persona_ajena!=None) else "",
            'fecha': str(visita.fecha) if(visita.fecha!=None) else "",
            'hora_entrada': str(visita.hora_entrada) if(visita.hora_entrada!=None) else "",
            'hora_salida': str(visita.hora_salida) if(visita.hora_salida!=None) else "",
            'identificacion': str(visita.identificacion) if(visita.identificacion!=None) else "",
            'numero_personas': str(visita.numero_personas) if(visita.numero_personas!=None) else "",
            'foto': str(visita.foto) if (visita.foto!=None) else "" ,
            'tipo_acceso': str(visita.tipo_acceso) if(visita.tipo_acceso!=None) else "",
            'fecha_estimada': str(visita.fecha_estimada) if(visita.fecha_estimada!=None) else "",
            'hora_estimada_min': str(visita.hora_estimada_min) if(visita.hora_estimada_min!=None) else "",
            'hora_estimada_max': str(visita.hora_estimada_max) if(visita.hora_estimada_max!=None) else "",

                    
                }
            )
        data = simplejson.dumps(visitasJson)
        return data

    def validar_vehiculo(context):
        if (context.method == 'POST'):
            form = FormVisitaVehiculoVigilante(context.POST)
            if form.is_valid():
                data = simplejson.dumps([{'exito': 'exito'}])
                return HttpResponse(data, content_type="application/json")
            data = simplejson.dumps([{'error': str(form.errors)}])
            return HttpResponse(data, content_type="application/json")


    
    #permission_required('visitantes.add_visitaautorizada', login_url='login')
    @login_required()
    def registrar_js_visita(context):
        print("exito____________________________")
        if (context.method == 'POST'):
            form = FormVisitaVigilante(context.POST)
            persona = PersonaAjena()
            if form.is_valid():
                """
                hora_estimada_min = context.POST.get('hora_estimada_min', None).strip()
                hora_estimada_max = context.POST.get('hora_estimada_max', None).strip()
                fecha_estimada = context.POST.get('fecha_estimada', None).strip()
                datetime_object = datetime.datetime.strptime(fecha_estimada, '%Y-%m-%d')
                uno = int(hora_estimada_min[:2] + hora_estimada_min[3:])
                dos = int(hora_estimada_max[:2] + hora_estimada_max[3:])

                datetime_object = datetime_object.replace(hour=int(hora_estimada_min[:2]),minute=int(hora_estimada_min[3:]))


                if (uno < dos and datetime_object >= datetime.datetime.today() ):
                """
                colono = Colono.objects.get(pk=context.POST.get('colono', None))
                persona.nombre = context.POST.get('nombre', None).strip()
                persona.apaterno = context.POST.get('apaterno', None).strip()
                persona.amaterno = context.POST.get('amaterno', None).strip()
                persona.telefono = context.POST.get('telefono', None).strip()
                persona.email = context.POST.get('email', None).strip()

                filtro = PersonaAjena.objects.filter(
                    nombre = persona.nombre,
                    apaterno = persona.apaterno,
                    amaterno = persona.amaterno,
                    usuario_creacion = context.user
                );

                if (len(filtro) == 0):
                    persona.usuario_creacion = context.user
                    persona.usuario_creacion = colono
                    persona.fecha_creacion = date.today()
                    tipoPersona = TipoPersonaAjena.objects.get(descripcion='VISITANTE')
                    persona.tipo = tipoPersona
                    persona.save()
                else:
                    persona = filtro[0]
                visita = form.save(commit=False)
                """
                persona_ajena = 
                fecha = 
                hora_entrada = models.TimeField('Hora_Entrada', blank=True, null=True)
                hora_salida = models.TimeField('Hora_Salida', blank=True, null=True)
                identificacion = models.CharField('Identificación', max_length=35, blank=True, null=True)
                numero_personas = models.SmallIntegerField('Numero_Personas', blank=True, null=True)
                foto = models.ImageField(upload_to='visitaautorizada', null=True)
                tipo_acceso = models.CharField('Tipo_Acceso', max_length=35, blank=True, null=True)
                fecha_estimada = models.DateTimeField('Fecha_Estimada', blank=True, null=True)
                hora_estimada_min = models.TimeField('Hora_Estimada_Min', blank=True, null=True)
                hora_estimada_max = models.TimeField('Hora_Estimada_Max', blank=True, null=True)
                fecha_creacion = models.DateTimeField('Creacion_VisitaAutorizada')
                fecha_edicion = models.DateTimeField('Edicion_VisitaAutorizada', null=True)
                usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_VisitaAutorizada')
                usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_VisitaAutorizada', null=True)
                estatus = models.SmallIntegerField('Estatus', default=1)
                """

                visita.colono = colono
                visita.persona_ajena = persona
                visita.fecha_estimada = date.today()
                now = datetime.now()
                # print now.year, now.month, now.day, now.hour, now.minute, now.second
                visita.hora_estimada_min = str(now.hour) + ':' + str(now.minute)
                visita.hora_estimada_max  = str(now.hour) + ':' + str(now.minute)
                

                visita.tipo_acceso = 1
                visita.usuario_creacion = context.user
                visita.fecha_creacion = date.today()
                visita.save()
                visitas = VisitaAutorizada.objects.filter(estatus=1, colono= colono.id).order_by('-id')[0:10]
                vehiculo = context.POST.get('vehiculo', None).strip()

                if(vehiculo == '1'):
                    new_vehiculo = VehiculoVisitante()
                    placas = str(context.POST.get('placas', None)).strip()

                    new_vehiculo.placas = placas;
                    new_vehiculo.color = str(context.POST.get('color', None)).strip()
                    new_vehiculo.marca = str(context.POST.get('marca', None)).strip()
                    new_vehiculo.linea = str(context.POST.get('linea', None)).strip()
                    new_vehiculo.tipo = str(context.POST.get('tipo', None)).strip()
                    new_vehiculo.fecha_creacion = date.today()
                    new_vehiculo.usuario_creacion = colono
                    new_vehiculo.visita_autorizada = visita
                    new_vehiculo.save()
                print("exito")
                data = simplejson.dumps([{'exito': 'Visita registrada con exito'}])
                return HttpResponse(data, content_type="application/json")
                """
                else:
                    cadena = ''
                    cadena = cadena + 'La hora minima debe ser menor a la hora maxima <br>' if (uno > dos) else cadena
                    cadena = cadena + 'La fecha y hora minima deben ser mayores a la fecha y hora actual' if (datetime_object < datetime.datetime.today()) else cadena
                    data = simplejson.dumps([{'error': cadena}])
                    except:
                        print('fallo')
                        data = simplejson.dumps([{'error': 'Error de conexión'}])
                        return HttpResponse(data, content_type="application/json")
                    return HttpResponse(data, content_type="application/json")
                """
            else:
                print(str(form.errors))
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            print('fallo{{{{{{{{{{{')
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")


    def aceptar_visita(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            visita = VisitaAutorizada.objects.get(pk=id)
            form = FormVisitaFoto (context.POST)

            if visita != None and form.is_valid():
                print(visita.hora_salida == None)
                print(visita.hora_entrada != None)
                now = datetime.now()
                #print(str(visita.hora_salida) == "00:00:00")
                if( visita.hora_salida ==  None and
                    #str(visita.hora_salida) == "00:00:00" and
                    visita.hora_entrada != None):
                    visita.hora_salida = str(now.hour) + ':' + str(now.minute)    
                    visita.save()
                    
                    data = simplejson.dumps([{'exito': "exito"}])
                    return HttpResponse(data, content_type="application/json")



                print("voy a ser exitoso")
                identificacion = context.POST.get('identificacion',None)
                archivo = context.POST.get('archivo',None)
                foto = context.POST.get('foto',None)
                
                archivo = archivo.split('.')[0] + str(now.year) + str(now.month) + str(now.day )+ str(now.hour) + str(now.minute) + str(now.second) +archivo.split('.')[1]
                if(foto != "-"):
                    cadena = base64.b64decode(foto)
                    fh = open("static/visitaautorizada/"+archivo, "wb")
                    fh.write(cadena)
                    fh.close()
                    visita.foto =  "visitaautorizada/"+archivo

                visita.fecha = datetime.now() #date.today()
                visita.hora_entrada = str(now.hour) + ':' + str(now.minute)
                visita.identificacion = identificacion
                #visita.hora_salida = "00:00"
                
                visita.save()
                print("soy a ser exitoso")
                data = VisitasView.obtener_json_visitas(VisitaAutorizada.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                er = form.errors if (form.errors != "") else 'La visita no se encuentra'
                print("error"+ str(er))
                data = simplejson.dumps([{'error': er}])
                return HttpResponse(data, content_type="application/json")
        else:
            print("error No es por metodo POST")
            data = simplejson.dumps([{'error': 'No es por metodo POST'}])
            return HttpResponse(data, content_type="application/json")
        

    
    #permission_required('visitantes.change_visitaautorizada', login_url='login')
    @login_required()
    def actualizar_js_visita(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            visita = VisitaAutorizada.objects.get(pk=id)

            form = FormVisitaVigilante(context.POST, instance=visita)
            if form.is_valid():
                visita = form.save()
                visita.save()
                data = VisitasView.obtener_json_visitas(VisitaAutorizada.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    def obtener_visitas(context):
        visitas = VisitaAutorizada.objects.filter(estatus=1).order_by('-id')[0:10]
        visitasJson = []

        for visita in visitas:
            print(visita.fecha_estimada);
            visitasJson.append(
                {
                    'id': str(visita.id),
                    # 'url': '/' ,
                    'title': str(visita.persona_ajena)+" con "+str(visita.numero_personas) + " ersonas",
                    'start': str(visita.fecha_estimada.strftime('%Y-%m-%d')) + " "+\
                        str(visita.hora_estimada_min),
                    'end': str(visita.fecha_estimada.strftime('%Y-%m-%d')) +" "+ str(visita.hora_estimada_max)

                }
            )
        data = simplejson.dumps(visitasJson)
        return JsonResponse(data, safe=False);
