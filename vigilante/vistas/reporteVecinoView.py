from django.db import connection
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from sancion.modelos.reporteVecinoModel import ReporteVecino
from colonos.formularios.formReporteVecino import FormReporteVecino
from usuarios.modelos.colonoModel import Colono

class ReporteVecinoView():

    def get_colono_logged(context):
        print(context.user.id)
        return Colono.objects.get(pk=context.user.id)

    @login_required()
    @permission_required('sancion.add_reportevecino', login_url='login')
    def nuevo_reportevecino(context):
        if (context.method == 'POST'):
            form = FormReporteVecino(context.POST, context.FILES)

            if form.is_valid():
                reportevecino = form.save(commit=False)
                reportevecino.colono = ReporteVecinoView.get_colono_logged(context)
                reportevecino.fecha = date.today()
                reportevecino.usuario_creacion = context.user
                reportevecino.fecha_creacion = date.today()
                reportevecino.save()
                reportevecinos = ReporteVecino.objects.filter(estatus=1,colono=ReporteVecinoView.get_colono_logged(context)).order_by('-id')
                return render(context, 'nuevo_reportevecino_colono.html',
                              {'formulario': form, 'accion': 'Nuevo', 'reportevecinos': reportevecinos, 'exito': True})
            else:
                reportevecinos = ReporteVecino.objects.filter(estatus=1,colono=ReporteVecinoView.get_colono_logged(context)).order_by('-id')    
                return render(context, 'nuevo_reportevecino_colono.html',
                              {'formulario': form, 'accion': 'Nuevo', 'reportevecinos': reportevecinos, 'error': form.errors})
        else:
            form = FormReporteVecino()
        reportevecinos = ReporteVecino.objects.filter(estatus=1,colono=ReporteVecinoView.get_colono_logged(context)).order_by('-id')    
        return render(context, 'nuevo_reportevecino_colono.html',
                      {'formulario': form, 'accion': 'Nuevo', 'reportevecinos': reportevecinos})

    @login_required()
    @permission_required('sancion.delete_reportevecino', login_url='login')
    def borrar_js_reportevecino(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            reportevecino = ReporteVecino.objects.get(pk=o)
            reportevecino.estatus = 0
            reportevecino.save()

        data = ReporteVecinoView.obtener_json_reportevecinos(ReporteVecino.objects.filter(estatus=1,colono=ReporteVecinoView.get_colono_logged(context)).order_by('-id')    )
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_reportevecinos(reportevecinos):
        
        reportevecinosJson = []
        for reportevecino in reportevecinos:
            reportevecinosJson.append(
                {
            'id' : reportevecino.id,
        	'colono': str(reportevecino.colono),
        	'fecha': str(reportevecino.fecha.strftime('%d de %B de %Y')),
        	'observaciones': str(reportevecino.observaciones),
        	'foto': str(reportevecino.foto),
                }
            )
        data = simplejson.dumps(reportevecinosJson)
        return data

    @login_required()
    @permission_required('sancion.change_reportevecino', login_url='login')
    def editar_reportevecino(context,id):
        reportevecino = ReporteVecino.objects.get(pk=id)
        if (context.method == 'POST'):
            form = FormReporteVecino(context.POST, instance=reportevecino)
            if form.is_valid():
                reportevecino = form.save()
                reportevecino.save()
                return redirect('nuevo_reportevecino')
            else:
                reportevecinos = ReporteVecino.objects.filter(estatus=1,colono=ReporteVecinoView.get_colono_logged(context)).order_by('-id')    
                return render(context, 'nuevo_reportevecino.html',
                              {'formulario': form, 'accion': 'Editar', 'reportevecinos': reportevecinos, 'error': form.errors})
        else:
            form = FormReporteVecino(instance=reportevecino)
        reportevecinos = ReporteVecino.objects.filter(estatus=1,colono=ReporteVecinoView.get_colono_logged(context)).order_by('-id')    
        return render(context, 'nuevo_reportevecino.html', {'formulario': form, 'accion': 'Editar', 'reportevecinos': reportevecinos})
