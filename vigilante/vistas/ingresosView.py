from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from ingresos.modelos.ingresoModel import Ingreso
from domicilios.modelos.domicilioModel import Domicilio
from ingresos.modelos.tipoIngresoModel import TipoIngreso

from vigilante.formularios.formIngreso import FormIngreso



class IngresosView():

    
    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def nuevo_ingreso(context):
        if (context.method == 'POST'):
            form = FormIngreso(context.POST, context.FILES)
            if form.is_valid():
                ingreso = form.save(commit=False)
                monto = context.POST.get('monto', None)
                abono = context.POST.get('abono', None)
                if(monto == abono):
                    ingreso.estatus = 1
                ingreso.usuario_creacion = context.user
                ingreso.fecha_creacion = date.today()
                ingreso.save()
                print("Guardar")
                ingresos = Ingreso.objects.filter(borrado=0)
                form = FormIngreso()
                return render(context, 'nuevo_ingreso.html',
                              {'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos, 'exito': True})
            else:
                ingresos = Ingreso.objects.filter(borrado=0)
                return render(context, 'nuevo_ingreso.html',
                              {'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos, 'error': form.errors})
        else:
            ingresos = Ingreso.objects.filter(borrado=0)
            form = FormIngreso()
        return render(context, 'nuevo_ingreso.html', {'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos})
    
    @login_required()
    @permission_required('ingresos.view_ingreso', login_url='ingreso')
    def obtener_ingresos(context):
        ingresos = Ingreso.objects.filter(borrado=0)
        return render(context, 'lista_ingreso.html', {'accion': 'Lista', 'ingresos': ingresos})

    @login_required()
    @permission_required('ingresos.change_ingreso',login_url='ingreso')
    def editar_ingreso(request, id):
        ingreso = Ingreso.objects.get(pk=id)
        if request.method == 'POST':
            form = FormIngreso(request.POST, request.FILES, instance=ingreso)
            print(form.is_valid())
            print(form.errors)
            if form.is_valid():
                ingreso = form.save()
                ingreso.save()
                return redirect('nuevo_ingreso')
            else:
                ingresos = Ingreso.objects.filter(borrado=0)
                return render(request, 'nuevo_ingreso.html',
                          {'formulario': form, 'accion': 'Editar', 'ingresos': ingresos, 'error': form.errors})
        else:
            form = FormIngreso(instance=ingreso)
        ingresos = Ingreso.objects.filter(borrado=0)
        return render(request, 'nuevo_ingreso.html', {'formulario': form, 'accion': 'Editar','ingresos':ingresos})


    @login_required()
    @permission_required('ingresos.change_ingreso', login_url='ingreso')
    def obteneruno_js_ingreso(context):
        id = context.POST.get('id',None)
        print('try______________________________________________________')
        try:
            print('try')
            descripcion = context.POST.get('descripcion',None)
            data = IngresosView.obtener_json_ingresos(Ingreso.objects.filter(pk=id))
            print('try')
        except:
            print('except')
            data = IngresosView.obtener_json_ingresos_carga(Ingreso.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.delete_ingreso', login_url='ingreso')
    def borrar_js_ingreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            ingreso = Ingreso.objects.get(pk=o)
            ingreso.borrado = 1
            ingreso.save()

        data = IngresosView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_ingresos(ingresos):
        
        ingresosJson = []
        for ingreso in ingresos:
            ingresosJson.append(
                {
            'id' : ingreso.id,
        	'colono': str(ingreso.colono),
        	'tipo_ingreso': str(ingreso.tipo_ingreso),
        	'fecha': str(ingreso.fecha.strftime('%d de %B de %Y')),
        	'monto': str(ingreso.monto),
        	'referencia': str(ingreso.referencia),
        	'abono': str(ingreso.abono),
        	'descripcion': str(ingreso.descripcion),
        	'ejercicio': str(ingreso.ejercicio),
        	'folio_recibo': str(ingreso.folio_recibo),
            'estatus': str(ingreso.estatus)

                    
                }
            )
        data = simplejson.dumps(ingresosJson)
        return data


    def obtener_json_ingresos_carga(ingresos):
        
        ingresosJson = []
        for ingreso in ingresos:
            ingresosJson.append(
                {
                    'id' : str(ingreso.id),
                    'colono': str(ingreso.colono.id),
                    'tipo_ingreso': str(ingreso.tipo_ingreso.id),
                    'fecha': str(ingreso.fecha.strftime('%Y-%m-%d')),
                    'monto': str(ingreso.monto),
                    'referencia': str(ingreso.referencia),
                    'abono': str(ingreso.abono),
                    'descripcion': str(ingreso.descripcion),
                    'ejercicio': str(ingreso.ejercicio),
                    'estatus': str(ingreso.estatus),
                    'folio_recibo': str(int(ingreso.folio_recibo)),
                }
            )
        data = simplejson.dumps(ingresosJson)
        return data


    

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def registrar_js_ingreso(context):
        if (context.method == 'POST'):
            form = FormIngreso(context.POST)
            if form.is_valid():
                ingreso = form.save(commit=False)
                ingreso.usuario_creacion = context.user
                ingreso.fecha_creacion = date.today()
                ingreso.save()
                data = IngresosView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def actualizar_js_ingreso(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            ingreso = Ingreso.objects.get(pk=id)

            form = FormIngreso(context.POST, instance=ingreso)
            if form.is_valid():
                ingreso = form.save()
                ingreso.save()
                data = IngresosView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0))

                return HttpResponse(data, content_type="application/json")
            else:
                print( str(form.errors))
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    

    @login_required()
    @permission_required('ingresos.delete_ingreso', login_url='ingreso')
    def borrar_js_ingreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            ingreso = Ingreso.objects.get(pk=o)
            ingreso.borrado = 1
            ingreso.save()

        data = IngresosView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0))
        return HttpResponse(data, content_type="application/json")
