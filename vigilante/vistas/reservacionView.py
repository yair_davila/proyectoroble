from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from areasComunes.modelos.reservacionModel import Reservacion
from usuarios.modelos.colonoModel import Colono
from colonos.formularios.formReservacion import FormReservacion
from datetime import date

class ReservacionVigilanteView():

    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='login')
    def nuevo_reservacion(context):
        form = FormReservacion()
        reservacions = Reservacion.objects.filter(estatus=1)
        return render(context, 'reservacion_vigilante.html',
                      {'formulario': form, 'accion': 'Nuevo', 'reservacions': reservacions})

    
    def obtener_json_reservacions(reservacions):
        
        reservacionsJson = []
        for reservacion in reservacions:
            reservacionsJson.append(
                {
                    'area_comun': str(reservacion.area_comun),
                    'tipo_evento': str(reservacion.tipo_evento),
                    'fecha': str(reservacion.fecha.strftime('%d de %B de %Y')),
                    'hora_inicial': str(reservacion.hora_inicial),
                    'hora_final': str(reservacion.hora_final),
                    'observaciones': str(reservacion.observaciones)
                }
            )
        data = simplejson.dumps(reservacionsJson)
        return data

    @login_required()
    @permission_required('areasComunes.add_reservacion', login_url='login')
    def registrar_js_reservacion(context):
        if (context.method == 'POST'):
            form = FormReservacion(context.POST)
            if (form.is_valid()):
                print("------------------------------")
                print("valido")
                print("------------------------------")
                reservacion = form.save(commit=False)
                reservacion.colono = Colono.objects.get(id=context.user.id)
                reservacion.usuario_creacion = context.user
                reservacion.fecha_creacion = date.today()
                reservacion.save()
                data = ReservacionVigilanteView.obtener_json_reservacions(Reservacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    