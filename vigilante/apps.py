from django.apps import AppConfig


class VigilanteConfig(AppConfig):
    name = 'vigilante'
