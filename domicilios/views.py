from django.shortcuts import render, redirect
from domicilios.models import Calle,ColonoDomicilio,Colono,TipoDomicilio,Domicilio,Variables
from domicilios.forms import FormCalle,FormColonoDomicilio,FormTipoDomicilio,FormColonoDomicilioDos,FormDomicilioDos,FormDomicilio,FormReporte
from django.contrib.auth.decorators import login_required
from domicilios.utils import tiene_permiso, convertir_a_mayusculas, Utils, DomicilioTemporal, get_letra, generar_pdf
from django.contrib.auth.decorators import permission_required
from django.http import FileResponse,HttpResponse
from django.db.models import Max

def tab(context):
    if (context.method == 'POST'):
        form = FormCalle(context.POST)
        if form.is_valid():
            calle = form.save()
            calle.save()
            return redirect('tab')
    else:
        form = FormCalle()        
    calles = Calle.objects.all()
    return render(context, 'dialog.html', {'formulario': form})

class CalleView():

    @login_required()
    @permission_required('domicilios.view_calle')
    def lista_calles(context):
        calles = Calle.objects.all()
        return render(context, 'calles.html', {'calles': calles})

    @login_required()
    @permission_required('domicilios.add_calle', login_url='calle')
    def nuevo_calle(context):
        if (context.method == 'POST'):
            form = FormCalle(context.POST)
            if form.is_valid():
                calle = form.save()
                max = Calle.objects.aggregate(Max('id'))
                new_id = max if (max == None) else 1
                calle.id = new_id
                calle.save()
                return redirect('nuevo_calle')
        else:
            form = FormCalle()
        calles = Calle.objects.all()
        return render(context, 'nuevo_calle.html', {'formulario': form, 'accion': 'Nuevo', 'calles': calles})

    @login_required()
    @permission_required('domicilios.delete_calle', login_url='calle')
    def eliminar_calle(context, id):
        calle = Calle.objects.get(pk=id)
        calle.delete()
        return redirect('nuevo_calle')

    @login_required()
    @permission_required('domicilios.change_calle', login_url='calle')
    def editar_calle(request, id):
        calle = Calle.objects.get(pk=id)
        if request.method == 'POST':
            form = FormCalle(request.POST, instance=calle)
            if form.is_valid():
                calle = form.save()
                calle.save()
                return redirect('nuevo_calle')
        else:
            form = FormCalle(instance=calle)
        calles = Calle.objects.all()
        return render(request, 'nuevo_calle.html', {'formulario': form, 'accion': 'Editar', 'calles': calles})


class TipoDomicilioView():

    @login_required()
    @permission_required('domicilios.view_tipodomicilio')
    def lista_tipodomicilios(context):
        tipodomicilios = TipoDomicilio.objects.all()
        return render(context, 'tipodomicilios.html', {'tipodomicilios': tipodomicilios})

    @login_required()
    @permission_required('domicilios.add_tipodomicilio', login_url='tipodomicilio')
    def nuevo_tipodomicilio(context):
        if (context.method == 'POST'):
            form = FormTipoDomicilio(context.POST)
            if form.is_valid():
                tipodomicilio = form.save()
                tipodomicilio.descripcion = convertir_a_mayusculas(
                    tipodomicilio.descripcion)
                max = TipoDomicilio.objects.aggregate(Max('id'))
                new_id = max if (max == None) else 1
                tipodomicilio.id = new_id
                tipodomicilio.save()
                return redirect('nuevo_tipodomicilio')
        form = FormTipoDomicilio()
        tipodomicilios = TipoDomicilio.objects.all()
        return render(context, 'nuevo_tipodomicilio.html', {'formulario': form, 'accion': 'Nuevo', 'tipodomicilios': tipodomicilios})

    @login_required()
    @permission_required('domicilios.delete_tipodomicilio', login_url='tipodomicilio')
    def eliminar_tipodomicilio(context, id):
        tipodomicilio = TipoDomicilio.objects.get(pk=id)
        domicilios = Domicilio.objects.filter(tipo_domicilio=tipodomicilio)
        if(len(domicilios) == 0):
            tipodomicilio.delete()
        return redirect('tipodomicilio')

    @login_required()
    @permission_required('domicilios.change_tipodomicilio', login_url='tipodomicilio')
    def editar_tipodomicilio(context, id):
        tipodomicilios = TipoDomicilio.objects.all()
        tipodomicilio = TipoDomicilio.objects.get(pk=id)
        if context.method == 'POST':
            form = FormTipoDomicilio(context.POST, instance=tipodomicilio)
            if form.is_valid():
                tipodomicilio = form.save()
                tipodomicilio.descripcion = convertir_a_mayusculas(
                    tipodomicilio.descripcion)
                tipodomicilio.save()
                return redirect('nuevo_tipodomicilio')
        form = FormTipoDomicilio(instance=tipodomicilio)
        tipodomicilios = TipoDomicilio.objects.all()
        return render(context, 'nuevo_tipodomicilio.html', {'formulario': form, 'accion': 'Editar', 'tipodomicilios': tipodomicilios})


class DomicilioView():

    @login_required()
    @permission_required('domicilios.view_domicilio')
    def lista_domicilios(context):
        domicilios = Domicilio.objects.all()
        return render(context, 'domicilios.html', {'domicilios': domicilios})

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicili')
    def nuevo_domicilio(context):
        if (context.method == 'POST'):
            form = FormDomicilio(context.POST)
            if form.is_valid():
                domicilio = form.save()
                letra = context.POST.get('letra', None)
                domicilio.num_domicilio = domicilio.num_domicilio+letra
                max = Domicilio.objects.aggregate(Max('id'))
                new_id = max if (max == None) else 1
                domicilio.id = new_id
                domicilio.save()
                return redirect('nuevo_domicilio')
        form = FormDomicilio()
        domicilios = Domicilio.objects.all()
        return render(context, 'nuevo_domicilio.html', {'formulario': form, 'accion': 'Nuevo', 'domicilios': domicilios})

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicili')
    def nuevo_domicilio_dos(context):
        domicilios = Domicilio.objects.all().order_by('calle', 'num_domicilio')
        form = FormDomicilioDos()
        if (context.method == 'POST'):
            min = context.POST.get('min', None)
            max = context.POST.get('max', None)
            calle = Calle.objects.get(id=context.POST.get('calle', None))

            rango = context.POST.get('rango', None)
            tipo_seleccionado = TipoDomicilio.objects.get(
                id=context.POST.get('tipo_domicilio', None))

            variables = Variables()
            variables.maximo = max
            variables.minimo = min
            variables.calle = calle
            variables.rango = rango
            variables.save()

            utilidades = Utils()
            lista = utilidades.numeros(rango, min, max)
            domicilios_temporales = []
            tipodomicilios = TipoDomicilio.objects.all()

            for numero in lista:
                dom = DomicilioTemporal(numero, variables.calle)
                domicilios_temporales.append(dom)

            return render(context, 'nuevo_domicilio_dos.html',
                          {'formulario': form,
                           'accion': 'Nuevo',
                           'domicilios': domicilios,
                           'domicilios_temporales': domicilios_temporales,
                           'tipos_domicilio': tipodomicilios,
                           'defult': tipo_seleccionado
                           })
        else:
            return render(context, 'nuevo_domicilio_dos.html', {'formulario': form, 'accion': 'Nuevo', 'domicilios': domicilios})

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicili')
    def nuevo_domicilio_cuatro(context):

        if (context.method == 'POST'):
            variables = Variables.objects.all()
            variable = variables[len(variables)-1]
            print(variable)
            min = variable.minimo
            max = variable.maximo
            print('*'*100)
            print(max)
            print('*'*100)
            rango = variable.rango
            calle = variable.calle

            Variables.objects.all().delete()

            utilidades = Utils()

            lista = utilidades.numeros_dos(rango, min, max)
            max = Domicilio.objects.aggregate(Max('id'))
            new_id = max if (max == None) else 1
            new_id = new_id-1

            for numero in lista:

                domicilio = Domicilio()
                domicilio.calle = calle
                domicilio.num_domicilio = numero

                tipo = context.POST.get(str(numero), None)
                print("tipo")
                print(tipo)
                domicilio.tipo_domicilio = TipoDomicilio.objects.filter(descripcion=tipo)[
                    0]
                new_id = new_id + 1
                domicilio.id = new_id

                domicilio.save()

        return redirect('nuevo_domicilio')

    @login_required()
    @permission_required('domicilios.delete_domicilio', login_url='domicili')
    def eliminar_domicilio(context, id):
        domicilio = Domicilio.objects.get(pk=id)
        domicilio.delete()
        return redirect('domicilio')

    @login_required()
    @permission_required('domicilios.change_domicilio', login_url='domicili')
    def editar_domicilio(context, id):
        domicilios = Domicilio.objects.all()
        domicilio = Domicilio.objects.get(pk=id)

        if context.method == 'POST':
            form = FormDomicilio(context.POST, instance=domicilio)
            if form.is_valid():
                domicilio = form.save()
                letra = context.POST.get('letra', None)
                domicilio.num_domicilio = domicilio.num_domicilio+letra
                domicilio.save()
                return redirect('nuevo_domicilio')

        numero = get_letra(domicilio.num_domicilio)
        data = {
            'calle': domicilio.calle,
            'tipo_domicilio': domicilio.tipo_domicilio,
            'num_domicilio': numero[0],
            'letra': numero[1]
        }
        form = FormDomicilio(data)

        domicilios = Domicilio.objects.all()
        return render(context, 'nuevo_domicilio.html', {'formulario': form, 'accion': 'Editar', 'domicilios': domicilios})


class ColonoDomicilioView():

    @login_required()
    @permission_required('domicilios.view_colonodomicilio')
    def lista_colonodomicilios(context):
        eleccion = 2
        if (context.method == 'POST'):
            form = FormReporte(context.POST)
            if form.is_valid():
                eleccion = int(context.POST.get('habitable', None))
                if eleccion==2:
                    colonodomicilios = ColonoDomicilio.objects.all()
                else:
                    colonodomicilios = ColonoDomicilio.objects.filter(domicilio__tipo_domicilio__habitable=eleccion)

        else:
            form = FormReporte()
            eleccion = 2
            colonodomicilios = ColonoDomicilio.objects.all()

        return render(context, 'colonodomicilios.html', {'formulario':form ,'colonodomicilios': colonodomicilios, 'eleccion':eleccion})

    @login_required()
    @permission_required('domicilios.view_colonodomicilio')
    def descargar_lista_colonodomicilios(context,id):
        if id == 2:
            colonodomicilios = ColonoDomicilio.objects.all()
        else:
            colonodomicilios = ColonoDomicilio.objects.filter(domicilio__tipo_domicilio__habitable=id)
        generar_pdf(colonodomicilios)
        return FileResponse(open('reportes/relaciondomicilios.pdf', 'rb'), content_type='application/pdf')



    @login_required()
    @permission_required('domicilios.add_colonodomicilio', login_url='colonodomcilio')
    def nuevo_colonodomicilio(context):
        if (context.method == 'POST'):
            form = FormColonoDomicilio(context.POST)
            if form.is_valid():
                colonodomicilio = form.save()
                max = ColonoDomicilio.objects.aggregate(Max('id'))
                new_id = max if (max == None) else 1
                colonodomicilio.id = new_id
                colonodomicilio.save()
                return redirect('nuevo_colonodomicilio')
        form = FormColonoDomicilio()
        colonodomicilios = ColonoDomicilio.objects.all()
        return render(context, 'nuevo_colonodomicilio.html', {'formulario': form, 'accion': 'Nuevo', 'colonodomicilios': colonodomicilios})

    @login_required()
    @permission_required('domicilios.delete_colonodomicilio', login_url='colonodomcilio')
    def eliminar_colonodomicilio(context, id):
        colonodomicilio = ColonoDomicilio.objects.get(pk=id)
        colonodomicilio.delete()
        return redirect('colonodomicilio')

    @login_required()
    @permission_required('domicilios.change_colonodomicilio', login_url='colonodomcilio')
    def editar_colonodomicilio(context, id):
        colonodomicilios = ColonoDomicilio.objects.all()
        colonodomicilio = ColonoDomicilio.objects.get(pk=id)
        if context.method == 'POST':
            form = FormColonoDomicilio(context.POST, instance=colonodomicilio)
            if form.is_valid():
                colonodomicilio = form.save()
                colonodomicilio.save()
                return redirect('nuevo_colonodomicilio')
        form = FormColonoDomicilio(instance=colonodomicilio)
        colonodomicilios = ColonoDomicilio.objects.all()
        return render(context, 'nuevo_colonodomicilio.html', {'formulario': form, 'accion': 'Editar', 'colonodomicilios': colonodomicilios})
