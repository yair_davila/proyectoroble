from django import forms
from domicilios.choices import LETRAS, SINO,CONFIRM
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from domicilios.validador import validador_cadenas, validador_numero_min, validador_numero_max

class FormTipoDomicilio(forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])
    habitable = forms.ChoiceField(choices=SINO, required=True)

    class Meta:
        model = TipoDomicilio
        fields = ('descripcion', 'habitable')
