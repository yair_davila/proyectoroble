from django import forms
from domicilios.choices import LETRAS, SINO,CONFIRM
from domicilios.modelos.domicilioModel import Domicilio
from domicilios.validador import validador_cadenas, validador_numero_min, validador_numero_max
from domicilios.modelos.calleModel import Calle
from usuarios.modelos.colonoModel import Colono
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio


class FormDomicilio(forms.ModelForm):
    num_domicilio = forms.IntegerField(
        validators=[validador_numero_min, validador_numero_max])
    letra = forms.ChoiceField(choices=LETRAS, required=False)
    calle = forms.ModelChoiceField(queryset=Calle.objects.filter(estatus=1))
    tipo_domicilio = forms.ModelChoiceField(queryset=TipoDomicilio.objects.filter(estatus=1))
    titular=forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))

    class Meta:
        model = Domicilio
        fields = ('calle', 'tipo_domicilio', 'num_domicilio', 'letra','titular')

class FormDomicilioFilter(forms.ModelForm):
    calle = forms.ModelChoiceField(queryset=Calle.objects.filter(estatus=1))
    tipo_domicilio = forms.ModelChoiceField(
        required=False,
        queryset = TipoDomicilio.objects.filter(estatus=1),
        label="TipoDomicilio",
        empty_label="Todos"
    )

    class Meta:
        model = Domicilio
        fields = ('calle', 'tipo_domicilio')