from django import forms
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio,Colono,Domicilio


class FormColonoDomicilio(forms.ModelForm):
    colono= forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    domicilio=forms.ModelChoiceField(queryset=Domicilio.objects.filter(estatus=1))

    class Meta:
        model = ColonoDomicilio
        fields = ('colono', 'domicilio')
