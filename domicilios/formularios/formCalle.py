from django import forms
from domicilios.modelos.calleModel import Calle
from domicilios.validador import validador_cadenas


class FormCalle(forms.ModelForm):
    nombre = forms.CharField(validators=[validador_cadenas])

    class Meta:
        model = Calle
        fields = ('nombre',)