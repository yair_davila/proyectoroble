from django.db import models
from django.contrib.auth.models import User

LONGITUD_MAXIMA = 'Ha excedido la longitud para este campo'
REQUERIDO = 'Este campo es requerido'


class Calle(models.Model):
    # id_calle = models.SmallIntegerField('ID_CALLE', primary_key=True)  # Field name made lowercase.
    # Field name made lowercase.
    nombre = models.CharField('Nombre', max_length=100, unique=True)
    fecha_creacion = models.DateTimeField('Creacion_Calle')
    fecha_edicion = models.DateTimeField('Edicion_Calle', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Calle')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Calle', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.nombre

    def imprimir(self):
        return str(self.nombre)

    class Meta:
        app_label = "domicilios"