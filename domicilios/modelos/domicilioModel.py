from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from domicilios.modelos.calleModel import Calle
from usuarios.modelos.colonoModel import Colono
from django.db import models
from django.contrib.auth.models import User

class Domicilio(models.Model):
    calle = models.ForeignKey(Calle, models.DO_NOTHING, 'ID_CALLE')
    num_domicilio = models.CharField('Numero_Domicilio', max_length=10)
    tipo_domicilio = models.ForeignKey(
        TipoDomicilio, models.DO_NOTHING, 'ID_TIPO_DOMICILIO', blank=True, null=True)
    titular = models.ForeignKey(
        Colono, models.DO_NOTHING, 'DOMICILIO_TITULAR', blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Domicilio')
    fecha_edicion = models.DateTimeField('Edicion_Domicilio', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Domicilio')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Domicilio', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.calle.__str__()+" "+self.num_domicilio

    def imprimir(self):
        if(self.titular is None):
            return str(self.id)+','+str(self.calle.id)+','+\
                str(self.num_domicilio)+','+str(self.tipo_domicilio.id)+','
        else:
            return str(self.id) + ',' + str(self.calle.id) + ',' + \
                   str(self.num_domicilio) + ',' + str(self.tipo_domicilio.id) + ',' + \
                   str(self.titular.id)

    class Meta:
        app_label = "domicilios"