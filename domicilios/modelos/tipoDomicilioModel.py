from django.db import models
from django.contrib.auth.models import User

class TipoDomicilio(models.Model):
    descripcion = models.CharField('Descripción', max_length=50, unique=True)
    habitable = models.SmallIntegerField('Habitable')
    fecha_creacion = models.DateTimeField('Creacion_TipoDomicilio')
    fecha_edicion = models.DateTimeField('Edicion_TipoDomicilio', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_TipoDomicilio')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_TipoDomicilio', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.descripcion

    def imprimir(self):
        return str(self.id)+','+str(self.descripcion)+','+\
            str(self.habitable)

    class Meta:
        app_label = "domicilios"