from domicilios.modelos.domicilioModel import Domicilio
from usuarios.modelos.colonoModel import Colono
from django.db import models
from django.contrib.auth.models import User

class ColonoDomicilio(models.Model):
    colono = models.ForeignKey(
        Colono, models.DO_NOTHING, 'ID_COLONO_DOMICILIO')
    domicilio = models.ForeignKey(Domicilio, models.DO_NOTHING, 'ID_DOMICILIO')
    fecha_creacion = models.DateTimeField('Creacion_ColonoDomicilio')
    fecha_edicion = models.DateTimeField('Edicion_ColonoDomicilio', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_ColonoDomicilio')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_ColonoDomicilio', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.colono.__str__()+" "+self.domicilio.__str__()

    def imprimir(self):
        return str(self.colono.id)+','+\
            str(self.domicilio.id)

    class Meta:
        app_label = "domicilios"