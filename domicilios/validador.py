from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _


def validador_numero_min(value):
    if(value) < 0:
        raise ValidationError(
            _('Error {0} debe ser mayor a 0'.format(value))
        )


def validador_numero_max(value):
    if(value) > 9999999999:
        raise ValidationError(
            _('Error {0} debe ser menor a 9999999999'.format(value))
        )


def validador_cadenas(value):
    es_cadena = True
    try:
        a = int(value)
        es_cadena = False
    except Exception as e:
        es_cadena = True
    print(es_cadena)
    if es_cadena == False:
        raise ValidationError(
            _('Error {0} debe ser un numero sin letras'.format(value))
        )



def validador_telefono(value):
    excepcion = False
    try:
        valor = int(value)
        if int(value) > 9999999999:
            excepcion = True
        elif valor < 1000000000:
            excepcion = True

    except Exception as e:
        excepcion = True

    if excepcion:
        raise ValidationError(
            _('Error {0} el formato es incorrecto'.format(value))
        )
