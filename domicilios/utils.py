#import pdfkit
from domicilios.validador import validador_cadenas

def get_numero_domicilio(num_domicilio):
    letras={"":1,"A":2,"B":3,"C":5,"D":6,"E":7,"F":8,"G":9,
            "H":10 ,"I":11 ,"J":12 ,"K":13 ,"L":14 ,"M":15,
            "N":16 ,"O":17 ,"P":18 ,"Q":19 ,"R":20 ,"S":21,
            "T":22 ,"U":23 ,"V":24 ,"W":25 ,"X":26 ,"Y":27,
            "Z":28 }
    if tiene_letra(num_domicilio):
        return [num_domicilio[:-1],letras[num_domicilio[-1]]]
    else:
        return [num_domicilio,letras[""]]



def tiene_permiso(user, permiso):
    if user.is_staff:
        return True

    if user.user_permissions.filter(name=permiso):
        return True

    grupos = user.groups.all()
    if len(grupos) > 0:
        if(grupos[0].permissions.filter(name=permiso)):
            return True

    return False


def convertir_a_mayusculas(cadena):
    return cadena.upper()


def get_letra(cadena):
    c = cadena[len(cadena)-1]
    letra = tiene_letra(c)
    if(letra):
        elemento = cadena[0:len(cadena)-1]
        return [elemento+'', c]
    print([cadena, ''])
    return [cadena, '']


def tiene_letra(cadena):
    try:
        c = int(cadena)
        return False
    except Exception as e:
        return True


class DomicilioTemporal():
    def __init__(self, num, cal):
        self.numero = num
        self.calle = cal


class Utils():
    def instance_to_data(instancia):
        data = {
            'calle': instancia.calle,
            'tipo_domicilio': instancia.tipo_domicilio
        }
        return data

    def numeros(self, decision, minimo, maximo):
        dec = 0
        incremento = 1
        min = self.convertir_numero(minimo)
        max = self.convertir_numero(maximo)
        lista = []

        if decision == '1':  # nones
            incremento = 2
            if self.es_par(min):
                min = min+1

        elif decision == '2':
            incremento = 2
            if self.es_par(min) == False:
                min = min + 1

        while min <= max:
            lista.append(min)
            print(min)
            min = min+incremento

        return lista

    def numeros_dos(self, decision, minimo, maximo):
        print('*'*100)
        print(minimo)
        print(maximo)
        dec = 0
        incremento = 1
        lista = []

        if decision == 1:  # nones
            incremento = 2
            if self.es_par(minimo):
                minimo = minimo+1

        elif decision == 2:
            incremento = 2
            if self.es_par(minimo) == False:
                minimo = minimo + 1

        while minimo <= maximo:
            lista.append(minimo)
            print(minimo)
            minimo = minimo+incremento

        return lista

    def convertir_numero(self, numero):
        limite = 1
        while numero != str(limite):
            limite = limite+1
        return limite

    def es_par(self, dec):
        if dec % 2 == 0:  # es par
            return True
        else:
            return False


class DomicilioTemporal():

    def __init__(self, num, cal):
        self.numero = num
        self.calle = cal

def generar_pdf(valores):
    c = ""
    for valor in valores:

        c = c + "<tr>" + \
            "<td>" + str(valor.domicilio) + "</td>" + \
            "<td>" + str(valor.domicilio.titular) + "</td>" + \
            "<td>" + str(valor.colono.email) + "</td>" + \
            "<td>" + str(valor.colono.telefono_celular) + "</td>" + \
            "<td>" + str(valor.colono.telefono_fijo) + "</td>" + \
            "</tr>"

    cadena = "<!DOCTYPE html>" + \
             "<html>" + \
             "<head>" + \
             "<title>Relaci&ocute;n Domicilios</title>" + \
             "<style>" + \
             "table {" + \
             "font-family: 'Arial', Times, serif;" + \
             "border: 1px solid black;" + \
             "border-collapse: collapse;" + \
             "width: 100%;" + \
             "}" + \
             "" + \
             "td, th {" + \
             "border: 1px solid black;" + \
             "text-align: left;" + \
             "padding: 8px;" + \
             "}" + \
             "" + \
             ".sin_borde{" + \
             "border: 0px;" + \
             "" + \
             "}" + \
             "body{" + \
             "margin-top: 3%;" + \
             "margin-right: 3%;" + \
             "margin-left: 3%;" + \
             "}" + \
             "table{" + \
             "width: 100%;" + \
             "}" + \
             ".sombra {" + \
             "" + \
             "background-color: #d9d9d9;" + \
             "}" + \
             ".my_title	{" + \
             "background-color: #bfbfbf;" + \
             "}" + \
             "</style>" + \
             "</head>" + \
             "<body>" + \
             "<table>" + \
             "<tr class='my_title'>" + \
             "<th>Domicilio</th>" + \
             "<th>Titular</th>" +\
             "<th>Email</th>" + \
             "<th>Telefono Fijo</th>" + \
             "<th>Telefono Celular</th>" + \
             "</tr>" + \
             c + \
             "</table> " + \
             "</body>" + \
             "</html>"
    #pdfkit.from_string(cadena, "reportes/relaciondomicilios.pdf")