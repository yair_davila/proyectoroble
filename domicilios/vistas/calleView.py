from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse,JsonResponse
import simplejson
from domicilios.modelos.calleModel import Calle
from domicilios.formularios.formCalle import FormCalle
from datetime import date
import json

class CalleView():

    @login_required()
    @permission_required('domicilios.add_calle', login_url='calle')
    def nuevo_calle(context):
        form = FormCalle()
        calles = Calle.objects.filter(estatus=1)
        return render(context, 'nuevo_calle.html',
                      {'formulario': form, 'accion': 'Nuevo', 'calles': calles})

    @login_required()
    def todas_calle(context):
        data = CalleView.obtener_json_calles(Calle.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")


    @login_required()
    @permission_required('domicilios.add_calle', login_url='calle')
    def registrar_js_calle(context):
        if (context.method == 'POST'):
            form = FormCalle(context.POST)
            if form.is_valid():
                calle = form.save(commit=False)
                calle.usuario_creacion = context.user
                calle.fecha_creacion = date.today()
                calle.save()
                response_data = {}
                response_data['result'] = 'Create post successful!'
                datos = CalleView.obtener_json_calles(Calle.objects.filter(estatus=1))
                return HttpResponse(datos, content_type="application/json")
            else:
                datos = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(datos, content_type="application/json")
        else:
            datos = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(datos, content_type="application/json")

    @login_required()
    @permission_required('domicilios.add_calle', login_url='calle')
    def actualizar_js_calle(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            calle = Calle.objects.get(pk=id)

            form = FormCalle(context.POST, instance=calle)
            if form.is_valid():
                calle = form.save()
                calle.save()
                data = CalleView.obtener_json_calles(Calle.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.change_calle', login_url='calle')
    def obteneruno_js_calle(context):
        id = context.POST.get('id',None)
        data = CalleView.obtener_json_calles(Calle.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.delete_calle', login_url='calle')
    def borrar_js_calle(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            calle = Calle.objects.get(pk=o)
            calle.estatus = 0
            calle.save()

        data = CalleView.obtener_json_calles(Calle.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_calles(calles):
        
        callesJson = []
        for calle in calles:
            callesJson.append(
                {
            'id' : calle.id,
        	'nombre': calle.nombre,

                    
                }
            )
        data = simplejson.dumps(callesJson)
        return data
