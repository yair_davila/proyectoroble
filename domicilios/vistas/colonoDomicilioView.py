from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio
from domicilios.formularios.formColonoDomicilio import FormColonoDomicilio

from django.http import JsonResponse

class ColonoDomicilioView():

    @login_required()
    def filtrar_por_colono (context):
        simplejson.dumps([{'error': 'no se encontro'}])
        if (context.method == 'GET'):
            try:
                id_colono = context.GET.get('id_colono', None)
                datos = ColonoDomicilio.objects.filter(colono=id_colono)
                data = ColonoDomicilioView.obtener_json_colonodomicilios_filtro(datos)
            except:
                print("error")                
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.add_colonodomicilio', login_url='colonodomicilio')
    def nuevo_colonodomicilio(context):
        form = FormColonoDomicilio()
        #colonodomicilios = ColonoDomicilio.objects.filter(estatus=1)[1:30]
        colonodomicilios = []
        return render(context, 'nuevo_colonodomicilio.html',
                      {'formulario': form, 'accion': 'Nuevo', 'colonodomicilios': colonodomicilios})

    @login_required()
    @permission_required('domicilios.change_colonodomicilio', login_url='colonodomicilio')
    def obteneruno_js_colonodomicilio(context):
        id = context.POST.get('id',None)
        data = ColonoDomicilioView.obtener_json_colonodomicilios_carga(ColonoDomicilio.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.delete_colonodomicilio', login_url='colonodomicilio')
    def borrar_js_colonodomicilio(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            colonodomicilio = ColonoDomicilio.objects.get(pk=o)
            colonodomicilio.estatus = 0
            colonodomicilio.save()

        data = ColonoDomicilioView.obtener_json_colonodomicilios(ColonoDomicilio.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    @login_required()
    @permission_required('domicilios.view_colonodomicilio', login_url='colonodomicilio')
    def get_colono_domicilio(context):
        data = ColonoDomicilioView.obtener_json_colonodomicilios(ColonoDomicilio.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")

    
    def obtener_json_colonodomicilios(colonodomicilios):

        colonodomiciliosJson = []
        for colonodomicilio in colonodomicilios:
            colonodomiciliosJson.append(
                {
                    'id' : colonodomicilio.id,
                    'colono': str(colonodomicilio.colono),
                    'domicilio': str(colonodomicilio.domicilio),
                }
            )
        data = simplejson.dumps(colonodomiciliosJson)
        return data

    def obtener_json_colonodomicilios_filtro(colonodomicilios):
        
        colonodomiciliosJson = []
        for colonodomicilio in colonodomicilios:
            colonodomiciliosJson.append(
                {
                    'id' : colonodomicilio.id,
                    'colono': colonodomicilio.colono.id,
                    'domicilio_id': colonodomicilio.domicilio.id,
                    'domicilio': str(colonodomicilio.domicilio),
                }
            )
        data = simplejson.dumps(colonodomiciliosJson)
        return data

    def obtener_json_colonodomicilios_carga(colonodomicilios):
        
        colonodomiciliosJson = []
        for colonodomicilio in colonodomicilios:
            colonodomiciliosJson.append(
                {
            'id' : colonodomicilio.id,
            'colono': colonodomicilio.colono.id,
            'domicilio': colonodomicilio.domicilio.id,

                    
                }
            )
        data = simplejson.dumps(colonodomiciliosJson)
        return data


    

    @login_required()
    @permission_required('domicilios.add_colonodomicilio', login_url='colonodomicilio')
    def registrar_js_colonodomicilio(context):
        if (context.method == 'POST'):
            form = FormColonoDomicilio(context.POST)

            try:
                query="INSERT INTO domicilios_colonodomicilio"+\
					"(colono_id,domicilio_id,estatus)"+\
					"VALUES ("
                max = ColonoDomicilio.objects.raw("SELECT MAX(id) as id FROM domicilios_colonodomicilio")[0].id
                new_id = max + 1 if (max != None) else 1          
                colono  = context.POST.get('colono',None)
                domicilio  = context.POST.get('domicilio',None)

                query= query+\
                    str(colono)+","+\
                    str(domicilio)+","+\
					"1);"
                print(query)
                cursor = connection.cursor()
                cursor.execute(query)
                row = cursor.fetchone()
				
                data = ColonoDomicilioView.obtener_json_colonodomicilios(ColonoDomicilio.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
				
            except Exception as e:
                
                print(e)
                data = simplejson.dumps([{'error': 'alerta'},{'alerta','Los datos ingresados son incorrectos'} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error': 'error'},{'descripcion':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.add_colonodomicilio', login_url='colonodomicilio')
    def actualizar_js_colonodomicilio(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            colonodomicilio = ColonoDomicilio.objects.get(pk=id)
            form = FormColonoDomicilio(context.POST, instance=colonodomicilio)
            try:

                colono  = context.POST.get('colono',None)
                domicilio  = context.POST.get('domicilio',None)

                id = context.POST.get('id', None)
                query = "UPDATE domicilios_colonodomicilio SET " + \
                   "colono_id = "+ colono + ","+\
                   "domicilio_id = "+ domicilio + ""+\
                        " WHERE id = "+id+";"
                print(query)
                cursor = connection.cursor()
                cursor.execute(query)
                row = cursor.fetchone()
                data = ColonoDomicilioView.obtener_json_colonodomicilios(ColonoDomicilio.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            except Exception as e:
                print(e)
                data = simplejson.dumps([{'error': 'alerta'},{'alerta','Los datos ingresados son incorrectos'}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'error'}, {'descripcion': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")