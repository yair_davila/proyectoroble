from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from domicilios.modelos.domicilioModel import Domicilio
from domicilios.modelos.calleModel import Calle
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from domicilios.formularios.formDomicilio import FormDomicilio, FormDomicilioFilter
from domicilios.utils import  get_numero_domicilio

class DomicilioView():

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicilio')
    def nuevo_domicilio(context):
        form = FormDomicilio()
        formFilter = FormDomicilioFilter(auto_id='filter_%s')
        domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[0:10]
        totales = Domicilio.objects.filter(estatus=1).count()
        totales = range(0, totales, 10)
        return render(context, 'nuevo_domicilio.html',
                      {'formulario': form,
                      'formFilter':formFilter,
                       'accion': 'Nuevo', 'domicilios': domicilios,'totales':totales})

    def calendario(context):
        return render(context, 'calendario.html')

    def obtener_diez(context):
        id = int(context.GET.get('id', None))
        id = (id*10)
        print(str(id-10)+" "+str(id))

        domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[id-10:id]
        data = DomicilioView.obtener_json_domicilios3(domicilios)
        return HttpResponse(data, content_type="application/json")

    def obtener_todos(context):
        domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')
        data = DomicilioView.obtener_json_domicilios3(domicilios)
        return HttpResponse(data, content_type="application/json")

    def obtener_por_calle(context):
        id = int(context.GET.get('id', None))
        domicilios = Domicilio.objects.filter(estatus=1, calle=id)
        data = DomicilioView.obtener_json_domicilios(domicilios)
        return HttpResponse(data, content_type="application/json")



    @login_required()
    @permission_required('domicilios.delete_domicilio', login_url='domicilio')
    def borrar_js_domicilio(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            domicilio = Domicilio.objects.get(pk=o)
            domicilio.estatus = 0
            domicilio.save()
        domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[0:10]
        data = DomicilioView.obtener_json_domicilios(domicilios)
        return HttpResponse(data, content_type="application/json")
    

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicilio')
    def filtrar_js_domicilio (context):
        if (context.method == 'GET'):
            try:
                calle = context.GET.get('calle', None)
                tipo =context.GET.get('tipo_domicilio', None)
                if (calle != ''):
                    if(tipo != ''):
                        data = DomicilioView.obtener_json_domicilios(
                            Domicilio.objects.filter(
                                estatus=1,
                                calle=calle,
                                tipo_domicilio=tipo).order_by(
                                '-num_domicilio')
                        )
                    else:
                        data = DomicilioView.obtener_json_domicilios(
                            Domicilio.objects.filter(
                                estatus=1,
                                calle=calle).order_by(
                                '-num_domicilio'
                            )
                        )
                    return HttpResponse(data, content_type="application/json")
            except:
                print("error")
        domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[0:10]
        data = DomicilioView.obtener_json_domicilios(domicilios)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicilio')
    def registrar_js_domicilio(context):
        if (context.method == 'POST'):
            form = FormDomicilioFilter(context.POST)
            if form.is_valid():
                domicilio = form.save(commit=False)
                domicilio.num_domicilio = str(form.data['num_domicilio']) + str(form.data['letra'])
                domicilio.usuario_creacion = context.user
                domicilio.fecha_creacion = date.today()
                domicilio.save()
                domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[0:10]
                data = DomicilioView.obtener_json_domicilios(domicilios)
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.add_domicilio', login_url='domicilio')
    def actualizar_js_domicilio(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            domicilio = Domicilio.objects.get(pk=id)

            form = FormDomicilio(context.POST, instance=domicilio)
            if form.is_valid():
                domicilio = form.save()
                domicilio.save()
                domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[0:10]
                data = DomicilioView.obtener_json_domicilios(domicilios)

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.change_domicilio', login_url='domicilio')
    def obteneruno_js_domicilio(context):
        id = context.POST.get('id',None)
        data = DomicilioView.obtener_json_domicilios2(Domicilio.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")


    @login_required()
    @permission_required('domicilios.change_domicilio', login_url='domicilio')
    def consultaruno_js_domicilio(context):
        nombre = context.POST.get('calle',None)
        numero = context.POST.get('numero', None)
        calle = Calle.objects.filter(nombre__contains= nombre)
        datos = Domicilio.objects.filter(calle = calle[0].id, num_domicilio= numero)
        data = DomicilioView.obtener_json_domicilios(datos)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    def consultar_casas (context):
        td = TipoDomicilio.objects.get(descripcion='CASA HABITACION')
        domicilios = Domicilio.objects.filter(tipo_domicilio=td.id, estatus=1)  
        domiciliosJson = []
        for domicilio in domicilios:

            arreglo = str(domicilio.calle.nombre).strip().split(' ')
            calle = arreglo[len(arreglo)-1]
            domiciliosJson.append(
                {
                    'id': str(calle)+"-"+domicilio.num_domicilio
                }
            )
        data = simplejson.dumps(domiciliosJson)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.delete_domicilio', login_url='domicilio')
    def borrar_js_domicilio(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            domicilio = Domicilio.objects.get(pk=o)
            domicilio.estatus = 0
            domicilio.save()
        domicilios = Domicilio.objects.filter(estatus=1).order_by('-calle__id','num_domicilio')[0:10]
        data = DomicilioView.obtener_json_domicilios(domicilios)
        return HttpResponse(data, content_type="application/json")

    def get_domicilios(context):
        data = DomicilioView.obtener_json_domicilios(Domicilio.objects.all())
        return HttpResponse(data, content_type="application/json")

    def obtener_json_domicilios(domicilios):

        domiciliosJson = []
        for domicilio in domicilios:
            domiciliosJson.append(
                {
            'id' : domicilio.id,
        	'calle': str(domicilio.calle),
        	'num_domicilio': domicilio.num_domicilio,
            'titular': "" if domicilio.titular==None else str(domicilio.titular),
        	'tipo_domicilio': str(domicilio.tipo_domicilio.id)
                }
            )
        data = simplejson.dumps(domiciliosJson)
        return data

    def obtener_json_domicilios3(domicilios):

        domiciliosJson = []
        for domicilio in domicilios:
            
                
            domiciliosJson.append(
                {
                    'id': domicilio.id,
                    'calle': str(domicilio.calle.nombre),
                    'num_domicilio': str(domicilio.num_domicilio),
                    'tipo_domicilio': str(domicilio.tipo_domicilio.descripcion),
                    'titular': str(domicilio.titular) if domicilio.titular!=None else ""

                }
            )
        data = simplejson.dumps(domiciliosJson)
        return data

    def obtener_json_domicilios2(domicilios):

        domiciliosJson = []
        for domicilio in domicilios:
            arreglo=get_numero_domicilio(domicilio.num_domicilio)
            titular=None
            if( domicilio.titular != None ):
                titular=domicilio.titular.id
            domiciliosJson.append(
                {
                    'id': domicilio.id,
                    'calle': str(domicilio.calle.id),
                    'num_domicilio': arreglo[0],
                    'tipo_domicilio': str(domicilio.tipo_domicilio.id),
                    'letra':arreglo[1],
                    'titular': titular

                }
            )
        data = simplejson.dumps(domiciliosJson)
        return data
