from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from domicilios.formularios.formTipoDomicilio import FormTipoDomicilio

class TipoDomicilioView():

    @login_required()
    @permission_required('domicilios.add_tipodomicilio', login_url='tipodomicilio')
    def nuevo_tipodomicilio(context):
        form = FormTipoDomicilio()
        tipodomicilios = TipoDomicilio.objects.filter(estatus=1)
        return render(context, 'nuevo_tipodomicilio.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tipodomicilios': tipodomicilios})

    @login_required()
    @permission_required('domicilios.add_tipodomicilio', login_url='tipodomicilio')
    def registrar_js_tipodomicilio(context):
        if (context.method == 'POST'):
            form = FormTipoDomicilio(context.POST)
            if form.is_valid():
                tipodomicilio = form.save(commit=False)
                tipodomicilio.usuario_creacion = context.user
                tipodomicilio.fecha_creacion = date.today()
                tipodomicilio.save()
                data = TipoDomicilioView.obtener_json_tipodomicilios(TipoDomicilio.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.add_tipodomicilio', login_url='tipodomicilio')
    def actualizar_js_tipodomicilio(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tipodomicilio = TipoDomicilio.objects.get(pk=id)

            form = FormTipoDomicilio(context.POST, instance=tipodomicilio)
            if form.is_valid():
                tipodomicilio = form.save()
                tipodomicilio.save()
                data = TipoDomicilioView.obtener_json_tipodomicilios(TipoDomicilio.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.change_tipodomicilio', login_url='tipodomicilio')
    def obteneruno_js_tipodomicilio(context):
        id = context.POST.get('id',None)
        data = TipoDomicilioView.obtener_json_tipodomicilios(TipoDomicilio.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('domicilios.delete_tipodomicilio', login_url='tipodomicilio')
    def borrar_js_tipodomicilio(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tipodomicilio = TipoDomicilio.objects.get(pk=o)
            tipodomicilio.estatus = 0
            tipodomicilio.save()

        data = TipoDomicilioView.obtener_json_tipodomicilios(TipoDomicilio.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_tipodomicilios(tipodomicilios):
        
        tipodomiciliosJson = []
        for tipodomicilio in tipodomicilios:
            tipodomiciliosJson.append(
                {
            'id' : tipodomicilio.id,
        	'descripcion': tipodomicilio.descripcion,
        	'habitable': tipodomicilio.habitable,

                    
                }
            )
        data = simplejson.dumps(tipodomiciliosJson)
        return data
