from domicilios.vistas.colonoDomicilioView import ColonoDomicilioView
from domicilios.vistas.domicilioView import DomicilioView
from domicilios.vistas.tipoDomicilioView import TipoDomicilioView
from domicilios.vistas.calleView import CalleView
from django.urls import path

lista = [
	path('calle_nuevo', CalleView.nuevo_calle,
         name='nuevo_calle'),

    path('calle_todas', CalleView.todas_calle,
         name='todas_calle'),
    path('domicilios_10', DomicilioView.obtener_diez,
         name='10dom'),

    path('domicilios_todos', DomicilioView.obtener_todos,
         name='all_dom'),

    path('calle_registrar', CalleView.registrar_js_calle,
         name='registrar_calle'),
    path('calle_actualizar', CalleView.actualizar_js_calle,
         name='actualizar_calle'),
    path('calle_obteneruno', CalleView.obteneruno_js_calle,
         name='obteneruno_calle'),
    path('calle_borrar/',
         CalleView.borrar_js_calle, name='borrar_calle'),

	path('tipodomicilio_nuevo', TipoDomicilioView.nuevo_tipodomicilio,
         name='nuevo_tipodomicilio'),
    path('tipodomicilio_registrar', TipoDomicilioView.registrar_js_tipodomicilio,
         name='registrar_tipodomicilio'),
    path('tipodomicilio_actualizar', TipoDomicilioView.actualizar_js_tipodomicilio,
         name='actualizar_tipodomicilio'),
    path('tipodomicilio_obteneruno', TipoDomicilioView.obteneruno_js_tipodomicilio,
         name='obteneruno_tipodomicilio'),
    path('tipodomicilio_borrar/',
         TipoDomicilioView.borrar_js_tipodomicilio, name='borrar_tipodomicilio'),

	path('domicilio_nuevo', DomicilioView.nuevo_domicilio,
         name='nuevo_domicilio'),
    path('domicilios', DomicilioView.get_domicilios,
         name='domicilios'),
    path('domicilio_registrar', DomicilioView.registrar_js_domicilio,
         name='registrar_domicilio'),
    path('domicilio_filtrar', DomicilioView.filtrar_js_domicilio,
         name='filtrar_domicilio'),
    path('domicilio_actualizar', DomicilioView.actualizar_js_domicilio,
         name='actualizar_domicilio'),
    path('calendario', DomicilioView.calendario,
         name='calendario'),
    path('domicilio_obteneruno', DomicilioView.obteneruno_js_domicilio,
         name='obteneruno_domicilio'),
    path('domicilio_consultaruno', DomicilioView.consultaruno_js_domicilio,
         name='consultaruno_domicilio'),
    path('domicilio_casas', DomicilioView.consultar_casas,
         name='casas_domicilio'),
    path('domicilio_borrar/',
         DomicilioView.borrar_js_domicilio, name='borrar_domicilio'),

	path('colonodomicilio_nuevo', ColonoDomicilioView.nuevo_colonodomicilio,
         name='nuevo_colonodomicilio'),
    path('colonodomicilio_registrar', ColonoDomicilioView.registrar_js_colonodomicilio,
         name='registrar_colonodomicilio'),
    path('colonodomicilio_actualizar', ColonoDomicilioView.actualizar_js_colonodomicilio,
         name='actualizar_colonodomicilio'),
    path('colonodomicilio_obteneruno', ColonoDomicilioView.obteneruno_js_colonodomicilio,
         name='obteneruno_colonodomicilio'),
    path('colonodomicilio_borrar/',
         ColonoDomicilioView.borrar_js_colonodomicilio, name='borrar_colonodomicilio'),
    path('colonodomicilio_filtro_colono',
         ColonoDomicilioView.filtrar_por_colono, name="filtro_domicilio_colono"),
     
     path('get_colono_domicilio',
         ColonoDomicilioView.get_colono_domicilio, name="colono_domicilio_obtener"),
    
    
    path('domicilios_por_calle', DomicilioView.obtener_por_calle, name='obtener_por_calle'),

    
]
urlpatterns = lista