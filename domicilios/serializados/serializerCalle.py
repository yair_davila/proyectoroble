from rest_framework import serializers
from domicilios.modelos.calleModel import Calle

class CalleSerializer(serializers.ModelSerializer):
    class Meta:
        model: Calle
        fields = '__all__'