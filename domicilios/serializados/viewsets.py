from domicilios.modelos.calleModel import Calle
from rest_framework import viewsets
from domicilios.serializados.serializerCalle import CalleSerializer

class CalleViewSet(viewsets.ModelViewSet):
    queryset  = Calle.objects.all()
    serializer_class = CalleSerializer