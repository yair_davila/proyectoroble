def leer(nombre):
    archivo = open(nombre, "r")
    texto = ''
    for linea in archivo.readlines():
        texto = texto+linea
    return texto

def escribir(cadena, nombre):
    archivo = open(nombre, "w")
    archivo.write(cadena)
    archivo.close()

def principal(modulo,minuscula,mayuscula):
    texto=leer("base.py")
    texto=texto.replace("~minuscula~",minuscula)
    texto=texto.replace("~mayuscula~",mayuscula)
    texto=texto.replace("~modulo~",modulo)
    escribir(texto,modulo+"/vistas/"+minuscula+"View.py")

#principal('areasComunes','areacomun','AreaComun')
#principal('areasComunes','tipoevento','TipoEvento')
#principal('asociaciones','puesto','Puesto')
#principal('asociaciones','asociacion','Asociacion')
#principal('domicilios','domicilio','Domicilio')
#principal('domicilios','calle','Calle')
#principal('egresos','egreso','Egreso')
#principal('egresos','tipoegreso','TipoEgreso')
#principal('ingresos','ingreso','Ingreso')
#principal('mascotas','animal','Animal')
#principal('mascotas','raza','Raza')
#principal('mascotas','mascota','Macota')
#principal('mascotas','colonoMascota','ColonoMascota')
#principal('sancion','reporteVecino','ReporteVecino')
#principal('sancion','sancion','Sancion')
#principal('usuarios','colono','Colono')
#principal('vehiculos','vehiculo','Vehiculo')
#principal('vehiculos','colonovehiculo','ColonoVehiculo')
principal('visitantes','motivosrestriccion','MotivosRestriccion')
principal('visitantes','personaajena','PersonaAjena')
principal('visitantes','tipopersonaajena','TipoPersonaAjena')
principal('visitantes','vehiculovisitante','VehiculoVisitante')
principal('visitantes','visitaautorizada','VisitaAutorizada')
