from usuarios.modelos.colonoModel import Colono
from django.db import models
from django.contrib.auth.models import User

class Administracion(models.Model):
    nombre = models.CharField(
        'NOMBRE', max_length=80)
    fecha_inicio = models.DateTimeField('FECHA_INICIO')
    fecha_fin = models.DateTimeField('FECHA_FIN', blank=True, null=True)
    fecha_creacion = models.DateTimeField('CREACION_ADMINISTRACION')
    fecha_edicion = models.DateTimeField('EDICION_ADMINISTRACION', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'CREADOR_ADMINISTRACION')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'EDITOR_ADMINISTRACION', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.nombre.__str__()


    def imprimir(self):
        return str(self.nombre)+','+str(self.fecha_inicio)+','+\
            str(self.fecha_fin)
