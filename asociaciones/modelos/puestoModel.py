from django.db import models
from django.contrib.auth.models import User

class Puesto(models.Model):
    descripcion = models.CharField(
        'DESCRIPCION', max_length=80, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Puesto')
    fecha_edicion = models.DateTimeField('Edicion_Puesto', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Puesto')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Puesto', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.descripcion


    def imprimir(self):
        return str(self.descripcion)

    class Meta:
        app_label = "asociaciones"