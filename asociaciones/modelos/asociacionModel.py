from asociaciones.modelos.puestoModel import Puesto
from usuarios.modelos.colonoModel import Colono
from django.db import models
from django.contrib.auth.models import User
from asociaciones.modelos.administracionModel import Administracion

class Asociacion(models.Model):
    administracion = models.ForeignKey(
        Administracion, models.DO_NOTHING, 'ID_COLONO_ADMINISTRACION')
    colono = models.ForeignKey(
        Colono, models.DO_NOTHING, 'ID_COLONO_ASOCIACION')
    puesto = models.ForeignKey(Puesto, models.DO_NOTHING, 'ID_PUESTO_ASOCIACION')
    fecha_inicio = models.DateTimeField('FECHA_INICIO')
    fecha_fin = models.DateTimeField('FECHA_FIN', blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Asociacion')
    fecha_edicion = models.DateTimeField('Edicion_Asociacion', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Asociacion')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Asociacion', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.colono.__str__()+" "+self.puesto.__str__()


    def imprimir(self):
        return str(self.colono.id)+','+str(self.administracion.id)+','+\
            str(self.puesto)+','+str(self.fecha_inicio)+','+\
            str(self.fecha_fin)
