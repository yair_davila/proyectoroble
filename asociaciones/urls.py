from asociaciones.modelos.asociacionModel import Asociacion
from asociaciones.vistas.asociacionView import AsociacionView
from asociaciones.vistas.administracionView import AdministracionView
from asociaciones.modelos.puestoModel import Puesto
from asociaciones.vistas.puestoView import PuestoView
from django.urls import path

urlpatterns = [
	path('puesto_nuevo', PuestoView.nuevo_puesto,
         name='nuevo_puesto'),
    path('puesto_registrar', PuestoView.registrar_js_puesto,
         name='registrar_puesto'),
    path('puesto_actualizar', PuestoView.actualizar_js_puesto,
         name='actualizar_puesto'),
    path('puesto_obteneruno', PuestoView.obteneruno_js_puesto,
         name='obteneruno_puesto'),
    path('puesto_borrar/',
         PuestoView.borrar_js_puesto, name='borrar_puesto'),

	path('asociacion_nuevo', AsociacionView.nuevo_asociacion,
         name='nuevo_asociacion'),
    path('asociacion_registrar', AsociacionView.registrar_js_asociacion,
         name='registrar_asociacion'),
    path('asociacion_actualizar', AsociacionView.actualizar_js_asociacion,
         name='actualizar_asociacion'),
    path('asociacion_obteneruno', AsociacionView.obteneruno_js_asociacion,
         name='obteneruno_asociacion'),
    path('asociacion_borrar/',
         AsociacionView.borrar_js_asociacion, name='borrar_asociacion'),

    path('administracion_nuevo', AdministracionView.nuevo_administracion,
         name='nuevo_administracion'),
    path('administracion_registrar', AdministracionView.registrar_js_administracion,
         name='registrar_administracion'),
    path('administracion_actualizar', AdministracionView.actualizar_js_administracion,
         name='actualizar_administracion'),
    path('administracion_obteneruno', AdministracionView.obteneruno_js_administracion,
         name='obteneruno_administracion'),
    path('administracion_borrar/',
         AdministracionView.borrar_js_administracion, name='borrar_administracion'),


]