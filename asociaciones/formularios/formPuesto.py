from asociaciones.modelos.puestoModel import Puesto
from asociaciones.validador import validador_cadenas
from django import forms

class FormPuesto (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])

    class Meta:
        model = Puesto
        fields = ('descripcion',)


