from asociaciones.modelos.asociacionModel import Asociacion
from asociaciones.modelos.puestoModel import Puesto
from asociaciones.modelos.administracionModel import Administracion
from usuarios.modelos.colonoModel import Colono
from django import forms

class FormAsociacion (forms.ModelForm):

    puesto= forms.ModelChoiceField(queryset=Puesto.objects.all())
    colono= forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    administracion = forms.ModelChoiceField(queryset=Administracion.objects.filter(estatus=1))
    fecha_inicio = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input',
        }))
    fecha_fin = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input',
        }))

    class Meta:
        model = Asociacion
        fields = ('administracion','colono', 'puesto', 'fecha_inicio', 'fecha_fin')
