from asociaciones.modelos.administracionModel import Administracion
from django import forms
from asociaciones.validador import validador_cadenas

class FormAdministracion (forms.ModelForm):

    nombre = forms.CharField(validators=[validador_cadenas])
    fecha_inicio = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input',
        }))
    fecha_fin = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input',
        }))

    class Meta:
        model = Administracion
        fields = ('nombre', 'fecha_inicio', 'fecha_fin')
