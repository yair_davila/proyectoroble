from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from asociaciones.modelos.puestoModel import Puesto
from usuarios.modelos.colonoModel import Colono,User
import simplejson
from datetime import date
from asociaciones.modelos.asociacionModel import Asociacion
from asociaciones.formularios.formAsociacion import FormAsociacion
from datetime import datetime
from django.utils import timezone
from django.db import connection


class AsociacionView():

    @login_required()
    @permission_required('asociaciones.add_asociacion', login_url='asociacion')
    def nuevo_asociacion(context):
        form = FormAsociacion()
        asociacions = Asociacion.objects.filter(estatus=1)
        return render(context, 'nuevo_asociacion.html',
                      {'formulario': form, 'accion': 'Nuevo', 'asociacions': asociacions})

    @login_required()
    @permission_required('asociaciones.change_asociacion', login_url='asociacion')
    def obteneruno_js_asociacion(context):
        id = context.POST.get('id',None)
        data = AsociacionView.obtener_json_asociacions_carga(Asociacion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.delete_asociacion', login_url='asociacion')
    def borrar_js_asociacion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            asociacion = Asociacion.objects.get(pk=o)
            asociacion.estatus = 0
            asociacion.save()

        data = AsociacionView.obtener_json_asociacions(Asociacion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    

    @login_required()
    @permission_required('asociaciones.add_asociacion', login_url='asociacion')
    def registrar_js_asociacion(context):
        if (context.method == 'POST'):
            form = FormAsociacion(context.POST)
            if form.is_valid():
                asociacion = form.save(commit=False)
                asociacion.usuario_creacion = context.user
                asociacion.fecha_creacion = date.today()
                asociacion.save()
                data = AsociacionView.obtener_json_asociacions(Asociacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.add_asociacion', login_url='asociacion')
    def actualizar_js_asociacion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            asociacion = Asociacion.objects.get(pk=id)

            form = FormAsociacion(context.POST, instance=asociacion)
            if form.is_valid():
                asociacion = form.save()
                asociacion.save()
                data = AsociacionView.obtener_json_asociacions(Asociacion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.change_asociacion', login_url='asociacion')
    def obteneruno_js_asociacion(context):
        id = context.POST.get('id',None)
        data = AsociacionView.obtener_json_asociacions2(Asociacion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.delete_asociacion', login_url='asociacion')
    def borrar_js_asociacion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            asociacion = Asociacion.objects.get(pk=o)
            asociacion.estatus = 0
            asociacion.save()

        data = AsociacionView.obtener_json_asociacions(Asociacion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")


    def obtener_json_asociacions(asociacions=None):
        # Idioma "es-ES" (código para el español de España)

        if(asociacions==None):
            asociacions=Asociacion.objects.filter(estatus=1)

        asociacionsJson = []
        for asociacion in asociacions:

            asociacionsJson.append(
                {
                    'id':str(asociacion.id),

            'administracion': str(asociacion.administracion),
        	'colono': str(asociacion.colono),
        	'puesto': str(asociacion.puesto),
        	'fecha_inicio': str(asociacion.fecha_inicio.strftime('%d de %B de %Y')),
        	'fecha_fin': str(asociacion.fecha_fin.strftime('%d de %B de %Y')),
        	'estatus': asociacion.estatus,

                    
                }
            )
        data = simplejson.dumps(asociacionsJson)
        return data

    def obtener_json_asociacions2(asociacions):

        asociacionsJson = []
        for asociacion in asociacions:
            asociacionsJson.append(
                {
                    'id': str(asociacion.id),
                    'colono': asociacion.colono_id,
                    'administracion': asociacion.administracion.id,
                    'puesto': asociacion.puesto_id,
                    'fecha_inicio': str(asociacion.fecha_inicio)[:10],
                    'fecha_fin': str(asociacion.fecha_fin)[:10],
                    'estatus': asociacion.estatus,

                }
            )
        data = simplejson.dumps(asociacionsJson)
        return data
