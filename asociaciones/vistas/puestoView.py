from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from selenium.webdriver.support.expected_conditions import element_selection_state_to_be
from datetime import date
from asociaciones.modelos.puestoModel import Puesto
from asociaciones.formularios.formPuesto import FormPuesto

class PuestoView():

    @login_required()
    @permission_required('asociaciones.add_puesto', login_url='puesto')
    def nuevo_puesto(context):
        form = FormPuesto()
        puestos = Puesto.objects.filter(estatus=1)
        print("MANDA LLAMAR NUEVO")
        return render(context, 'nuevo_puesto.html',
                      {'formulario': form, 'accion': 'Nuevo', 'puestos': puestos})

    @login_required()
    @permission_required('asociaciones.add_puesto', login_url='puesto')
    def registrar_js_puesto(context):
        if (context.method == 'POST'):
            form = FormPuesto(context.POST)
            if form.is_valid():
                puesto = form.save(commit=False)
                puesto.usuario_creacion = context.user
                puesto.fecha_creacion = date.today()
                puesto.save()
                data = PuestoView.obtener_json_puestos(Puesto.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.add_puesto', login_url='puesto')
    def actualizar_js_puesto(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            puesto = Puesto.objects.get(pk=id)

            form = FormPuesto(context.POST, instance=puesto)
            if form.is_valid():
                puesto = form.save()
                puesto.save()
                print('guardo formulario')
                data = PuestoView.obtener_json_puestos(Puesto.objects.filter(estatus=1))
                print('regresare datos')
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.change_puesto', login_url='puesto')
    def obteneruno_js_puesto(context):
        id = context.POST.get('id',None)
        data = PuestoView.obtener_json_puestos(Puesto.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('asociaciones.delete_puesto', login_url='puesto')
    def borrar_js_puesto(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            puesto = Puesto.objects.get(pk=o)
            puesto.estatus = 0
            puesto.save()

        data = PuestoView.obtener_json_puestos(Puesto.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")


    def obtener_activos(numero):
        puestos=Puesto.objects.filter(estatus=numero)
        puestosJson = "CHOICE  = ("
        for puesto in puestos:
            puestosJson= puestosJson+\
                    "("+str(puesto.id) +"," +puesto.descripcion+"),"


        return puestosJson[:-1]+")"


    def obtener_json_puestos(puestos):
        
        puestosJson = []
        for puesto in puestos:
            puestosJson.append(
                {
            'id' : puesto.id,
        	'descripcion': puesto.descripcion,

                    
                }
            )
        data = simplejson.dumps(puestosJson)
        return data
