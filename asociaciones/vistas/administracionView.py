from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from asociaciones.modelos.puestoModel import Puesto
from usuarios.modelos.colonoModel import Colono,User
import simplejson
from datetime import date
from asociaciones.modelos.administracionModel import Administracion
from asociaciones.formularios.formAdministracion import FormAdministracion
from datetime import datetime
from django.utils import timezone
from django.db import connection


class AdministracionView():

    @login_required()
    @permission_required('administraciones.add_administracion', login_url='administracion')
    def nuevo_administracion(context):
        form = FormAdministracion()
        administracions = Administracion.objects.filter(estatus=1)
        return render(context, 'nuevo_administracion.html',
                      {'formulario': form, 'accion': 'Nuevo', 'administracions': administracions})

    @login_required()
    @permission_required('administraciones.change_administracion', login_url='administracion')
    def obteneruno_js_administracion(context):
        id = context.POST.get('id',None)
        data = AdministracionView.obtener_json_administracions_carga(Administracion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('administraciones.delete_administracion', login_url='administracion')
    def borrar_js_administracion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            administracion = Administracion.objects.get(pk=o)
            administracion.estatus = 0
            administracion.save()

        data = AdministracionView.obtener_json_administracions(Administracion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    

    @login_required()
    @permission_required('administraciones.add_administracion', login_url='administracion')
    def registrar_js_administracion(context):
        if (context.method == 'POST'):
            form = FormAdministracion(context.POST)
            if form.is_valid():
                administracion = form.save(commit=False)
                administracion.usuario_creacion = context.user
                administracion.fecha_creacion = date.today()
                administracion.save()
                data = AdministracionView.obtener_json_administracions(Administracion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('administraciones.add_administracion', login_url='administracion')
    def actualizar_js_administracion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            administracion = Administracion.objects.get(pk=id)

            form = FormAdministracion(context.POST, instance=administracion)
            if form.is_valid():
                administracion = form.save()
                administracion.save()
                data = AdministracionView.obtener_json_administracions(Administracion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('administraciones.change_administracion', login_url='administracion')
    def obteneruno_js_administracion(context):
        id = context.POST.get('id',None)
        data = AdministracionView.obtener_json_administracions2(Administracion.objects.filter(pk=id))
        print(data)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('administraciones.delete_administracion', login_url='administracion')
    def borrar_js_administracion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            administracion = Administracion.objects.get(pk=o)
            administracion.estatus = 0
            administracion.save()

        data = AdministracionView.obtener_json_administracions(Administracion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")


    def obtener_json_administracions(administracions=None):
        # Idioma "es-ES" (código para el español de España)

        if(administracions==None):
            administracions=Administracion.objects.filter(estatus=1)

        administracionsJson = []
        for administracion in administracions:

            administracionsJson.append(
                {
                    'id':str(administracion.id),
        	'nombre': str(administracion.nombre),
        	'fecha_inicio': str(administracion.fecha_inicio.strftime('%d de %B de %Y')),
        	'fecha_fin': str(administracion.fecha_fin.strftime('%d de %B de %Y')),
        	'estatus': administracion.estatus,

                    
                }
            )
        data = simplejson.dumps(administracionsJson)
        return data

    def obtener_json_administracions2(administracions):

        administracionsJson = []
        for administracion in administracions:
            administracionsJson.append(
                {
                    'id': str(administracion.id),
                    'nombre': administracion.nombre,
                    'fecha_inicio': str(administracion.fecha_inicio)[:10],
                    'fecha_fin': str(administracion.fecha_fin)[:10],
                    'estatus': administracion.estatus,

                }
            )
        data = simplejson.dumps(administracionsJson)
        return data
