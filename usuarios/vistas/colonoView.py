from django.db import connection
from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.contrib.auth.models import User
import simplejson
from datetime import date
from django.db import connection
from usuarios.modelos.colonoModel import Colono
from usuarios.utils import generador_user,generador_password_random,enviar_portercero
from usuarios.formularios.formColonoYDomicilio import FormColonoYDomiciliocilio
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio,Domicilio
from django.contrib.auth.models import Group

class ColonoView():

    @login_required()
    def perfil_colono(context):
        return render(context, 'principal_colono.html')

    @login_required()
    def lista_colono(context):
        
        """
        colonos = Colono.objects.all()
        for c in colonos:
            c.is_active=0
            c.save()
        """
        #.exclude(email = "noaplica@noaplica.com")
        colonos = Colono.objects.filter(estatus=1,is_active=0).order_by('first_name')
        return render(context, 'colonos.html',
                      {'colonos': colonos})


    @login_required()
    def enviar_tercero(context):
        print("-"*100)
        try:
            parametro = context.GET.get(
                    'id', None)
            colonoEnCuestion = Colono.objects.get(id=parametro)
            if(colonoEnCuestion.email != None
                    and colonoEnCuestion.email != 'noaplica@noaplica.com'
            ):
                enviar_portercero(
                    colonoEnCuestion.email,
                    colonoEnCuestion.first_name+" "+colonoEnCuestion.last_name,
                    colonoEnCuestion.username,
                    colonoEnCuestion.password
                )
            
            colonoEnCuestion.is_active=1
            colonoEnCuestion.save()
            
        except:
            print("error")
        print("-"*100)
        
        return redirect('nuevo_colono')


    @login_required()
    def consultar_js_titular(context):
        titular = '0'
        domicilio = context.POST.get('domicilio', None)

        if (domicilio!='' or domicilio!=0 ):
            titular = Domicilio.objects.get(id=domicilio).titular

            if(titular != None):
                titular='1'
        colonosJson = [{'titular': titular}]
        data = simplejson.dumps(colonosJson)
        return HttpResponse(data, content_type="application/json")
    
    def my_custom_sql():
        c = connection.cursor()
        try:
            c.execute('SELECT COUNT(*) FROM usuarios_colono WHERE estatus=1')
            row = c.fetchone()
            print(row)
            print(row[0])
        finally:
            c.close()
        return row[0]

    @login_required()
    @permission_required('usuarios.add_colono', login_url='colono')
    def nuevo_colono(context):
        form = FormColonoYDomiciliocilio()
        colonos = Colono.objects.filter(estatus=1).order_by('first_name')[1:10]
        totales = Colono.objects.filter(estatus=1).order_by('first_name').count()
        """
        totales = ColonoView.my_custom_sql()
        """
        totales = range(0,totales,10)
        #print("falta return")

        return render(context, 'nuevo_colono.html',
                      {'formulario': form, 
                      'accion': 'Nuevo', 
                      'colonos': colonos,
                      'totales': totales}
                      )
                      
    def obtener_diez(context):
        id = int(context.GET.get('id', None))
        id = (id*10)
        print(str(id-10)+" "+str(id))
        colonos = Colono.objects.filter(estatus=1).order_by('first_name')[id-10:id]
        data = ColonoView.obtener_json_colonos(colonos)
        return HttpResponse(data, content_type="application/json")
    
    def obtener_todos(context):
        colonos = Colono.objects.filter(estatus=1).order_by('first_name')
        data = ColonoView.obtener_json_colonos(colonos)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('usuarios.add_colono', login_url='colono')
    def registrar_js_colono(context):
        if (context.method == 'POST'):
            print(context.POST)
            form = FormColonoYDomiciliocilio(context.POST)
            
            if form.is_valid():
                print('fue valido')
                new_colono = Colono()
                new_colono.first_name = context.POST.get(
                    'first_name', None).upper()
                new_colono.last_name = context.POST.get(
                    'last_name', None).upper()
                new_colono.amaterno = context.POST.get(
                    'amaterno', None).upper()
                new_colono.genero = context.POST.get('genero', None)
                new_colono.telefono_fijo = context.POST.get(
                    'telefono_fijo', None)
                new_colono.telefono_celular = context.POST.get(
                    'telefono_celular', None)
                new_colono.acepta_visitas = context.POST.get(
                    'acepta_visitas', None)
                new_colono.email = context.POST.get('email', None)
                password = generador_password_random()
                new_colono.password = password[0]
                new_colono.username = generador_user(new_colono)
                new_colono.confirmado = 0
                new_colono.usuario_creacion = context.user
                new_colono.fecha_creacion = date.today()
                new_colono.save()
                my_group = Group.objects.get(name='Colonos') 
                my_group.user_set.add(new_colono)
                print('el colono se guardo')

                colono_creado = Colono.objects.get(username=new_colono.username);

                print('antes de obtener domicilio')
                print('colono guardado')
                print(colono_creado)
                domicilio = context.POST.get('domicilio', None)
                domicilio_pertenece = Domicilio.objects.get(id=domicilio)
                print('domicilio')
                print(domicilio_pertenece)

                new_colono_domicilio = ColonoDomicilio()
                new_colono_domicilio.colono = colono_creado
                new_colono_domicilio.domicilio = domicilio_pertenece
                new_colono_domicilio.usuario_creacion = context.user
                new_colono_domicilio.fecha_creacion = date.today()
                print('antes de guardar relacion')
                new_colono_domicilio.save()
                print('despues de guardar relacion')

                es_titular = context.POST.get('titular', None)
                print('*'*100)
                print(str(es_titular) != '0')
                if(str(es_titular) != '0'):
                    domicilio_pertenece.titular=colono_creado
                    print("lllllll")
                    domicilio_pertenece.save()
                print('#####')
                lista = Colono.objects.filter(estatus=1).order_by('first_name')
                print(lista)
                data = ColonoView.obtener_json_colonos(lista)
                print(data)

                return HttpResponse(data, content_type="application/json")
            else:
                print(form._errors)
                print(str(form._errors))
                data = simplejson.dumps([{'error':str(form._errors) }, str(form._errors)])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'error'},{'descripcion':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('usuarios.add_colono', login_url='colono')
    def actualizar_js_colono(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            colono = Colono.objects.get(pk=id)

            form = FormColonoYDomiciliocilio(context.POST, instance=colono)
            if form.is_valid():
                colono = form.save()
                colono.save()
                data = ColonoView.obtener_json_colonos(Colono.objects.filter(estatus=1).order_by('first_name'))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': 'error'}, form._errors])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'error'}, {'descripcion': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('usuarios.change_colono', login_url='colono')
    def obteneruno_js_colono(context):
        id = context.POST.get('id',None)
        colono = Colono.objects.get(pk=id)
        colonosJson = []
        domicilio = 0
        calle = 0
        
        try:
            colono_domicilio = ColonoDomicilio.objects.get(colono=colono)
            domicilio = colono_domicilio.domicilio.id
            calle = colono_domicilio.domicilio.calle.id
        except:
            print("Sin domicilio")
        
        colonosJson.append(
                {
            'id':colono.id,
            'first_name' :colono.first_name,
            'last_name' : colono.last_name,
            'amaterno': colono.amaterno,
            'genero': colono.genero,
            'telefono_celular': colono.telefono_celular,
            'telefono_fijo': colono.telefono_fijo,
            'acepta_visitas': colono.acepta_visitas,
            'username':colono.username,
            'email':colono.email,
            'calle':calle
            ,'domicilio':domicilio
                }
            )
        data = simplejson.dumps(colonosJson)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('usuarios.delete_colono', login_url='colono')
    def borrar_js_colono(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            colono = Colono.objects.get(pk=o)
            colono.estatus = 0
            colono.save()

        data = ColonoView.obtener_json_colonos(Colono.objects.filter(estatus=1).order_by('first_name'))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_colonos(colonos):
        
        colonosJson = []
        for colono in colonos:
            colonosJson.append(
                {
            'id': colono.id,
            'first_name' :colono.first_name,
            'last_name' : colono.last_name,
        	'amaterno': colono.amaterno,
        	'genero': colono.genero,
        	'telefono_celular': colono.telefono_celular,
        	'telefono_fijo': colono.telefono_fijo,
        	'acepta_visitas': colono.acepta_visitas,
            'username':colono.username,
            'email':colono.email
                }
            )
        data = simplejson.dumps(colonosJson)
        return data
