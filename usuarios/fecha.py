import datetime


class Fecha():

    def getAno():
        now = datetime.datetime.now()
        return now.year

    def getAnos():
        now = datetime.datetime.now()
        ano = now.year-70
        anos = []
        while ano <= now.year:
            anos.append(ano)
            ano = ano+1
        return anos
