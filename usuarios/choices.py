GENDER_CHOICES = (
    ('-', 'No aplica'),
    ('M', 'Masculino'),
    ('F', "Femenino"),
)

PROFILE_CHOICES = (
    ("Administrador", "Administrador"),
    ("Árbitro", "Árbitro"),
    ("Entrenador", "Entrenador"),
    ("Usuario", "Usuario"),
)
VISITAS_CHOICES = (
    ('0', "No Acepto"),
    ('1', "Acepto"),
)
