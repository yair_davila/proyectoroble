from random import choice
from django.core.mail import EmailMessage
import django.contrib.auth.hashers
import random
from django.contrib.auth.models import User
#from django.contrib.auth.hashers import *
from mailjet_rest import Client
import os
api_key = '1e88a5d4a561cae27998ebd147500986'
api_secret = 'cdec97ac823adc18ebdc76e5462298b0'
mailjet = Client(auth=(api_key, api_secret), version='v3.1')

def enviar_portercero(email,nombre,user,passw):

    data = {
      'Messages': [
        {
          "From": {
            "Email": "benitomasjuarez@gmail.com",
            "Name": "Sistema Administrativo"
          },
          "To": [
            {
              "Email": email,
              "Name": nombre
            }
          ],
          "Subject": "Bienvenido Al Sistema",
          "TextPart": "Acceda a su cuenta",
          "HTMLPart": "<h1>Bienvenido al sistema</h1><br><p>Un administrador te a registrado<br>Ingresa a tu cuenta en la dirección privadadelroble.herokuapp.com<br>Con el usuario: " + user +"<br></p>"
        }
      ]
    }
    result = mailjet.send.create(data=data)
    print (result.status_code)
    print (result.json())


def tiene_permiso(user, permiso):
    if user.is_staff:
        return True

    if user.user_permissions.filter(name=permiso):
        return True

    grupos = user.groups.all()
    if len(grupos) > 0:
        if(grupos[0].permissions.filter(name=permiso)):
            return True

    return False


def enviar_mail(password, username, email):
    asunto = "Bienvenida A Privada"
    rollo = "Bienvenido al Sistema de Administración para tu Privada\n" +\
        "Uno de los Administradores del sistema te han creado una cuenta\n\n" +\
        "Usuario\t\""+username+"\"\n" +\
        "Entra a: http://148.217.200.124:8080 para comenzar a disfrutar tus beneficios"
"""
    destinatario = [email]
    email = EmailMessage(
        asunto,
        rollo,
        'sistemaprivada@gmail.com',
        [destinatario],
    )
    email.send()
"""

def generador_password_random():
    longitud = 18
    valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<=>@#%&+"
    cadena = ""
    cadena = cadena.join([choice(valores) for i in range(longitud)])
    salida = django.contrib.auth.hashers.make_password(cadena)
    return [salida, cadena]


def generador_user(colono):
    materno = ''
    paterno = colono.last_name.lower()
    nombre = colono.first_name.split(" ")[0].lower()

    if colono.amaterno is not None:
        if len(colono.amaterno) > 0:
            materno = colono.amaterno.lower()
            materno = materno[0]+materno[1]
    if len(paterno) > 1:
        paterno = paterno[0]+paterno[1]
    else:
        paterno = paterno
    new_username = nombre + paterno + materno
    return verificador(new_username,0,len(new_username))


def verificador(new_username,contador,longitud):
    ya_esta = User.objects.filter(username=new_username).exists()
    if ya_esta:
        new_username = new_username[:longitud] + str(contador)
        contador = contador+1
        return verificador(new_username,contador,longitud)
    else:
        return new_username


def make_user(first_name, last_name, amaterno):
    materno = amaterno
    paterno = last_name.lower()
    nombre = first_name.split(" ")[0].lower()

    if(materno!=None):
        if len(materno) > 1:
            materno=materno.lower()
            materno = materno[0]+materno[1]
        elif len(materno)==1:
            materno = materno.lower()
            materno = materno[0]
    else:
        materno=""
    if len(paterno) > 1:
        paterno = paterno[0]+paterno[1]
    else:
        paterno = paterno[0]
    new_username = nombre + paterno + materno
    return str(verificador(new_username, 0,len(new_username)))


def convertir_a_mayusculas(colono):
    colono.first_name = colono.first_name.upper()
    colono.last_name = colono.last_name.upper()
    colono.amaterno = colono.amaterno.upper()
    return colono
