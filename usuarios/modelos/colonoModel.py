from django.db import models
from usuarios.validador import validador_telefono
from django.contrib.auth.models import User

class Colono (User):
    amaterno = models.CharField(
        'Amaterno', max_length=35, blank=True, null=True)
    genero = models.CharField('Genero', max_length=1, blank=True, null=True)
    telefono_celular = models.CharField('Telefono_Celular', max_length=10, blank=True, null=True, validators=[
                                        validador_telefono])  # Field name made lowercase.
    telefono_fijo = models.CharField('Telefono_Fijo', max_length=10, blank=True, null=True, validators=[
                                     validador_telefono])  # Field name made lowercase.
    acepta_visitas = models.SmallIntegerField(
        'Acepta_Visitas', blank=True, null=True)

    confirmado = models.CharField(
        'confirmado', max_length=1, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Colono')
    fecha_edicion = models.DateTimeField('Edicion_Colono', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Colono')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Colono', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.first_name+" "+self.last_name

    def string(self):
        return str(self.first_name)+','+\
            str(self.last_name)+','+str(self.amaterno)

    def imprimir(self):
        return str(self.id)+','+str(self.first_name)+','+\
            str(self.last_name)+','+str(self.amaterno)+','+\
            str(self.genero)+','+str(self.telefono_celular)+','+\
            str(self.telefono_fijo)+','+str(self.acepta_visitas)+','+\
            str(self.email)

    class Meta:
    	app_label = "usuarios"