from usuarios.modelos.colonoModel import Colono
from usuarios.vistas.colonoView import ColonoView
from django.urls import path

urlpatterns = [
	path('colono_nuevo', ColonoView.nuevo_colono,
         name='nuevo_colono'),
    path('colono_lista', ColonoView.lista_colono,
         name='lista_colono'),
    path('colonos_10', ColonoView.obtener_diez,
         name='10colonos'),
         
     path('colonos_todos', ColonoView.obtener_todos,
         name='todos_colonos'),
    path('colono_registrar', ColonoView.registrar_js_colono,
         name='registrar_colono'),
    path('colono_actualizar', ColonoView.actualizar_js_colono,
         name='actualizar_colono'),
    path('colono_obteneruno', ColonoView.obteneruno_js_colono,
         name='obteneruno_colono'),
    path('colono_borrar/',
         ColonoView.borrar_js_colono, name='borrar_colono'),
    path('consulta_titular/',
         ColonoView.consultar_js_titular, name='titular_consulta'),
    path('enviar/',
         ColonoView.enviar_tercero, name='enviar'),
    path('perfil_colono', ColonoView.perfil_colono,name = 'perfil_colono'),

]
