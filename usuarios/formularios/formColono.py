from usuarios.modelos.colonoModel import Colono
from usuarios.validador import *
from django import forms
from usuarios.choices import GENDER_CHOICES, VISITAS_CHOICES

class FormColono (forms.ModelForm):
    first_name = forms.CharField(
        label='Nombre', max_length=50, validators=[validador_cadenas_2])
    last_name = forms.CharField(
        label='A Paterno', max_length=35, validators=[validador_cadenas_2])
    amaterno = forms.CharField(label='A Materno', max_length=35, validators=[
                               validador_cadenas_2], required=False)
    genero = forms.ChoiceField(choices=GENDER_CHOICES, required=False)
    email = forms.EmailField(label='E-mail', required=False)
    acepta_visitas = forms.ChoiceField(choices=VISITAS_CHOICES, required=False)
    telefono_fijo = forms.CharField(max_length=10, validators=[
                                    validador_telefono], required=False)
    telefono_celular = forms.CharField(max_length=10, validators=[
                                       validador_telefono], required=False)

    class Meta:
        model = Colono
        fields = (
            'first_name', 'last_name', 'amaterno', 'genero',
            'telefono_celular', 'telefono_fijo', 'acepta_visitas',
            'email'
        )


