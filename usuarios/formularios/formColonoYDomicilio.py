from usuarios.modelos.colonoModel import Colono
from domicilios.modelos.domicilioModel import Domicilio
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio
from domicilios.modelos.calleModel import Calle
from usuarios.choices import GENDER_CHOICES, VISITAS_CHOICES
from usuarios.validador import *
from django import forms
CHOICES = [ ('0', 'No Titular'),('1', 'Titular')]


class FormColonoYDomiciliocilio (forms.ModelForm):
    first_name = forms.CharField(
        label='Nombre', max_length=50, validators=[validador_cadenas_2])
    last_name = forms.CharField(
        label='A Paterno', max_length=35, validators=[validador_cadenas_2])
    amaterno = forms.CharField(label='A Materno', max_length=35, validators=[
                               validador_cadenas_2], required=False)
    genero = forms.ChoiceField(choices=GENDER_CHOICES, required=False)
    email = forms.EmailField(label='E-mail', required=False)
    acepta_visitas = forms.ChoiceField(choices=VISITAS_CHOICES, required=False)
    telefono_fijo = forms.CharField(max_length=10, validators=[
                                    validador_telefono], required=False)
    telefono_celular = forms.CharField(max_length=10, validators=[
                                       validador_telefono], required=False)
    calle = forms.ModelChoiceField(queryset=Calle.objects.filter(estatus=1))
    titular = forms.ChoiceField(choices=CHOICES, required=False)


    class Meta:
        model = Colono
        fields = (
            'first_name', 'last_name', 'amaterno', 'genero',
            'telefono_celular', 'telefono_fijo', 'acepta_visitas',
            'email'
        )
