PDFKit.configure do |config|
  if File.executable? '/app/bin/wkhtmltopdf'
    config.wkhtmltopdf = '/app/bin/wkhtmltopdf'
  end
end
