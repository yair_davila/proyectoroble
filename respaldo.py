def leer(nombre):
    archivo = open(nombre, "r")
    texto = ''
    for linea in archivo.readlines():
        if(linea.find("INSERT")>-1):
            texto = texto+linea
    return texto

def escribir(cadena, nombre):
    archivo = open(nombre, "w")
    archivo.write(cadena)
    archivo.close()

def principal():
    texto=leer("respaldo.sql")
    print(texto)

principal()