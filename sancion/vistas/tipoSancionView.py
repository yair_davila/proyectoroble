from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from sancion.modelos.tipoSancionModel import TipoSancion
from sancion.formularios.formTipoSancion import FormTipoSancion

class TipoSancionView():

    @login_required()
    @permission_required('sancion.add_tiposancion', login_url='tiposancion')
    def nuevo_tiposancion(context):
        form = FormTipoSancion()
        tiposancions = TipoSancion.objects.filter(estatus=1)
        return render(context, 'nuevo_tiposancion.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tiposancions': tiposancions})

    @login_required()
    @permission_required('sancion.add_tiposancion', login_url='tiposancion')
    def registrar_js_tiposancion(context):
        if (context.method == 'POST'):
            form = FormTipoSancion(context.POST)
            if form.is_valid():
                tiposancion = form.save(commit=False)
                tiposancion.usuario_creacion = context.user
                tiposancion.fecha_creacion = date.today()
                tiposancion.save()
                data = TipoSancionView.obtener_json_tiposancions(TipoSancion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('sancion.add_tiposancion', login_url='tiposancion')
    def actualizar_js_tiposancion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tiposancion = TipoSancion.objects.get(pk=id)

            form = FormTipoSancion(context.POST, instance=tiposancion)
            if form.is_valid():
                tiposancion = form.save()
                tiposancion.save()
                data = TipoSancionView.obtener_json_tiposancions(TipoSancion.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('sancion.change_tiposancion', login_url='tiposancion')
    def obteneruno_js_tiposancion(context):
        id = context.POST.get('id',None)
        data = TipoSancionView.obtener_json_tiposancions(TipoSancion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('sancion.delete_tiposancion', login_url='tiposancion')
    def borrar_js_tiposancion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tiposancion = TipoSancion.objects.get(pk=o)
            tiposancion.estatus = 0
            tiposancion.save()

        data = TipoSancionView.obtener_json_tiposancions(TipoSancion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_tiposancions(tiposancions):
        
        tiposancionsJson = []
        for tiposancion in tiposancions:
            tiposancionsJson.append(
                {
            'id' : tiposancion.id,
        	'descripcion': tiposancion.descripcion,
        	'monto': tiposancion.monto,

                    
                }
            )
        data = simplejson.dumps(tiposancionsJson)
        return data
