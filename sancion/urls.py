from sancion.modelos.reporteVecinoModel import ReporteVecino
from sancion.vistas.reporteVecinoView import ReporteVecinoView
from sancion.modelos.sancionModel import Sancion
from sancion.vistas.sancionView import SancionView
from sancion.modelos.tipoSancionModel import TipoSancion
from sancion.vistas.tipoSancionView import TipoSancionView
from django.urls import path

urlpatterns = [
	path('tiposancion_nuevo', TipoSancionView.nuevo_tiposancion,
         name='nuevo_tiposancion'),
    path('tiposancion_registrar', TipoSancionView.registrar_js_tiposancion,
         name='registrar_tiposancion'),
    path('tiposancion_actualizar', TipoSancionView.actualizar_js_tiposancion,
         name='actualizar_tiposancion'),
    path('tiposancion_obteneruno', TipoSancionView.obteneruno_js_tiposancion,
         name='obteneruno_tiposancion'),
    path('tiposancion_borrar/',
         TipoSancionView.borrar_js_tiposancion, name='borrar_tiposancion'),

	path('sancion_nuevo', SancionView.nuevo_sancion,
         name='nuevo_sancion'),
    path('sancion_registrar', SancionView.registrar_js_sancion,
         name='registrar_sancion'),
    path('sancion_actualizar', SancionView.actualizar_js_sancion,
         name='actualizar_sancion'),
    path('sancion_obteneruno', SancionView.obteneruno_js_sancion,
         name='obteneruno_sancion'),
    path('sancion_borrar/',
         SancionView.borrar_js_sancion, name='borrar_sancion'),

	path('reportevecino_nuevo', ReporteVecinoView.nuevo_reportevecino,
         name='nuevo_reportevecino'),
    path('reportevecino_editar/<int:id>', ReporteVecinoView.editar_reportevecino,
         name='editar_reportevecino'),
    path('reportevecino_borrar/',
         ReporteVecinoView.borrar_js_reportevecino, name='borrar_reportevecino'),


]