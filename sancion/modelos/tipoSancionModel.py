from django.db import models
from django.contrib.auth.models import User

class TipoSancion(models.Model):
    descripcion = models.CharField(
        'Descripción', max_length=50, blank=True, null=True)
    monto = models.DecimalField(
        'Monto', max_digits=8, decimal_places=2, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_TipoSancion')
    fecha_edicion = models.DateTimeField('Edicion_TipoSancion', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_TipoSancion')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_TipoSancion', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.descripcion

    def imprimir(self):
        return str(self.id)+','+str(self.descripcion)+','+\
            str(self.monto)

    class Meta:
        app_label = "sancion"