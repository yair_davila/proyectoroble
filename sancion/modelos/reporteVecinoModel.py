from django.db import models
from usuarios.modelos.colonoModel import Colono
from django.contrib.auth.models import User

class ReporteVecino(models.Model):
    colono = models.ForeignKey(
        Colono, models.DO_NOTHING, 'ID_COLONO_REPORTE_VECINO', blank=True, null=True)
    fecha = models.DateTimeField('Fecha', blank=True, null=True)
    observaciones = models.CharField(
        'Observaciones', max_length=500, blank=True, null=True)
    foto = models.ImageField(upload_to='sancion', null=True)
    fecha_creacion = models.DateTimeField('Creacion_ReporteVecino')
    fecha_edicion = models.DateTimeField('Edicion_ReporteVecino', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_ReporteVecino')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_ReporteVecino', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.fecha+self.colono

    class Meta:
        app_label = "sancion"