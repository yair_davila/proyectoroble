from usuarios.modelos.colonoModel import Colono
from sancion.modelos.tipoSancionModel import TipoSancion
from django.db import models
from django.contrib.auth.models import User

class Sancion(models.Model):
    colono = models.ForeignKey(Colono, models.DO_NOTHING, 'ID_COLONO_SANCION')
    tipo_sancion = models.ForeignKey(
        TipoSancion, models.DO_NOTHING, 'ID_TIPO_SANCION')
    fecha = models.DateTimeField('Fecha')  # Field name made lowercase.
    descripcion = models.CharField(
        'Descripción', max_length=250, blank=True, null=True)
    monto = models.DecimalField(
        'Monto', max_digits=8, decimal_places=2, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Sancion')
    fecha_edicion = models.DateTimeField('Edicion_Sancion', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Sancion')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Sancion', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.descripcion

    def imprimir(self):
        return str(self.id)+','+str(self.colono.id)+','+\
            str(self.tipo_sancion.id)+','+str(self.fecha)+','+\
            str(self.descripcion)+','+str(self.monto)

    class Meta:
        app_label = "sancion"