from sancion.modelos.tipoSancionModel import TipoSancion
from sancion.validador import *
from django import forms

class FormTipoSancion (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])

    class Meta:
        model = TipoSancion
        fields = ('descripcion', 'monto')


