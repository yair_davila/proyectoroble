from sancion.modelos.reporteVecinoModel import ReporteVecino
from sancion.validador import *
from django import forms

class FormReporteVecino (forms.ModelForm):
    observaciones = forms.CharField(validators=[validador_cadenas])
    fecha = forms.DateTimeField(input_formats=['%Y-%m-%d','%d/%m/%Y %I:%M:%S'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input',
        }))

    class Meta:
        model = ReporteVecino
        fields = ('colono','fecha', 'observaciones', 'foto')
