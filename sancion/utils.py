def tiene_permiso(user, permiso):
    if user.is_staff:
        return True

    if user.user_permissions.filter(name=permiso):
        return True

    grupos = user.groups.all()
    if len(grupos) > 0:
        if(grupos[0].permissions.filter(name=permiso)):
            return True

    return False


def es_colono(user):
    if user.groups.filter(name='Colonos'):
        return True
    return False
