from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion

