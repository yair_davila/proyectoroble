"""
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson

from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion
from visitantes.formularios.formMotivosRestriccion import FormMotivosRestriccion

class MotivosRestriccionView():

    @login_required()
    @permission_required('visitantes.add_motivosrestriccion', login_url='motivosrestriccion')
    def nuevo_motivosrestriccion(context):
        form = FormMotivosRestriccion()
        motivosrestriccions = MotivosRestriccion.objects.filter(estatus=1)
        return render(context, 'nuevo_motivosrestriccion.html',
                      {'formulario': form, 'accion': 'Nuevo', 'motivosrestriccions': motivosrestriccions})

    @login_required()
    @permission_required('visitantes.add_motivosrestriccion', login_url='motivosrestriccion')
    def registrar_js_motivosrestriccion(context):
        if (context.method == 'POST'):
            form = FormMotivosRestriccion(context.POST)
            if form.is_valid():
                motivosrestriccion = form.save()
                motivosrestriccion.save()
                data = MotivosRestriccionView.obtener_json_motivosrestriccions(MotivosRestriccion.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.add_motivosrestriccion', login_url='motivosrestriccion')
    def actualizar_js_motivosrestriccion(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            motivosrestriccion = MotivosRestriccion.objects.get(pk=id)

            form = FormMotivosRestriccion(context.POST, instance=motivosrestriccion)
            if form.is_valid():
                motivosrestriccion = form.save()
                motivosrestriccion.save()
                data = MotivosRestriccionView.obtener_json_motivosrestriccions(MotivosRestriccion.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.change_motivosrestriccion', login_url='motivosrestriccion')
    def obteneruno_js_motivosrestriccion(context):
        id = context.POST.get('id',None)
        data = MotivosRestriccionView.obtener_json_motivosrestriccions_carga(MotivosRestriccion.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.delete_motivosrestriccion', login_url='motivosrestriccion')
    def borrar_js_motivosrestriccion(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            motivosrestriccion = MotivosRestriccion.objects.get(pk=o)
            motivosrestriccion.estatus = 0
            motivosrestriccion.save()

        data = MotivosRestriccionView.obtener_json_motivosrestriccions(MotivosRestriccion.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_motivosrestriccions(motivosrestriccions):
        
        motivosrestriccionsJson = []
        for motivosrestriccion in motivosrestriccions:
            motivosrestriccionsJson.append(
                {
        	'motivo': motivosrestriccion.motivo,
        	'fecha': motivosrestriccion.fecha,

                    
                }
            )
        data = simplejson.dumps(motivosrestriccionsJson)
        return data
"""
