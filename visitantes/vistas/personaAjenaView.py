from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.formularios.formPersonaAjena import FormPersonaAjena

class PersonaAjenaView():

    @login_required()
    @permission_required('visitantes.add_personaajena', login_url='personaajena')
    def nuevo_personaajena(context):
        form = FormPersonaAjena()
        personaajenas = PersonaAjena.objects.filter(estatus=1)
        return render(context, 'nuevo_personaajena.html',
                      {'formulario': form, 'accion': 'Nuevo', 'personaajenas': personaajenas})

    @login_required()
    @permission_required('visitantes.add_personaajena', login_url='personaajena')
    def registrar_js_personaajena(context):
        if (context.method == 'POST'):
            form = FormPersonaAjena(context.POST)
            if form.is_valid():
                personaajena = form.save(commit=False)
                personaajena.usuario_creacion = context.user
                personaajena.fecha_creacion = date.today()
                personaajena.save()
                data = PersonaAjenaView.obtener_json_personaajenas(PersonaAjena.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.add_personaajena', login_url='personaajena')
    def actualizar_js_personaajena(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            personaajena = PersonaAjena.objects.get(pk=id)

            form = FormPersonaAjena(context.POST, instance=personaajena)
            if form.is_valid():
                personaajena = form.save()
                personaajena.save()
                data = PersonaAjenaView.obtener_json_personaajenas(PersonaAjena.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.change_personaajena', login_url='personaajena')
    def obteneruno_js_personaajena(context):
        id = context.POST.get('id',None)
        data = PersonaAjenaView.obtener_json_personaajenas(PersonaAjena.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.delete_personaajena', login_url='personaajena')
    def borrar_js_personaajena(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            personaajena = PersonaAjena.objects.get(pk=o)
            personaajena.estatus = 0
            personaajena.save()

        data = PersonaAjenaView.obtener_json_personaajenas(PersonaAjena.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_personaajenas(personaajenas):
        
        personaajenasJson = []
        for personaajena in personaajenas:
            personaajenasJson.append(
                {
            'id' : personaajena.id,
        	'tipo': str(personaajena.tipo),
        	'apaterno': personaajena.apaterno,
        	'amaterno': personaajena.amaterno,
        	'nombre': personaajena.nombre,
        	'telefono': personaajena.telefono,
        	'empresa': personaajena.empresa,
        	'restriccion_acceso': str(personaajena.restriccion_acceso.id),

                    
                }
            )
        data = simplejson.dumps(personaajenasJson)
        return data
