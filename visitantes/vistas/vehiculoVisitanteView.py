from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from visitantes.formularios.formVehiculoVisitante import FormVehiculoVisitante

class VehiculoVisitanteView():

    @login_required()
    @permission_required('visitantes.add_vehiculovisitante', login_url='vehiculovisitante')
    def nuevo_vehiculovisitante(context):
        form = FormVehiculoVisitante()
        vehiculovisitantes = VehiculoVisitante.objects.filter(estatus=1)
        return render(context, 'nuevo_vehiculovisitante.html',
                      {'formulario': form, 'accion': 'Nuevo', 'vehiculovisitantes': vehiculovisitantes})

    @login_required()
    @permission_required('visitantes.change_vehiculovisitante', login_url='vehiculovisitante')
    def obteneruno_js_vehiculovisitante(context):
        id = context.POST.get('id',None)
        data = VehiculoVisitanteView.obtener_json_vehiculovisitantes_carga(VehiculoVisitante.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.delete_vehiculovisitante', login_url='vehiculovisitante')
    def borrar_js_vehiculovisitante(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            vehiculovisitante = VehiculoVisitante.objects.get(pk=o)
            vehiculovisitante.estatus = 0
            vehiculovisitante.save()

        data = VehiculoVisitanteView.obtener_json_vehiculovisitantes(VehiculoVisitante.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_vehiculovisitantes(vehiculovisitantes):
        
        vehiculovisitantesJson = []
        for vehiculovisitante in vehiculovisitantes:
            vehiculovisitantesJson.append(
                {
            'id' : vehiculovisitante.id,
        	'visita_autorizada': str(vehiculovisitante.visita_autorizada),
        	'fecha': str(vehiculovisitante.fecha),
        	'placas': str(vehiculovisitante.placas),
        	'color': str(vehiculovisitante.color),
        	'marca': str(vehiculovisitante.marca),
        	'linea': str(vehiculovisitante.linea),
        	'tipo': str(vehiculovisitante.tipo),
        	'foto': str(vehiculovisitante.foto),

                    
                }
            )
        data = simplejson.dumps(vehiculovisitantesJson)
        return data


    def obtener_json_vehiculovisitantes_carga(vehiculovisitantes):
        
        vehiculovisitantesJson = []
        for vehiculovisitante in vehiculovisitantes:
            vehiculovisitantesJson.append(
                {
            'id' : vehiculovisitante.id,
          'visita_autorizada': vehiculovisitante.visita_autorizada.id,
          'fecha': vehiculovisitante.fecha.id,
          'placas': vehiculovisitante.placas.id,
          'color': vehiculovisitante.color.id,
          'marca': vehiculovisitante.marca.id,
          'linea': vehiculovisitante.linea.id,
          'tipo': vehiculovisitante.tipo.id,
          'foto': vehiculovisitante.foto.id,

                    
                }
            )
        data = simplejson.dumps(vehiculovisitantesJson)
        return data


    

    @login_required()
    @permission_required('visitantes.add_vehiculovisitante', login_url='vehiculovisitante')
    def registrar_js_vehiculovisitante(context):
        if (context.method == 'POST'):
            form = FormVehiculoVisitante(context.POST)
            if form.is_valid():
                vehiculovisitante = form.save(commit=False)
                vehiculovisitante.usuario_creacion = context.user
                vehiculovisitante.fecha_creacion = date.today()
                vehiculovisitante.save()
                data = VehiculoVisitanteView.obtener_json_vehiculovisitantes(VehiculoVisitante.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.add_vehiculovisitante', login_url='vehiculovisitante')
    def actualizar_js_vehiculovisitante(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            vehiculovisitante = VehiculoVisitante.objects.get(pk=id)

            form = FormVehiculoVisitante(context.POST, instance=vehiculovisitante)
            if form.is_valid():
                vehiculovisitante = form.save()
                vehiculovisitante.save()
                data = VehiculoVisitanteView.obtener_json_vehiculovisitantes(VehiculoVisitante.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
