from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.formularios.formVisitaAutorizada import FormVisitaAutorizada

class VisitaAutorizadaView():

    @login_required()
    @permission_required('visitantes.add_visitaautorizada', login_url='visitaautorizada')
    def nuevo_visitaautorizada(context):
        form = FormVisitaAutorizada()
        visitaautorizadas = VisitaAutorizada.objects.filter(estatus=1)
        return render(context, 'nuevo_visitaautorizada.html',
                      {'formulario': form, 'accion': 'Nuevo', 'visitaautorizadas': visitaautorizadas})

    @login_required()
    @permission_required('visitantes.change_visitaautorizada', login_url='visitaautorizada')
    def obteneruno_js_visitaautorizada(context):
        id = context.POST.get('id',None)
        data = VisitaAutorizadaView.obtener_json_visitaautorizadas_carga(VisitaAutorizada.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.delete_visitaautorizada', login_url='visitaautorizada')
    def borrar_js_visitaautorizada(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            visitaautorizada = VisitaAutorizada.objects.get(pk=o)
            visitaautorizada.estatus = 0
            visitaautorizada.save()

        data = VisitaAutorizadaView.obtener_json_visitaautorizadas(VisitaAutorizada.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_visitaautorizadas(visitaautorizadas):
        
        visitaautorizadasJson = []
        for visitaautorizada in visitaautorizadas:
            visitaautorizadasJson.append(
                {
            'id' : visitaautorizada.id,
        	'colono': str(visitaautorizada.colono),
        	'persona_ajena': str(visitaautorizada.persona_ajena),
        	'fecha': str(visitaautorizada.fecha),
        	'hora_entrada': str(visitaautorizada.hora_entrada),
        	'hora_salida': str(visitaautorizada.hora_salida),
        	'identificacion': str(visitaautorizada.identificacion),
        	'numero_personas': str(visitaautorizada.numero_personas),
        	'foto': str(visitaautorizada.foto),
        	'tipo_acceso': str(visitaautorizada.tipo_acceso),
        	'fecha_estimada': str(visitaautorizada.fecha_estimada),
        	'hora_estimada_min': str(visitaautorizada.hora_estimada_min),
        	'hora_estimada_max': str(visitaautorizada.hora_estimada_max),

                    
                }
            )
        data = simplejson.dumps(visitaautorizadasJson)
        return data


    def obtener_json_visitaautorizadas_carga(visitaautorizadas):
        
        visitaautorizadasJson = []
        for visitaautorizada in visitaautorizadas:
            visitaautorizadasJson.append(
                {
            'id' : visitaautorizada.id,
          'colono': visitaautorizada.colono.id,
          'persona_ajena': visitaautorizada.persona_ajena.id,
          'fecha': visitaautorizada.fecha.id,
          'hora_entrada': visitaautorizada.hora_entrada.id,
          'hora_salida': visitaautorizada.hora_salida.id,
          'identificacion': visitaautorizada.identificacion.id,
          'numero_personas': visitaautorizada.numero_personas.id,
          'foto': visitaautorizada.foto.id,
          'tipo_acceso': visitaautorizada.tipo_acceso.id,
          'fecha_estimada': visitaautorizada.fecha_estimada.id,
          'hora_estimada_min': visitaautorizada.hora_estimada_min.id,
          'hora_estimada_max': visitaautorizada.hora_estimada_max.id,

                    
                }
            )
        data = simplejson.dumps(visitaautorizadasJson)
        return data


    

    @login_required()
    @permission_required('visitantes.add_visitaautorizada', login_url='visitaautorizada')
    def registrar_js_visitaautorizada(context):
        if (context.method == 'POST'):
            form = FormVisitaAutorizada(context.POST)
            if form.is_valid():
                visitaautorizada = form.save(commit=False)
                visitaautorizada.usuario_creacion = context.user
                visitaautorizada.fecha_creacion = date.today()
                visitaautorizada.save()
                data = VisitaAutorizadaView.obtener_json_visitaautorizadas(VisitaAutorizada.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.add_visitaautorizada', login_url='visitaautorizada')
    def actualizar_js_visitaautorizada(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            visitaautorizada = VisitaAutorizada.objects.get(pk=id)

            form = FormVisitaAutorizada(context.POST, instance=visitaautorizada)
            if form.is_valid():
                visitaautorizada = form.save()
                visitaautorizada.save()
                data = VisitaAutorizadaView.obtener_json_visitaautorizadas(VisitaAutorizada.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
