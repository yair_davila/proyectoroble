from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.formularios.formTipoPersonaAjena import FormTipoPersonaAjena

class TipoPersonaAjenaView():

    @login_required()
    @permission_required('visitantes.add_tipopersonaajena', login_url='tipopersonaajena')
    def nuevo_tipopersonaajena(context):
        form = FormTipoPersonaAjena()
        tipopersonaajenas = TipoPersonaAjena.objects.filter(estatus=1)
        return render(context, 'nuevo_tipopersonaajena.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tipopersonaajenas': tipopersonaajenas})

    @login_required()
    @permission_required('visitantes.add_tipopersonaajena', login_url='tipopersonaajena')
    def registrar_js_tipopersonaajena(context):
        if (context.method == 'POST'):
            form = FormTipoPersonaAjena(context.POST)
            if form.is_valid():
                tipopersonaajena = form.save(commit=False)
                tipopersonaajena.usuario_creacion = context.user
                tipopersonaajena.fecha_creacion = date.today()
                tipopersonaajena.save()
                data = TipoPersonaAjenaView.obtener_json_tipopersonaajenas(TipoPersonaAjena.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.add_tipopersonaajena', login_url='tipopersonaajena')
    def actualizar_js_tipopersonaajena(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tipopersonaajena = TipoPersonaAjena.objects.get(pk=id)

            form = FormTipoPersonaAjena(context.POST, instance=tipopersonaajena)
            if form.is_valid():
                tipopersonaajena = form.save()
                tipopersonaajena.save()
                data = TipoPersonaAjenaView.obtener_json_tipopersonaajenas(TipoPersonaAjena.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.change_tipopersonaajena', login_url='tipopersonaajena')
    def obteneruno_js_tipopersonaajena(context):
        id = context.POST.get('id',None)
        data = TipoPersonaAjenaView.obtener_json_tipopersonaajenas(TipoPersonaAjena.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('visitantes.delete_tipopersonaajena', login_url='tipopersonaajena')
    def borrar_js_tipopersonaajena(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tipopersonaajena = TipoPersonaAjena.objects.get(pk=o)
            tipopersonaajena.estatus = 0
            tipopersonaajena.save()

        data = TipoPersonaAjenaView.obtener_json_tipopersonaajenas(TipoPersonaAjena.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_tipopersonaajenas(tipopersonaajenas):
        
        tipopersonaajenasJson = []
        for tipopersonaajena in tipopersonaajenas:
            tipopersonaajenasJson.append(
                {
            'id' : tipopersonaajena.id,
        	'descripcion': tipopersonaajena.descripcion,

                    
                }
            )
        data = simplejson.dumps(tipopersonaajenasJson)
        return data
