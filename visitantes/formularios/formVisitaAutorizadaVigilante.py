from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.validador import *
from visitantes.choices import IDENTIFICACION_CHOICES,ACCESO_CHOICES
from django import forms

class FormVisitaAutorizadaVigilante (forms.ModelForm):
    duracion = forms.IntegerField(label='Duración en Horas')
    identificacion = forms.ChoiceField(choices=IDENTIFICACION_CHOICES)
    tipo_acceso = forms.ChoiceField(choices=ACCESO_CHOICES)
    numero_personas = forms.IntegerField(
        validators=[validador_numero_min, validador_numero_max])

    class Meta:
        model = VisitaAutorizada
        fields = ('persona_ajena','duracion','identificacion',
                  'numero_personas', 'foto', 'tipo_acceso')
