from visitantes.modelos.tipoPersonaAjenaModel import  TipoPersonaAjena
from visitantes.validador import *
from django import forms

class FormTipoPersonaAjena(forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])

    class Meta:
        model = TipoPersonaAjena
        fields = ('descripcion',)


