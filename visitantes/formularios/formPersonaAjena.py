from selenium.webdriver.support.expected_conditions import element_selection_state_to_be

from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.validador import *
from visitantes.choices import ACCESO_CHOICES
from django import forms
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion

class FormPersonaAjena (forms.ModelForm):
    tipo = forms.ModelChoiceField(queryset=TipoPersonaAjena.objects.filter(estatus=1))
    restriccion_acceso = forms.ModelChoiceField(queryset=MotivosRestriccion.objects.filter(estatus=1))
    apaterno = forms.CharField(validators=[validador_cadenas])
    amaterno = forms.CharField(validators=[validador_cadenas])
    nombre = forms.CharField(validators=[validador_cadenas])
    telefono = forms.CharField(validators=[validador_telefono])
    empresa = forms.CharField(validators=[validador_cadenas])

    class Meta:
        model = PersonaAjena
        fields = ('tipo', 'apaterno', 'amaterno', 'nombre',
                  'telefono', 'empresa','restriccion_acceso')


