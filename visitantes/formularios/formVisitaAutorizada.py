from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.validador import *
from visitantes.choices import HOUR_CHOICES, IDENTIFICACION_CHOICES, ACCESO_CHOICES
from django import forms

class FormVisitaAutorizada (forms.ModelForm):
    fecha = forms.DateField(widget=forms.SelectDateWidget())
    fecha_estimada = forms.DateField(widget=forms.SelectDateWidget())
    hora_entrada = forms.ChoiceField(choices=HOUR_CHOICES)
    hora_salida = forms.ChoiceField(choices=HOUR_CHOICES)
    hora_estimada_min = forms.ChoiceField(choices=HOUR_CHOICES)
    hora_estimada_max = forms.ChoiceField(choices=HOUR_CHOICES)
    identificacion = forms.ChoiceField(choices=IDENTIFICACION_CHOICES)
    tipo_acceso = forms.ChoiceField(choices=ACCESO_CHOICES)
    numero_personas = forms.IntegerField(
        validators=[validador_numero_min, validador_numero_max])

    class Meta:
        model = VisitaAutorizada
        fields = ('colono', 'persona_ajena', 'fecha', 'hora_entrada', 'hora_salida', 'identificacion',
                  'numero_personas', 'foto', 'tipo_acceso', 'fecha_estimada', 'hora_estimada_min', 'hora_estimada_max')


