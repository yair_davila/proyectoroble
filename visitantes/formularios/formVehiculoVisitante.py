from visitantes.modelos.vehiculoVisitanteModel import VehiculoVisitante
from vehiculos.choices import COLOR_CHOICES, MARCA_CHOICES , TIPO_CHOICES,LINEA_CHOICES
from django import forms

class FormVehiculoVisitante(forms.ModelForm):
    fecha_estimada = forms.DateField(widget=forms.SelectDateWidget())
    color = forms.ChoiceField(choices=COLOR_CHOICES)
    marca = forms.ChoiceField(choices=MARCA_CHOICES)
    tipo = forms.ChoiceField(choices=TIPO_CHOICES)
    linea = forms.ChoiceField(choices=LINEA_CHOICES)

    class Meta:
        model = VehiculoVisitante
        fields = ('visita_autorizada', 'fecha', 'placas',
                  'color', 'marca', 'linea', 'tipo', 'foto')

