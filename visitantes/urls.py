from visitantes.vistas.vehiculoVisitanteView import VehiculoVisitanteView
from visitantes.vistas.visitaAutorizadaView import VisitaAutorizadaView
from visitantes.vistas.personaAjenaView import PersonaAjenaView
from visitantes.vistas.tipoPersonaAjenaView import TipoPersonaAjenaView
from django.urls import path

urlpatterns = [
	path('tipopersonaajena_nuevo', TipoPersonaAjenaView.nuevo_tipopersonaajena,
         name='nuevo_tipopersonaajena'),
    path('tipopersonaajena_registrar', TipoPersonaAjenaView.registrar_js_tipopersonaajena,
         name='registrar_tipopersonaajena'),
    path('tipopersonaajena_actualizar', TipoPersonaAjenaView.actualizar_js_tipopersonaajena,
         name='actualizar_tipopersonaajena'),
    path('tipopersonaajena_obteneruno', TipoPersonaAjenaView.obteneruno_js_tipopersonaajena,
         name='obteneruno_tipopersonaajena'),
    path('tipopersonaajena_borrar/',
         TipoPersonaAjenaView.borrar_js_tipopersonaajena, name='borrar_tipopersonaajena'),

	path('personaajena_nuevo', PersonaAjenaView.nuevo_personaajena,
         name='nuevo_personaajena'),
    path('personaajena_registrar', PersonaAjenaView.registrar_js_personaajena,
         name='registrar_personaajena'),
    path('personaajena_actualizar', PersonaAjenaView.actualizar_js_personaajena,
         name='actualizar_personaajena'),
    path('personaajena_obteneruno', PersonaAjenaView.obteneruno_js_personaajena,
         name='obteneruno_personaajena'),
    path('personaajena_borrar/',
         PersonaAjenaView.borrar_js_personaajena, name='borrar_personaajena'),

	path('visitaautorizada_nuevo', VisitaAutorizadaView.nuevo_visitaautorizada,
         name='nuevo_visitaautorizada'),
    path('visitaautorizada_registrar', VisitaAutorizadaView.registrar_js_visitaautorizada,
         name='registrar_visitaautorizada'),
    path('visitaautorizada_actualizar', VisitaAutorizadaView.actualizar_js_visitaautorizada,
         name='actualizar_visitaautorizada'),
    path('visitaautorizada_obteneruno', VisitaAutorizadaView.obteneruno_js_visitaautorizada,
         name='obteneruno_visitaautorizada'),
    path('visitaautorizada_borrar/',
         VisitaAutorizadaView.borrar_js_visitaautorizada, name='borrar_visitaautorizada'),

	path('vehiculovisitante_nuevo', VehiculoVisitanteView.nuevo_vehiculovisitante,
         name='nuevo_vehiculovisitante'),
    path('vehiculovisitante_registrar', VehiculoVisitanteView.registrar_js_vehiculovisitante,
         name='registrar_vehiculovisitante'),
    path('vehiculovisitante_actualizar', VehiculoVisitanteView.actualizar_js_vehiculovisitante,
         name='actualizar_vehiculovisitante'),
    path('vehiculovisitante_obteneruno', VehiculoVisitanteView.obteneruno_js_vehiculovisitante,
         name='obteneruno_vehiculovisitante'),
    path('vehiculovisitante_borrar/',
         VehiculoVisitanteView.borrar_js_vehiculovisitante, name='borrar_vehiculovisitante'),


]