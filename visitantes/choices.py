import datetime as dt
HOUR_CHOICES = [(dt.time(hour=x), '{:02d}:00'.format(x)) for x in range(0, 24)]

ACCESO_CHOICES = (
    ("1", "Todo"),
    ("2", "Restingido"),
)

IDENTIFICACION_CHOICES = (
    ("INE", "Credencial de elector"),
    ("LC", "Licencia de condicir"),
)
MARCA_CHOICES = (
    ("NISSAN", "NISSAN"),
    ("FORD", "FORD"),
    ("HONDA", "NISSAN"),
    ("KIA", "KIA"),
    ("CHEVROLET", "CHEVROLET"),
    ("PONTIAC", "PONTIAC"),
    ("SEAT", "SEAT"),
    ("OTRA", "Otra"),
)


COLOR_CHOICES = (
    ("V", "Verde"),
    ("AZ", "Azul"),
    ("R", "Rojo"),
    ("B", "Blanco"),
    ("N", "Negro"),
    ("C", "Cafe"),
    ("AM", "Amarillo"),
    ("G", "Gris"),
    ("R", "Rosa"),

)


TIPO_CHOICES = (
    ("SEDAN", "Sedan"),
    ("COMPACTO", "Compacto"),
    ("TRABAJO", "Trabajo"),
    ("MINIVAN", "MiniVan"),
)

LINEA_CHOICES = (
    ("LUJO", "Sedan"),
    ("ESTANDAR", "ESTANDAR"),
)
