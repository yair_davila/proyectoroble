from visitantes.modelos.visitaAutorizadaModel import VisitaAutorizada
from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion
from django.db import models
from django.contrib.auth.models import User

class VehiculoVisitante(models.Model):
    visita_autorizada = models.ForeignKey(VisitaAutorizada, models.DO_NOTHING)
    fecha = models.DateTimeField('Fecha',null=True)  # Field name made lowercase.
    placas = models.CharField('Placas', max_length=10, blank=True, null=True)
    color = models.CharField('Color', max_length=35, blank=True, null=True)
    marca = models.CharField('Marca', max_length=35, blank=True, null=True)
    linea = models.CharField('Linea', max_length=35, blank=True, null=True)
    tipo = models.CharField('Tipo', max_length=35, blank=True, null=True)
    foto = models.ImageField(upload_to='vehiculovisitante', null=True)
    fecha_creacion = models.DateTimeField('Creacion_VehiculoVisitante')
    fecha_edicion = models.DateTimeField('Edicion_VehiculoVisitante', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_VehiculoVisitante')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_VehiculoVisitante', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.visita_autorizada.__str__()+' '+str(self.color)+' '+str(self.marca)+' '+str(self.fecha)

    def imprimir(self):
        return str(self.id)+','+str(self.visita_autorizada.id)+','+\
            str(self.fecha)+','+str(self.placas)+','+\
            str(self.color)+','+str(self.marca)+','+\
            str(self.linea)+','+str(self.tipo)+','+\
            str(self.foto)

    class Meta:
        app_label = "visitantes"