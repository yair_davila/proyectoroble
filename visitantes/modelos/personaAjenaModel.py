from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion
from django.db import models
from django.contrib.auth.models import User

class PersonaAjena(models.Model):
    tipo = models.ForeignKey(TipoPersonaAjena, models.DO_NOTHING, 'ID_TIPO')
    apaterno = models.CharField(
        'Apeido_Paterno', max_length=35, blank=True, null=True)
    amaterno = models.CharField(
        'Apeido_Materno', max_length=35, blank=True, null=True)
    nombre = models.CharField('Nombre', max_length=35, blank=True, null=True)
    telefono = models.CharField(
        'Telefono', max_length=10, blank=True, null=True)
    empresa = models.CharField('Empresa', max_length=35, blank=True, null=True)
    restriccion_acceso = models.ForeignKey(
        MotivosRestriccion, models.DO_NOTHING, 'ID_MOTIVO_RESTRICCION',null=True)
    fecha_creacion = models.DateTimeField('Creacion_PersonaAjena')
    fecha_edicion = models.DateTimeField('Edicion_PersonaAjena', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_PersonaAjena')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_PersonaAjena', null=True)

    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.nombre+" "+self.apaterno

    def imprimir(self):
        return str(self.id)+','+str(self.tipo)+','+\
            str(self.apaterno)+','+str(self.amaterno)+','+\
            str(self.nombre)+','+str(self.telefono)+','+\
            str(self.empresa)+','+str(self.restriccion_acceso)

    class Meta:
        app_label = "visitantes"