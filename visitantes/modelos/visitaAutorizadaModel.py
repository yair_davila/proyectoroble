from visitantes.modelos.personaAjenaModel import PersonaAjena
from visitantes.modelos.tipoPersonaAjenaModel import TipoPersonaAjena
from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion
from django.contrib.auth.models import User
from django.db import models

from usuarios.modelos.colonoModel import Colono,User

class VisitaAutorizada(models.Model):
    colono = models.ForeignKey(
        Colono, models.DO_NOTHING, 'ID_COLONO_VISITA_AUTORIZADA')
    persona_ajena = models.ForeignKey(
        PersonaAjena, models.DO_NOTHING, 'ID_PERSONA_AJENA')
    fecha = models.DateTimeField('Fecha',null=True)  # Field name made lowercase.
    hora_entrada = models.TimeField('Hora_Entrada', blank=True, null=True)
    hora_salida = models.TimeField('Hora_Salida', blank=True, null=True)
    identificacion = models.CharField(
        'Identificación', max_length=35, blank=True, null=True)
    numero_personas = models.SmallIntegerField(
        'Numero_Personas', blank=True, null=True)
    foto = models.ImageField(upload_to='visitaautorizada', null=True)
    tipo_acceso = models.CharField(
        'Tipo_Acceso', max_length=35, blank=True, null=True)
    fecha_estimada = models.DateTimeField(
        'Fecha_Estimada', blank=True, null=True)
    hora_estimada_min = models.TimeField(
        'Hora_Estimada_Min', blank=True, null=True)
    hora_estimada_max = models.TimeField(
        'Hora_Estimada_Max', blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_VisitaAutorizada')
    fecha_edicion = models.DateTimeField('Edicion_VisitaAutorizada', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_VisitaAutorizada')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_VisitaAutorizada', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        #+" "+str(self.fecha)+" "+str(self.fecha_estimada)
        return self.persona_ajena.__str__()

    def imprimir(self):
        return str(self.id)+','+str(self.colono.id)+','+\
            str(self.persona_ajena.id)+','+str(self.fecha)+','+\
            str(self.hora_entrada)+','+str(self.hora_salida)+','+\
            str(self.identificacion)+','+str(self.numero_personas)+','+\
            str(self.foto)+','+str(self.tipo_acceso)+','+\
            str(self.fecha_estimada)+','+str(self.hora_estimada_min)+','+\
            str(self.hora_estimada_max)

    class Meta:
        app_label = "visitantes"