from django.db import models
from django.contrib.auth.models import User

class MotivosRestriccion(models.Model):
    motivo = models.CharField(
        'Motivo', max_length=300, blank=True, null=True)
    fecha = models.DateTimeField('Fecha')
    fecha_creacion = models.DateTimeField('Creacion_MotivosRestriccion')
    fecha_edicion = models.DateTimeField('Edicion_MotivosRestriccion', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_MotivosRestriccion')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_MotivosRestriccion', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.motivo

    def imprimir(self):
        return str(self.id)+','+str(self.motivo)+','+\
            str(self.fecha)

    class Meta:
        app_label = "visitantes"