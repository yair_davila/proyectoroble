from visitantes.modelos.motivosRestriccionModel import MotivosRestriccion
from django.db import models
from django.contrib.auth.models import User

class TipoPersonaAjena(models.Model):
    descripcion = models.CharField('Descripción', max_length=35)
    fecha_creacion = models.DateTimeField('Creacion_TipoPersonaAjena')
    fecha_edicion = models.DateTimeField('Edicion_TipoPersonaAjena', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_TipoPersonaAjena')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_TipoPersonaAjena', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.descripcion

    def imprimir(self):
        return str(self.id)+','+str(self.descripcion)

    class Meta:
        app_label = "visitantes"