from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from usuarios.modelos.colonoModel import Colono
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.template import RequestContext


# Create your views here.
def login_inicio(context):
    context.session.set_expiry(3600)
    if context.user.is_authenticated:
        return cargar_sesion(context)
    return render(context, 'loginUser.html')

def cargar_sesion(context):
    usuario = User.objects.get(id=context.user.id)
    if es_colono(usuario):
        return render(context, 'principal_colono.html')
    elif es_vigilante(usuario):
        return render(context, 'principal_vigilante.html')
    else:
        return render(context, 'principal.html')

def login_privada_user(context):
    if context.method == 'POST':
        user_name = context.POST.get('username', None)
        usuarios = User.objects.filter(username=user_name)
        
        if len(usuarios) > 0:
            usuario_actual=usuarios[0]
            es_colono=usuario_actual.groups.filter(name="Colonos").exists()
            es_vigilante=usuario_actual.groups.filter(name="Vigilantes").exists()
            confirmado = usuario_actual.last_login
            if(es_colono or es_vigilante):
                if(confirmado == None):
                    return render(context, 'primer_login.html',{'user_name':usuario_actual.username})
            return render(context, 'loginPassword.html',{'user_name':user_name})
    return render(context,'loginUser.html',{'error':'Usuario no valido'})

def login_privada_password(context):
    if context.method == 'POST':
        password = context.POST.get('password', None)
        user_name = context.POST.get('username', None)
        return iniciar_sesion(context,user_name,password)

def iniciar_sesion(context,user_name,password):
    usuarios = User.objects.filter(username=user_name)
    if len(usuarios)>0:
        user = authenticate(username=user_name, password=password)
        if (user):
            login(context, user)
            return redirect('login')
            """
            if (es_colono(usuarios[0])):
                return render(context, 'principal_colono.html')

            if (es_vigilante(usuarios[0])):
                return render(context, 'principal_vigilante.html')

            
            if 'next' in context.POST:
                return redirect(context.POST.get('next'))
            """
            return render(context, 'principal.html')
    return render(context, 'loginUser.html', {'error': 'Usuario no válido'})


def es_colono(user):
    return user.groups.filter(name="Colonos").exists()


def es_vigilante(user):
    return user.groups.filter(name="Vigilantes").exists()


def primer_login(context):
    if context.method == 'POST':
        user_name = context.POST.get('username', None)
        password2 = context.POST.get('password2', None)
        password3 = context.POST.get('password3', None)


        if( password2 != '' and password3 != '' and password2 == password3):
            password2 = make_password(password2)

            usuario = User.objects.get(username=user_name)
            usuario.password = password2
            usuario.save()

            if es_colono(usuario):
                #colono=Colono.objects.get(user_ptr_id = usuario.id)
                colono=Colono.objects.get(username=user_name)
                colono.confirmado = '1'
                colono.save()


            return iniciar_sesion(context,user_name,password3)
        else:
            return render(context, 'primer_login.html', {'error': 'Las nuevas contraseñas no coinciden'})

    return render(context, 'primer_login.html')


def logout_privada(context):
    logout(context)
    return redirect('login')
