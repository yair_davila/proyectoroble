from django.urls import path
from .views import *

urlpatterns = [
    path('', login_inicio, name='login'),
    path('primero', primer_login, name='primer_login'),
    path('step1', login_privada_user, name='login_privada_usuario'),
    path('entrar', login_privada_password, name='login_privada_password'),
    path('salir', logout_privada, name='logout_privada'),
]