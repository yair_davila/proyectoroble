from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from ingresos.formularios.formTipoIngreso import FormTipoIngreso

class TipoIngresoView():

    @login_required()
    @permission_required('ingresos.add_tipoingreso', login_url='tipoingreso')
    def nuevo_tipoingreso(context):
        form = FormTipoIngreso()
        tipoingresos = TipoIngreso.objects.filter(estatus=1)
        return render(context, 'nuevo_tipoingreso.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tipoingresos': tipoingresos})

    @login_required()
    @permission_required('ingresos.add_tipoingreso', login_url='tipoingreso')
    def registrar_js_tipoingreso(context):
        if (context.method == 'POST'):
            form = FormTipoIngreso(context.POST)
            if form.is_valid():
                tipoingreso = form.save(commit=False)
                tipoingreso.usuario_creacion = context.user
                tipoingreso.fecha_creacion = date.today()
                tipoingreso.save()
                data = TipoIngresoView.obtener_json_tipoingresos2(TipoIngreso.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_tipoingreso', login_url='tipoingreso')
    def actualizar_js_tipoingreso(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tipoingreso = TipoIngreso.objects.get(pk=id)

            form = FormTipoIngreso(context.POST, instance=tipoingreso)
            if form.is_valid():
                tipoingreso = form.save()
                tipoingreso.save()
                data = TipoIngresoView.obtener_json_tipoingresos2(TipoIngreso.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.change_tipoingreso', login_url='tipoingreso')
    def obteneruno_js_tipoingreso(context):
        id = context.POST.get('id',None)
        data = TipoIngresoView.obtener_json_tipoingresos(TipoIngreso.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.delete_tipoingreso', login_url='tipoingreso')
    def borrar_js_tipoingreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tipoingreso = TipoIngreso.objects.get(pk=o)
            tipoingreso.estatus = 0
            tipoingreso.save()

        data = TipoIngresoView.obtener_json_tipoingresos2(TipoIngreso.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_tipoingresos(tipoingresos):
        
        tipoingresosJson = []
        for tipoingreso in tipoingresos:
            tipoingresosJson.append(
                {
            'id' : tipoingreso.id,
            'descripcion': tipoingreso.descripcion,
            'monto': tipoingreso.monto,
            'tipo': tipoingreso.tipo,
            'estatus': tipoingreso.estatus

                    
                }
            )
        data = simplejson.dumps(tipoingresosJson)
        return data

    def obtener_json_tipoingresos2(tipoingresos):
        
        tipoingresosJson = []
        for tipoingreso in tipoingresos:
            tipoingresosJson.append(
                {
            'id' : tipoingreso.id,
        	'descripcion': tipoingreso.descripcion,
        	'monto': tipoingreso.monto,
        	'tipo': 'ORDINARIO' if (tipoingreso.tipo == 1 ) else 'EXTRORDINARIO',
            'estatus': tipoingreso.estatus

                    
                }
            )
        data = simplejson.dumps(tipoingresosJson)
        return data
