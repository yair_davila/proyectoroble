from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from ingresos.modelos.tipoingresosModel import TipoIngresos
from ingresos.formularios.formTipoIngresos import FormTipoIngresos

class TipoIngresosView():

    @login_required()
    @permission_required('ingresos.add_tipoingresos', login_url='tipoingresos')
    def nuevo_tipoingresos(context):
        form = FormTipoIngresos()
        tipoingresoss = TipoIngresos.objects.filter(estatus=1)
        return render(context, 'nuevo_tipoingresos.html',
                      {'formulario': form, 'accion': 'Nuevo', 'tipoingresoss': tipoingresoss})

    @login_required()
    @permission_required('ingresos.add_tipoingresos', login_url='tipoingresos')
    def registrar_js_tipoingresos(context):
        if (context.method == 'POST'):
            form = FormTipoIngresos(context.POST)
            if form.is_valid():
                tipoingresos = form.save()
                tipoingresos.save()
                data = TipoIngresosView.obtener_json_tipoingresoss(TipoIngresos.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_tipoingresos', login_url='tipoingresos')
    def actualizar_js_tipoingresos(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            tipoingresos = TipoIngresos.objects.get(pk=id)

            form = FormTipoIngresos(context.POST, instance=tipoingresos)
            if form.is_valid():
                tipoingresos = form.save(commit=False)
                tipoingresos.usuario_creacion = context.user
                tipoingresos.fecha_creacion = date.today()
                tipoingresos.save()
                data = TipoIngresosView.obtener_json_tipoingresoss(TipoIngresos.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.change_tipoingresos', login_url='tipoingresos')
    def obteneruno_js_tipoingresos(context):
        id = context.POST.get('id',None)
        data = TipoIngresosView.obtener_json_tipoingresoss_carga(TipoIngresos.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.delete_tipoingresos', login_url='tipoingresos')
    def borrar_js_tipoingresos(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            tipoingresos = TipoIngresos.objects.get(pk=o)
            tipoingresos.estatus = 0
            tipoingresos.save()

        data = TipoIngresosView.obtener_json_tipoingresoss(TipoIngresos.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")