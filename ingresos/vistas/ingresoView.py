from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from ingresos.modelos.ingresoModel import Ingreso
from ingresos.formularios.formIngreso import FormIngreso
from ingresos.formularios.formIngresoGenerar import FormIngresoGenerar
from domicilios.modelos.domicilioModel import Domicilio
from domicilios.modelos.colonoDomicilioModel import ColonoDomicilio
from ingresos.modelos.tipoIngresoModel import TipoIngreso
import datetime
from django.db.models import Max
from vigilante.utils import escribir_pdf
from vigilante.formularios.formIngreso import FormIngreso as FromIngresoPagar


class IngresoView():

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def nuevo_ingreso_generar(context):
        if (context.method == 'POST'):
            form = FormIngresoGenerar(context.POST)
            if form.is_valid():
                tipo_ingreso_id = context.POST.get('tipo_ingreso',None)
                monto = context.POST.get('monto',None)
                ejercicio = context.POST.get('ejercicio',None)
                tipo_domicilio = context.POST.get('tipo_domicilio',None)
                estatus = context.POST.get('estatus',None)
                domicilios = Domicilio.objects.filter(tipo_domicilio=tipo_domicilio,estatus=1)
                tipo_ingreso_object = TipoIngreso.objects.get(pk=tipo_ingreso_id)
                abono = 0
                if (str(estatus) == "1"):
                    abono = monto
                for domicilio in domicilios:
                    ingreso = Ingreso()
                    ingreso.usuario_creacion = context.user
                    ingreso.fecha_creacion = date.today()
                    ingreso.descripcion=str(tipo_ingreso_object.descripcion)+" "+str(ejercicio)
                    ingreso.ejercicio=ejercicio
                    ingreso.estatus = estatus
                    ingreso.abono=abono
                    ingreso.monto=monto
                    ingreso.estatus=estatus
                    ingreso.colono=domicilio.titular
                    ingreso.fecha=date.today()
                    ingreso.borrado=0
                    #ingreso.folio_recibo=''
                    ingreso.tipo_ingreso= tipo_ingreso_object
                    ingreso.referencia=''
                    ingreso.save()
                    ## print("Guardar" + ingreso.imprimir()+"\n")
                ingresos = Ingreso.objects.filter(borrado=0)[0:10]
                totales = Ingreso.objects.filter(borrado=0).count()
                totales = range(0, totales, 10)
                form = FormIngresoGenerar()
                return render(context, 'nuevo_ingreso.html',
                              {'totales': totales,'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos, 'exito': True})
            else:
                ingresos = Ingreso.objects.filter(borrado=0)[0:10]
                totales = Ingreso.objects.filter(borrado=0).count()
                totales = range(0, totales, 10)
                return render(context, 'nuevo_ingreso.html',
                              {'totales': totales,'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos, 'error': form.errors})
        else:
            ingresos = Ingreso.objects.filter(borrado=0)[0:10]
            totales = Ingreso.objects.filter(borrado=0).count()
            totales = range(0, totales, 10)
            form = FormIngresoGenerar()
        return render(context, 'nuevo_ingreso.html', {'totales': totales,'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos})

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def nuevo_ingreso(context):
        if (context.method == 'POST'):
            form = FormIngreso(context.POST, context.FILES)
            if form.is_valid():
                ingreso = form.save(commit=False)
                monto = context.POST.get('monto', None)
                abono = context.POST.get('abono', None)
                if(monto == abono):
                    ingreso.estatus = 1
                ingreso.usuario_creacion = context.user
                ingreso.fecha_creacion = date.today()
                domicilio = ColonoDomicilio.objects.filter(colono=ingreso.colono)[0].domicilio
                ingreso.domicilio= domicilio
                ingreso.save()
                
                
                #print("Guardar")
                ingresos = Ingreso.objects.filter(borrado=0)[0:10]
                totales = Ingreso.objects.filter(borrado=0).count()
                totales = range(0, totales, 10)
                form = FormIngreso()
                return render(context, 'nuevo_ingreso.html',
                              {'totales': totales,'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos, 'exito': True})
            else:
                ingresos = Ingreso.objects.filter(borrado=0)[0:10]
                totales = Ingreso.objects.filter(borrado=0).count()
                totales = range(0, totales, 10)
                return render(context, 'nuevo_ingreso.html',
                              {'totales': totales,'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos, 'error': form.errors})
        else:
            ingresos = Ingreso.objects.filter(borrado=0)[0:10]
            totales = Ingreso.objects.filter(borrado=0).count()
            totales = range(0, totales, 10)
            form = FormIngreso()
        return render(context, 'nuevo_ingreso.html', {'totales': totales,'formulario': form, 'accion': 'Nuevo', 'ingresos': ingresos})
    
    @login_required()
    @permission_required('ingresos.view_ingreso', login_url='ingreso')
    def obtener_ingresos(context):
        ingresos = Ingreso.objects.filter(borrado=0)[0:10]
        return render(context, 'lista_ingreso.html', {'accion': 'Lista', 'ingresos': ingresos})

    @login_required()
    @permission_required('ingresos.change_ingreso',login_url='ingreso')
    def editar_ingreso(request, id):
        ingreso = Ingreso.objects.get(pk=id)
        if request.method == 'POST':
            form = FormIngreso(request.POST, request.FILES, instance=ingreso)
            #print(form.is_valid())
            #print(form.errors)
            if form.is_valid():
                ingreso = form.save()
                ingreso.save()
                return redirect('nuevo_ingreso')
            else:
                ingresos = Ingreso.objects.filter(borrado=0)[0:10]
                totales = Ingreso.objects.filter(borrado=0).count()
                totales = range(0, totales, 10)
                return render(request, 'nuevo_ingreso.html',
                          {'totales': totales,'formulario': form, 'accion': 'Editar', 'ingresos': ingresos, 'error': form.errors})
        else:
            form = FormIngreso(instance=ingreso)
        ingresos = Ingreso.objects.filter(borrado=0)[0:10]
        totales = Ingreso.objects.filter(borrado=0).count()
        totales = range(0, totales, 10)
        return render(request, 'nuevo_ingreso.html', {'totales': totales,'formulario': form, 'accion': 'Editar','ingresos':ingresos})

    def obtener_diez(context):
        id = int(context.GET.get('id', None))
        id = (id*10)
        #print(str(id-10)+" "+str(id))
        ingresos = Ingreso.objects.filter(estatus=1).order_by('folio_recibo')[id-10:id]
        data = IngresoView.obtener_json_ingresos(ingresos)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.change_ingreso', login_url='ingreso')
    def obteneruno_js_ingreso(context):
        id = context.POST.get('id',None)
        #print('try______________________________________________________')
        try:
            #print('try')
            descripcion = context.POST.get('descripcion',None)
            data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(pk=id))
            #print('try')
        except:
            #print('except')
            data = IngresoView.obtener_json_ingresos_carga(Ingreso.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.delete_ingreso', login_url='ingreso')
    def borrar_js_ingreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            ingreso = Ingreso.objects.get(pk=o)
            ingreso.borrado = 1
            ingreso.save()

        data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0)[0:10])
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_ingresos(ingresos):
        
        ingresosJson = []
        for ingreso in ingresos:
            ingresosJson.append(
                {
            'id' : ingreso.id,
        	'colono': str(ingreso.colono),
        	'tipo_ingreso': str(ingreso.tipo_ingreso),
        	'fecha': str(ingreso.fecha.strftime('%d de %B de %Y')),
        	'monto': str(ingreso.monto),
        	'referencia': str(ingreso.referencia),
        	'abono': str(ingreso.abono),
        	'descripcion': str(ingreso.descripcion),
        	'ejercicio': str(ingreso.ejercicio),
        	'folio_recibo': str(ingreso.folio_recibo),
            'estatus': str(ingreso.estatus)

                    
                }
            )
        data = simplejson.dumps(ingresosJson)
        return data


    def obtener_json_ingresos_carga(ingresos):
        
        ingresosJson = []
        for ingreso in ingresos:
            ingresosJson.append(
                {
                    'id' : str(ingreso.id),
                    'colono': str(ingreso.colono.id),
                    'tipo_ingreso': str(ingreso.tipo_ingreso.id),
                    'fecha': str(ingreso.fecha.strftime('%Y-%m-%d')),
                    'monto': str(ingreso.monto),
                    'referencia': str(ingreso.referencia),
                    'abono': str(ingreso.abono),
                    'descripcion': str(ingreso.descripcion),
                    'ejercicio': str(ingreso.ejercicio),
                    'estatus': str(ingreso.estatus),
                    'folio_recibo': str(int(ingreso.folio_recibo)),
                }
            )
        data = simplejson.dumps(ingresosJson)
        return data


    

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def registrar_js_ingreso(context):
        if (context.method == 'POST'):
            form = FormIngreso(context.POST)
            if form.is_valid():
                ingreso = form.save(commit=False)
                ingreso.usuario_creacion = context.user
                ingreso.fecha_creacion = date.today()
                ingreso.save()
                data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0)[0:10])
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def actualizar_js_ingreso(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            ingreso = Ingreso.objects.get(pk=id)

            form = FormIngreso(context.POST, instance=ingreso)
            if form.is_valid():
                ingreso = form.save()
                ingreso.save()
                data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0)[0:10])

                return HttpResponse(data, content_type="application/json")
            else:
                #print( str(form.errors))
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    

    @login_required()
    @permission_required('ingresos.delete_ingreso', login_url='ingreso')
    def borrar_js_ingreso(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            ingreso = Ingreso.objects.get(pk=o)
            ingreso.borrado = 1
            ingreso.save()

        data = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0)[0:10])
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def pagar_ingreso(context):
        lolo = Ingreso.objects.all().aggregate(Max('folio_recibo'))
        #print(lolo['folio_recibo__max'])
        form = FromIngresoPagar()
        fecha = datetime.datetime.now()
        ingresos = Ingreso.objects.filter(borrado=0)[0:10]
        fecha = datetime.datetime.now().strftime("%Y-%m-%d")
        totales = Ingreso.objects.filter(borrado=0).count()
        totales = range(0, totales, 10)
        return render(context, 'pagar_ingreso.html',
                      {'formulario': form,
                       'totales':totales,
                       'accion': 'Nuevo', 'ingresos': ingresos,'fecha':fecha})


    @login_required()
    @permission_required('ingresos.add_ingreso', login_url='ingreso')
    def pagar_js_ingreso(context):
        #print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        if (context.method == 'POST'):
            id_domicilio = context.POST.get('domicilio', None)
            
            if(id_domicilio != None):
                
                form = FromIngresoPagar(context.POST)
                if form.is_valid():
                
                    ingreso = form.save(commit=False)
                    ingreso.domicilio = Domicilio.objects.get(id=id_domicilio)
                    ingreso.usuario_creacion = context.user
                    ingreso.fecha = date.today()
                    ingreso.fecha_creacion = date.today()
                    ingreso.referencia = 'static/media/b.txt'
                    ingreso.estatus=0
                    lolo = Ingreso.objects.all().aggregate(Max('folio'))
                    numero = str(int(lolo['folio__max'])+1) if lolo['folio__max']!=None else '0'
                    #print(numero)
                    while(len(numero)<6):
                        numero="0"+numero
                    ingreso.folio = numero
                    ingreso.folio_recibo = str(ingreso.ejercicio)+str(numero)
                    if ingreso.abono == ingreso.monto:
                        ingreso.estatus = 1
                    ingreso.borrado=0
                    #ingreso.referencia=escribir_pdf(ingreso)
                    #print("antes de guardar")
                    ingreso.save()
                    ingreso.referencia=escribir_pdf(ingreso)
                    #print(ingreso.referencia)
                    ingreso.save()
                    #print("despues de guardar")
                
                    datos = IngresoView.obtener_json_ingresos(Ingreso.objects.filter(borrado=0)[0:10])
                    return HttpResponse(datos, content_type="application/json")
                else:
                    datos = simplejson.dumps([{'error': str(form.errors)} ])
                    return HttpResponse(datos, content_type="application/json")
            else:
                
                datos = simplejson.dumps([{'error': "Ingrese domicilio"} ])
                return HttpResponse(datos, content_type="application/json")
        else:
            datos = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(datos, content_type="application/json")

