from ingresos.modelos.tipoIngresoModel import TipoIngreso
from usuarios.modelos.colonoModel import Colono
from domicilios.modelos.domicilioModel import Domicilio
from django.db import models
from django.contrib.auth.models import User

class Ingreso(models.Model):
    colono = models.ForeignKey(Colono, models.DO_NOTHING, 'ID_COLONO_INGRESO')
    domicilio = models.ForeignKey(Domicilio, models.DO_NOTHING, 'ID_DOMICILIO_INGRESO')
    tipo_ingreso = models.ForeignKey(
        TipoIngreso, models.DO_NOTHING, 'ID_TIPO_INGRESO')
    fecha = models.DateTimeField('Fecha')  # Field name made lowercase.
    monto = models.DecimalField('Monto', max_digits=6, decimal_places=2)
    referencia = models.FileField(upload_to='Ingresos');
    abono = models.SmallIntegerField('Abono', blank=True, null=True)
    descripcion = models.CharField(
        'Descripción', max_length=100, blank=True, null=True)
    ejercicio = models.SmallIntegerField('Ejercicio', blank=True, null=True)
    folio_recibo = models.CharField(
        'Folio_Recibo', max_length=10,blank=True, null=True)
    folio = models.SmallIntegerField(
        'Folio', blank=True, null=True)
    estatus = models.SmallIntegerField('Estatus', blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Ingreso')
    fecha_edicion = models.DateTimeField('Edicion_Ingreso', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Ingreso')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Ingreso', null=True)
    borrado = models.SmallIntegerField('Borrado', default=0)

    def __str__(self):
        return str(self.monto)+' '+str(self.fecha)

    def imprimir(self):
        return str(self.id)+','+str(self.colono.id)+','+\
            str(self.tipo_ingreso.id)+','+str(self.fecha)+','+\
            str(self.monto)+','+\
            str(self.estatus)+','+str(self.abono)+','+\
            str(self.descripcion)+','+str(self.ejercicio)+','+\
            str(self.folio_recibo)+','+str(self.referencia)

    class Meta:
        app_label = "ingresos"