from django.db import models
from django.contrib.auth.models import User
class TipoIngreso(models.Model):
    descripcion = models.CharField(
        'Descripción', max_length=35, blank=True, null=True)
    estatus = models.SmallIntegerField('Estatus', blank=True, null=True)
    monto = models.DecimalField(
        'Monto', max_digits=8, decimal_places=2, blank=True, null=True)
    tipo = models.SmallIntegerField('Tipo', blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_TipoIngreso')
    fecha_edicion = models.DateTimeField('Edicion_TipoIngreso', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_TipoIngreso')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_TipoIngreso', null=True)
    borrado = models.SmallIntegerField('Borrado', default=1)

    def __str__(self):
        return self.descripcion

    def imprimir(self):
        return str(self.id)+','+str(self.descripcion)+','+\
            str(self.estatus)+','+str(self.monto)+','+\
            str(self.tipo)
    
    

    class Meta:
        app_label = "ingresos"