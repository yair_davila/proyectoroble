from ingresos.modelos.ingresoModel import Ingreso
from ingresos.vistas.ingresoView import IngresoView
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from ingresos.vistas.tipoIngresoView import TipoIngresoView
from django.urls import path

urlpatterns = [
	path('tipoingreso_nuevo', TipoIngresoView.nuevo_tipoingreso,
         name='nuevo_tipoingreso'),
    path('tipoingreso_registrar', TipoIngresoView.registrar_js_tipoingreso,
         name='registrar_tipoingreso'),
    path('tipoingreso_actualizar', TipoIngresoView.actualizar_js_tipoingreso,
         name='actualizar_tipoingreso'),
    path('tipoingreso_obteneruno', TipoIngresoView.obteneruno_js_tipoingreso,
         name='obteneruno_tipoingreso'),
    path('tipoingreso_borrar/',
         TipoIngresoView.borrar_js_tipoingreso, name='borrar_tipoingreso'),

	path('ingreso_nuevo', IngresoView.nuevo_ingreso,
         name='nuevo_ingreso'),
    path('ingreso_registrar', IngresoView.registrar_js_ingreso,
         name='registrar_ingreso'),
    path('ingreso_actualizar', IngresoView.actualizar_js_ingreso,
         name='actualizar_ingreso'),
    path('ingreso_obteneruno', IngresoView.obteneruno_js_ingreso,
         name='obteneruno_ingreso'),
    path('ingreso_borrar/',
         IngresoView.borrar_js_ingreso, name='borrar_ingreso'),
    path('ingreso_editar/<int:id>', IngresoView.editar_ingreso, name='editar_ingreso'),
    path('ingreso_lista', IngresoView.obtener_ingresos, name='lista_ingresos'),
    path('ingreso_nuevo_generar', IngresoView.nuevo_ingreso_generar,
         name='nuevo_ingreso_generar'),
    path('ingresos_10', IngresoView.obtener_diez,
         name='10ingresos'),
    path('ingreso_pagar/',
         IngresoView.pagar_ingreso,name='pagar_ingreso'),
     path('ingreso_pagar_js/',
          IngresoView.pagar_js_ingreso,name='pagar_ingreso')


]