from ingresos.modelos.ingresoModel import Ingreso
from ingresos.validador import *
from django import forms
from ingresos.choices import MES_CHOICES,ESTATUS_CHOICES
from usuarios.modelos.colonoModel import Colono
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from egresos.utils import obtener_ano_actual
from domicilios.modelos.tipoDomicilioModel import TipoDomicilio
from ingresos.choices import PAGADO_CHOICES

class FormIngresoGenerar (forms.ModelForm):
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    ejercicio = forms.IntegerField(initial=obtener_ano_actual)
    tipo_ingreso = forms.ModelChoiceField(queryset=TipoIngreso.objects.filter(estatus=1))
    tipo_domicilio = forms.ModelChoiceField(queryset=TipoDomicilio.objects.filter(estatus=1))
    estatus = forms.ChoiceField(choices=PAGADO_CHOICES, required=True)

    #fecha = forms.DateField(widget=forms.SelectDateWidget())
    #fecha = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
    #        'class': 'datetimepicker-input',
    #    }))

    class Meta:
        model = Ingreso
        fields = ('tipo_ingreso','monto','ejercicio','tipo_domicilio')
