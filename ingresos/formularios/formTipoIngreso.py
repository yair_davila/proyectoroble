from ingresos.modelos.tipoIngresoModel import TipoIngreso
from ingresos.choices import ESTATUS_CHOICES,TIPO_CHOICES
from ingresos.validador import *
from django import forms

class FormTipoIngreso (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    estatus = forms.ChoiceField(choices=ESTATUS_CHOICES)
    tipo = forms.ChoiceField(choices=TIPO_CHOICES)


    class Meta:
        model = TipoIngreso
        fields = ('descripcion', 'estatus', 'monto', 'tipo')


