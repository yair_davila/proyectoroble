from ingresos.modelos.ingresoModel import Ingreso
from ingresos.validador import *
from django import forms
from ingresos.choices import MES_CHOICES,ESTATUS_CHOICES
from usuarios.modelos.colonoModel import Colono
from ingresos.modelos.tipoIngresoModel import TipoIngreso
from egresos.utils import obtener_ano_actual
from ingresos.choices import PAGADO_CHOICES

class FormIngreso (forms.ModelForm):
    descripcion = forms.CharField(validators=[validador_cadenas])
    monto = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    fecha = forms.DateTimeField()
    ejercicio = forms.IntegerField(initial=obtener_ano_actual)
    abono = forms.DecimalField(
        validators=[validador_numero_min, validador_numero_max])
    colono = forms.ModelChoiceField(queryset=Colono.objects.filter(estatus=1))
    tipo_ingreso = forms.ModelChoiceField(queryset=TipoIngreso.objects.filter(estatus=1))


    #fecha = forms.DateField(widget=forms.SelectDateWidget())
    #fecha = forms.DateTimeField(input_formats=['%Y-%m-%d'],widget=forms.DateTimeInput(attrs={
    #        'class': 'datetimepicker-input',
    #    }))

    class Meta:
        model = Ingreso
        fields = ('colono', 'tipo_ingreso', 'fecha', 'monto','abono','referencia',
                    'descripcion', 'ejercicio', 'folio_recibo')
