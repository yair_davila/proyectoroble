MES_CHOICES = (
    (1, "Enero"),
    (2, "Febrero"),
    (3, "Marzo"),
    (4, "Abril"),
    (5, "Mayo"),
    (6, "Junio"),
    (7, "Julio"),
    (8, "Agosto"),
    (9, "Septiembre"),
    (10, "Octubre"),
    (11, "Noviembre"),
    (12, "Diciembre"),

)

ESTATUS_CHOICES = (
    (1, 'Activo'),
    (0, "Inactivo"),
)

PAGADO_CHOICES = (
    (1, "PAGADO"),
    (0, "EN DEUDA")
)

TIPO_CHOICES = (
    (1, "ORDINARIO"),
    (2, "EXTRAORDINARIO")
)