TIPO_CHOICES = (
    ("0", "----"),
    ("1", "Gato"),
    ("2", "Perro")

)
TAMANO_CHOICES = (
    ("CH", "Chico"),
    ("M", "Mediano"),
    ("G", "Grande")
)
COLOR_CHOICES = (
    ("CAFE", "Cafe"),
    ("NEGRO", "Negro"),
    ("BLANCO", "Blanco"),
    ("CREMA", "Crema")
)
GENERO_CHOICES = (
    ("HEMBRA", "Hembra"),
    ("MACHO", "Macho")
)
