from django.core.validators import ValidationError
from django.utils.translation import gettext_lazy as _


def validador_cadenas(value):
    es_int = True
    try:
        a = int(value)
        es_int = True
    except Exception as e:
        es_int = False
    print(es_int)
    if es_int:
        raise ValidationError(
            _('Error {0} debe ser una cadena de texto'.format(value))
        )


def validador_longitud(value):
    if len(value) < 0:
        raise ValidationError(
            _('Error {0} debe ser mayor a 0'.format(value))
        )
