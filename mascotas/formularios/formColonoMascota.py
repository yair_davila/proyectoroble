from mascotas.modelos.colonoMascotaModel import ColonoMascota
from mascotas.validador import *
from django import forms

class FormColonoMascota(forms.ModelForm):

    class Meta:
        model = ColonoMascota
        fields = ('colono', 'mascota')
