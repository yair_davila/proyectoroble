from mascotas.modelos.mascotaModel import Mascota
from mascotas.validador import *
from mascotas.choices import GENERO_CHOICES, TAMANO_CHOICES
from django import forms

class FormMascota (forms.ModelForm):
    genero = forms.ChoiceField(choices=GENERO_CHOICES)
    tamano = forms.ChoiceField(choices=TAMANO_CHOICES)
    nombre = forms.CharField(
        validators=[validador_cadenas, validador_longitud])

    class Meta:
        model = Mascota
        fields = ('animal', 'raza', 'tamano',
                  'color', 'genero', 'nombre', 'foto')


