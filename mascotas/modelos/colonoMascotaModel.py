from django.db import models
from mascotas.modelos.mascotaModel import Mascota
from usuarios.modelos.colonoModel import Colono
from django.contrib.auth.models import User

class ColonoMascota(models.Model):
    colono = models.ForeignKey(Colono, models.DO_NOTHING, 'ID_COLONO_MASCOTA')
    mascota = models.ForeignKey(Mascota, models.DO_NOTHING, 'ID_MASCOTA')
    fecha_creacion = models.DateTimeField('Creacion_ColonoMascota')
    fecha_edicion = models.DateTimeField('Edicion_ColonoMascota', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_ColonoMascota')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_ColonoMascota', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.colono.__str__()+" "+self.mascota.__str__()

    class Meta:
        app_label = "mascotas"