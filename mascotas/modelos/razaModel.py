from mascotas.modelos.animalModel import Animal
from django.db import models
from django.contrib.auth.models import User

class Raza(models.Model):
    animal = models.ForeignKey(Animal, models.DO_NOTHING, 'ID_RAZA_ANIMAL')
    nombre = models.CharField('Raza', max_length=35, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Raza')
    fecha_edicion = models.DateTimeField('Edicion_Raza', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Raza')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Raza', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.nombre



    class Meta:
        app_label = "mascotas"