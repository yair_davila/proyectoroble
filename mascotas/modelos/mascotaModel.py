from mascotas.modelos.razaModel import Raza
from mascotas.modelos.animalModel import Animal
from django.db import models
from django.contrib.auth.models import User
class Mascota(models.Model):
    animal = models.ForeignKey(Animal, models.DO_NOTHING, 'ID_ANIMAL_MASCOTA')
    raza = models.ForeignKey(Raza, models.DO_NOTHING, 'ID_RAZA_MASCOTA')
    tamano = models.CharField('Tamano', max_length=35, blank=True, null=True)
    color = models.CharField('Color', max_length=35, blank=True, null=True)
    genero = models.CharField('Genero', max_length=10, blank=True, null=True)
    nombre = models.CharField('Nombre', max_length=35, blank=True, null=True)
    foto = models.ImageField(upload_to='mascotas')
    fecha_creacion = models.DateTimeField('Creacion_Mascota')
    fecha_edicion = models.DateTimeField('Edicion_Mascota', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Mascota')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Mascota', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)

    def __str__(self):
        return self.nombre+self.color

    class Meta:
        app_label = "mascotas"