from django.db import models
from django.contrib.auth.models import User
class Animal(models.Model):
    nombre = models.CharField('Animal', max_length=35, blank=True, null=True)
    fecha_creacion = models.DateTimeField('Creacion_Animal')
    fecha_edicion = models.DateTimeField('Edicion_Animal', null=True)
    usuario_creacion = models.ForeignKey(User, models.DO_NOTHING, 'Creador_Animal')
    usuario_edicion = models.ForeignKey(User, models.DO_NOTHING, 'Editor_Animal', null=True)
    estatus = models.SmallIntegerField('Estatus', default=1)


    def __str__(self):
        return self.nombre




    class Meta:
        app_label = "mascotas"