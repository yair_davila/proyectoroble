from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from mascotas.modelos.mascotaModel import Mascota
from mascotas.formularios.formMascota import FormMascota
from mascotas.modelos.razaModel import Raza
from datetime import date
class MascotaView():

    def carga_raza(request):
        o = request.GET.get('id', '')
        print(o)
        id_var = int(o)
        razas = []
        if (id_var == 1):
            razas = Raza.objects.filter(animal=1)

        elif (id_var == 2):
            razas = Raza.objects.filter(animal=2)

        razasJson = []
        contador = 0
        for raza in razas:
            razasJson.append(
                {
                    'id': raza.id,
                    'raza': raza.nombre
                }
            )
            contador = contador + 1

        data = simplejson.dumps(razasJson)
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.add_mascota', login_url='mascota')
    def nuevo_mascota(context):
        if (context.method == 'POST'):
            form = FormMascota(context.POST, context.FILES)
            if form.is_valid():
                mascota = form.save(commit=False)
                mascota.usuario_creacion = context.user
                mascota.fecha_creacion = date.today()
                mascota.save()
                mascotas = Mascota.objects.filter(estatus=1).order_by('-id')
                return render(context, 'nuevo_mascota.html',
                              {'formulario': form, 'accion': 'Nuevo', 'mascotas': mascotas,'exito':True})
            else:
                mascotas = Mascota.objects.filter(estatus=1)
                return render(context, 'nuevo_mascota.html',
                              {'formulario': form, 'accion': 'Nuevo', 'mascotas': mascotas, 'error': form.errors})
        else:
            form = FormMascota()
        mascotas = Mascota.objects.filter(estatus=1)
        return render(context, 'nuevo_mascota.html', {'formulario': form, 'accion': 'Nuevo', 'mascotas': mascotas})

    @login_required()
    @permission_required('mascotas.change_mascota', login_url='mascota')
    def editar_mascota(context,id):
        mascota = Mascota.objects.get(pk=id)
        if context.method == 'POST':
            form = FormMascota(context.POST, context.FILES, instance=mascota)
            print(form.is_valid())
            print(form.errors)
            if form.is_valid():
                mascota = form.save()
                mascota.save()
                return redirect('nuevo_mascota')
            else:
                mascotas = Mascota.objects.filter(estatus=1)
                return render(context, 'nuevo_mascota.html',
                          {'formulario': form, 'accion': 'Editar', 'mascotas': mascotas, 'error': form.errors})
        else:
            form = FormMascota(instance=mascota)
        mascotas = Mascota.objects.filter(estatus=1)
        return render(context, 'nuevo_mascota.html', {'formulario': form, 'accion': 'Editar','mascotas':mascotas})


    @login_required()
    @permission_required('mascotas.delete_mascota', login_url='mascota')
    def borrar_js_mascota(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            mascota = Mascota.objects.get(pk=o)
            mascota.estatus = 0
            mascota.save()
        data = MascotaView.obtener_json_mascotas(Mascota.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_mascotas(mascotas):
        mascotasJson = []
        for mascota in mascotas:
            mascotasJson.append(
                {
            'id' : mascota.id,
        	'animal': mascota.animal.id,
        	'raza': mascota.raza.id,
        	'tamano': mascota.tamano,
        	'color': mascota.color,
        	'genero': mascota.genero,
        	'nombre': mascota.nombre,
        	'foto': mascota.foto,
                }
            )
        data = simplejson.dumps(mascotasJson)
        return data
