from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from mascotas.modelos.animalModel import Animal
from mascotas.formularios.formAnimal import FormAnimal

class AnimalView():

    @login_required()
    @permission_required('mascotas.add_animal', login_url='animal')
    def nuevo_animal(context):
        form = FormAnimal()
        animals = Animal.objects.filter(estatus=1)
        return render(context, 'nuevo_animal.html',
                      {'formulario': form, 'accion': 'Nuevo', 'animals': animals})

    @login_required()
    @permission_required('mascotas.add_animal', login_url='animal')
    def registrar_js_animal(context):
        if (context.method == 'POST'):
            form = FormAnimal(context.POST)
            if form.is_valid():
                animal = form.save(commit=False)
                animal.usuario_creacion = context.user
                animal.fecha_creacion = date.today()
                animal.save()
                data = AnimalView.obtener_json_animals(Animal.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.add_animal', login_url='animal')
    def actualizar_js_animal(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            animal = Animal.objects.get(pk=id)

            form = FormAnimal(context.POST, instance=animal)
            if form.is_valid():
                animal = form.save()
                animal.save()
                data = AnimalView.obtener_json_animals(Animal.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.change_animal', login_url='animal')
    def obteneruno_js_animal(context):
        id = context.POST.get('id',None)
        data = AnimalView.obtener_json_animals(Animal.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.delete_animal', login_url='animal')
    def borrar_js_animal(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            animal = Animal.objects.get(pk=o)
            animal.estatus = 0
            animal.save()

        data = AnimalView.obtener_json_animals(Animal.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_animals(animals):
        
        animalsJson = []
        for animal in animals:
            animalsJson.append(
                {
            'id' : animal.id,
        	'nombre': animal.nombre,

                    
                }
            )
        data = simplejson.dumps(animalsJson)
        return data
