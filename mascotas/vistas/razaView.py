from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from mascotas.modelos.razaModel import Raza
from mascotas.formularios.formRaza import FormRaza

class RazaView():

    @login_required()
    @permission_required('mascotas.add_raza', login_url='raza')
    def nuevo_raza(context):
        form = FormRaza()
        razas = Raza.objects.filter(estatus=1)
        return render(context, 'nuevo_raza.html',
                      {'formulario': form, 'accion': 'Nuevo', 'razas': razas})

    @login_required()
    @permission_required('mascotas.add_raza', login_url='raza')
    def registrar_js_raza(context):
        if (context.method == 'POST'):
            form = FormRaza(context.POST)
            if form.is_valid():
                raza = form.save(commit=False)
                raza.usuario_creacion = context.user
                raza.fecha_creacion = date.today()
                raza.save()
                data = RazaView.obtener_json_razas(Raza.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.add_raza', login_url='raza')
    def actualizar_js_raza(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            raza = Raza.objects.get(pk=id)

            form = FormRaza(context.POST, instance=raza)
            if form.is_valid():
                raza = form.save()
                raza.save()
                data = RazaView.obtener_json_razas(Raza.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.change_raza', login_url='raza')
    def obteneruno_js_raza(context):
        id = context.POST.get('id',None)
        data = RazaView.obtener_json_razas_carga(Raza.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.delete_raza', login_url='raza')
    def borrar_js_raza(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            raza = Raza.objects.get(pk=o)
            raza.estatus = 0
            raza.save()

        data = RazaView.obtener_json_razas(Raza.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_razas(razas):
        
        razasJson = []
        for raza in razas:
            razasJson.append(
                {
            'id' : raza.id,
        	'animal': raza.animal,
        	'nombre': raza.nombre,

                    
                }
            )
        data = simplejson.dumps(razasJson)
        return data
