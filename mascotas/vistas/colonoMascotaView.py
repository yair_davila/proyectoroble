from django.db import connection
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson
from datetime import date
from mascotas.modelos.colonoMascotaModel import ColonoMascota
from mascotas.formularios.formColonoMascota import FormColonoMascota

class ColonoMascotaView():

    @login_required()
    @permission_required('mascotas.add_colonomascota', login_url='colonomascota')
    def nuevo_colonomascota(context):
        form = FormColonoMascota()
        colonomascotas = ColonoMascota.objects.filter(estatus=1)
        return render(context, 'nuevo_colonomascota.html',
                      {'formulario': form, 'accion': 'Nuevo', 'colonomascotas': colonomascotas})

    @login_required()
    @permission_required('mascotas.change_colonomascota', login_url='colonomascota')
    def obteneruno_js_colonomascota(context):
        id = context.POST.get('id',None)
        data = ColonoMascotaView.obtener_json_colonomascotas_carga(ColonoMascota.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.delete_colonomascota', login_url='colonomascota')
    def borrar_js_colonomascota(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            colonomascota = ColonoMascota.objects.get(pk=o)
            colonomascota.estatus = 0
            colonomascota.save()

        data = ColonoMascotaView.obtener_json_colonomascotas(ColonoMascota.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")
    
    def obtener_json_colonomascotas(colonomascotas):
        
        colonomascotasJson = []
        for colonomascota in colonomascotas:
            colonomascotasJson.append(
                {
            'id' : colonomascota.id,
        	'colono': str(colonomascota.colono),
        	'mascota': str(colonomascota.mascota),

                    
                }
            )
        data = simplejson.dumps(colonomascotasJson)
        return data


    def obtener_json_colonomascotas_carga(colonomascotas):
        
        colonomascotasJson = []
        for colonomascota in colonomascotas:
            colonomascotasJson.append(
                {
            'id' : colonomascota.id,
          'colono': colonomascota.colono.id,
          'mascota': colonomascota.mascota.id,

                    
                }
            )
        data = simplejson.dumps(colonomascotasJson)
        return data


    

    @login_required()
    @permission_required('mascotas.add_colonomascota', login_url='colonomascota')
    def registrar_js_colonomascota(context):
        if (context.method == 'POST'):
            form = FormColonoMascota(context.POST)

            try:
                query="INSERT INTO mascotas_colonomascota"+\
					"(colono_id,mascota_id,estatus)"+\
					"VALUES ("
                max = ColonoMascota.objects.raw("SELECT MAX(id) as id FROM mascotas_colonomascota")[0].id
                new_id = max + 1 if (max != None) else 1          
                colono  = context.POST.get('colono',None)
                mascota  = context.POST.get('mascota',None)

                query= query+\
                    str(colono)+","+\
                    str(mascota)+","+\
					"1);"
                print(query)
                cursor = connection.cursor()
                cursor.execute(query)
                row = cursor.fetchone()
				
                data = ColonoMascotaView.obtener_json_colonomascotas(ColonoMascota.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
				
            except Exception as e:
                
                print(e)
                data = simplejson.dumps([{'error': 'alerta'},{'alerta','Los datos ingresados son incorrectos'} ])
                return HttpResponse(data, content_type="application/json")

        else:
            data = simplejson.dumps([{'error': 'error'},{'descripcion':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('mascotas.add_colonomascota', login_url='colonomascota')
    def actualizar_js_colonomascota(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            colonomascota = ColonoMascota.objects.get(pk=id)
            form = FormColonoMascota(context.POST, instance=colonomascota)
            try:

                colono  = context.POST.get('colono',None)
                mascota  = context.POST.get('mascota',None)

                id = context.POST.get('id', None)
                query = "UPDATE mascotas_colonomascota SET " + \
                   "colono_id = "+ colono + ","+\
                   "mascota_id = "+ mascota + ""+\
                        " WHERE id = "+id+";"
                print(query)
                cursor = connection.cursor()
                cursor.execute(query)
                row = cursor.fetchone()
                data = ColonoMascotaView.obtener_json_colonomascotas(ColonoMascota.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")

            except Exception as e:
                print(e)
                data = simplejson.dumps([{'error': 'alerta'},{'alerta','Los datos ingresados son incorrectos'}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'error'}, {'descripcion': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")
