
from usuarios.modelos.colonoModel import Colono


def tiene_permiso(user, permiso):
    if user.is_staff:
        return True

    if user.user_permissions.filter(name=permiso):
        return True

    grupos = user.groups.all()
    if len(grupos) > 0:
        if(grupos[0].permissions.filter(name=permiso)):
            return True

    return False


def es_colono(user):
    if user.groups.filter(name='Colonos'):
        return True
    return False


def get_mascotas(user):
    if es_colono(user):
        ids = []
        colono = Colono.objects.filter(usuario=user)[0]
        lista = ColonoMascota.objects.filter(colono=colono)
        for elemento in lista:
            ids.append(int(elemento.mascota.id))

        return Mascota.objects.filter(id__in=ids)
    else:
        return Mascota.objects.all()
