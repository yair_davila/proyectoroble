from mascotas.vistas.colonoMascotaView import ColonoMascotaView
from mascotas.vistas.mascotaView import MascotaView
from django.urls import path

urlpatterns = [


	path('mascota_nuevo', MascotaView.nuevo_mascota,
         name='nuevo_mascota'),
    path('mascota_editar/<int:id>', MascotaView.editar_mascota, name='editar_mascota'),
    path('mascota_borrar/',
         MascotaView.borrar_js_mascota, name='borrar_mascota'),
    path('carga_razas',MascotaView.carga_raza,name='razas_carga'),

	path('colonomascota_nuevo', ColonoMascotaView.nuevo_colonomascota,
         name='nuevo_colonomascota'),
    path('colonomascota_registrar', ColonoMascotaView.registrar_js_colonomascota,
         name='registrar_colonomascota'),
    path('colonomascota_actualizar', ColonoMascotaView.actualizar_js_colonomascota,
         name='actualizar_colonomascota'),
    path('colonomascota_obteneruno', ColonoMascotaView.obteneruno_js_colonomascota,
         name='obteneruno_colonomascota'),
    path('colonomascota_borrar/',
         ColonoMascotaView.borrar_js_colonomascota, name='borrar_colonomascota'),


]