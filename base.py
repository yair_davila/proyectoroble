from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
import simplejson

from ~modulo~.modelos.~minuscula~Model import ~mayuscula~
from ~modulo~.formularios.form~mayuscula~ import Form~mayuscula~

class ~mayuscula~View():

    @login_required()
    @permission_required('~modulo~.add_~minuscula~', login_url='~minuscula~')
    def nuevo_~minuscula~(context):
        form = Form~mayuscula~()
        ~minuscula~s = ~mayuscula~.objects.filter(estatus=1)
        return render(context, 'nuevo_~minuscula~.html',
                      {'formulario': form, 'accion': 'Nuevo', '~minuscula~s': ~minuscula~s})

    @login_required()
    @permission_required('~modulo~.add_~minuscula~', login_url='~minuscula~')
    def registrar_js_~minuscula~(context):
        if (context.method == 'POST'):
            form = Form~mayuscula~(context.POST)
            if form.is_valid():
                ~minuscula~ = form.save()
                ~minuscula~.save()
                data = ~mayuscula~View.obtener_json_~minuscula~s(~mayuscula~.objects.filter(estatus=1))
                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)} ])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error':'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('~modulo~.add_~minuscula~', login_url='~minuscula~')
    def actualizar_js_~minuscula~(context):
        if (context.method == 'POST'):
            id=context.POST.get('id',None)
            ~minuscula~ = ~mayuscula~.objects.get(pk=id)

            form = Form~mayuscula~(context.POST, instance=~minuscula~)
            if form.is_valid():
                ~minuscula~ = form.save()
                ~minuscula~.save()
                data = ~mayuscula~View.obtener_json_~minuscula~s(~mayuscula~.objects.filter(estatus=1))

                return HttpResponse(data, content_type="application/json")
            else:
                data = simplejson.dumps([{'error': str(form.errors)}])
                return HttpResponse(data, content_type="application/json")
        else:
            data = simplejson.dumps([{'error': 'Error de conexión'}])
            return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('~modulo~.change_~minuscula~', login_url='~minuscula~')
    def obteneruno_js_~minuscula~(context):
        id = context.POST.get('id',None)
        data = ~mayuscula~View.obtener_json_~minuscula~s_carga(~mayuscula~.objects.filter(pk=id))
        return HttpResponse(data, content_type="application/json")

    @login_required()
    @permission_required('~modulo~.delete_~minuscula~', login_url='~minuscula~')
    def borrar_js_~minuscula~(context):
        if (context.method == 'POST'):
            o = context.POST.get('id', None)
            ~minuscula~ = ~mayuscula~.objects.get(pk=o)
            ~minuscula~.estatus = 0
            ~minuscula~.save()

        data = ~mayuscula~View.obtener_json_~minuscula~s(~mayuscula~.objects.filter(estatus=1))
        return HttpResponse(data, content_type="application/json")